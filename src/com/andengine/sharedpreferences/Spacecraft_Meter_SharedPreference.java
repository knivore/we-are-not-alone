/*
 * Copyright (C) 2013 Knivore Studios
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.andengine.sharedpreferences;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * @author Keh Chin Leong
 */
public class Spacecraft_Meter_SharedPreference {
	
	private static final String DB_NAME = "SPACECRAFT_METER";

	private SharedPreferences mSpacecraftMeterDB;
	private SharedPreferences.Editor mSpacecraftMeterDBEditor;

	public SharedPreferences getmSpacecraftMeterDB() {
		return mSpacecraftMeterDB;
	}

	public void setmSpacecraftMeterDB(SharedPreferences mSpacecraftMeterDB) {
		this.mSpacecraftMeterDB = mSpacecraftMeterDB;
	}

	public static String getDbName() {
		return DB_NAME;
	}
	
	public Spacecraft_Meter_SharedPreference(Context context){
		mSpacecraftMeterDB = context.getSharedPreferences(DB_NAME, Context.MODE_PRIVATE);
		mSpacecraftMeterDBEditor = this.mSpacecraftMeterDB.edit();
	}
	
	public float getEvilMeter() {
		return mSpacecraftMeterDB.getFloat("EVIL_METER", 0f);
	}
	
	public boolean setEvilMeter(float value) {
		mSpacecraftMeterDBEditor.putFloat("EVIL_METER", value);
		return mSpacecraftMeterDBEditor.commit();
	}
	
	public float getGoodnessMeter() {
		return mSpacecraftMeterDB.getFloat("GOODNESS_METER", 0f);
	}
	
	public boolean setGoodnessMeter(float value) {
		mSpacecraftMeterDBEditor.putFloat("GOODNESS_METER", value);
		return mSpacecraftMeterDBEditor.commit();
	}

	public boolean incrementEvilMeter(float incrementValue) {
		mSpacecraftMeterDBEditor.putFloat("EVIL_METER", getEvilMeter() + incrementValue);
		return mSpacecraftMeterDBEditor.commit();
	}
	
	public boolean decrementEvilMeter(float decrementValue) {
		mSpacecraftMeterDBEditor.putFloat("EVIL_METER", getEvilMeter() - decrementValue);
		return mSpacecraftMeterDBEditor.commit();
	}

	public boolean incrementGoodnessMeter(float incrementValue) {
		mSpacecraftMeterDBEditor.putFloat("GOODNESS_METER", getGoodnessMeter() + incrementValue);
		return mSpacecraftMeterDBEditor.commit();
	}
	
	public boolean decrementGoodnessMeter(float decrementValue) {
		mSpacecraftMeterDBEditor.putFloat("GOODNESS_METER", getGoodnessMeter() - decrementValue);
		return mSpacecraftMeterDBEditor.commit();
	}
}