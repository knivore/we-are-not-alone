/*
 * Copyright (C) 2013 Knivore Studios
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.andengine.sharedpreferences;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * @author Keh Chin Leong
 */
public class Score_SharedPreference {
	
	private static final String DB_NAME = "HIGHSCORES";
		
	private String COUNTRY_NAME;

	private SharedPreferences mScoreDB;
	private SharedPreferences.Editor mScoreDBEditor;

	public SharedPreferences getmScoreDB() {
		return mScoreDB;
	}

	public void setmScoreDB(SharedPreferences mScoreDB) {
		this.mScoreDB = mScoreDB;
	}

	public static String getDbName() {
		return DB_NAME;
	}
	
	public Score_SharedPreference(Context context, String countryName) {
		mScoreDB = context.getSharedPreferences(DB_NAME, Context.MODE_PRIVATE);
		mScoreDBEditor = this.mScoreDB.edit();
		COUNTRY_NAME = countryName;
	}

	public boolean setCombinedDistance(int value) {
		this.mScoreDBEditor.putInt("DISTANCE_LABEL", value);
		return this.mScoreDBEditor.commit();
	}
	
	public boolean saveCombinedDistance(int distanceTravelled) {
		this.mScoreDBEditor.putInt("Distance", distanceTravelled + loadCombinedDistance());
		return this.mScoreDBEditor.commit();
	}

	public int loadCombinedDistance() {
		return this.mScoreDB.getInt("Distance", 0);
	}
	
	public boolean saveMaxDistancePerCountry(int distanceTravelled) {
		this.mScoreDBEditor.putInt("Distance_"+COUNTRY_NAME, distanceTravelled);
		return this.mScoreDBEditor.commit();
	}

	public int loadMaxDistancePerCountry() {
		return this.mScoreDB.getInt("Distance_"+COUNTRY_NAME, 0);
	}

	public boolean setObtainedDetuctableGold(int gold) {
		this.mScoreDBEditor.putInt("DetuctableGoldObtained", gold);
		return this.mScoreDBEditor.commit();
	}
	
	public boolean saveObtainedDetuctableGold(int goldObtained) {
		this.mScoreDBEditor.putInt("DetuctableGoldObtained", goldObtained + loadObtainedDetuctableGold());
		return this.mScoreDBEditor.commit();
	}

	public int loadObtainedDetuctableGold() {
		int count = this.mScoreDB.getInt("DetuctableGoldObtained", 0);
		if(count > 99999999)
			return 99999999;
		else 
			return count;
	}

	public boolean setTotalObtainedGold(int gold) {
		this.mScoreDBEditor.putInt("TotalGoldObtained", gold);
		return this.mScoreDBEditor.commit();
	}
	
	public boolean saveTotalObtainedGold(int goldObtained) {
		saveObtainedDetuctableGold(goldObtained);
		this.mScoreDBEditor.putInt("TotalGoldObtained", goldObtained + loadTotalObtainedGold());
		return this.mScoreDBEditor.commit();
	}

	public int loadTotalObtainedGold() {
		int count = this.mScoreDB.getInt("TotalGoldObtained", 0);
		if(count > 99999999)
			return 99999999;
		else 
			return count;
	}
	
	public boolean saveMaxGoldPerCountry(int goldObtained) {
		this.mScoreDBEditor.putInt("Max_GoldObtained_"+COUNTRY_NAME, goldObtained);
		return this.mScoreDBEditor.commit();
	}

	public int loadMaxGoldPerCountry() {
		return this.mScoreDB.getInt("Max_GoldObtained_"+COUNTRY_NAME, 0);
	}

	public boolean setAbsorbedHumansPerCountry(int humansCaptured) {
		this.mScoreDBEditor.putInt("AbsorbedHumans_"+COUNTRY_NAME, humansCaptured);
		return this.mScoreDBEditor.commit();
	}
	
	public boolean saveAbsorbedHumansPerCountry(int humansCaptured) {
		this.mScoreDBEditor.putInt("AbsorbedHumans_"+COUNTRY_NAME, humansCaptured + loadAbsorbedHumansPerCountry());
		return this.mScoreDBEditor.commit();
	}

	public int loadAbsorbedHumansPerCountry() {
		return this.mScoreDB.getInt("AbsorbedHumans_"+COUNTRY_NAME, 0);
	}
	
	public boolean saveMaxAbsorbedHumansPerCountry(int humansCaptured) {
		this.mScoreDBEditor.putInt("Max_AbsorbedHumans_"+COUNTRY_NAME, humansCaptured);
		return this.mScoreDBEditor.commit();
	}

	public int loadMaxAbsorbedHumansPerCountry() {
		return this.mScoreDB.getInt("Max_AbsorbedHumans_"+COUNTRY_NAME, 0);
	}

	public boolean setDisintegratedHumansPerCountry(int humansDisintegrated) {
		this.mScoreDBEditor.putInt("DisintegratedHumans_"+COUNTRY_NAME, humansDisintegrated);
		return this.mScoreDBEditor.commit();
	}
	
	public boolean saveDisintegratedHumansPerCountry(int humansDisintegrated) {
		this.mScoreDBEditor.putInt("DisintegratedHumans_"+COUNTRY_NAME, humansDisintegrated + loadDisintegratedHumansPerCountry());
		return this.mScoreDBEditor.commit();
	}

	public int loadDisintegratedHumansPerCountry() {
		return this.mScoreDB.getInt("DisintegratedHumans_"+COUNTRY_NAME, 0);
	}
	
	public boolean saveMaxDisintegratedHumansPerCountry(int humansDisintegrated) {
		this.mScoreDBEditor.putInt("Max_DisintegratedHumans_"+COUNTRY_NAME, humansDisintegrated);
		return this.mScoreDBEditor.commit();
	}

	public int loadMaxDisintegratedHumansPerCountry() {
		return this.mScoreDB.getInt("Max_DisintegratedHumans_"+COUNTRY_NAME, 0);
	}

	public boolean setGamesCompleted(int games) {
		this.mScoreDBEditor.putInt("GamesCompleted", games);
		return this.mScoreDBEditor.commit();
	}
	
	public boolean saveGamesCompleted() {
		this.mScoreDBEditor.putInt("GamesCompleted", loadGamesCompleted() + 1);
		return this.mScoreDBEditor.commit();
	}

	public int loadGamesCompleted() {
		return this.mScoreDB.getInt("GamesCompleted", 0);
	}

	public boolean setDeathByLaserStrike(int death) {
		this.mScoreDBEditor.putInt("DeathByLaserStrike_"+COUNTRY_NAME, death);
		return this.mScoreDBEditor.commit();
	}
	
	public boolean saveDeathByLaserStrike() {
		this.mScoreDBEditor.putInt("DeathByLaserStrike_"+COUNTRY_NAME, loadDeathByLaserStrike() + 1);
		return this.mScoreDBEditor.commit();
	}

	public int loadDeathByLaserStrike() {
		return this.mScoreDB.getInt("DeathByLaserStrike_"+COUNTRY_NAME, 0);
	}

	public boolean setDeathByMissile(int death) {
		this.mScoreDBEditor.putInt("DeathByMissile_"+COUNTRY_NAME, death);
		return this.mScoreDBEditor.commit();
	}
	
	public boolean saveDeathByMissile() {
		this.mScoreDBEditor.putInt("DeathByMissile_"+COUNTRY_NAME, loadDeathByMissile() + 1);
		return this.mScoreDBEditor.commit();
	}

	public int loadDeathByMissile() {
		return this.mScoreDB.getInt("DeathByMissile_"+COUNTRY_NAME, 0);
	}

	public boolean setDeathByScatterMissile(int death) {
		this.mScoreDBEditor.putInt("DeathByScatterMissile_"+COUNTRY_NAME, death);
		return this.mScoreDBEditor.commit();
	}
	
	public boolean saveDeathByScatterMissile() {
		this.mScoreDBEditor.putInt("DeathByScatterMissile_"+COUNTRY_NAME, loadDeathByScatterMissile() + 1);
		return this.mScoreDBEditor.commit();
	}

	public int loadDeathByScatterMissile() {
		return this.mScoreDB.getInt("DeathByScatterMissile_"+COUNTRY_NAME, 0);
	}

	public boolean setHitByHumanRocketLauncher(int hits) {
		this.mScoreDBEditor.putInt("HitByHumanRocketLauncher_"+COUNTRY_NAME, hits);
		return this.mScoreDBEditor.commit();
	}
	
	public boolean saveHitByHumanRocketLauncher() {
		this.mScoreDBEditor.putInt("HitByHumanRocketLauncher_"+COUNTRY_NAME, loadHitByHumanRocketLauncher() + 1);
		return this.mScoreDBEditor.commit();
	}

	public int loadHitByHumanRocketLauncher() {
		return this.mScoreDB.getInt("HitByHumanRocketLauncher_"+COUNTRY_NAME, 0);
	}
}
