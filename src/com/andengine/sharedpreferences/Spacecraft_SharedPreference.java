/*
 * Copyright (C) 2013 Knivore Studios
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.andengine.sharedpreferences;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * @author Keh Chin Leong
 */
public class Spacecraft_SharedPreference {
	
	private static final String DB_NAME = "SPACECRAFT_UPGRADES";

	private SharedPreferences mSpacecraftDB;
	String spacecraftName;

	public SharedPreferences getmSpacecraftDB() {
		return mSpacecraftDB;
	}

	public void setmSpacecraftDB(SharedPreferences mSpacecraftDB) {
		this.mSpacecraftDB = mSpacecraftDB;
	}

	public static String getDbName() {
		return DB_NAME;
	}	

	public Spacecraft_SharedPreference(Context context){
		mSpacecraftDB = context.getSharedPreferences(DB_NAME, Context.MODE_PRIVATE);
	}

	public int getActiveSpacecraft(){
		return mSpacecraftDB.getInt("Active_Spacecraft",0);
	}

	public boolean saveActiveSpacecraft(int spacecraft_no) {
		// Save the changes made to options
		SharedPreferences.Editor editor = mSpacecraftDB.edit();
		editor.putInt("Active_Spacecraft", spacecraft_no);
		editor.commit();
		return true;
	}
	
	public int getBeamLength(){
		return mSpacecraftDB.getInt("Spacecraft_" + getActiveSpacecraft() + "_BeamLength",1);
	}

	public boolean saveBeamLength(int beamLength) {
		// Save the changes made to options
		SharedPreferences.Editor editor = mSpacecraftDB.edit();
		editor.putInt("Spacecraft_" + getActiveSpacecraft() + "_BeamLength", beamLength);
		editor.commit();
		return true;
	}
	
	public boolean getSpacecraftStatus(int spacecraft_no) {
		if(spacecraft_no < 10) {
			spacecraftName = "Spacecraft_0";
		} else {
			spacecraftName = "Spacecraft_";
		}
		if(spacecraft_no == 0) return true;
		return mSpacecraftDB.getBoolean(spacecraftName + spacecraft_no + "_Status",false);
	}

	public boolean saveSpacecraftStatus(int spacecraft_no) {
		if(spacecraft_no < 10) {
			spacecraftName = "Spacecraft_0";
		} else {
			spacecraftName = "Spacecraft_";
		}
		// Save the changes made to options
		SharedPreferences.Editor editor = mSpacecraftDB.edit();
		editor.putBoolean(spacecraftName + spacecraft_no + "_Status", true);
		editor.commit();
		return true;
	}
}