/*
 * Copyright (C) 2013 Knivore Studios
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.andengine.sharedpreferences;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * @author Keh Chin Leong
 */
public class Gadgets_SharedPreference {
	
	private static final String DB_NAME = "GADGETS";

	public final int mEquippedGadgetConstant = 2;
	
	// Standard each gadget on start given 1
	private static int mGadgetCount = 1;

	private int mEquippedGadget1 = 99, mEquippedGadget2 = 99; //Value 99 = No gadget attached to Equipped Gadget Box 1 & 2
	//private boolean mGadgetUnlockedStatus = false;
	private String mGadgetName;
	
	private SharedPreferences mGadgetDB;
	private SharedPreferences.Editor mGadgetDBEditor;

	public SharedPreferences getmGadgetDB() {
		return mGadgetDB;
	}

	public void setmGadgetDB(SharedPreferences mGadgetDB) {
		this.mGadgetDB = mGadgetDB;
	}

	public static String getDbName() {
		return DB_NAME;
	}
	
	public Gadgets_SharedPreference(Context context) {
		mGadgetDB = context.getSharedPreferences(DB_NAME, Context.MODE_PRIVATE);
		mGadgetDBEditor = this.mGadgetDB.edit();
	}
	
	private void gadgetName(int gadgetNumber) {
		if(gadgetNumber < 10) {
			mGadgetName = "Gadget_0";
		} else {
			mGadgetName = "Gadget_";
		}
	}
	
	public int getGadgetCount(int gadgetNumber) {
		gadgetName(gadgetNumber);
		return mGadgetDB.getInt(mGadgetName + gadgetNumber, mGadgetCount);
	}

	public boolean setGadgetCount(String gadgetNumber, int count) {
		mGadgetDBEditor.putInt(gadgetNumber, count);
		return mGadgetDBEditor.commit();
	}
	
	public int getEquippedGadget1() {
		return mGadgetDB.getInt("EquippedBox1", mEquippedGadget1);
	}

	public int getEquippedGadget2() {
		return mGadgetDB.getInt("EquippedBox2", mEquippedGadget2);
	}
	/*
	public boolean getGadgetUnlockedStatus(int gadgetNumber) {
		gadgetName(gadgetNumber);
		return mGadgetDB.getBoolean(mGadgetName + gadgetNumber + "_UnlockedStatus", mGadgetUnlockedStatus);
	}
	*/

	public boolean setEquippedGadget1(int gadgetNumber) {
		mGadgetDBEditor.putInt("EquippedBox1", gadgetNumber);
		return mGadgetDBEditor.commit();
	}
	
	public boolean setEquippedGadget2(int gadgetNumber) {
		mGadgetDBEditor.putInt("EquippedBox2", gadgetNumber);
		return mGadgetDBEditor.commit();
	}	
	
	public boolean buyGadget(int gadgetNumber, int incrementCountValue) {
		gadgetName(gadgetNumber);
		totalGadgetBrought();
		mGadgetDBEditor.putInt(mGadgetName + gadgetNumber, getGadgetCount(gadgetNumber) + incrementCountValue);
		return mGadgetDBEditor.commit();
	}
	
	/*
	public boolean setGadgetUnlockedStatus(int gadgetNumber, boolean unlockedStatus) {
		gadgetName(gadgetNumber);
		mGadgetDBEditor.putBoolean(mGadgetName + gadgetNumber + "_UnlockedStatus", unlockedStatus);
		return mGadgetDBEditor.commit();
	}*/
	
	public boolean useGadget(int gadgetNumber, int decrementCountValue) {
		gadgetName(gadgetNumber);
		totalGadgetUsed();
		mGadgetDBEditor.putInt(mGadgetName + gadgetNumber, getGadgetCount(gadgetNumber) - decrementCountValue);
		return mGadgetDBEditor.commit();
	}
	
	public boolean totalGadgetBrought() {
		this.mGadgetDBEditor.putInt("totalGadgetBrought", loadTotalGadgetBrought() + 1);
		return this.mGadgetDBEditor.commit();
	}

	public boolean setTotalGadgetBrought(int count) {
		mGadgetDBEditor.putInt("totalGadgetBrought", count);
		return mGadgetDBEditor.commit();
	}
	
	public int loadTotalGadgetBrought() {
		return this.mGadgetDB.getInt("totalGadgetBrought", 0);
	}
	
	public boolean totalGadgetUsed() {
		this.mGadgetDBEditor.putInt("totalGadgetUsed", loadTotalGadgetUsed() + 1);
		return this.mGadgetDBEditor.commit();
	}

	public boolean setTotalGadgetUsed(int count) {
		mGadgetDBEditor.putInt("totalGadgetUsed", count);
		return mGadgetDBEditor.commit();
	}
	
	public int loadTotalGadgetUsed() {
		return this.mGadgetDB.getInt("totalGadgetUsed", 0);
	}
}