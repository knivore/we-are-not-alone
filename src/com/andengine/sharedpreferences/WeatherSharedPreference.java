/*
 * Copyright (C) 2013 Knivore Studios
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.andengine.sharedpreferences;

import java.util.Date;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * @author Keh Chin Leong
 */
public class WeatherSharedPreference {
	
	private static final String DB_NAME = "WEATHER";
	
	private String mWeatherResult = "";
	private String mWeatherWebServiceReceivedTiming = "";
	
	private static final String WEATHER = "weather";
	private static final String TIMING = "ReceivedTime";
	private String COUNTRY_NAME;

	private SharedPreferences mWeatherDB;
	private SharedPreferences.Editor mWeatherDBEditor;

	public SharedPreferences getmWeatherDB() {
		return mWeatherDB;
	}

	public void setmWeatherDB(SharedPreferences mWeatherDB) {
		this.mWeatherDB = mWeatherDB;
	}

	public static String getDbName() {
		return DB_NAME;
	}
	
	public WeatherSharedPreference(Context context, String countryName) {
		mWeatherDB = context.getSharedPreferences(DB_NAME, Context.MODE_PRIVATE);
		mWeatherDBEditor = this.mWeatherDB.edit();
		COUNTRY_NAME = countryName;
	}

	public boolean saveWeather(String weather) {
		this.mWeatherDBEditor.putString(COUNTRY_NAME+"_"+WEATHER, weather);
		return this.mWeatherDBEditor.commit();
	}

	public String loadWeather() {
		return this.mWeatherDB.getString(COUNTRY_NAME+"_"+WEATHER, this.mWeatherResult);
	}
	
	public boolean saveWSCallTime(Date timing) {
		this.mWeatherDBEditor.putString(TIMING, ""+timing);
		return this.mWeatherDBEditor.commit();
	}

	public String loadWSCallTime() {
		return this.mWeatherDB.getString(TIMING, this.mWeatherWebServiceReceivedTiming);
	}
}
