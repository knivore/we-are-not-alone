/*
 * Copyright (C) 2013 Knivore Studios
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.andengine.sharedpreferences;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * @author Keh Chin Leong
 */
public class Options_SharedPreference {
	
	private static final String DB_NAME = "OPTIONS";

	private SharedPreferences mOptionsDB;

	public SharedPreferences getmOptionsDB() {
		return mOptionsDB;
	}

	public void setmOptionsDB(SharedPreferences mOptionsDB) {
		this.mOptionsDB = mOptionsDB;
	}

	public static String getDbName() {
		return DB_NAME;
	}
	
	public Options_SharedPreference(Context context){
		mOptionsDB = context.getSharedPreferences(DB_NAME, Context.MODE_PRIVATE);
	}

	public boolean getSoundEffects(){
		return mOptionsDB.getBoolean("SFX",true);
	}

	public boolean getMusic(){
		return mOptionsDB.getBoolean("Music",true);
	}

	public boolean getControlPads(){
		return mOptionsDB.getBoolean("ControlPads",false);
	}

	public boolean saveSFX(boolean sfx) {
		SharedPreferences.Editor editor = mOptionsDB.edit();
		editor.putBoolean("SFX", sfx);
		editor.apply();
		return true;
	}
	public boolean saveMusic(boolean music) {
		SharedPreferences.Editor editor = mOptionsDB.edit();
		editor.putBoolean("Music", music);
		editor.apply();
		return true;
	}
	public boolean saveControlPads(boolean controlpads) {
		SharedPreferences.Editor editor = mOptionsDB.edit();
		editor.putBoolean("ControlPads", controlpads);
		editor.apply();
		return true;
	}
	public boolean save(boolean sfx, boolean music, boolean controlpads) {
		SharedPreferences.Editor editor = mOptionsDB.edit();
		editor.putBoolean("SFX", sfx);
		editor.putBoolean("Music", music);
		editor.putBoolean("ControlPads", controlpads);
		editor.apply();
		return true;
	}
}