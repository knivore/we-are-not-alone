/*
 * Copyright (C) 2013 Knivore Studios
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.andengine.invaders;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.andengine.audio.music.Music;
import org.andengine.audio.music.MusicFactory;
import org.andengine.engine.Engine;
import org.andengine.engine.camera.Camera;
import org.andengine.entity.text.Text;
import org.andengine.opengl.font.Font;
import org.andengine.opengl.font.FontFactory;
import org.andengine.opengl.font.FontManager;
import org.andengine.opengl.texture.ITexture;
import org.andengine.opengl.texture.TextureManager;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.texture.region.ITiledTextureRegion;
import org.andengine.opengl.texture.region.TextureRegion;
import org.andengine.opengl.texture.region.TiledTextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.math.MathUtils;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Color;
import android.util.Log;

import com.andengine.classes.AchievementContainer;
import com.andengine.classes.SaveGame;
import com.android.vending.billing.util.IabHelper;
import com.android.vending.billing.util.ProductDetails;
import com.basegameutils.GameHelper;
import com.makersf.andengine.extension.collisions.opengl.texture.region.PixelPerfectTextureRegion;
import com.makersf.andengine.extension.collisions.opengl.texture.region.PixelPerfectTextureRegionFactory;
import com.makersf.andengine.extension.collisions.opengl.texture.region.PixelPerfectTiledTextureRegion;

/**
 * @author Keh Chin Leong
 */
public class GameResources {
	
	//===========================================================
	// Constants
	//===========================================================
	protected final String TAG = "WeAreNotAlone";
    protected final String mainLeaderboardID = "CgkIkOO54bYVEAIQAA";
    protected final String humansAbsorbedLeaderboardID = "CgkIkOO54bYVEAIQGw";
    protected final String humansDisintegratedLeaderboardID = "CgkIkOO54bYVEAIQHA";
    protected final String currentSaveName = "WeAreNotAlone";
    
    /* base64EncodedPublicKey should be YOUR APPLICATION'S PUBLIC KEY
     * (that you got from the Google Play developer console). This is not your
     * developer public key, it's the *app-specific* public key.
     *
     * Instead of just storing the entire literal string here embedded in the
     * program,  construct the key at runtime from pieces or
     * use bit manipulation (for example, XOR with some other string) to hide
     * the actual key.  The key itself is not secret information, but we don't
     * want to make it easy for an attacker to replace the public key with one
     * of their own and then fake messages from the server.
     */
    private static final String base64EncodedPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAxxhGkAL5fvaj8QhTIhWSKD+OZYAIHXsdRwyfZD2TTCmOF0xuz58K1pGGiz9dt7rXC0qzcRYdH43/fxcz7mUi0nFlbvHBfxlb9tclvtHcoKarLiJ4N8SGYMSRFW6sgfjxQ3LDVfKCwUhySsMys0PPqdrBRoHsqg6Y0DcwUzx8q8fUdtUiWzO42UBiK+WloxH8RGjaDYoV/U1dxRdP1ESmCf7yEdAGs8UxtzEJ5sjw+dIUjDXM9q7D5fSXAouSZG1KjzsQfF05WkA1jb5Rol8c0ahfVuT4Dr+xNrcELIqZl5cxm7mRtLoF6QkyE3rckim1Iixum4cd+oIBAOXviOmemwIDAQAB";
    
	private static final float POSITION_ANGLE_0 = 0f;
	private static final float POSITION_ANGLE_45 = 5.0f;
	
	//===== Gadget List =====
	//01 - Shield, 02 - Slow-Motion/Extend Timer, 03 - Mega Beam, 04 - Missile Jammer
	//05 - Magnet, 06 - Cloaking, 07 - Conversion, 08 - Mothership SOS, 09 - , 10 -  
	//11 - 		 , 12 - 
	private static int GADGETS_ASSETS_COUNT = 4;
	private static int SPACECRAFT_MODEL_ASSETS_COUNT = 4;
	private static int NPC_ASSETS_COUNT = 6;
	private static int HUMAN_ASSETS_COUNT = 6;
	
	public boolean mainMenuResourcesLoaded = false,
			calibrationResourcesLoaded = false,
			gameResources_PART1_Loaded = false,
			gameResources_PART2_Loaded = false,
			gameResources_PART3_Loaded = false;
	
    protected GameHelper mGameHelper;
	protected SaveGame mSaveGame;
    private AchievementContainer mAchievement;

	private TextureManager mTextureManager;
	private FontManager mFontManager;
	private AssetManager mAssetManager;
	private VertexBufferObjectManager mVertexBufferObjectManager;
	private Context pContext, mBaseContext;

	private Engine mEngine;

	public IabHelper mIabHelper;
	
	public String googleAccountID = "";
	public String googleAccountName = "";
	
	private String[] mCountryName = {"Singapore","Sydney"};
	
	private List<String> additionalSkuList = new ArrayList<String>();
	private String mPremiumNoAds = "remove_advertisement";
	private List<ProductDetails> productDetailsList = new ArrayList<ProductDetails>();

	//===========================================================
	// Main Menu & Game Resource Shared Fields
	//===========================================================
	private Text loadingText;
	
	// Human/UFO/Spacecraft Model List
	private List<PixelPerfectTiledTextureRegion> mUFO_Model_List = new ArrayList<PixelPerfectTiledTextureRegion>();
	private List<PixelPerfectTiledTextureRegion> mUFO_Model_Explosion_List = new ArrayList<PixelPerfectTiledTextureRegion>();
	private List<PixelPerfectTiledTextureRegion> mUFO_Beam_List = new ArrayList<PixelPerfectTiledTextureRegion>();
	//private List<PixelPerfectTiledTextureRegion> mUFO_Beam_Base_List = new ArrayList<PixelPerfectTiledTextureRegion>();
	private List<PixelPerfectTiledTextureRegion> mHuman_List = new ArrayList<PixelPerfectTiledTextureRegion>();
	// Powerup List
	private List<TiledTextureRegion> mGadget_List = new ArrayList<TiledTextureRegion>();
	// NPC List (For displaying at hanger only)
	private List<TiledTextureRegion> mNPC_List = new ArrayList<TiledTextureRegion>();
	
	//===========================================================
	// Main Menu Resource Fields
	//===========================================================
	private ITextureRegion mSplashTextureRegion;
	private ITextureRegion mLoadingTextureRegion;
	private BitmapTextureAtlas mGameStyleHelpTexture, mHelpButtonTexture, mNextArrowButtonTexture;
	private TextureRegion mTip1TextureRegion, mTip2TextureRegion, mTip3TextureRegion, mGameStyleHelpTextureRegion, mHelpButtonTextureRegion, mNextArrowButtonTextureRegion;
	
	private BitmapTextureAtlas mGoogleButtonTexture;
	private TiledTextureRegion mGoogleButtonTextureRegion;
	private BitmapTextureAtlas mFacebookButtonTexture;
	private TiledTextureRegion mFacebookButtonTextureRegion;
	private BitmapTextureAtlas mTwitterButtonTexture;
	private TiledTextureRegion mTwitterButtonTextureRegion;
	
	private BitmapTextureAtlas mGooglePlayIconTexture;
	private TiledTextureRegion mGooglePlayIconTextureRegion;
	
	// Main Menu Scene
	private BitmapTextureAtlas mMenuBackgroundTexture;
	private ITextureRegion mMenuBackgroundTextureRegion;

	private BitmapTextureAtlas mTitleTexture;
	private ITextureRegion mTitleTextureRegion;

	private BitmapTextureAtlas mPlayButtonTexture;
	private ITextureRegion mPlayButtonTextureRegion;
	
	private BitmapTextureAtlas mMultiplayerButtonsTexture;
	private ITiledTextureRegion mMultiplayerButtonsTextureRegion;
		
	private BitmapTextureAtlas mConnectorTexture;
	private ITextureRegion mConnectorTextureRegion;
	
	private BitmapTextureAtlas mCloseButtonTexture;
	private ITextureRegion mCloseButtonTextureRegion;

	// Common 
	private BitmapTextureAtlas mWashoutBackgroundTexture;
	private ITextureRegion mWashoutBackgroundTextureRegion;

	private BitmapTextureAtlas mCommonBackButtonTexture;
	private TiledTextureRegion mCommonBackButtonTextureRegion;

	private BitmapTextureAtlas mComingSoonIconTexture;
	private ITextureRegion mComingSoonIconTextureRegion;
	
	// High Score_SharedPreference Scene
	private BitmapTextureAtlas mHighScoreButtonTexture;
	private ITextureRegion mHighScoreButtonTextureRegion;

	// Options Scene
	private BitmapTextureAtlas mSettingsButtonTexture;
	private ITextureRegion mSettingsButtonTextureRegion;

	private BitmapTextureAtlas mSFXTexture;
	private TiledTextureRegion mSFXTextureRegion;

	private BitmapTextureAtlas mMusicTexture;
	private TiledTextureRegion mMusicTextureRegion;

	private BitmapTextureAtlas mMotionControlTexture;
	private TiledTextureRegion mMotionControlTextureRegion;

	private BitmapTextureAtlas mHangerButtonTexture;
	private ITextureRegion mHangerButtonTextureRegion;
	
	private BitmapTextureAtlas mHangerBackgroundTexture;
	private ITextureRegion mHangerBackgroundTextureRegion;
	
	private BitmapTextureAtlas mHangerSubBackgroundTexture;
	private ITextureRegion mHangerSubBackgroundTextureRegion;
	
	private BitmapTextureAtlas mHangerDisplayPanelTexture;
	private ITextureRegion mHangerDisplayPanelTextureRegion;
	
	private BitmapTextureAtlas mMissionDisplayTexture;
	private ITextureRegion mMissionDisplayTextureRegion;
	
	private BitmapTextureAtlas mMissionSpotTexture;
	private ITextureRegion mMissionSpotTextureRegion;
	
	private BitmapTextureAtlas mUFOWindowDisplayTexture;
	private ITiledTextureRegion mUFOWindowDisplayTextureRegion;
	
	private BitmapTextureAtlas mGadgetWindowDisplayTexture;
	private ITiledTextureRegion mGadgetWindowDisplayTextureRegion;
	
	private BitmapTextureAtlas mUFOIconTexture;
	private TiledTextureRegion mUFOIconTextureRegion;

	private BitmapTextureAtlas mGadgetIconTexture;
	private TiledTextureRegion mGadgetIconTextureRegion;

	private BitmapTextureAtlas mProfileIconTexture;
	private TiledTextureRegion mProfileIconTextureRegion;

	private BitmapTextureAtlas mProfileSceneBackgroundTexture;
	private ITiledTextureRegion mProfileSceneBackgroundTextureRegion;

	private BitmapTextureAtlas mProfileSubSceneBackgroundTexture;
	private ITiledTextureRegion mProfileSubSceneBackgroundTextureRegion;
	
	private BitmapTextureAtlas mProfileSub2SceneBackgroundTexture;
	private ITiledTextureRegion mProfileSub2SceneBackgroundTextureRegion;
	
	private BitmapTextureAtlas mAchievementSubheaderTexture;
	private TextureRegion mAchievementSubheaderTextureRegion;
	
	private BitmapTextureAtlas mMissionSubheaderTexture;
	private TextureRegion mMissionSubheaderTextureRegion;
	
	private BitmapTextureAtlas mProfileSceneTextTexture;
	private TiledTextureRegion mProfileSceneTextTextureRegion;

	private BitmapTextureAtlas mAchievementIconTexture;
	private TiledTextureRegion mAchievementIconTextureRegion;

	private BitmapTextureAtlas mCreditsIconTexture;
	private ITiledTextureRegion mCreditsIconTextureRegion;
	
	private BitmapTextureAtlas mCreditsScreenTexture;
	private TextureRegion mCreditsScreenTextureRegion;
	
	private BitmapTextureAtlas mHangerShopIconTexture;
	private TiledTextureRegion mHangerShopIconTextureRegion;
	
	private BitmapTextureAtlas mEquipIconTexture;
	private TiledTextureRegion mEquipIconTextureRegion;
	
	private BitmapTextureAtlas mUFOBoxTexture;
	private ITextureRegion mUFOBoxTextureRegion;
	
	private BitmapTextureAtlas mGadgetCounterBoxTexture;
	private TiledTextureRegion mGadgetCounterBoxTextureRegion;
	
	private BitmapTextureAtlas mYouHaveIconTexture;
	private ITextureRegion mYouHaveIconTextureRegion;

	private BitmapTextureAtlas mGetMoreGoldIconTexture;
	private ITextureRegion mGetMoreGoldIconTextureRegion;
	
	private BitmapTextureAtlas mHangerBackIconTexture;
	private TiledTextureRegion mHangerBackIconTextureRegion;
	
	private BitmapTextureAtlas mPurchaseIconTexture;
	private TiledTextureRegion mPurchaseIconTextureRegion;

	private BitmapTextureAtlas mPurchaseItemsTexture;
	private TiledTextureRegion mPurchaseItemsTextureRegion;

	private BitmapTextureAtlas mAchievementTrophiesTexture;
	private TiledTextureRegion mAchievementTrophiesTextureRegion;
	
	// Distance Font texture
	private Font mFont;

	//===========================================================
	// Calibration Resource Fields
	//===========================================================
	private BitmapTextureAtlas mAccelerometerTextureAtlas;
	private ITextureRegion mAccelerometer0TextureRegion;
	private ITextureRegion mAccelerometer45TextureRegion;
	private ITextureRegion mAccelerometerCustomTextureRegion;
	
	private BitmapTextureAtlas mCalibrationCustomTextureAtlas;
	private ITextureRegion mCalibrationCustomScreenTextureRegion;
	private ITextureRegion mCalibrationCustomHeaderTextureRegion;
	private ITextureRegion mCalibrationCustomBallTextureRegion;
	private PixelPerfectTextureRegion mCalibrationCustomRingTextureRegion;
	private ITextureRegion mCalibrateButtonTextureRegion;
	

	//===========================================================
	// Game Resource Fields
	//===========================================================
	// Parallax Background Textures
	private BitmapTextureAtlas mCommonParallaxBackgroundTexture, mSingaporeParallaxBackgroundTexture, mSydneyParallaxBackgroundTexture;
	private ITextureRegion mCommonParallaxLayer1_1, mCommonParallaxLayer1_2, mCommonParallaxLayer1_3;
	private ITextureRegion mSingaporeParallaxLayer5, mSingaporeParallaxLayer3, mSingaporeParallaxLayer2_1;//, mSingaporeParallaxLayer2_2, mSingaporeParallaxLayer2_3;
	private ITextureRegion mSydneyParallaxLayer6, mSydneyParallaxLayer5A, mSydneyParallaxLayer5B, mSydneyParallaxLayer4, mSydneyParallaxLayer3_1, mSydneyParallaxLayer3_2, mSydneyParallaxLayer2_1, mSydneyParallaxLayer2_2, mSydneyParallaxLayer2_3, mSydneyParallaxLayer2_4, mSydneyParallaxLayer1;

	// Obstacles Textures
	private BitmapTextureAtlas mLampPostTexture, mTrafficLightTexture, mCarsTexture;
	private PixelPerfectTiledTextureRegion mLampPostTextureRegion, mTrafficLightTextureRegion, mCarsTextureRegion;

	// Beam Textures
	private BitmapTextureAtlas mMilitaryAlertTexture;
	private TiledTextureRegion mMilitaryAlertTextureRegion;
	
	// Military Humans Textures
	private BitmapTextureAtlas mFemaleMilitaryTexture, mMaleMilitaryTexture;
	private PixelPerfectTiledTextureRegion mFemaleMilitaryTextureRegion, mMaleMilitaryTextureRegion;

	// Humans Textures
	private BitmapTextureAtlas mDisintegrateHumanTexture;
	private PixelPerfectTiledTextureRegion mDisintegrateHumanTextureRegion;

	// Emotions Textures
	private BitmapTextureAtlas mEmotionsTexture;
	private TiledTextureRegion mEmotionsTextureRegion;
	
	// Smoke Textures
	private BitmapTextureAtlas mSmokeTexture;
	private TiledTextureRegion mSmokeTextureRegion;

	// Player Forcefield Textures
	private BitmapTextureAtlas mForcefieldTexture;
	private ITextureRegion mForcefieldTextureRegion;

	// Player Angel & Devil Bar Textures
	private BitmapTextureAtlas mPlayerBarCaseTexture;
	private ITextureRegion mPlayerBarCaseTextureRegion;
	private BitmapTextureAtlas mPlayerBarTexture;
	private ITextureRegion mPlayerBarTextureRegion;
	
	// Analog On Screen Controls Textures
	private BitmapTextureAtlas mOnScreenControlTexture, mOnScreenControlAngelButtonTexture, mOnScreenControlDevilButtonTexture;
	private ITextureRegion mOnScreenControlBaseTextureRegion;
	private ITextureRegion mOnScreenControlKnobTextureRegion;
	private ITextureRegion mOnScreenControlAngelButtonTextureRegion;
	private ITextureRegion mOnScreenControlDevilButtonTextureRegion;

	// BlackHole Textures
	private BitmapTextureAtlas mBlackHoleTexture;
	private TiledTextureRegion mBlackHoleTextureRegion;
	
	// Explosion Textures
	private BitmapTextureAtlas mExplosionTexture;
	private TiledTextureRegion mExplosionTextureRegion;

	// Weapons Textures
	private BitmapTextureAtlas mMissileAlertTexture, mScatterMissileAlertTexture, mLaserAlertTexture;
	private TiledTextureRegion mMissileAlertTextureRegion, mScatterMissileAlertTextureRegion, mLaserAlertTextureRegion;
	
	private BitmapTextureAtlas mMissileHeadTexture, mScatterMissileHeadTexture;
	private PixelPerfectTiledTextureRegion mMissileHeadTextureRegion, mScatterMissileHeadTextureRegion;
	
	private BitmapTextureAtlas mMissileTailTexture, mScatterMissileTailTexture;
	private PixelPerfectTiledTextureRegion mMissileTailTextureRegion, mScatterMissileTailTextureRegion;
	
	private BitmapTextureAtlas mLaserBeamTexture;
	private PixelPerfectTiledTextureRegion mLaserBeamTextureRegion;
	
	private BitmapTextureAtlas mMilitaryMissileTexture;
	private PixelPerfectTiledTextureRegion mMilitaryMissileTextureRegion;

	// Wind landing Textures
	private BitmapTextureAtlas mWindLandingTexture;
	private PixelPerfectTiledTextureRegion mWindLandingTextureRegion;

	// Gold Textures
	private BitmapTextureAtlas mGoldTexture;
	private TiledTextureRegion mGoldTextureRegion;
	
	// Background/SFX Sounds
	private Music mainBGM, gameBGM, buttonSFX, purchaseSFX, laserBeamSFX, ufoHoverSFX, ufoBeamSFX, explosionSFX, countdownSFX, missileLaunchSFX, shieldHitSFX;
	
	//--------------------------------------------------------
	// ------------------ Game Over Scene --------------------
	//--------------------------------------------------------
	private BitmapTextureAtlas mGameOverTexture;
	private TiledTextureRegion mGameOverTextureRegion;
	private BitmapTextureAtlas mNewHighScoreTextTexture;
	private TiledTextureRegion mNewHighScoreTextTextureRegion;
	
	// Pause Menu & Game Over textures
	private BitmapTextureAtlas mPauseButtonTexture;
	private TiledTextureRegion mPauseButtonTextureRegion;
	private BitmapTextureAtlas mGameplayPauseMenuButtonTexture;
	private TiledTextureRegion mGameplayPauseMenuButtonTextureRegion;
	
	private BitmapTextureAtlas mFacebookShareButtonTexture;
	private TextureRegion mFacebookShareButtonTextureRegion;
	
	
	//===========================================================
	// Constructor
	//===========================================================
	public GameResources(TextureManager tm, FontManager fm, AssetManager am, Context c, Context baseC, VertexBufferObjectManager mVertexBufferObjectManager, Engine engine, Camera camera) {
		this.mTextureManager = tm;
		this.mFontManager = fm;
		this.mAssetManager = am;
		this.pContext = c;
		this.mBaseContext = baseC;
		this.mEngine = engine;
		
		// Load font textures of the game
		final ITexture fontTexture = new BitmapTextureAtlas(this.getTextureManager(), 256, 256, TextureOptions.BILINEAR);
		this.mFont = FontFactory.createFromAsset(this.getFontManager(), fontTexture, this.getAssets(), "font/Acknowledge-TT-BRK-Regular.ttf", 42, true, android.graphics.Color.CYAN);
		this.mFont.load();
        
	}

	//===========================================================
	// Get Other Methods
	//===========================================================
	public TextureManager getTextureManager() {
		return mTextureManager;
	}
	public FontManager getFontManager() {
		return mFontManager;
	}
	public AssetManager getAssets() {
		return mAssetManager;
	}
	public VertexBufferObjectManager getVertexBufferObjectManager() {
		return mVertexBufferObjectManager;
	}
	public Context getmBaseContext() {
		return mBaseContext;
	}

	//===========================================================
	// Load Resources
	//===========================================================
	public void loadMainMenuResources() {
		this.mNextArrowButtonTexture = new BitmapTextureAtlas(this.getTextureManager(), 84, 102);
		this.setmNextArrowButtonTextureRegion(BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mNextArrowButtonTexture, pContext, "gfx/Tip/nextArrow.png", 0, 0));
		this.mNextArrowButtonTexture.load();
		
		this.mGameStyleHelpTexture = new BitmapTextureAtlas(this.getTextureManager(), 1161, 700);
		this.setmGameStyleHelpTextureRegion(BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mGameStyleHelpTexture, pContext, "gfx/Tip/GAMESTYLE_HELP.png", 0, 0));
		this.mGameStyleHelpTexture.load();		
		
		this.mHelpButtonTexture = new BitmapTextureAtlas(this.getTextureManager(), 170, 173);
		this.setmHelpButtonTextureRegion(BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mHelpButtonTexture, pContext, "gfx/help.png", 0, 0));
		this.mHelpButtonTexture.load();
		
		this.mGoogleButtonTexture = new BitmapTextureAtlas(this.getTextureManager(), 286, 164);
		this.mGoogleButtonTextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.mGoogleButtonTexture, pContext, "gfx/MainMenu/google_plus_icon.png", 0, 0, 2, 1);
		this.mGoogleButtonTexture.load();

		this.mFacebookButtonTexture = new BitmapTextureAtlas(this.getTextureManager(), 286, 164);
		this.mFacebookButtonTextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.mFacebookButtonTexture, pContext, "gfx/MainMenu/facebook_icon.png", 0, 0, 2, 1);
		this.mFacebookButtonTexture.load();
		
		this.mTwitterButtonTexture = new BitmapTextureAtlas(this.getTextureManager(), 286, 164);
		this.mTwitterButtonTextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.mTwitterButtonTexture, pContext, "gfx/MainMenu/twitter_icon.png", 0, 0, 2, 1);
		this.mTwitterButtonTexture.load();
		
		this.mGooglePlayIconTexture = new BitmapTextureAtlas(this.getTextureManager(), 256, 92);
		this.mGooglePlayIconTextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.mGooglePlayIconTexture, pContext, "gfx/Google/play_games_badge.png", 0, 0, 2, 1);
		this.mGooglePlayIconTexture.load();
		
		// Main Menu Resource
		this.mMenuBackgroundTexture = new BitmapTextureAtlas(this.getTextureManager(), 1280, 820);
		this.mMenuBackgroundTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mMenuBackgroundTexture, pContext, "gfx/MainMenu/gametitle_screen.png", 0, 0);
		this.mMenuBackgroundTexture.load();

		this.mTitleTexture = new BitmapTextureAtlas(this.getTextureManager(), 500, 180);
		this.mTitleTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mTitleTexture, pContext, "gfx/MainMenu/game_title.png", 0, 0);
		this.mTitleTexture.load();

		this.mPlayButtonTexture = new BitmapTextureAtlas(this.getTextureManager(), 200, 200);
		this.mPlayButtonTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mPlayButtonTexture, pContext, "gfx/MainMenu/play_button.png", 0, 0);
		this.mPlayButtonTexture.load();
		
		this.mMultiplayerButtonsTexture = new BitmapTextureAtlas(this.getTextureManager(), 635, 384);
		this.mMultiplayerButtonsTextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.mMultiplayerButtonsTexture, pContext, "gfx/MainMenu/multiplayer_buttons.png", 0, 0, 1, 4);
		this.mMultiplayerButtonsTexture.load();
		
		this.mConnectorTexture = new BitmapTextureAtlas(this.getTextureManager(), 5, 77);
		this.mConnectorTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mConnectorTexture, pContext, "gfx/MainMenu/connector.png", 0, 0);
		this.mConnectorTexture.load();
		
		// Common Textures
		this.mCloseButtonTexture = new BitmapTextureAtlas(this.getTextureManager(), 100, 87);
		this.mCloseButtonTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mCloseButtonTexture, pContext, "gfx/MainMenu/close_icon.png", 0, 0);
		this.mCloseButtonTexture.load();

		this.mWashoutBackgroundTexture = new BitmapTextureAtlas(this.getTextureManager(), 1280, 720);
		this.mWashoutBackgroundTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mWashoutBackgroundTexture, pContext, "gfx/Common/washout_background.png", 0, 0);
		this.mWashoutBackgroundTexture.load();

		this.mCommonBackButtonTexture = new BitmapTextureAtlas(this.getTextureManager(), 101, 96);
		this.mCommonBackButtonTextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.mCommonBackButtonTexture, pContext, "gfx/Common/back_button.png", 0, 0, 1, 1);
		this.mCommonBackButtonTexture.load();

		this.mComingSoonIconTexture = new BitmapTextureAtlas(this.getTextureManager(), 128, 128);
		this.mComingSoonIconTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mComingSoonIconTexture, pContext, "gfx/Common/coming_soon.png", 0, 0);
		this.mComingSoonIconTexture.load();
		
		// HighScore Menu
		this.mHighScoreButtonTexture = new BitmapTextureAtlas(this.getTextureManager(), 94, 254);
		this.mHighScoreButtonTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mHighScoreButtonTexture, pContext, "gfx/Hanger/hanger_achievements_icon.png", 0, 0);
		this.mHighScoreButtonTexture.load();
		
		// Options Menu
		this.mSettingsButtonTexture = new BitmapTextureAtlas(this.getTextureManager(), 143, 164);
		this.mSettingsButtonTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mSettingsButtonTexture, pContext, "gfx/MainMenu/settings_icon.png", 0, 0);
		this.mSettingsButtonTexture.load();
		
		this.mSFXTexture = new BitmapTextureAtlas(this.getTextureManager(), 286, 164);
		this.mSFXTextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.mSFXTexture, pContext, "gfx/MainMenu/sfx_icon.png", 0, 0, 2, 1);
		this.mSFXTexture.load();

		this.mMusicTexture = new BitmapTextureAtlas(this.getTextureManager(), 286, 164);
		this.mMusicTextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.mMusicTexture, pContext, "gfx/MainMenu/music_icon.png", 0, 0, 2, 1);
		this.mMusicTexture.load();

		this.mMotionControlTexture = new BitmapTextureAtlas(this.getTextureManager(), 286, 164);
		this.mMotionControlTextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.mMotionControlTexture, pContext, "gfx/MainMenu/motion_icon.png", 0, 0, 2, 1);
		this.mMotionControlTexture.load();

		// Hanger Menu
		this.mHangerButtonTexture = new BitmapTextureAtlas(this.getTextureManager(), 142, 162);
		this.mHangerButtonTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mHangerButtonTexture, pContext, "gfx/MainMenu/hanger_icon.png", 0, 0);
		this.mHangerButtonTexture.load();
		
		this.mHangerBackgroundTexture = new BitmapTextureAtlas(this.getTextureManager(), 1280, 720);
		this.mHangerBackgroundTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mHangerBackgroundTexture, pContext, "gfx/Hanger/hanger_screen.png", 0, 0);
		this.mHangerBackgroundTexture.load();
		
		this.mHangerSubBackgroundTexture = new BitmapTextureAtlas(this.getTextureManager(), 1280, 720);
		this.mHangerSubBackgroundTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mHangerSubBackgroundTexture, pContext, "gfx/Hanger/hanger_sub_background.png", 0, 0);
		this.mHangerSubBackgroundTexture.load();
		
		this.mMissionDisplayTexture = new BitmapTextureAtlas(this.getTextureManager(), 201, 512);
		this.mMissionDisplayTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mMissionDisplayTexture, pContext, "gfx/Hanger/hanger_mission_icon.png", 0, 0);
		this.mMissionDisplayTexture.load();

		this.mMissionSpotTexture = new BitmapTextureAtlas(this.getTextureManager(), 34, 51);
		this.mMissionSpotTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mMissionSpotTexture, pContext, "gfx/Hanger/mission_spot.png", 0, 0);
		this.mMissionSpotTexture.load();

		this.mHangerDisplayPanelTexture = new BitmapTextureAtlas(this.getTextureManager(), 285, 465);
		this.mHangerDisplayPanelTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mHangerDisplayPanelTexture, pContext, "gfx/Hanger/hanger_object_display_panel.png", 0, 0);
		this.mHangerDisplayPanelTexture.load();
		
		// NPC List
		for (int i=0; i < NPC_ASSETS_COUNT; i++) {
			String count = "0";
			if(i < 9) {
				count += (i+1);
			} else {
				count = String.valueOf(i+1);
			}
			BitmapTextureAtlas mNPCTexture = new BitmapTextureAtlas(this.getTextureManager(), 200, 260,	TextureOptions.BILINEAR_PREMULTIPLYALPHA);
			TiledTextureRegion mNPCTextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(mNPCTexture, pContext, "gfx/Hanger/NPC_" + count + ".png", 0, 0, 1, 1);
			mNPCTexture.load();
			mNPC_List.add(mNPCTextureRegion);
		}
				
		// Powerups
		for (int i=0; i < GADGETS_ASSETS_COUNT; i++) {
			String count = "0";
			if(i < 9) {
				count += (i+1);
			} else {
				count = String.valueOf(i+1);
			}
			BitmapTextureAtlas mGadgetTexture = new BitmapTextureAtlas(this.getTextureManager(), 256, 128,	TextureOptions.BILINEAR_PREMULTIPLYALPHA);
			TiledTextureRegion mGadgetTextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(mGadgetTexture, pContext, "gfx/Hanger/Gadget_" + count + ".png", 0, 0, 2, 1);
			mGadgetTexture.load();
			mGadget_List.add(mGadgetTextureRegion);
		}
		
		this.mUFOWindowDisplayTexture = new BitmapTextureAtlas(this.getTextureManager(), 716, 335);
		this.mUFOWindowDisplayTextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.mUFOWindowDisplayTexture, pContext, "gfx/Hanger/ufo_window.png", 0, 0, 2, 1);
		this.mUFOWindowDisplayTexture.load();
		
		this.mGadgetWindowDisplayTexture = new BitmapTextureAtlas(this.getTextureManager(), 487, 222);
		this.mGadgetWindowDisplayTextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.mGadgetWindowDisplayTexture, pContext, "gfx/Hanger/gadgets_window.png", 0, 0, 2, 1);
		this.mGadgetWindowDisplayTexture.load();
		
		this.mUFOIconTexture = new BitmapTextureAtlas(this.getTextureManager(), 256, 256);
		this.mUFOIconTextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.mUFOIconTexture, pContext, "gfx/Hanger/UFO_icon.png", 0, 0, 2, 1);
		this.mUFOIconTexture.load();
		
		this.mGadgetIconTexture = new BitmapTextureAtlas(this.getTextureManager(), 256, 256);
		this.mGadgetIconTextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.mGadgetIconTexture, pContext, "gfx/Hanger/gadgets_icon.png", 0, 0, 2, 1);
		this.mGadgetIconTexture.load();

		this.mPurchaseIconTexture = new BitmapTextureAtlas(this.getTextureManager(), 248, 108);
		this.mPurchaseIconTextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.mPurchaseIconTexture, pContext, "gfx/Hanger/purchase_icon.png", 0, 0, 2, 1);
		this.mPurchaseIconTexture.load();

		this.mPurchaseItemsTexture = new BitmapTextureAtlas(this.getTextureManager(), 635, 128);
		this.mPurchaseItemsTextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.mPurchaseItemsTexture, pContext, "gfx/Hanger/hanger_purchase_items.png", 0, 0, 5, 1);
		this.mPurchaseItemsTexture.load();
		
		this.mProfileIconTexture = new BitmapTextureAtlas(this.getTextureManager(), 75, 279);
		this.mProfileIconTextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.mProfileIconTexture, pContext, "gfx/Hanger/hanger_profile_icon.png", 0, 0, 1, 1);
		this.mProfileIconTexture.load();

		this.mProfileSceneBackgroundTexture = new BitmapTextureAtlas(this.getTextureManager(), 1161, 700);
		this.mProfileSceneBackgroundTextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.mProfileSceneBackgroundTexture, pContext, "gfx/Hanger/stats_screen.png", 0, 0, 1, 1);
		this.mProfileSceneBackgroundTexture.load();

		this.mProfileSubSceneBackgroundTexture = new BitmapTextureAtlas(this.getTextureManager(), 1161, 720);
		this.mProfileSubSceneBackgroundTextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.mProfileSubSceneBackgroundTexture, pContext, "gfx/Hanger/sub_stats_screen.png", 0, 0, 1, 1);
		this.mProfileSubSceneBackgroundTexture.load();

		this.mProfileSub2SceneBackgroundTexture = new BitmapTextureAtlas(this.getTextureManager(), 1161, 720);
		this.mProfileSub2SceneBackgroundTextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.mProfileSub2SceneBackgroundTexture, pContext, "gfx/Hanger/sub_stats_screen2.png", 0, 0, 1, 1);
		this.mProfileSub2SceneBackgroundTexture.load();
		
		this.mAchievementSubheaderTexture = new BitmapTextureAtlas(this.getTextureManager(), 1160, 53);
		this.mAchievementSubheaderTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mAchievementSubheaderTexture, pContext, "gfx/Hanger/achievements_subheader.png", 0, 0);
		this.mAchievementSubheaderTexture.load();

		this.mAchievementTrophiesTexture = new BitmapTextureAtlas(this.getTextureManager(), 164, 384);
		this.mAchievementTrophiesTextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.mAchievementTrophiesTexture, pContext, "gfx/Hanger/achievement_trophies.png", 0, 0, 2, 3);
		this.mAchievementTrophiesTexture.load();
		
		this.mMissionSubheaderTexture = new BitmapTextureAtlas(this.getTextureManager(), 1160, 53);
		this.mMissionSubheaderTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mMissionSubheaderTexture, pContext, "gfx/Hanger/mission_subheader.png", 0, 0);
		this.mMissionSubheaderTexture.load();
		
		this.mProfileSceneTextTexture = new BitmapTextureAtlas(this.getTextureManager(), 1160, 384);
		this.mProfileSceneTextTextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.mProfileSceneTextTexture, pContext, "gfx/Hanger/stats_subheader.png", 0, 0, 1, 6);
		this.mProfileSceneTextTexture.load();
		
		this.mAchievementIconTexture = new BitmapTextureAtlas(this.getTextureManager(), 94, 254);
		this.mAchievementIconTextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.mAchievementIconTexture, pContext, "gfx/Hanger/hanger_throphy_icon.png", 0, 0, 1, 1);
		this.mAchievementIconTexture.load();
		
		this.mCreditsIconTexture = new BitmapTextureAtlas(this.getTextureManager(), 981, 394);
		this.mCreditsIconTextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.mCreditsIconTexture, pContext, "gfx/Hanger/hanger_credits_icon.png", 0, 0, 3, 2);
		this.mCreditsIconTexture.load();

		this.mCreditsScreenTexture = new BitmapTextureAtlas(this.getTextureManager(), 1162, 700);
		this.mCreditsScreenTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mCreditsScreenTexture, pContext, "gfx/Hanger/credits_screen.png", 0, 0);
		this.mCreditsScreenTexture.load();
		
		this.mHangerShopIconTexture = new BitmapTextureAtlas(this.getTextureManager(), 770, 251);
		this.mHangerShopIconTextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.mHangerShopIconTexture, pContext, "gfx/Hanger/hanger_shop_icon.png", 0, 0, 4, 1);
		this.mHangerShopIconTexture.load();

		this.mUFOBoxTexture = new BitmapTextureAtlas(this.getTextureManager(), 128, 128);
		this.mUFOBoxTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mUFOBoxTexture, pContext, "gfx/Hanger/UFO_box.png", 0, 0);
		this.mUFOBoxTexture.load();
		
		this.mGadgetCounterBoxTexture = new BitmapTextureAtlas(this.getTextureManager(), 128, 128);
		this.mGadgetCounterBoxTextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.mGadgetCounterBoxTexture, pContext, "gfx/Hanger/gadget_count_box.png", 0, 0, 1, 1);
		this.mGadgetCounterBoxTexture.load();
		
		this.mEquipIconTexture = new BitmapTextureAtlas(this.getTextureManager(), 500, 400);
		this.mEquipIconTextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.mEquipIconTexture, pContext, "gfx/Hanger/equip_buttons.png", 0, 0, 1, 4);
		this.mEquipIconTexture.load();
				
		this.mYouHaveIconTexture = new BitmapTextureAtlas(this.getTextureManager(), 300, 100);
		this.mYouHaveIconTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mYouHaveIconTexture, pContext, "gfx/Hanger/YouHave_icon.png", 0, 0);
		this.mYouHaveIconTexture.load();
		
		this.mGetMoreGoldIconTexture = new BitmapTextureAtlas(this.getTextureManager(), 315, 100);
		this.mGetMoreGoldIconTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mGetMoreGoldIconTexture, pContext, "gfx/Hanger/get_more_gold.png", 0, 0);
		this.mGetMoreGoldIconTexture.load();
		
		this.mHangerBackIconTexture = new BitmapTextureAtlas(this.getTextureManager(), 180, 63);
		this.mHangerBackIconTextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.mHangerBackIconTexture, pContext, "gfx/Hanger/hanger_main_icon.png", 0, 0, 2, 1);
		this.mHangerBackIconTexture.load();

		// -------------- Load the in game purchase product id --------------
		loadingText.setText("Loading InApp Purchases...");
		additionalSkuList.add("500_gold");
		additionalSkuList.add("1500_gold");
		additionalSkuList.add("5000_gold");
		additionalSkuList.add("10000_gold");
		additionalSkuList.add(mPremiumNoAds);

		// ------------------------ Load Pause Menu Items ------------------------ 
		loadingText.setText("Loading Pause...");
		this.mPauseButtonTexture = new BitmapTextureAtlas(this.getTextureManager(), 100, 87, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
		this.mPauseButtonTextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.mPauseButtonTexture, pContext, "gfx/Game/pause_button.png", 0, 0, 1, 1);
		this.mPauseButtonTexture.load();

		this.mGameplayPauseMenuButtonTexture = new BitmapTextureAtlas(this.getTextureManager(), 500, 300, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
		this.mGameplayPauseMenuButtonTextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.mGameplayPauseMenuButtonTexture, pContext, "gfx/Game/gamepause_buttons.png", 0, 0, 1, 3);
		this.mGameplayPauseMenuButtonTexture.load();
		
		// ------------------------ Load Game Over Items ------------------------ 
		loadingText.setText("Loading Gameover...");
		this.mGameOverTexture = new BitmapTextureAtlas(this.getTextureManager(), 406, 325, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
		this.mGameOverTextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.mGameOverTexture, pContext, "gfx/Game/GameOver.png", 0, 0, 1, 1);
		this.mGameOverTexture.load();
		
		this.mNewHighScoreTextTexture = new BitmapTextureAtlas(this.getTextureManager(), 282, 168, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
		this.mNewHighScoreTextTextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.mNewHighScoreTextTexture, pContext, "gfx/Game/NewHighScore.png", 0, 0, 1, 1);
		this.mNewHighScoreTextTexture.load();
		
		this.mFacebookShareButtonTexture = new BitmapTextureAtlas(this.getTextureManager(), 498, 96, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
		this.mFacebookShareButtonTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mFacebookShareButtonTexture, pContext, "gfx/Game/fbShare.png", 0, 0);
		this.mFacebookShareButtonTexture.load();
		
		this.mainMenuResourcesLoaded = true;
	}
	
	public void loadCalibrationResources() {
		// Textures for accelerometer menu
		this.mAccelerometerTextureAtlas = new BitmapTextureAtlas(this.getTextureManager(), 774, 690, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
		this.mAccelerometer0TextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(mAccelerometerTextureAtlas, pContext, "gfx/MainMenu/0_degrees.png", 0, 0);
		this.mAccelerometer45TextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(mAccelerometerTextureAtlas, pContext, "gfx/MainMenu/45_degrees.png", 258,0);
		this.mAccelerometerCustomTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(mAccelerometerTextureAtlas, pContext, "gfx/MainMenu/custom.png", 516,0);
		this.mAccelerometerTextureAtlas.load();
		
		this.mCalibrationCustomTextureAtlas = new BitmapTextureAtlas(this.getTextureManager(), 2000, 2000, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
		this.mCalibrationCustomScreenTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(mCalibrationCustomTextureAtlas, pContext, "gfx/Calibration/calibration_screen.png", 0,0);
		this.mCalibrationCustomHeaderTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(mCalibrationCustomTextureAtlas, pContext, "gfx/Calibration/calibrate_screen_header.png", 668,0);
		this.mCalibrationCustomBallTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(mCalibrationCustomTextureAtlas, pContext, "gfx/Calibration/calibrate_ball.png", 1225,0);
		this.mCalibrationCustomRingTextureRegion = PixelPerfectTextureRegionFactory.createFromAsset(mCalibrationCustomTextureAtlas, this.mAssetManager, "gfx/Calibration/calibrate_ring.png", 1262,0, Color.TRANSPARENT);
		this.mCalibrateButtonTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(mCalibrationCustomTextureAtlas, pContext, "gfx/Calibration/calibrate_button.png", 1500,0);
		this.mCalibrationCustomTextureAtlas.load();

		// ------------------------ Load Analog On Screen Controls textures of the game ------------------------ 
		loadingText.setText("Loading Analog...");
		this.mOnScreenControlTexture = new BitmapTextureAtlas(this.getTextureManager(), 384, 256, TextureOptions.BILINEAR);
		this.mOnScreenControlBaseTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mOnScreenControlTexture, pContext, "gfx/Game/onscreen_control_base.png", 0, 0);
		this.mOnScreenControlKnobTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mOnScreenControlTexture, pContext, "gfx/Game/onscreen_control_knob.png", 256, 0);
		this.mOnScreenControlTexture.load();
		
		this.mOnScreenControlAngelButtonTexture = new BitmapTextureAtlas(this.getTextureManager(), 128, 128, TextureOptions.BILINEAR);
		this.mOnScreenControlAngelButtonTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mOnScreenControlAngelButtonTexture, pContext, "gfx/Game/angelButton.png", 0, 0);
		this.mOnScreenControlAngelButtonTexture.load();
		
		this.mOnScreenControlDevilButtonTexture = new BitmapTextureAtlas(this.getTextureManager(), 128, 128, TextureOptions.BILINEAR);
		this.mOnScreenControlDevilButtonTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mOnScreenControlDevilButtonTexture, pContext, "gfx/Game/devilButton.png", 0, 0);
		this.mOnScreenControlDevilButtonTexture.load();
		
		this.calibrationResourcesLoaded = true;
	}

	public void loadGameResources_PART1() {
		// ------------------------ Load UFO Images ------------------------ 
		loadingText.setText("Loading Spacecrafts...");
		for (int i=0; i < SPACECRAFT_MODEL_ASSETS_COUNT; i++) {
			BitmapTextureAtlas mUFOBitmapTextureAtlas = new BitmapTextureAtlas(this.getTextureManager(), 1333, 442, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
			PixelPerfectTiledTextureRegion mUFOTextureRegion;

			String modelNumber = "";
			if((i+1) < 9) {
				modelNumber = "0" + (i+1);
			} else {
				modelNumber = "" + (i+1);
			}
			
			//Due to different UFO have diff number of frames, there's a need to declare the difference
			if(i == 0 || i == 4)
				mUFOTextureRegion = PixelPerfectTextureRegionFactory.createTiledFromAsset(mUFOBitmapTextureAtlas, this.mAssetManager, "gfx/Game/Spacecrafts/Model_" + modelNumber + ".png", 0, 0, 10, 3, Color.TRANSPARENT);
			else 
				mUFOTextureRegion = PixelPerfectTextureRegionFactory.createTiledFromAsset(mUFOBitmapTextureAtlas, this.mAssetManager, "gfx/Game/Spacecrafts/Model_" + modelNumber + ".png", 0, 0, 10, 4, Color.TRANSPARENT);
			
			mUFOBitmapTextureAtlas.load();
			mUFO_Model_List.add(mUFOTextureRegion);
		}

		//BitmapTextureAtlas mBeamBaseBitmapTextureAtlas = new BitmapTextureAtlas(this.getTextureManager(), 333, 75, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
		//PixelPerfectTiledTextureRegion mBeamBaseTextureRegion = PixelPerfectTextureRegionFactory.createTiledFromAsset(mBeamBitmapTextureAtlas, this.mAssetManager, "gfx/Game/beam_base.png", 0, 0, 2, 1, Color.TRANSPARENT);
		//mBeamBaseBitmapTextureAtlas.load();
		//mUFO_Beam_Base_List.add(mBeamBaseTextureRegion);
				
		// ------------------------ Load UFO Wind Landing Image ------------------------ 
		loadingText.setText("Loading Wind...");
		this.mWindLandingTexture = new BitmapTextureAtlas(this.getTextureManager(), 1500, 600, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
		this.mWindLandingTextureRegion = PixelPerfectTextureRegionFactory.createTiledFromAsset(this.mWindLandingTexture, this.mAssetManager, "gfx/Game/landing_wind.png", 0, 0, 5, 4, Color.TRANSPARENT);
		this.mWindLandingTexture.load();

		// ------------------------ Load gold texture of the game ------------------------ 
		loadingText.setText("Loading Gold...");
		this.mGoldTexture = new BitmapTextureAtlas(this.getTextureManager(), 300, 57, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
		this.mGoldTextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.mGoldTexture, pContext, "gfx/Game/gold_sprite.png", 0, 0, 3, 1);
		this.mGoldTexture.load();

		// ------------------------ Load blackhole texture of the game ------------------------ 
		loadingText.setText("Loading Blackhole...");
		this.mBlackHoleTexture = new BitmapTextureAtlas(this.getTextureManager(), 2000, 600, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
		this.mBlackHoleTextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.mBlackHoleTexture, pContext, "gfx/Game/blackhole.png", 0, 0, 8, 3);
		this.mBlackHoleTexture.load();

		// ------------------------ Load player emotions texture of the game ------------------------ 
		this.mEmotionsTexture = new BitmapTextureAtlas(this.getTextureManager(), 448, 96, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
		this.mEmotionsTextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.mEmotionsTexture, pContext, "gfx/Game/EmotionsSprite.png", 0, 0, 4, 1);
		this.mEmotionsTexture.load();		
		
		// ------------------------ Load player smoke texture of the game ------------------------ 
		loadingText.setText("Loading Smoke...");
		this.mSmokeTexture = new BitmapTextureAtlas(this.getTextureManager(), 300, 130, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
		this.mSmokeTextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.mSmokeTexture, pContext, "gfx/Game/smoke.png", 0, 0, 2, 1);
		this.mSmokeTexture.load();

		// ------------------------ Load player forcefield texture of the game ------------------------ 
		loadingText.setText("Loading Forcefield...");
		this.mForcefieldTexture = new BitmapTextureAtlas(this.getTextureManager(), 200, 150, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
		this.mForcefieldTextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.mForcefieldTexture, pContext, "gfx/Game/forcefield.png", 0, 0, 1, 1);
		this.mForcefieldTexture.load();
		
		// ------------------------ Load explosion of the game ------------------------ 
		this.mExplosionTexture = new BitmapTextureAtlas(this.getTextureManager(), 2000, 600, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
		this.mExplosionTextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.mExplosionTexture, pContext, "gfx/Game/explosion.png", 0, 0, 8, 3);
		this.mExplosionTexture.load();
		
		this.gameResources_PART1_Loaded = true;
	}
	
	public void loadGameResources_PART2() {
		// ------------------------ Load UFO Beam Image ------------------------ 
		loadingText.setText("Loading Beams...");
		BitmapTextureAtlas mBeamBitmapTextureAtlas = new BitmapTextureAtlas(this.getTextureManager(), 1800, 1400, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
		PixelPerfectTiledTextureRegion mBeamTextureRegion = PixelPerfectTextureRegionFactory.createTiledFromAsset(mBeamBitmapTextureAtlas, this.mAssetManager, "gfx/Game/UFOBeam.png", 0, 0, 9, 4, Color.TRANSPARENT);
		mBeamBitmapTextureAtlas.load();
		mUFO_Beam_List.add(mBeamTextureRegion);
		
		// ------------------------ Load SFX & Music ------------------------ 
		loadingText.setText("Loading Sounds...");
		try {
			this.mainBGM = MusicFactory.createMusicFromAsset(this.mEngine.getMusicManager(), pContext, "mfx/black_hole.mp3");
			this.mainBGM.setLooping(true);
			this.mainBGM.setVolume(0.65f);
			this.gameBGM = MusicFactory.createMusicFromAsset(this.mEngine.getMusicManager(), pContext, "mfx/gameBGM.mp3");
			this.gameBGM.setLooping(true);
			this.gameBGM.setVolume(0.4f);
			this.ufoHoverSFX = MusicFactory.createMusicFromAsset(this.mEngine.getMusicManager(), pContext, "mfx/ufo_hover.mp3");
			this.ufoHoverSFX.setLooping(true);
			this.ufoHoverSFX.setVolume(0.35f);
			this.ufoBeamSFX = MusicFactory.createMusicFromAsset(this.mEngine.getMusicManager(), pContext, "mfx/ufo_beam.mp3");
			this.ufoBeamSFX.setLooping(true);
			this.explosionSFX = MusicFactory.createMusicFromAsset(this.mEngine.getMusicManager(), pContext, "mfx/heavy_explosion.mp3");
			this.explosionSFX.setVolume(0.2f);
			this.countdownSFX = MusicFactory.createMusicFromAsset(this.mEngine.getMusicManager(), pContext, "mfx/countdown_timer.mp3");
			this.countdownSFX.setVolume(0.2f);
			this.buttonSFX = MusicFactory.createMusicFromAsset(this.mEngine.getMusicManager(), pContext, "mfx/button.mp3");
			this.purchaseSFX = MusicFactory.createMusicFromAsset(this.mEngine.getMusicManager(), pContext, "mfx/kaching.mp3");
			this.purchaseSFX.setVolume(0.35f);
			this.shieldHitSFX = MusicFactory.createMusicFromAsset(this.mEngine.getMusicManager(), pContext, "mfx/shield_hit.mp3");
			this.shieldHitSFX.setVolume(0.2f);
			this.laserBeamSFX = MusicFactory.createMusicFromAsset(this.mEngine.getMusicManager(), pContext, "mfx/laser_ray_cannon.mp3");
			this.laserBeamSFX.setVolume(0.1f);
			this.missileLaunchSFX = MusicFactory.createMusicFromAsset(this.mEngine.getMusicManager(), pContext, "mfx/missile_fly_by.mp3");
			this.missileLaunchSFX.setVolume(0.1f);
		} catch (final IOException e) {
			Log.e(TAG, e.getMessage());
		}

		this.mDisintegrateHumanTexture = new BitmapTextureAtlas(this.getTextureManager(), 475, 250, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
		this.mDisintegrateHumanTextureRegion = PixelPerfectTextureRegionFactory.createTiledFromAsset(this.mDisintegrateHumanTexture, this.mAssetManager, "gfx/Game/Humans/disintegrate.png", 0, 0, 5, 2, Color.TRANSPARENT);
		this.mDisintegrateHumanTexture.load();
		
		// ------------------------ Load Military Humans & its Weapons Images ------------------------ 
		loadingText.setText("Loading Military...");
		this.mFemaleMilitaryTexture = new BitmapTextureAtlas(this.getTextureManager(), 500, 550, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
		this.mFemaleMilitaryTextureRegion = PixelPerfectTextureRegionFactory.createTiledFromAsset(this.mFemaleMilitaryTexture, this.mAssetManager, "gfx/Game/Humans/Military_Female.png", 0, 0, 4, 4, Color.TRANSPARENT);
		this.mFemaleMilitaryTexture.load();
		
		this.mMaleMilitaryTexture = new BitmapTextureAtlas(this.getTextureManager(), 500, 550, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
		this.mMaleMilitaryTextureRegion = PixelPerfectTextureRegionFactory.createTiledFromAsset(this.mMaleMilitaryTexture, this.mAssetManager, "gfx/Game/Humans/Military_Male.png", 0, 0, 4, 4, Color.TRANSPARENT);
		this.mMaleMilitaryTexture.load();
		
		this.mMilitaryAlertTexture = new BitmapTextureAtlas(this.getTextureManager(), 374, 23, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
		this.mMilitaryAlertTextureRegion = PixelPerfectTextureRegionFactory.createTiledFromAsset(this.mMilitaryAlertTexture, this.mAssetManager, "gfx/Game/Aim_Timer.png", 0, 0, 4, 1, Color.TRANSPARENT);
		this.mMilitaryAlertTexture.load();
		
		this.mMilitaryMissileTexture = new BitmapTextureAtlas(this.getTextureManager(), 120, 30, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
		this.mMilitaryMissileTextureRegion = PixelPerfectTextureRegionFactory.createTiledFromAsset(this.mMilitaryMissileTexture, this.mAssetManager, "gfx/Game/Weapons/Military_Missile.png", 0, 0, 3, 1, Color.TRANSPARENT);
		this.mMilitaryMissileTexture.load();
		
		// ------------------------ Load Obstacles Images ------------------------ 
		this.mLampPostTexture = new BitmapTextureAtlas(this.getTextureManager(), 80, 110, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
		this.mLampPostTextureRegion = PixelPerfectTextureRegionFactory.createTiledFromAsset(this.mLampPostTexture, this.mAssetManager, "gfx/Game/Obstacles/LampPost.png", 0, 0, 2, 1, Color.TRANSPARENT);
		this.mLampPostTexture.load();
		
		this.mTrafficLightTexture = new BitmapTextureAtlas(this.getTextureManager(), 35, 128, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
		this.mTrafficLightTextureRegion = PixelPerfectTextureRegionFactory.createTiledFromAsset(this.mTrafficLightTexture, this.mAssetManager, "gfx/Game/Obstacles/TrafficLight.png", 0, 0, 1, 1, Color.TRANSPARENT);
		this.mTrafficLightTexture.load();
		
		this.mCarsTexture = new BitmapTextureAtlas(this.getTextureManager(), 1140, 141, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
		this.mCarsTextureRegion = PixelPerfectTextureRegionFactory.createTiledFromAsset(this.mCarsTexture, this.mAssetManager, "gfx/Game/Obstacles/Cars.png", 0, 0, 5, 1, Color.TRANSPARENT);
		this.mCarsTexture.load();
		
		// ------------------------ Load Weaponary Images & Pool ------------------------ 
		loadingText.setText("Loading Weapons...");
		this.mLaserBeamTexture = new BitmapTextureAtlas(this.getTextureManager(), 400, 1600, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
		this.mLaserBeamTextureRegion = PixelPerfectTextureRegionFactory.createTiledFromAsset(this.mLaserBeamTexture, this.mAssetManager, "gfx/Game/Weapons/laser_beam_tiled.png", 0, 0, 3, 2, Color.TRANSPARENT);
		this.mLaserBeamTexture.load();
		
		// Load missle alert texture of the game
		this.mMissileAlertTexture = new BitmapTextureAtlas(this.getTextureManager(), 232, 80, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
		this.mMissileAlertTextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.mMissileAlertTexture, pContext, "gfx/Game/Weapons/MissileAlert.png", 0, 0, 3, 1);
		this.mMissileAlertTexture.load();

		// Load missle texture of the game
		this.mMissileHeadTexture = new BitmapTextureAtlas(this.getTextureManager(), 64, 64, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
		this.mMissileHeadTextureRegion = PixelPerfectTextureRegionFactory.createTiledFromAsset(this.mMissileHeadTexture, this.mAssetManager, "gfx/Game/Weapons/missile_head.png", 0, 0, 1, 1, Color.TRANSPARENT);
		this.mMissileHeadTexture.load();
		
		this.mMissileTailTexture = new BitmapTextureAtlas(this.getTextureManager(), 256, 64, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
		this.mMissileTailTextureRegion = PixelPerfectTextureRegionFactory.createTiledFromAsset(this.mMissileTailTexture, this.mAssetManager, "gfx/Game/Weapons/missile_tail.png", 0, 0, 1, 1, Color.TRANSPARENT);
		this.mMissileTailTexture.load();

		// Load scatter missle alert texture of the game.
		this.mScatterMissileAlertTexture = new BitmapTextureAtlas(this.getTextureManager(), 232, 80, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
		this.mScatterMissileAlertTextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.mScatterMissileAlertTexture, pContext, "gfx/Game/Weapons/MiniMissileAlert.png", 0, 0, 3, 1);
		this.mScatterMissileAlertTexture.load();
		
		// Load scatter missle texture of the game
		this.mScatterMissileHeadTexture = new BitmapTextureAtlas(this.getTextureManager(), 100, 64, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
		this.mScatterMissileHeadTextureRegion = PixelPerfectTextureRegionFactory.createTiledFromAsset(this.mScatterMissileHeadTexture, this.mAssetManager, "gfx/Game/Weapons/scatter_missile_head.png", 0, 0, 1, 1, Color.TRANSPARENT);
		this.mScatterMissileHeadTexture.load();
		
		this.mScatterMissileTailTexture = new BitmapTextureAtlas(this.getTextureManager(), 256, 64, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
		this.mScatterMissileTailTextureRegion = PixelPerfectTextureRegionFactory.createTiledFromAsset(this.mScatterMissileTailTexture, this.mAssetManager, "gfx/Game/Weapons/missile_tail.png", 0, 0, 1, 1, Color.TRANSPARENT);
		this.mScatterMissileTailTexture.load();
		
		// Load laser alert texture of the game
		this.mLaserAlertTexture = new BitmapTextureAtlas(this.getTextureManager(), 282, 100, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
		this.mLaserAlertTextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.mLaserAlertTexture, pContext, "gfx/Game/Weapons/LaserBeamAlert.png", 0, 0, 3, 1);
		this.mLaserAlertTexture.load();

		// ------------------------ Load player angel & devil texture of the game ------------------------ 
		loadingText.setText("Loading decisions...");
		this.mPlayerBarCaseTexture = new BitmapTextureAtlas(this.getTextureManager(), 1179, 225, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
		this.mPlayerBarCaseTextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.mPlayerBarCaseTexture, pContext, "gfx/Game/bar_case.png", 0, 0, 2, 1);
		this.mPlayerBarCaseTexture.load();
		
		this.mPlayerBarTexture = new BitmapTextureAtlas(this.getTextureManager(), 1032, 135, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
		this.mPlayerBarTextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.mPlayerBarTexture, pContext, "gfx/Game/bar_fill.png", 0, 0, 2, 1);
		this.mPlayerBarTexture.load();
		
		this.gameResources_PART2_Loaded = true;
	}

	public void loadGameResources_PART3() {
		// ------------------------ Load Human Images ------------------------ 
		// Images for the humans
		loadingText.setText("Loading Humans...");
		for (int i=0; i < HUMAN_ASSETS_COUNT; i++) {
			BitmapTextureAtlas mHumanTextureAtlas = new BitmapTextureAtlas(this.getTextureManager(), 650, 650, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
			PixelPerfectTiledTextureRegion mHumanTextureRegion;
			mHumanTextureRegion = PixelPerfectTextureRegionFactory.createTiledFromAsset(mHumanTextureAtlas, this.mAssetManager, "gfx/Game/Humans/Human_" + (i+1) + ".png", 0, 0, 6, 5, Color.TRANSPARENT);
			mHumanTextureAtlas.load();
			mHuman_List.add((PixelPerfectTiledTextureRegion) mHumanTextureRegion);
		}
		
		// ------------------------ Load UFO Explosion Images ------------------------ 
		loadingText.setText("Loading Explosion Assets...");
		for (int i=0; i < SPACECRAFT_MODEL_ASSETS_COUNT; i++) {
			
			String modelNumber = "";
			if((i+1) < 9) {
				modelNumber = "0" + (i+1);
			} else {
				modelNumber = "" + (i+1);
			}
			
			BitmapTextureAtlas mUFOBitmapTextureAtlas = new BitmapTextureAtlas(this.getTextureManager(), 2275, 1560, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
			PixelPerfectTiledTextureRegion mUFOExplosionTextureRegion = PixelPerfectTextureRegionFactory.createTiledFromAsset(mUFOBitmapTextureAtlas, this.mAssetManager, "gfx/Game/Spacecrafts/Model_" + modelNumber + "_Explosion.png", 0, 0, 10, 4, Color.TRANSPARENT);
			mUFOBitmapTextureAtlas.load();
			
			mUFO_Model_Explosion_List.add(mUFOExplosionTextureRegion);
		}

		// ------------------------ Load Map Images ------------------------ 
		loadingText.setText("Loading Maps Assets...");
		this.mCommonParallaxBackgroundTexture = new BitmapTextureAtlas(this.getTextureManager(), 1395, 207, TextureOptions.DEFAULT);
		this.mCommonParallaxLayer1_1 = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mCommonParallaxBackgroundTexture, pContext, "gfx/Game/Maps/Common/Layer1_1.png", 0, 0);
		this.mCommonParallaxLayer1_2 = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mCommonParallaxBackgroundTexture, pContext, "gfx/Game/Maps/Common/Layer1_2.png", 0, 69);
		this.mCommonParallaxLayer1_3 = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mCommonParallaxBackgroundTexture, pContext, "gfx/Game/Maps/Common/Layer1_3.png", 0, 138);
		this.mCommonParallaxBackgroundTexture.load();
		
		this.mSingaporeParallaxBackgroundTexture = new BitmapTextureAtlas(this.getTextureManager(), 2368, 1360, TextureOptions.DEFAULT);
		this.mSingaporeParallaxLayer2_1 = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mSingaporeParallaxBackgroundTexture, pContext, "gfx/Game/Maps/Singapore/SG_Layer2_1.png", 0, 0);
		//this.mSingaporeParallaxLayer2_2 = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mSingaporeParallaxBackgroundTexture, pContext, "gfx/Game/Maps/Singapore/SG_Layer2_2.png", 0, 150);
		this.mSingaporeParallaxLayer3 = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mSingaporeParallaxBackgroundTexture, pContext, "gfx/Game/Maps/Singapore/SG_Layer3.png", 0, 265);
		this.mSingaporeParallaxLayer5 = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mSingaporeParallaxBackgroundTexture, pContext, "gfx/Game/Maps/Singapore/SG_Layer5.png", 0, 705);
		this.mSingaporeParallaxBackgroundTexture.load();
		
		this.mSydneyParallaxBackgroundTexture = new BitmapTextureAtlas(this.getTextureManager(), 1516, 1693, TextureOptions.DEFAULT);
		this.mSydneyParallaxLayer1 = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mSydneyParallaxBackgroundTexture, pContext, "gfx/Game/Maps/Sydney/Sydney_Layer1_1.png", 0, 0);
		this.mSydneyParallaxLayer2_1 = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mSydneyParallaxBackgroundTexture, pContext, "gfx/Game/Maps/Sydney/Sydney_Layer2_1.png", 0, 56);
		this.mSydneyParallaxLayer2_2 = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mSydneyParallaxBackgroundTexture, pContext, "gfx/Game/Maps/Sydney/Sydney_Layer2_2.png", 0, 106);
		this.mSydneyParallaxLayer2_3 = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mSydneyParallaxBackgroundTexture, pContext, "gfx/Game/Maps/Sydney/Sydney_Layer2_3.png", 0, 176);
		this.mSydneyParallaxLayer2_4 = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mSydneyParallaxBackgroundTexture, pContext, "gfx/Game/Maps/Sydney/Sydney_Layer2_4.png", 0, 236);
		this.mSydneyParallaxLayer3_1 = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mSydneyParallaxBackgroundTexture, pContext, "gfx/Game/Maps/Sydney/Sydney_Layer3_1.png", 0, 286);
		this.mSydneyParallaxLayer3_2 = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mSydneyParallaxBackgroundTexture, pContext, "gfx/Game/Maps/Sydney/Sydney_Layer3_2.png", 0, 368);
		this.mSydneyParallaxLayer4 = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mSydneyParallaxBackgroundTexture, pContext, "gfx/Game/Maps/Sydney/Sydney_Layer4.png", 0, 438);
		this.mSydneyParallaxLayer5A = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mSydneyParallaxBackgroundTexture, pContext, "gfx/Game/Maps/Sydney/Sydney_Layer5A.png", 0, 798);
		this.mSydneyParallaxLayer5B = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mSydneyParallaxBackgroundTexture, pContext, "gfx/Game/Maps/Sydney/Sydney_Layer5B.png", 0, 916);
		this.mSydneyParallaxLayer6 = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mSydneyParallaxBackgroundTexture, pContext, "gfx/Game/Maps/Sydney/Sydney_Layer6.png", 0, 1043);
		this.mSydneyParallaxBackgroundTexture.load();

		this.gameResources_PART3_Loaded = true;
	}
	
	public float getPositionAngle0() {
		return POSITION_ANGLE_0;
	}

	public float getPositionAngle45() {
		return POSITION_ANGLE_45;
	}
	
	public GameHelper getmGameHelper() {
		return mGameHelper;
	}
	
	public void setmGameHelper(GameHelper helper) {
		mGameHelper = helper;
	}
	
	public SaveGame getmSaveGame() {
		return mSaveGame;
	}

	public void setmSaveGame(SaveGame mSaveGame) {
		this.mSaveGame = mSaveGame;
	}

	public AchievementContainer getmAchievement() {
		return mAchievement;
	}

	public void setmAchievement(AchievementContainer achievement) {
		this.mAchievement = achievement;
	}

	public IabHelper getmIabHelper() {		
		mIabHelper = new IabHelper(mBaseContext, base64EncodedPublicKey);		
		// TODO: enable debug logging (for a production application, you should set this to false).
        mIabHelper.enableDebugLogging(true);
		return mIabHelper;
	}
	
	public String getGoogleAccountID() {
		return googleAccountID;
	}

	public void setGoogleAccountID(String googleAccountID) {
		this.googleAccountID = googleAccountID;
	}
	
	public String getGoogleAccountName() {
		return googleAccountName;
	}
	
	public void setGoogleAccountName(String googleAccountName) {
		this.googleAccountName = googleAccountName;
	}
	
	public String[] getCountryName() {
		return mCountryName;
	}

	public List<String> getAdditionalSkuList() {
		return additionalSkuList;
	}

	public void setAdditionalSkuList(List<String> additionalSkuList) {
		this.additionalSkuList = additionalSkuList;
	}

	public String getmPremiumNoAds() {
		return mPremiumNoAds;
	}

	public void setmPremiumNoAds(String mPremiumNoAds) {
		this.mPremiumNoAds = mPremiumNoAds;
	}
	
	public List<ProductDetails> getProductDetailsList() {
		return productDetailsList;
	}

	public void setProductDetailsList(List<ProductDetails> productDetailsList) {
		this.productDetailsList = productDetailsList;
	}
		
	public List<PixelPerfectTiledTextureRegion> getUFOModelList() {
		return mUFO_Model_List;
	}
	
	public List<PixelPerfectTiledTextureRegion> getUFOModelExplosionList() {
		return mUFO_Model_Explosion_List;
	}

	public List<TiledTextureRegion> getGadgetList() {
		return mGadget_List;
	}

	public List<TiledTextureRegion> getNPCList() {
		return mNPC_List;
	}
	
	public ITextureRegion getmSplashTextureRegion() {
		return mSplashTextureRegion;
	}

	public void setmSplashTextureRegion(ITextureRegion mSplashTextureRegion) {
		this.mSplashTextureRegion = mSplashTextureRegion;
	}

	public ITextureRegion getmLoadingTextureRegion() {
		return mLoadingTextureRegion;
	}

	public void setmLoadingTextureRegion(ITextureRegion mLoadingTextureRegion) {
		this.mLoadingTextureRegion = mLoadingTextureRegion;
	}

	public TextureRegion getmTip1TextureRegion() {
		return mTip1TextureRegion;
	}

	public void setmTip1TextureRegion(TextureRegion mTip1TextureRegion) {
		this.mTip1TextureRegion = mTip1TextureRegion;
	}

	public TextureRegion getmTip2TextureRegion() {
		return mTip2TextureRegion;
	}

	public void setmTip2TextureRegion(TextureRegion mTip2TextureRegion) {
		this.mTip2TextureRegion = mTip2TextureRegion;
	}

	public TextureRegion getmTip3TextureRegion() {
		return mTip3TextureRegion;
	}

	public void setmTip3TextureRegion(TextureRegion mTip3TextureRegion) {
		this.mTip3TextureRegion = mTip3TextureRegion;
	}

	public TiledTextureRegion getmGoogleButtonTextureRegion() {
		return mGoogleButtonTextureRegion;
	}
	
	public TiledTextureRegion getmFacebookButtonTextureRegion() {
		return mFacebookButtonTextureRegion;
	}

	public TiledTextureRegion getmTwitterButtonTextureRegion() {
		return mTwitterButtonTextureRegion;
	}

	public void setmGoogleButtonCurrentTileIndex(boolean status) {
		if(status) 
			mGoogleButtonTextureRegion.setCurrentTileIndex(0);
		else 
			mGoogleButtonTextureRegion.setCurrentTileIndex(1);
	}
	
	public TiledTextureRegion getmGooglePlayIconTextureRegion() {
		return mGooglePlayIconTextureRegion;
	}
	
	public ITextureRegion getmMenuBackgroundTextureRegion() {
		return mMenuBackgroundTextureRegion;
	}

	public ITextureRegion getmTitleTextureRegion() {
		return mTitleTextureRegion;
	}

	public ITextureRegion getmPlayButtonTextureRegion() {
		return mPlayButtonTextureRegion;
	}

	public ITiledTextureRegion getmMultiplayerButtonsTextureRegion() {
		return mMultiplayerButtonsTextureRegion;
	}

	public ITextureRegion getmConnectorTextureRegion() {
		return mConnectorTextureRegion;
	}

	public ITextureRegion getmCloseButtonTextureRegion() {
		return mCloseButtonTextureRegion;
	}

	public ITextureRegion getmHighScoreButtonTextureRegion() {
		return mHighScoreButtonTextureRegion;
	}

	public ITextureRegion getmWashoutBackgroundTextureRegion() {
		return mWashoutBackgroundTextureRegion;
	}

	public TiledTextureRegion getmCommonBackButtonTextureRegion() {
		return mCommonBackButtonTextureRegion;
	}

	public ITextureRegion getmComingSoonIconTextureRegion() {
		return mComingSoonIconTextureRegion;
	}
	
	public ITextureRegion getmSettingsButtonTextureRegion() {
		return mSettingsButtonTextureRegion;
	}

	public TiledTextureRegion getmSFXTextureRegion() {
		return mSFXTextureRegion;
	}

	public TiledTextureRegion getmMusicTextureRegion() {
		return mMusicTextureRegion;
	}

	public TiledTextureRegion getmMotionControlTextureRegion() {
		return mMotionControlTextureRegion;
	}
	
	public ITextureRegion getmHangerButtonTextureRegion() {
		return mHangerButtonTextureRegion;
	}

	public ITextureRegion getmHangerBackgroundTextureRegion() {
		return mHangerBackgroundTextureRegion;
	}

	public ITextureRegion getmHangerSubBackgroundTextureRegion() {
		return mHangerSubBackgroundTextureRegion;
	}

	public ITextureRegion getmHangerDisplayPanelTextureRegion() {
		return mHangerDisplayPanelTextureRegion;
	}

	public ITextureRegion getmMissionDisplayTextureRegion() {
		return mMissionDisplayTextureRegion;
	}
	
	public ITextureRegion getmMissionSpotTextureRegion() {
		return mMissionSpotTextureRegion;
	}
	
	public ITiledTextureRegion getmUFOWindowDisplayTextureRegion() {
		return mUFOWindowDisplayTextureRegion;
	}
	
	public ITiledTextureRegion getmGadgetWindowDisplayTextureRegion() {
		return mGadgetWindowDisplayTextureRegion;
	}

	public TiledTextureRegion getmUFOIconTextureRegion() {
		return mUFOIconTextureRegion;
	}

	public TiledTextureRegion getmGadgetIconTextureRegion() {
		return mGadgetIconTextureRegion;
	}

	public TiledTextureRegion getmProfileIconTextureRegion() {
		return mProfileIconTextureRegion;
	}

	public ITiledTextureRegion getmProfileSceneBackgroundTextureRegion() {
		return mProfileSceneBackgroundTextureRegion;
	}
	
	public ITiledTextureRegion getmProfileSubSceneBackgroundTextureRegion() {
		return mProfileSubSceneBackgroundTextureRegion;
	}
	
	public ITiledTextureRegion getmProfileSub2SceneBackgroundTextureRegion() {
		return mProfileSub2SceneBackgroundTextureRegion;
	}
	
	public TextureRegion getmAchievementSubheaderTextureRegion() {
		return mAchievementSubheaderTextureRegion;
	}
	
	public TiledTextureRegion getmAchievementTrophiesTextureRegion() {
		return mAchievementTrophiesTextureRegion;
	}
	
	public TextureRegion getmMissionSubheaderTextureRegion() {
		return mMissionSubheaderTextureRegion;
	}
	
	public TiledTextureRegion getmProfileSceneTextTextureRegion() {
		return mProfileSceneTextTextureRegion;
	}
	
	public TiledTextureRegion getmAchievementIconTextureRegion() {
		return mAchievementIconTextureRegion;
	}

	public ITiledTextureRegion getmCreditsIconTextureRegion() {
		return mCreditsIconTextureRegion;
	}

	public TextureRegion getmCreditsScreenTextureRegion() {
		return mCreditsScreenTextureRegion;
	}

	public TiledTextureRegion getmHangerShopIconTextureRegion() {
		return mHangerShopIconTextureRegion;
	}

	public ITextureRegion getmUFOBoxTextureRegion() {
		return mUFOBoxTextureRegion;
	}
	
	public ITextureRegion getmGadgetCounterBoxTextureRegion() {
		return mGadgetCounterBoxTextureRegion;
	}
	
	public TiledTextureRegion getmEquipIconTextureRegion() {
		return mEquipIconTextureRegion;
	}

	public TiledTextureRegion getmHangerBackIconTextureRegion() {
		return mHangerBackIconTextureRegion;
	}
	
	public ITextureRegion getmYouHaveIconTextureRegion() {
		return mYouHaveIconTextureRegion;
	}
	
	public ITextureRegion getmGetMoreGoldIconTextureRegion() {
		return mGetMoreGoldIconTextureRegion;
	}

	public void setmGetMoreGoldIconTextureRegion(ITextureRegion mGetMoreGoldIconTextureRegion) {
		this.mGetMoreGoldIconTextureRegion = mGetMoreGoldIconTextureRegion;
	}

	public TiledTextureRegion getmPurchaseIconTextureRegion() {
		return mPurchaseIconTextureRegion;
	}
	
	public TiledTextureRegion getmPurchaseItemsTextureRegion() {
		return mPurchaseItemsTextureRegion;
	}

	public Font getmFont() {
		return mFont;
	}

	public Text getLoadingText() {
		return loadingText;
	}

	public void setLoadingText(Text loadingText) {
		this.loadingText = loadingText;
	}

	public ITextureRegion getmAccelerometer0TextureRegion() {
		return mAccelerometer0TextureRegion;
	}

	public ITextureRegion getmAccelerometer45TextureRegion() {
		return mAccelerometer45TextureRegion;
	}

	public ITextureRegion getmAccelerometerCustomTextureRegion() {
		return mAccelerometerCustomTextureRegion;
	}

	public ITextureRegion getmCalibrationCustomScreenTextureRegion() {
		return mCalibrationCustomScreenTextureRegion;
	}

	public ITextureRegion getmCalibrationCustomHeaderTextureRegion() {
		return mCalibrationCustomHeaderTextureRegion;
	}

	public ITextureRegion getmCalibrationCustomBallTextureRegion() {
		return mCalibrationCustomBallTextureRegion;
	}

	public PixelPerfectTextureRegion getmCalibrationCustomRingTextureRegion() {
		return mCalibrationCustomRingTextureRegion;
	}

	public ITextureRegion getmCalibrateButtonTextureRegion() {
		return mCalibrateButtonTextureRegion;
	}

	public Music getGameBGM() {
		return gameBGM;
	}

	public Music getMainBGM() {
		return mainBGM;
	}

	public Music getPurchaseSFX() {
		return purchaseSFX;
	}

	public Music getShieldHitSFX() {
		return shieldHitSFX;
	}

	public Music getButtonSFX() {
		return buttonSFX;
	}	

	public Music getLaserBeamSFX() {
		return laserBeamSFX;
	}

	public Music getUfoHoverSFX() {
		return ufoHoverSFX;
	}

	public Music getUfoBeamSFX() {
		return ufoBeamSFX;
	}

	public Music getExplosionSFX() {
		return explosionSFX;
	}

	public Music getCountdownSFX() {
		return countdownSFX;
	}

	public Music getMissileLaunchSFX() {
		return missileLaunchSFX;
	}
	
	public ITextureRegion getmOnScreenControlBaseTextureRegion() {
		return mOnScreenControlBaseTextureRegion;
	}

	public ITextureRegion getmOnScreenControlKnobTextureRegion() {
		return mOnScreenControlKnobTextureRegion;
	}

	public ITextureRegion getmOnScreenControlAngelButtonTextureRegion() {
		return mOnScreenControlAngelButtonTextureRegion;
	}

	public ITextureRegion getmOnScreenControlDevilButtonTextureRegion() {
		return mOnScreenControlDevilButtonTextureRegion;
	}

	public TiledTextureRegion getmBlackHoleTextureRegion() {
		return mBlackHoleTextureRegion;
	}
	
	public TiledTextureRegion getmEmotionsTextureRegion() {
		return mEmotionsTextureRegion;
	}
	
	public TiledTextureRegion getmSmokeTextureRegion() {
		return mSmokeTextureRegion;
	}
	
	public ITextureRegion getmForcefieldTextureRegion() {
		return mForcefieldTextureRegion;
	}
	
	public ITextureRegion getmPlayerBarCaseTextureRegion() {
		return mPlayerBarCaseTextureRegion;
	}

	public ITextureRegion getmPlayerBarTextureRegion() {
		return mPlayerBarTextureRegion;
	}

	public TiledTextureRegion getmExplosionTextureRegion() {
		return mExplosionTextureRegion;
	}

	public TiledTextureRegion getmMissileAlertTextureRegion() {
		return mMissileAlertTextureRegion;
	}
	
	public PixelPerfectTiledTextureRegion getmMissileHeadTextureRegion() {
		return mMissileHeadTextureRegion;
	}

	public PixelPerfectTiledTextureRegion getmMissileTailTextureRegion() {
		return mMissileTailTextureRegion;
	}
	
	public PixelPerfectTiledTextureRegion getmMilitaryMissileTextureRegion() {
		return mMilitaryMissileTextureRegion;
	}
	
	public TiledTextureRegion getmScatterMissileAlertTextureRegion() {
		return mScatterMissileAlertTextureRegion;
	}

	public TiledTextureRegion getmLaserAlertTextureRegion() {
		return mLaserAlertTextureRegion;
	}
	
	public PixelPerfectTiledTextureRegion getmScatterMissileHeadTextureRegion() {
		return mScatterMissileHeadTextureRegion;
	}

	public PixelPerfectTiledTextureRegion getmScatterMissileTailTextureRegion() {
		return mScatterMissileTailTextureRegion;
	}

	public PixelPerfectTiledTextureRegion getmLaserBeamTextureRegion() {
		return mLaserBeamTextureRegion;
	}

	public PixelPerfectTiledTextureRegion getmBeamTextureRegion() {
		/*Random r = new Random();
		int value = r.nextInt(4);
		switch(value) {
		case 0: return mBlueBeamTextureRegion;
		case 1: return mGreenBeamTextureRegion;
		case 2: return mRedBeamTextureRegion;
		case 3: return mYellowBeamTextureRegion;
		case 4: return mOrangeBeamTextureRegion;
		default: return mOrangeBeamTextureRegion;
		}
		*/
		return mUFO_Beam_List.get(0);
	}
	
	//public PixelPerfectTiledTextureRegion getmEvilBeamTextureRegion() {
	//	return UFO_Beam_List.get(1);
	//}

	public PixelPerfectTiledTextureRegion getmWindLandingTextureRegion() {
		return mWindLandingTextureRegion;
	}
	
	public TiledTextureRegion getmGoldTextureRegion() {
		return mGoldTextureRegion;
	}

	public TiledTextureRegion getmGameOverTextureRegion() {
		return mGameOverTextureRegion;
	}

	public TiledTextureRegion getmNewHighScoreTextTextureRegion() {
		return mNewHighScoreTextTextureRegion;
	}

	public TextureRegion getmFacebookShareButtonTextureRegion() {
		return mFacebookShareButtonTextureRegion;
	}

	public TiledTextureRegion getmPauseButtonTextureRegion() {
		return mPauseButtonTextureRegion;
	}
	
	public TiledTextureRegion getmGameplayPauseMenuButtonTextureRegion() {
		return mGameplayPauseMenuButtonTextureRegion;
	}

	public int[] getSpacecraft_cost() {
		int SPACECRAFTS_COST[] = new int[SPACECRAFT_MODEL_ASSETS_COUNT];
		for(int i=0; i<SPACECRAFT_MODEL_ASSETS_COUNT; i++) {
			String spacecraftCost = "";
			
			if(i == 0) spacecraftCost = pContext.getString(R.string.ufo_01_cost);
			else if(i == 1) spacecraftCost = pContext.getString(R.string.ufo_02_cost);
			else if(i == 2) spacecraftCost = pContext.getString(R.string.ufo_03_cost);
			else if(i == 3) spacecraftCost = pContext.getString(R.string.ufo_04_cost);
			else if(i == 4) spacecraftCost = pContext.getString(R.string.ufo_05_cost);
			else if(i == 5) spacecraftCost = pContext.getString(R.string.ufo_06_cost);
			else if(i == 6) spacecraftCost = pContext.getString(R.string.ufo_07_cost);
			else if(i == 7) spacecraftCost = pContext.getString(R.string.ufo_08_cost);
			else if(i == 8) spacecraftCost = pContext.getString(R.string.ufo_09_cost);
			else if(i == 9) spacecraftCost = pContext.getString(R.string.ufo_10_cost);
			
			SPACECRAFTS_COST[i] = Integer.parseInt(spacecraftCost);		
		}
		return SPACECRAFTS_COST;
	}

	public int[] getGadget_cost() {
		int GADGETS_COST[] = new int[GADGETS_ASSETS_COUNT];
		for(int i=0; i<GADGETS_ASSETS_COUNT; i++) {
			String gadgetCost = "";
			
			if(i == 0) gadgetCost = pContext.getString(R.string.gadget_01_cost);
			else if(i == 1) gadgetCost = pContext.getString(R.string.gadget_02_cost);
			else if(i == 2) gadgetCost = pContext.getString(R.string.gadget_03_cost);
			else if(i == 3) gadgetCost = pContext.getString(R.string.gadget_04_cost);
			else if(i == 4) gadgetCost = pContext.getString(R.string.gadget_05_cost);
			else if(i == 5) gadgetCost = pContext.getString(R.string.gadget_06_cost);
			else if(i == 6) gadgetCost = pContext.getString(R.string.gadget_07_cost);
			else if(i == 7) gadgetCost = pContext.getString(R.string.gadget_08_cost);
			else if(i == 8) gadgetCost = pContext.getString(R.string.gadget_09_cost);
			else if(i == 9) gadgetCost = pContext.getString(R.string.gadget_10_cost);
			
			GADGETS_COST[i] = Integer.parseInt(gadgetCost);		
		}
		return GADGETS_COST;
	}
	
	public ITextureRegion getmCommonParallaxLayer1_1() {
		return mCommonParallaxLayer1_1;
	}

	public ITextureRegion getmCommonParallaxLayer1_2() {
		return mCommonParallaxLayer1_2;
	}

	public ITextureRegion getmCommonParallaxLayer1_3() {
		return mCommonParallaxLayer1_3;
	}

	public ITextureRegion getmSingaporeParallaxLayer5() {
		return mSingaporeParallaxLayer5;
	}
	
	public ITextureRegion getmSingaporeParallaxLayer3() {
		return mSingaporeParallaxLayer3;
	}

	public ITextureRegion getmSingaporeParallaxLayer2_1() {
		return mSingaporeParallaxLayer2_1;
	}

	//public ITextureRegion getmSingaporeParallaxLayer2_2() {
	//	return mSingaporeParallaxLayer2_2;
	//}

	//public ITextureRegion getmSingaporeParallaxLayer2_3() {
	//	return mSingaporeParallaxLayer2_3;
	//}

	public ITextureRegion getmSydneyParallaxLayer5A() {
		return mSydneyParallaxLayer5A;
	}
	
	public ITextureRegion getmSydneyParallaxLayer5B() {
		return mSydneyParallaxLayer5B;
	}
	
	public ITextureRegion getmSydneyParallaxLayer6() {
		return mSydneyParallaxLayer6;
	}

	public ITextureRegion getmSydneyParallaxLayer4() {
		return mSydneyParallaxLayer4;
	}

	public ITextureRegion getmSydneyParallaxLayer3_1() {
		return mSydneyParallaxLayer3_1;
	}

	public ITextureRegion getmSydneyParallaxLayer3_2() {
		return mSydneyParallaxLayer3_2;
	}

	public ITextureRegion getmSydneyParallaxLayer2_1() {
		return mSydneyParallaxLayer2_1;
	}

	public ITextureRegion getmSydneyParallaxLayer2_2() {
		return mSydneyParallaxLayer2_2;
	}

	public ITextureRegion getmSydneyParallaxLayer2_3() {
		return mSydneyParallaxLayer2_3;
	}

	public ITextureRegion getmSydneyParallaxLayer2_4() {
		return mSydneyParallaxLayer2_4;
	}

	public ITextureRegion getmSydneyParallaxLayer1() {
		return mSydneyParallaxLayer1;
	}

	public PixelPerfectTiledTextureRegion getmLampPostTextureRegion() {
		return mLampPostTextureRegion;
	}

	public PixelPerfectTiledTextureRegion getmTrafficLightTextureRegion() {
		return mTrafficLightTextureRegion;
	}
	
	public PixelPerfectTiledTextureRegion getmCarsTextureRegion() {
		return mCarsTextureRegion;
	}
	
	public PixelPerfectTiledTextureRegion createMilitary() {
		if(new Random().nextBoolean()) 
			return mMaleMilitaryTextureRegion;
		else
			return mFemaleMilitaryTextureRegion;
	}
	
	public TiledTextureRegion getmMilitaryAlertTextureRegion() {
		return mMilitaryAlertTextureRegion;
	}
	
	public PixelPerfectTiledTextureRegion createHumans() {
		PixelPerfectTiledTextureRegion humanTextureRegion = null;
		humanTextureRegion = mHuman_List.get(MathUtils.random(0, HUMAN_ASSETS_COUNT-1));
		return humanTextureRegion;
	}
	
	public PixelPerfectTiledTextureRegion createFemaleHumans() {
		PixelPerfectTiledTextureRegion humanTextureRegion = null;
		humanTextureRegion = mHuman_List.get(MathUtils.random(0, HUMAN_ASSETS_COUNT/2-1));
		return humanTextureRegion;
	}
	
	public PixelPerfectTiledTextureRegion createMaleHumans() {
		PixelPerfectTiledTextureRegion humanTextureRegion = null;
		humanTextureRegion = mHuman_List.get(MathUtils.random(HUMAN_ASSETS_COUNT/2, HUMAN_ASSETS_COUNT-1));
		return humanTextureRegion;
	}
	
	
	public PixelPerfectTiledTextureRegion getDisintegratedHumans() {
		return mDisintegrateHumanTextureRegion;
	}

	public TextureRegion getmGameStyleHelpTextureRegion() {
		return mGameStyleHelpTextureRegion;
	}

	public void setmGameStyleHelpTextureRegion(TextureRegion mGameStyleHelpTextureRegion) {
		this.mGameStyleHelpTextureRegion = mGameStyleHelpTextureRegion;
	}

	public TextureRegion getmHelpButtonTextureRegion() {
		return mHelpButtonTextureRegion;
	}

	public void setmHelpButtonTextureRegion(TextureRegion mHelpButtonTextureRegion) {
		this.mHelpButtonTextureRegion = mHelpButtonTextureRegion;
	}

	public TextureRegion getmNextArrowButtonTextureRegion() {
		return mNextArrowButtonTextureRegion;
	}

	public void setmNextArrowButtonTextureRegion(
			TextureRegion mNextArrowButtonTextureRegion) {
		this.mNextArrowButtonTextureRegion = mNextArrowButtonTextureRegion;
	}
}
