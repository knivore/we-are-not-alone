package com.andengine.invaders;

import java.util.Arrays;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

import com.facebook.FacebookException;
import com.facebook.FacebookOperationCanceledException;
import com.facebook.HttpMethod;
import com.facebook.Request;
import com.facebook.Request.Callback;
import com.facebook.RequestAsyncTask;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.Session.Builder;
import com.facebook.Session.OpenRequest;
import com.facebook.Session.StatusCallback;
import com.facebook.SessionLoginBehavior;
import com.facebook.SessionState;
import com.facebook.model.GraphUser;
import com.facebook.widget.WebDialog;
import com.facebook.widget.WebDialog.OnCompleteListener;

public final class FacebookManager {
	private static final List<String> PERMISSIONS = Arrays.asList("publish_actions");
	private static String mFirstName, mUserName;
	private static boolean mUserLoggedIn;
	private static MainActivity mMainActivity;

	//Constructor
	public FacebookManager(MainActivity mainActivity) {
		mMainActivity = mainActivity;    		
	}

	//Getter & Setters
	public String getmFirstName() {
		return mFirstName;
	}

	public void setmFirstName(String mFirstName) {
		FacebookManager.mFirstName = mFirstName;
	}

	public String getmUserName() {
		return mUserName;
	}

	public void setmUserName(String mUserName) {
		FacebookManager.mUserName = mUserName;
	}

	public boolean ismUserLoggedIn() {
		return mUserLoggedIn;
	}

	public void setmUserLoggedIn(boolean mUserLoggedIn) {
		FacebookManager.mUserLoggedIn = mUserLoggedIn;
	}
	
	
	private static Session openActiveSession(final Activity pActivity, final boolean pAllowLoginUI, final StatusCallback pCallback, final List<String> pPermissions) {
		final OpenRequest openRequest = new OpenRequest(pActivity).setPermissions(pPermissions).setCallback(pCallback);
		openRequest.setLoginBehavior(SessionLoginBehavior.SUPPRESS_SSO);
		final Session session = new Builder(pActivity.getBaseContext()).build();
		if (SessionState.CREATED_TOKEN_LOADED.equals(session.getState()) || pAllowLoginUI) {
			Session.setActiveSession(session);
			session.openForPublish(openRequest);
			return session;
		}
		return null;
	}
		
	public void facebookLogin() {
		mMainActivity.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				openActiveSession(mMainActivity, false, new Session.StatusCallback() {
					@Override
					public void call(Session session, SessionState state, Exception exception) {
						if (session.isOpened()) {
							// make request to the /me API
					        Request.newMeRequest(session, new Request.GraphUserCallback() {
					        	// callback after Graph API response with user object
								@Override
								public void onCompleted(GraphUser user,	Response response) {
									if (user != null) {
										mUserName = user.getUsername();
										mFirstName = user.getFirstName();
				                    }
								}
					        }).executeAsync();
						}
					}
				}, PERMISSIONS);
			}
		});
	}

	public static void checkFacebookLoggedIn() {
		mMainActivity.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				openActiveSession(mMainActivity, false, new Session.StatusCallback() {
					@Override
					public void call(Session session, SessionState state, Exception exception) {
						if (session.isOpened()) {
							Request.newMeRequest(session, new Request.GraphUserCallback() {
								@Override
								public void onCompleted(GraphUser user, Response response) {
									if (user != null) {
										mUserName = user.getUsername();
										mFirstName = user.getFirstName();
										FacebookManager.mUserLoggedIn = true;
									} else {
										FacebookManager.mUserLoggedIn = false;
									}
								}
							}).executeAsync();
						}
					}
				}, PERMISSIONS);
			}
		});
	}

	private static void loginAndPost(final String pData) {
		mMainActivity.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				openActiveSession(mMainActivity, true, new Session.StatusCallback() {
					@Override
					public void call(Session session, SessionState state, Exception exception) {
						if (session.isOpened()) {
							Request.newMeRequest(session, new Request.GraphUserCallback() {
								@Override
								public void onCompleted(GraphUser user, Response response) {
									if (user != null) {
										mUserName = user.getUsername();
										mFirstName = user.getFirstName();
										final Session.OpenRequest openRequest;
										openRequest = new Session.OpenRequest(mMainActivity);
										openRequest.setPermissions(PERMISSIONS);
										mUserLoggedIn = true;
										post(user.getFirstName(), pData);
									} else {
										mUserLoggedIn = false;
									}
								}
							}).executeAsync();
						}
					}
				}, PERMISSIONS);
			}
		});
	}

	private static void post(final String pFirstName, final String pData) {
		mMainActivity.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				Bundle params = new Bundle();
				params.putString("name", pFirstName + pData);
				params.putString("caption", "We Are Not Alone");
				params.putString("description", "Click on Get button to get it!");
				params.putString("link", "https://play.google.com/store/apps/details?id=yourPackageName");
				params.putString("picture", "https://lh3.ggpht.com/gA1vmKasHsxk47L_6InKoakH5nvjSpePosEkVuuLL37-b_o20aqTbaKxLhDEn7tWO6yZ=h150-rw");
				
				JSONObject actions = new JSONObject();
				try {
					actions.put("name", "Get");
					actions.put("link", "https://play.google.com/store/apps/details?id=yourPackageName");
				} catch (Exception e) {};
				
				params.putString("actions", actions.toString());
				Request.Callback callback = new Request.Callback() {
					@Override
					public void onCompleted(Response response) {
						try {
							JSONObject graphResponse = response.getGraphObject().getInnerJSONObject();
							@SuppressWarnings("unused")
							String postID = null;
							try {
								postID = graphResponse.getString("id");
							} catch (JSONException e) {}
						} catch (NullPointerException e) {
						}
					}
				};
				Request request = new Request(Session.getActiveSession(), "me/feed", params, HttpMethod.POST, callback);
				RequestAsyncTask task = new RequestAsyncTask(request);
				task.execute();
			}
		});
	}

	private static void post2(final String pFirstName, final String pData) {
		mMainActivity.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				Bundle params = new Bundle();
				params.putString("name", pFirstName + pData);
				params.putString("caption", "We Are Not Alone");
				params.putString("link", "https://play.google.com/store/apps/details?id=com.andengine.invaders");
				params.putString("description", "Decide humanity fate now!");
				params.putString("picture", "https://lh3.ggpht.com/gA1vmKasHsxk47L_6InKoakH5nvjSpePosEkVuuLL37-b_o20aqTbaKxLhDEn7tWO6yZ=w300-rw");
		
				final WebDialog feedDialog = (new WebDialog.FeedDialogBuilder(mMainActivity, Session.getActiveSession(), params)).setOnCompleteListener(new OnCompleteListener() {
					@Override
					public void onComplete(Bundle values, FacebookException error) {
						if (error == null) {
							final String postId = values.getString("post_id");
							if (postId != null) {
								// POSTED
								
								
							} else {
								// POST CANCELLED
							}
						} else if (error instanceof FacebookOperationCanceledException) {
							// POST CANCELLED
						} else {
							// ERROR POSTING
						}
					}
				}).build();
				feedDialog.show();
			}
		});
	}
	
	public static void checkLikeStatus() {
		mMainActivity.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				final Session session = Session.getActiveSession();
				if (session != null) {
					Request.Callback callback = new Request.Callback() {
		
						@Override
						public void onCompleted(Response response) {
							if (response.getGraphObject() != null) {
								try {
									if (!response.getGraphObject().getInnerJSONObject().getString("data").equalsIgnoreCase("[]")) {
										// Give credit, disable ads or whatever here                                                           
									}
								} catch (JSONException e) {
									Log.d("JSONException", e.getMessage());
								}
							}
						}
					};
					Request request = new Request(session, "me/likes/" + "YOURPAGEID", null, HttpMethod.GET, callback);
					RequestAsyncTask task = new RequestAsyncTask(request);
					task.execute();
				}
			}
		});
	}

	public void facebookLogout() {
		mMainActivity.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				Session activeSession = Session.getActiveSession();
				if (activeSession != null) {
					activeSession.closeAndClearTokenInformation();
				}
			}
		});
	}

	//Adding Open Graph features, Set up actions and objects & post on the timeline
	public void postOnTFacebookTimeline() {
		mMainActivity.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				Bundle params = new Bundle();
				params.putString("your object", "your object url");
		
				Request request = new Request(Session.getActiveSession(), "me/<your app namespace>:<your action>", params, HttpMethod.POST, new Callback() {
					@Override
					public void onCompleted(Response response) {
						if (response.getError() == null) {
							// post successful
						} else {
							// Error occured
						}
					}
				});
				request.executeAndWait();
			}
		});
	}
	
	public void facebookRequestDialog() {
		mMainActivity.runOnUiThread(new Runnable() {
			@Override
			public void run() {
		        Bundle params = new Bundle();
		        params.putString("message", mUserName + " invited you to check out this game!");
		 
				WebDialog requestsDialog = (new WebDialog.RequestsDialogBuilder(mMainActivity, Session.getActiveSession(), params)).setOnCompleteListener(new OnCompleteListener() {
					@Override
					public void onComplete(Bundle values, FacebookException error) {
						if (error != null) {
							if (error instanceof FacebookOperationCanceledException) {     
								// Request cancelled
							} else {
								// Network Error
							}
				        } else {
				        	final String requestId = values.getString("request");
				        	if (requestId != null) {                       
				        		// Request sent
				        	} else {               
				        		// Request cancelled
				        	}
				        }
					}
				}).build();
				requestsDialog.show();
			}
		});
	}
	
	public void postScore(final String pScore) {
		if (mUserLoggedIn) {
			post(mFirstName, " has achieved " + pScore + " points! Try beating that!");
		} else {
			loginAndPost(" has achieved " + pScore + " points! Try beating that!");
		}
	}

	public void postAchievementCompletion(final String achievement) {
		if (mUserLoggedIn) {
			post(mFirstName, " has unlock new achievement : " + achievement);
		} else {
			loginAndPost(" has unlock new achievement : " + achievement);
		}
	}
}