/*
 * Copyright (C) 2013 Knivore Studios
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.andengine.invaders;

import org.andengine.engine.Engine;
import org.andengine.engine.camera.Camera;
import org.andengine.engine.handler.IUpdateHandler;
import org.andengine.engine.handler.timer.ITimerCallback;
import org.andengine.engine.handler.timer.TimerHandler;
import org.andengine.entity.modifier.MoveModifier;
import org.andengine.entity.modifier.MoveXModifier;
import org.andengine.entity.modifier.MoveYModifier;
import org.andengine.entity.scene.CameraScene;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.sprite.AnimatedSprite;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.sprite.TiledSprite;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

import com.andengine.invaders.MainActivity.TrackerName;
import com.andengine.sharedpreferences.Options_SharedPreference;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.games.Games;
import com.makersf.andengine.extension.collisions.entity.sprite.PixelPerfectSprite;

/**
 * @author Keh Chin Leong
 */
public class MainMenuScene implements SensorEventListener {

	//===========================================================
	// Constants
	//===========================================================
	private static final int CAMERA_WIDTH = 1280;
	private static final int CAMERA_HEIGHT = 720;

	private Engine mEngine;
	
	private Tracker myTracker;
	private MainActivity mMainActivity;

	private boolean mSettingsPressed = false;
	private boolean mSettingsOpened = false;
	private boolean mHangerPressed = false;
	private boolean mStartGamePressed = false;
	private boolean mGooglePressed = false;
	private boolean mFacebookPressed = false;
	private boolean mTwitterPressed = false;
	private boolean mSFXPressed = false;
	private boolean mMusicPressed = false;
	private boolean mMotionPressed = false;
	private boolean mAngle0Pressed = false;
	private boolean mAngle45Pressed = false;
	private boolean mAngleCustomPressed = false;
	private boolean mCalibrateButtonPressed = false;
	private boolean mSoloButtonPressed = false;
	private boolean mQuickMatchButtonPressed = false;
	private boolean mChallengeButtonPressed = false;
	private boolean mInvitationButtonPressed = false;
	private boolean mHelpButtonPressed = false;
	private boolean mCloseButtonPressed = false;
	
	private boolean mSoloPlay = false;
	private boolean mMultiPlay = false;
	
	private SceneManager mSceneManager;
	private Scene mMenuScene, mAccelerometerDetailsScene, mGameplayScene, mCustomCalibrationScene, mAccelerometerWHelpDetailsScene;

	private Camera mCamera;
	private GameResources mGameResources;
	private VertexBufferObjectManager mVertexBufferObjectManager;

	boolean mExplicitSignOut = false;
	boolean mInSignInFlow = false; // set to true when you're in the middle of the sign in flow, to know you should not attempt to connect on onStart()
	

	//===========================================================
	// Accelerometer Sensor
	//===========================================================
	private SensorManager sensorManager;

	private float accelerometerSpeedX;
	private float accelerometerSpeedY;
	private float accelerometerSpeedZ;
	private Sprite calibrate_ball;
	private float sX, sY; // calibrate ball coordinates
	private IUpdateHandler updateCalibrateBallPosition;
	//===========================================================
	// Main Menu Screen
	//===========================================================
	private AnimatedSprite mGoogleButtonSprite, mFacebookButtonSprite, mTwitterButtonSprite;
	private Sprite mSettingsButtonSprite, mConnectorSprite1, mConnectorSprite2, mConnectorSprite3;
	private TiledSprite mSFXSprite, mMusicSprite, mMotionSprite;
	
	// Sharedpreference
	private Options_SharedPreference options;
	private boolean music_on;
	private boolean sfx_on;
	private boolean controlpads_on;
	
	
	//===========================================================
	// Constructor
	//===========================================================
	public MainMenuScene(Engine engine, SensorManager sensor, GameResources gr, VertexBufferObjectManager vbom, Camera c, Options_SharedPreference o, SceneManager sm, MainActivity mainActivity) {
		mEngine = engine;
		mGameResources = gr;
		mVertexBufferObjectManager = vbom;
		mCamera = c;
		options = o;
		mSceneManager = sm;
		mMainActivity = mainActivity;
		sensorManager = sensor;

		myTracker = mainActivity.getTracker(TrackerName.APP_TRACKER);
	}

	public Scene getmMenuScene() {
		return mMenuScene;
	}
	
	public void loadMainMenuScene() {
		mMenuScene = new Scene();
		mMenuScene.setTouchAreaBindingOnActionDownEnabled(true);
		
		// Center the background on the camera.
		final float centerX = (CAMERA_WIDTH - mGameResources.getmMenuBackgroundTextureRegion().getWidth());
		final float centerY = (CAMERA_HEIGHT - mGameResources.getmMenuBackgroundTextureRegion().getHeight());

		// Add the background, help and other stuffs
		float X2 = CAMERA_WIDTH - mGameResources.getmGoogleButtonTextureRegion().getWidth();

		// Google+ Signin	
		mGoogleButtonSprite = new AnimatedSprite(X2, 0, mGameResources.getmGoogleButtonTextureRegion(), mVertexBufferObjectManager) {
		//mGoogleButtonSprite = new AnimatedSprite(X2, mGameResources.getmGoogleButtonTextureRegion().getHeight() - 10, mGameResources.getmGoogleButtonTextureRegion(), mVertexBufferObjectManager) {
			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent,float pTouchAreaLocalX, float pTouchAreaLocalY) {
				if(pSceneTouchEvent.isActionUp()) {
					if(mGooglePressed) {
						if(sfx_on) mGameResources.getButtonSFX().play();
						mGooglePressed = false;
						this.setScale(0.7f);
						//this.setCurrentTileIndex(this.getCurrentTileIndex() - 1);
	
				    	// start the asynchronous sign in flow
						mMainActivity.runOnUiThread(new Runnable() {
				            public void run() {
				            	if(!mGameResources.getmGameHelper().isSignedIn()) {
				            		mGameResources.getmGameHelper().beginUserInitiatedSignIn();
				            	} else {
					                // Sign-Out
							    	mGameResources.getmGameHelper().signOut();
							    	mMainActivity.mAlreadyLoadedState = false;
				            		mGameResources.setGoogleAccountID("");
							    	if(mSceneManager != null) mSceneManager.updateGoogleButton(false);
				            	}
				            }
				        });
					}
				}
				if(pSceneTouchEvent.isActionDown()) {
					mGooglePressed = true;
					this.setScale(0.6f);
					//this.setCurrentTileIndex(this.getCurrentTileIndex() + 1);
				}
				return true;
			}
		};
		mGoogleButtonSprite.setScale(0.7f);
		if(mMainActivity.getGameHelper().isSignedIn())
			mSceneManager.updateGoogleButton(true);
		else 
			mSceneManager.updateGoogleButton(false);

		// Facebook Signin	
		mFacebookButtonSprite = new AnimatedSprite(X2 - mGameResources.getmFacebookButtonTextureRegion().getWidth(), 0, mGameResources.getmFacebookButtonTextureRegion(), mVertexBufferObjectManager) {
			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent,float pTouchAreaLocalX, float pTouchAreaLocalY) {
				if(pSceneTouchEvent.isActionUp()) {
					if(mFacebookPressed) {
						if(sfx_on) mGameResources.getButtonSFX().play();
						mFacebookPressed = false;
						this.setScale(0.7f);
						
						if(!mMainActivity.useFacebook)
							mMainActivity.facebookLogin();
						else
							mMainActivity.facebookLogout();
					}
				}
				if(pSceneTouchEvent.isActionDown()) {
					mFacebookPressed = true;
					this.setScale(0.6f);
				}
				return true;
			}
		};
		mFacebookButtonSprite.setScale(0.7f);
		if(mMainActivity.useFacebook)
			mSceneManager.updateFacebookButton(true);
		else 
			mSceneManager.updateFacebookButton(false);
		/*
		if(mMainActivity.facebookManager.ismUserLoggedIn())
			mSceneManager.updateFacebookButton(true);
		else 
			mSceneManager.updateFacebookButton(false);
		*/
		
		// Twitter Signin	
		mTwitterButtonSprite = new AnimatedSprite(X2 - mGameResources.getmTwitterButtonTextureRegion().getWidth(), 0, mGameResources.getmTwitterButtonTextureRegion(), mVertexBufferObjectManager) {
			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent,float pTouchAreaLocalX, float pTouchAreaLocalY) {
				if(pSceneTouchEvent.isActionUp()) {
					if(mTwitterPressed) {
						if(sfx_on) mGameResources.getButtonSFX().play();
						mTwitterPressed = false;
						this.setScale(0.7f);
						//TODO: TWITTER LOGIN / TWITTER POSTING 
					}
				}
				if(pSceneTouchEvent.isActionDown()) {
					mTwitterPressed = true;
					this.setScale(0.6f);
				}
				return true;
			}
		};
		mTwitterButtonSprite.setScale(0.7f);
		
		// Hanger Scene
		Sprite mHangerButtonSprite = new Sprite(X2, CAMERA_HEIGHT - mGameResources.getmHangerButtonTextureRegion().getHeight(), mGameResources.getmHangerButtonTextureRegion(), mVertexBufferObjectManager) {
			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent,float pTouchAreaLocalX, float pTouchAreaLocalY) {
				if(pSceneTouchEvent.isActionUp()) {
					if(mHangerPressed) {
						if(sfx_on) mGameResources.getButtonSFX().play();
						mHangerPressed = false;
						this.setScale(0.7f);
						if(!mMenuScene.hasChildScene()){
							mSceneManager.loadHangerScene();
						}
					}
				}
				if(pSceneTouchEvent.isActionDown()) {
					mHangerPressed = true;
					this.setScale(0.6f);
				}
				return true;
			}
		};
		mHangerButtonSprite.setScale(0.7f);

		mSettingsButtonSprite = new Sprite(5, CAMERA_HEIGHT - mGameResources.getmSettingsButtonTextureRegion().getHeight(), mGameResources.getmSettingsButtonTextureRegion(), mVertexBufferObjectManager) {
			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent,float pTouchAreaLocalX, float pTouchAreaLocalY) {
				if(pSceneTouchEvent.isActionUp()) {
					if(mSettingsPressed) {
						if(sfx_on) mGameResources.getButtonSFX().play();
						mSettingsPressed = false;
						setScale(0.7f);
						if(mSettingsOpened == false) {
							mSettingsOpened = true;
							mMenuScene.attachChild(mConnectorSprite1);
							mMenuScene.attachChild(mConnectorSprite2);
							mMenuScene.attachChild(mConnectorSprite3);
							mMenuScene.attachChild(mMusicSprite);
							mMenuScene.attachChild(mSFXSprite);
							mMenuScene.attachChild(mMotionSprite);
							mConnectorSprite1.registerEntityModifier(new MoveXModifier(0.14f, mConnectorSprite1.getX(), this.getX() + mSettingsButtonSprite.getWidth() + 13));
							mConnectorSprite2.registerEntityModifier(new MoveYModifier(0.14f, mConnectorSprite2.getY(), this.getY() - 47));
							mConnectorSprite3.setScaleY(1.35f);
							mConnectorSprite3.registerEntityModifier(new MoveModifier(0.14f, mConnectorSprite1.getX(), this.getX() + mSettingsButtonSprite.getWidth() - 12, mConnectorSprite2.getY(), this.getY() - 30));
							mMusicSprite.registerEntityModifier(new MoveXModifier(0.07f, this.getX(), this.getX() + 96 + mConnectorSprite1.getHeight()));
							mSFXSprite.registerEntityModifier(new MoveYModifier(0.07f, this.getY(), this.getY() - 103 - mConnectorSprite2.getHeight()));
							mMotionSprite.registerEntityModifier(new MoveModifier(0.07f, this.getX(), this.getX() + 60 + mConnectorSprite3.getHeight(),  this.getY(), this.getY() - 67 - mConnectorSprite3.getHeight()));
							mMenuScene.registerTouchArea(mMusicSprite);
							mMenuScene.registerTouchArea(mSFXSprite);
							mMenuScene.registerTouchArea(mMotionSprite);
						} else {
							mSettingsOpened = false;
							mMenuScene.unregisterTouchArea(mMusicSprite);
							mMenuScene.unregisterTouchArea(mSFXSprite);
							mMenuScene.unregisterTouchArea(mMotionSprite);
							mMusicSprite.clearEntityModifiers();
							mSFXSprite.clearEntityModifiers();
							mMotionSprite.clearEntityModifiers();
							mConnectorSprite1.clearEntityModifiers();
							mConnectorSprite2.clearEntityModifiers();
							mConnectorSprite3.clearEntityModifiers();
							mMenuScene.detachChild(mConnectorSprite1);
							mMenuScene.detachChild(mConnectorSprite2);
							mMenuScene.detachChild(mConnectorSprite3);
							mMenuScene.detachChild(mMusicSprite);
							mMenuScene.detachChild(mSFXSprite);
							mMenuScene.detachChild(mMotionSprite);
						}
					}
				}
				if(pSceneTouchEvent.isActionDown()) {
					mSettingsPressed = true;
					this.setScale(0.6f);
				}
				return true;
			}
		};
		mSettingsButtonSprite.setScale(0.7f);
		
		// Play Button
		Sprite mPlayButtonSprite = new Sprite((CAMERA_WIDTH - mGameResources.getmPlayButtonTextureRegion().getWidth())/2, 350, mGameResources.getmPlayButtonTextureRegion(), mVertexBufferObjectManager) {
			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent,float pTouchAreaLocalX, float pTouchAreaLocalY) {
				if(pSceneTouchEvent.isActionUp()) {
					if(mStartGamePressed) {
						if(sfx_on) mGameResources.getButtonSFX().play();
						mStartGamePressed = false;
						this.setScale(0.85f);
						if(!mMenuScene.hasChildScene()) {
							//mSoloPlay = true;
							//loadAccelerometerScene();
							//mMenuScene.setChildScene(mAccelerometerDetailsScene, false, true, true);
							loadGamePlayScene();
							mMenuScene.setChildScene(mGameplayScene, false, true, true);
						}
					}
				}
				if(pSceneTouchEvent.isActionDown()) {
					mStartGamePressed = true;
					this.setScale(0.75f);
				}
				return true;
			}
		};
		mPlayButtonSprite.setScale(0.85f);
		music_on = options.getMusic();
		sfx_on = options.getSoundEffects();
		controlpads_on = options.getControlPads();
		
		final float Label_Pos_X = mSettingsButtonSprite.getX();
		final float Label_Pos_Y = mSettingsButtonSprite.getY();

		mConnectorSprite1 = new Sprite(Label_Pos_X + mSettingsButtonSprite.getWidth(), Label_Pos_Y + 45, mGameResources.getmConnectorTextureRegion(), mVertexBufferObjectManager);
		mConnectorSprite2 = new Sprite(Label_Pos_X - 2 + mSettingsButtonSprite.getWidth()/2, Label_Pos_Y - 55, mGameResources.getmConnectorTextureRegion(), mVertexBufferObjectManager);
		mConnectorSprite1.setRotation(90);
		mConnectorSprite3 = new Sprite(Label_Pos_X + mSettingsButtonSprite.getWidth(), Label_Pos_Y + 45, mGameResources.getmConnectorTextureRegion(), mVertexBufferObjectManager);
		mConnectorSprite3.setRotation(50);
		
		mMusicSprite = new TiledSprite(Label_Pos_X, Label_Pos_Y, mGameResources.getmMusicTextureRegion(), mVertexBufferObjectManager) {
			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent,float pTouchAreaLocalX, float pTouchAreaLocalY) {
				if(pSceneTouchEvent.isActionUp()) {
					if(mMusicPressed) {
						if(sfx_on) mGameResources.getButtonSFX().play();
						mMusicPressed = false;
						this.setScale(0.7f);
						if(mMusicSprite.getCurrentTileIndex() == 1){
							mMusicSprite.setCurrentTileIndex(0);
							music_on = true;
							if (!mGameResources.getMainBGM().isPlaying())
								mGameResources.getMainBGM().play();
						} else {
							mMusicSprite.setCurrentTileIndex(1);
							music_on = false;
							if (mGameResources.getMainBGM().isPlaying())
								mGameResources.getMainBGM().pause();
						}
						options.save(sfx_on, music_on, controlpads_on);
					}
				}
				if(pSceneTouchEvent.isActionDown()) {
					mMusicPressed = true;
					this.setScale(0.6f);
				}
				return true;
			}
		};
		mMusicSprite.setScale(0.7f);
		if(music_on) mMusicSprite.setCurrentTileIndex(0);
		else mMusicSprite.setCurrentTileIndex(1);
		
		mSFXSprite = new TiledSprite(Label_Pos_X, Label_Pos_Y, mGameResources.getmSFXTextureRegion(), mVertexBufferObjectManager){
			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent,float pTouchAreaLocalX, float pTouchAreaLocalY) {
				if(pSceneTouchEvent.isActionUp()) {
					if(mSFXPressed) {
						if(!sfx_on) mGameResources.getButtonSFX().play();
						mSFXPressed = false;
						this.setScale(0.7f);
						if(mSFXSprite.getCurrentTileIndex() == 1){
							mSFXSprite.setCurrentTileIndex(0);
							sfx_on = true;
						} else {
							mSFXSprite.setCurrentTileIndex(1);
							sfx_on = false;
						}
						options.save(sfx_on, music_on, controlpads_on);
					}
				}
				if(pSceneTouchEvent.isActionDown()) {
					mSFXPressed = true;
					this.setScale(0.6f);
				}
				return true;
			}
		};
		mSFXSprite.setScale(0.7f);
		if(sfx_on) mSFXSprite.setCurrentTileIndex(0);
		else mSFXSprite.setCurrentTileIndex(1);

		mMotionSprite = new TiledSprite(Label_Pos_X, Label_Pos_Y, mGameResources.getmMotionControlTextureRegion(), mVertexBufferObjectManager){
			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent,float pTouchAreaLocalX, float pTouchAreaLocalY) {
				if(pSceneTouchEvent.isActionUp()) {
					if(mMotionPressed) {
						if(sfx_on) mGameResources.getButtonSFX().play();
						mMotionPressed = false;
						this.setScale(0.7f);
						if(mMotionSprite.getCurrentTileIndex() == 1){
							mMotionSprite.setCurrentTileIndex(0);
							controlpads_on = false;
						} else {
							mMotionSprite.setCurrentTileIndex(1);
							controlpads_on = true;
						}
						options.save(sfx_on, music_on, controlpads_on);
					}
				}
				if(pSceneTouchEvent.isActionDown()) {
					mMotionPressed = true;
					this.setScale(0.6f);
				}
				return true;
			}
		};
		mMotionSprite.setScale(0.7f);
		if(controlpads_on) mMotionSprite.setCurrentTileIndex(1);
		else mMotionSprite.setCurrentTileIndex(0);				
		
		mMenuScene.attachChild(new Sprite(centerX, centerY, mGameResources.getmMenuBackgroundTextureRegion(), mVertexBufferObjectManager));
		mMenuScene.attachChild(new Sprite((CAMERA_WIDTH - mGameResources.getmTitleTextureRegion().getWidth())/2, 185, mGameResources.getmTitleTextureRegion(), mVertexBufferObjectManager));
		
		mMenuScene.attachChild(mGoogleButtonSprite);
		mMenuScene.attachChild(mFacebookButtonSprite);
		//mMenuScene.attachChild(mTwitterButtonSprite);
		
		mMenuScene.attachChild(mPlayButtonSprite);
		mMenuScene.attachChild(mSettingsButtonSprite);
		mMenuScene.attachChild(mHangerButtonSprite);
		
		mMenuScene.registerTouchArea(mGoogleButtonSprite);
		mMenuScene.registerTouchArea(mFacebookButtonSprite);
		//mMenuScene.registerTouchArea(mTwitterButtonSprite);
		mMenuScene.registerTouchArea(mPlayButtonSprite);
		mMenuScene.registerTouchArea(mSettingsButtonSprite);
		mMenuScene.registerTouchArea(mHangerButtonSprite);		
	}
	
	//========================================================================
	// Other Scenes Methods (Accelerometer & Multplayer/Solo Scene)
	//========================================================================
	//Created a game play scene to allow user to choose whether to play solo or multiplayer
	private void loadGamePlayScene() {
		mGameplayScene = new CameraScene(mCamera);
		mGameplayScene.setTouchAreaBindingOnActionDownEnabled(true);
		
		float centerX = (CAMERA_WIDTH - mGameResources.getmCalibrationCustomScreenTextureRegion().getWidth()) / 2;
		float centerY = (CAMERA_HEIGHT - mGameResources.getmCalibrationCustomScreenTextureRegion().getHeight()) / 2;
		
		Sprite background = new Sprite(centerX, centerY, mGameResources.getmCalibrationCustomScreenTextureRegion(), mVertexBufferObjectManager);
		background.setScale(1.2f);
		
		float closeXPosition = background.getX() + background.getWidth() + 14;
		float closeYPosition = background.getY() - 78;
		Sprite close_button = new Sprite(closeXPosition, closeYPosition, mGameResources.getmCloseButtonTextureRegion(), mVertexBufferObjectManager) {
			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent,float pTouchAreaLocalX, float pTouchAreaLocalY) {
				if(pSceneTouchEvent.isActionUp()) {
					if(mCloseButtonPressed) {
						this.setScale(0.7f);
						
						if(sfx_on) mGameResources.getButtonSFX().play();
						mCloseButtonPressed = false;
						mMenuScene.clearChildScene();
					}
				}
				if(pSceneTouchEvent.isActionDown()) {
					mCloseButtonPressed = true;					
					this.setScale(0.6f);
				}
				return true;
			}
		};
		close_button.setScale(0.7f);
		
		AnimatedSprite solo_button = new AnimatedSprite(140, 300, mGameResources.getmMultiplayerButtonsTextureRegion(), mVertexBufferObjectManager) {
			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent,float pTouchAreaLocalX, float pTouchAreaLocalY) {
				if(pSceneTouchEvent.isActionUp()) {
					if(mSoloButtonPressed) {
						this.setScale(0.55f);
						this.setScaleY(0.7f);
						
						if(sfx_on) mGameResources.getButtonSFX().play();
						mSoloPlay = true;
						mSoloButtonPressed = false;
						if(controlpads_on) {
							mSoloPlay = false;
							mSceneManager.loadGameScene(0, mGameResources.getPositionAngle0(), false);
						} else {
							loadAccelerometerScene();
							mMenuScene.setChildScene(mAccelerometerDetailsScene, false, true, true);
						}
						//TODO: Release after multiplayer
						//myTracker.send(new HitBuilders.EventBuilder("UI Action", "Solo Button Pressed").build());
					}
				}
				if(pSceneTouchEvent.isActionDown()) {
					mSoloButtonPressed = true;
					
					this.setScale(0.45f);
					this.setScaleY(0.6f);
				}
				return true;
			}
		};
		solo_button.setCurrentTileIndex(0);
		solo_button.setScale(0.55f);
		solo_button.setScaleY(0.7f);
		
		AnimatedSprite quick_match_button = new AnimatedSprite(510, 200, mGameResources.getmMultiplayerButtonsTextureRegion(), mVertexBufferObjectManager) {
			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent,float pTouchAreaLocalX, float pTouchAreaLocalY) {
				if(pSceneTouchEvent.isActionUp()) {
					if(mQuickMatchButtonPressed) {
						this.setScale(0.5f);
						this.setScaleY(0.6f);
						
						if(sfx_on) mGameResources.getButtonSFX().play();
						mQuickMatchButtonPressed = false;

						//Load tip image while waiting to check whether all resource has loaded
						mSceneManager.loadTipScene(false);
						
						mEngine.registerUpdateHandler(new TimerHandler(0.2f, true, new ITimerCallback() {
							@Override
							public void onTimePassed(final TimerHandler pTimerHandler) {
								if(mGameResources.gameResources_PART3_Loaded) {
									mSceneManager.clearTipScene();
									mEngine.unregisterUpdateHandler(pTimerHandler);
									
									mMultiPlay = true;
									
									if(controlpads_on) {
										mMultiPlay = false;
										mMainActivity.setMultiplayerActivity(true);
										mMainActivity.startQuickGame(0, mGameResources.getPositionAngle0());
									} else {
										loadAccelerometerScene();
										mMenuScene.setChildScene(mAccelerometerDetailsScene, false, true, true);
									}
								}
							}
						}));
					}
				}
				if(pSceneTouchEvent.isActionDown()) {
					mQuickMatchButtonPressed = true;
					
					//this.setScale(0.4f);
					//this.setScaleY(0.5f);
				}
				return true;
			}
		};
		quick_match_button.setCurrentTileIndex(1);
		quick_match_button.setColor(0.66196f, 0.66196f, 0.66196f);
		quick_match_button.setScale(0.55f);
		quick_match_button.setScaleY(0.7f);

		AnimatedSprite challenge_button = new AnimatedSprite(510, 300, mGameResources.getmMultiplayerButtonsTextureRegion(), mVertexBufferObjectManager) {
			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent,float pTouchAreaLocalX, float pTouchAreaLocalY) {
				if(pSceneTouchEvent.isActionUp()) {
					if(mChallengeButtonPressed) {
						this.setScale(0.5f);
						this.setScaleY(0.6f);
						
						if(sfx_on) mGameResources.getButtonSFX().play();
						mChallengeButtonPressed = false;						

						//Load tip image while waiting to check whether all resource has loaded
						mSceneManager.loadTipScene(false);
						
						mEngine.registerUpdateHandler(new TimerHandler(0.2f, true, new ITimerCallback() {
							@Override
							public void onTimePassed(final TimerHandler pTimerHandler) {
								if(mGameResources.gameResources_PART3_Loaded) {
									mSceneManager.clearTipScene();
									mEngine.unregisterUpdateHandler(pTimerHandler);
									
					                // show list of invitable players
									if(mGameResources.getmGameHelper().isSignedIn()) {
										Intent intent = Games.RealTimeMultiplayer.getSelectOpponentsIntent(mGameResources.getmGameHelper().getApiClient(), 1, 1);
										mMainActivity.startActivityForResult(intent, MainActivity.RC_SELECT_PLAYERS);
									} else {
										mMainActivity.runOnUiThread(new Runnable() {
								    		public void run() {
								    			mGameResources.getmGameHelper().makeSimpleDialog(mGameResources.getmBaseContext().getString(R.string.signin_to_join)).show();
								    		}
										});
						        	}
								}
							}
						}));
					}
				}
				if(pSceneTouchEvent.isActionDown()) {
					mChallengeButtonPressed = true;
					
					//this.setScale(0.4f);
					//this.setScaleY(0.5f);
				}
				return true;
			}
		};
		challenge_button.setCurrentTileIndex(2);
		challenge_button.setColor(0.66196f, 0.66196f, 0.66196f);
		challenge_button.setScale(0.55f);
		challenge_button.setScaleY(0.7f);
		
		AnimatedSprite invitations_button = new AnimatedSprite(510, 400, mGameResources.getmMultiplayerButtonsTextureRegion(), mVertexBufferObjectManager) {
			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent,float pTouchAreaLocalX, float pTouchAreaLocalY) {
				if(pSceneTouchEvent.isActionUp()) {
					if(mInvitationButtonPressed) {
						this.setScale(0.5f);
						this.setScaleY(0.6f);
						
						if(sfx_on) mGameResources.getButtonSFX().play();
						mInvitationButtonPressed = false;
						
						//Load tip image while waiting to check whether all resource has loaded
						mSceneManager.loadTipScene(false);
						
						mEngine.registerUpdateHandler(new TimerHandler(0.2f, true, new ITimerCallback() {
							@Override
							public void onTimePassed(final TimerHandler pTimerHandler) {
								if(mGameResources.gameResources_PART3_Loaded) {
									mSceneManager.clearTipScene();
									mEngine.unregisterUpdateHandler(pTimerHandler);
									
					                // show invitations
									if(mGameResources.getmGameHelper().isSignedIn()) {
										Intent intent = Games.Invitations.getInvitationInboxIntent(mGameResources.getmGameHelper().getApiClient());
										mMainActivity.startActivityForResult(intent, MainActivity.RC_INVITATION_INBOX);
									} else {
										mMainActivity.runOnUiThread(new Runnable() {
								    		public void run() {
								    			mGameResources.getmGameHelper().makeSimpleDialog(mGameResources.getmBaseContext().getString(R.string.signin_to_join)).show();
								    		}
										});
						        	}
								}
							}
						}));
					}
				}
				if(pSceneTouchEvent.isActionDown()) {
					mInvitationButtonPressed = true;
					
					//this.setScale(0.4f);
					//this.setScaleY(0.5f);
				}
				return true;
			}
		};
		invitations_button.setCurrentTileIndex(3);
		invitations_button.setColor(0.66196f, 0.66196f, 0.66196f);
		invitations_button.setScale(0.55f);
		invitations_button.setScaleY(0.7f);
		
		mGameplayScene.attachChild(new Sprite(0, 0, mGameResources.getmWashoutBackgroundTextureRegion(), mVertexBufferObjectManager));
		mGameplayScene.attachChild(background);
		mGameplayScene.attachChild(close_button);
		mGameplayScene.attachChild(solo_button);
		mGameplayScene.attachChild(quick_match_button);
		mGameplayScene.attachChild(challenge_button);
		mGameplayScene.attachChild(invitations_button);
		
		mGameplayScene.registerTouchArea(close_button);
		mGameplayScene.registerTouchArea(solo_button);
		mGameplayScene.registerTouchArea(quick_match_button);
		mGameplayScene.registerTouchArea(challenge_button);
		mGameplayScene.registerTouchArea(invitations_button);
		
		mGameplayScene.setBackgroundEnabled(false);
	}
	
	//Created a accelerometer scene with prompting gamer to choose their playing style
	private void loadAccelerometerScene() {
		mAccelerometerDetailsScene = new CameraScene(mCamera);
		mAccelerometerDetailsScene.setTouchAreaBindingOnActionDownEnabled(true);
		
		float centerX = (CAMERA_WIDTH - mGameResources.getmCalibrationCustomScreenTextureRegion().getWidth()) / 2;
		float centerY = (CAMERA_HEIGHT - mGameResources.getmCalibrationCustomScreenTextureRegion().getHeight()) / 2;
		
		Sprite background = new Sprite(centerX, centerY, mGameResources.getmCalibrationCustomScreenTextureRegion(), mVertexBufferObjectManager);
		background.setScale(1.2f);

		Sprite help_button = new Sprite(background.getX() + background.getWidth() - mGameResources.getmHelpButtonTextureRegion().getWidth()/2.75f, background.getY() + background.getHeight() - mGameResources.getmHelpButtonTextureRegion().getHeight()/2.1f, mGameResources.getmHelpButtonTextureRegion(), mVertexBufferObjectManager) {
			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent,float pTouchAreaLocalX, float pTouchAreaLocalY) {
				if(pSceneTouchEvent.isActionUp()) {
					if(mHelpButtonPressed) {
						this.setScale(0.3f);
						
						if(sfx_on) mGameResources.getButtonSFX().play();
						mHelpButtonPressed = false;
						
						loadAccelerometerWHelpScene();
						mAccelerometerDetailsScene.setChildScene(mAccelerometerWHelpDetailsScene, false, true, true);
					}
				}
				if(pSceneTouchEvent.isActionDown()) {
					mHelpButtonPressed = true;
					this.setScale(0.2f);
				}
				return true;
			}
		};
		help_button.setScale(0.3f);
		
		float closeXPosition = background.getX() + background.getWidth() + 14;
		float closeYPosition = background.getY() - 78;
		Sprite close_button = new Sprite(closeXPosition, closeYPosition, mGameResources.getmCloseButtonTextureRegion(), mVertexBufferObjectManager) {
			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent,float pTouchAreaLocalX, float pTouchAreaLocalY) {
				if(pSceneTouchEvent.isActionUp()) {
					if(mCloseButtonPressed) {
						this.setScale(0.7f);
						
						if(sfx_on) mGameResources.getButtonSFX().play();
						mCloseButtonPressed = false;
						mMenuScene.clearChildScene();
					}
				}
				if(pSceneTouchEvent.isActionDown()) {
					mCloseButtonPressed = true;
					this.setScale(0.6f);
				}
				return true;
			}
		};
		close_button.setScale(0.7f);
		
		Sprite Angle_0_Sprite = new Sprite(349, 135, mGameResources.getmAccelerometer0TextureRegion(), mVertexBufferObjectManager) {
			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent,float pTouchAreaLocalX, float pTouchAreaLocalY) {
				if(pSceneTouchEvent.isActionUp()) {
					if(mAngle0Pressed) {
						this.setScale(0.85f);
						
						if(sfx_on) mGameResources.getButtonSFX().play();
						mAngle0Pressed = false;

						//myTracker.setScreenName("Main Menu Screen");
						myTracker.send(new HitBuilders.EventBuilder("UI Action", "Angle 0 Button Pressed").build());
						
						if(mMultiPlay) {
							mMultiPlay = false;
							mMainActivity.setMultiplayerActivity(true);
							mMainActivity.startQuickGame(0, mGameResources.getPositionAngle0());
						} else if (mSoloPlay) {
							mSoloPlay = false;
							mSceneManager.loadGameScene(0, mGameResources.getPositionAngle0(), false);
						}
					}
				}
				if(pSceneTouchEvent.isActionDown()) {
					mAngle0Pressed = true;
					this.setScale(0.75f);
				}
				return true;
			}
		};
		Angle_0_Sprite.setScale(0.85f);
		
		Sprite Angle_45_Sprite = new Sprite(690, 135, mGameResources.getmAccelerometer45TextureRegion(), mVertexBufferObjectManager) {
			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent,float pTouchAreaLocalX, float pTouchAreaLocalY) {
				if(pSceneTouchEvent.isActionUp()) {
					if(mAngle45Pressed) {
						this.setScale(0.85f);
						
						if(sfx_on) mGameResources.getButtonSFX().play();
						mAngle45Pressed = false;
						
						//myTracker.setScreenName("Main Menu Screen");
						myTracker.send(new HitBuilders.EventBuilder("UI Action", "Angle 45 Button Pressed").build());
						
						if(mMultiPlay) {
							mMultiPlay = false;
							mMainActivity.setMultiplayerActivity(true);
							mMainActivity.startQuickGame(0, mGameResources.getPositionAngle45());
						} else if (mSoloPlay) {
							mSoloPlay = false;
							mSceneManager.loadGameScene(0, mGameResources.getPositionAngle45(), false);
						}
					}
				}
				if(pSceneTouchEvent.isActionDown()) {
					mAngle45Pressed = true;
					this.setScale(0.75f);
				}
				return true;
			}
		};
		Angle_45_Sprite.setScale(0.85f);
		
		Sprite Angle_Custom_Sprite = new Sprite(520, 335, mGameResources.getmAccelerometerCustomTextureRegion(), mVertexBufferObjectManager) {
			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent,float pTouchAreaLocalX, float pTouchAreaLocalY) {
				if(pSceneTouchEvent.isActionUp()) {
					if(mAngleCustomPressed) {
						this.setScale(0.85f);
						
						if(sfx_on) mGameResources.getButtonSFX().play();
						mAngleCustomPressed = false;

						//myTracker.setScreenName("Main Menu Screen");
						myTracker.send(new HitBuilders.EventBuilder("UI Action", "Custom Angle Button Pressed").build());
						
						loadCustomCalibrationScene();
						mMenuScene.setChildScene(mCustomCalibrationScene, false, true, true);
					}
				}
				if(pSceneTouchEvent.isActionDown()) {
					mAngleCustomPressed = true;
					this.setScale(0.75f);
				}
				return true;
			}
		};
		Angle_Custom_Sprite.setScale(0.85f);

		mAccelerometerDetailsScene.attachChild(new Sprite(0, 0, mGameResources.getmWashoutBackgroundTextureRegion(), mVertexBufferObjectManager));
		mAccelerometerDetailsScene.attachChild(background);
		mAccelerometerDetailsScene.attachChild(close_button);
		mAccelerometerDetailsScene.attachChild(help_button);
		mAccelerometerDetailsScene.attachChild(Angle_0_Sprite);
		mAccelerometerDetailsScene.attachChild(Angle_45_Sprite);
		mAccelerometerDetailsScene.attachChild(Angle_Custom_Sprite);

		mAccelerometerDetailsScene.registerTouchArea(close_button);
		mAccelerometerDetailsScene.registerTouchArea(help_button);
		mAccelerometerDetailsScene.registerTouchArea(Angle_0_Sprite);
		mAccelerometerDetailsScene.registerTouchArea(Angle_45_Sprite);
		mAccelerometerDetailsScene.registerTouchArea(Angle_Custom_Sprite);
		
		mAccelerometerDetailsScene.setBackgroundEnabled(false);
	}

	private void loadAccelerometerWHelpScene() {
		mAccelerometerWHelpDetailsScene = new CameraScene(mCamera);
		mAccelerometerWHelpDetailsScene.setTouchAreaBindingOnActionDownEnabled(true);
		
		float centerX = (CAMERA_WIDTH - mGameResources.getmGameStyleHelpTextureRegion().getWidth()) / 2;
		float centerY = (CAMERA_HEIGHT - mGameResources.getmGameStyleHelpTextureRegion().getHeight()) / 2;
		
		Sprite background = new Sprite(centerX, centerY, mGameResources.getmGameStyleHelpTextureRegion(), mVertexBufferObjectManager);
		background.setScale(0.8f);

		Sprite close_button = new Sprite(1050, 42, mGameResources.getmCloseButtonTextureRegion(), mVertexBufferObjectManager) {
			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent,float pTouchAreaLocalX, float pTouchAreaLocalY) {
				if(pSceneTouchEvent.isActionUp()) {
					if(mCloseButtonPressed) {
						this.setScale(0.7f);
						
						if(sfx_on) mGameResources.getButtonSFX().play();
						mCloseButtonPressed = false;
						mAccelerometerDetailsScene.clearChildScene();
					}
				}
				if(pSceneTouchEvent.isActionDown()) {
					mCloseButtonPressed = true;
					this.setScale(0.6f);
				}
				return true;
			}
		};
		close_button.setScale(0.7f);

		Sprite Angle_0_Sprite = new Sprite(165, 160, mGameResources.getmAccelerometer0TextureRegion(), mVertexBufferObjectManager) {
			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent,float pTouchAreaLocalX, float pTouchAreaLocalY) {
				if(pSceneTouchEvent.isActionUp()) {
					if(mAngle0Pressed) {
						this.setScale(0.85f);
						
						if(sfx_on) mGameResources.getButtonSFX().play();
						mAngle0Pressed = false;

						//myTracker.setScreenName("Main Menu Screen");
						myTracker.send(new HitBuilders.EventBuilder("UI Action", "Angle 0 Button Pressed").build());
						
						if(mMultiPlay) {
							mMultiPlay = false;
							mMainActivity.setMultiplayerActivity(true);
							mMainActivity.startQuickGame(0, mGameResources.getPositionAngle0());
						} else if (mSoloPlay) {
							mSoloPlay = false;
							mSceneManager.loadGameScene(0, mGameResources.getPositionAngle0(), false);
						}
					}
				}
				if(pSceneTouchEvent.isActionDown()) {
					mAngle0Pressed = true;
					this.setScale(0.75f);
				}
				return true;
			}
		};
		Angle_0_Sprite.setScale(0.85f);
		
		Sprite Angle_45_Sprite = new Sprite(625, 160, mGameResources.getmAccelerometer45TextureRegion(), mVertexBufferObjectManager) {
			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent,float pTouchAreaLocalX, float pTouchAreaLocalY) {
				if(pSceneTouchEvent.isActionUp()) {
					if(mAngle45Pressed) {
						this.setScale(0.85f);
						
						if(sfx_on) mGameResources.getButtonSFX().play();
						mAngle45Pressed = false;
						
						//myTracker.setScreenName("Main Menu Screen");
						myTracker.send(new HitBuilders.EventBuilder("UI Action", "Angle 45 Button Pressed").build());
						
						if(mMultiPlay) {
							mMultiPlay = false;
							mMainActivity.setMultiplayerActivity(true);
							mMainActivity.startQuickGame(0, mGameResources.getPositionAngle45());
						} else if (mSoloPlay) {
							mSoloPlay = false;
							mSceneManager.loadGameScene(0, mGameResources.getPositionAngle45(), false);
						}
					}
				}
				if(pSceneTouchEvent.isActionDown()) {
					mAngle45Pressed = true;
					this.setScale(0.75f);
				}
				return true;
			}
		};
		Angle_45_Sprite.setScale(0.85f);
		
		Sprite Angle_Custom_Sprite = new Sprite(355, 380, mGameResources.getmAccelerometerCustomTextureRegion(), mVertexBufferObjectManager) {
			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent,float pTouchAreaLocalX, float pTouchAreaLocalY) {
				if(pSceneTouchEvent.isActionUp()) {
					if(mAngleCustomPressed) {
						this.setScale(0.85f);
						
						if(sfx_on) mGameResources.getButtonSFX().play();
						mAngleCustomPressed = false;

						//myTracker.setScreenName("Main Menu Screen");
						myTracker.send(new HitBuilders.EventBuilder("UI Action", "Custom Angle Button Pressed").build());
						
						loadCustomCalibrationScene();
						mMenuScene.setChildScene(mCustomCalibrationScene, false, true, true);
					}
				}
				if(pSceneTouchEvent.isActionDown()) {
					mAngleCustomPressed = true;
					this.setScale(0.75f);
				}
				return true;
			}
		};
		Angle_Custom_Sprite.setScale(0.85f);

		mAccelerometerWHelpDetailsScene.attachChild(new Sprite(0, 0, mGameResources.getmWashoutBackgroundTextureRegion(), mVertexBufferObjectManager));
		mAccelerometerWHelpDetailsScene.attachChild(background);
		mAccelerometerWHelpDetailsScene.attachChild(close_button);
		mAccelerometerWHelpDetailsScene.attachChild(Angle_0_Sprite);
		mAccelerometerWHelpDetailsScene.attachChild(Angle_45_Sprite);
		mAccelerometerWHelpDetailsScene.attachChild(Angle_Custom_Sprite);

		mAccelerometerWHelpDetailsScene.registerTouchArea(close_button);
		mAccelerometerWHelpDetailsScene.registerTouchArea(Angle_0_Sprite);
		mAccelerometerWHelpDetailsScene.registerTouchArea(Angle_45_Sprite);
		mAccelerometerWHelpDetailsScene.registerTouchArea(Angle_Custom_Sprite);
		
		mAccelerometerWHelpDetailsScene.setBackgroundEnabled(false);
	}
	
	//Created a accelerometer custom scene with prompting gamer to set their own calibration
	private void loadCustomCalibrationScene() {
		sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_GAME);
		mCustomCalibrationScene = new CameraScene(mCamera);
		mCustomCalibrationScene.setTouchAreaBindingOnActionDownEnabled(true);
		
		float centerX = (CAMERA_WIDTH - mGameResources.getmCalibrationCustomScreenTextureRegion().getWidth()) / 2;
		float centerY = (CAMERA_HEIGHT - mGameResources.getmCalibrationCustomScreenTextureRegion().getHeight()) / 2;
		
		Sprite background = new Sprite(centerX, centerY, mGameResources.getmCalibrationCustomScreenTextureRegion(), mVertexBufferObjectManager);
		background.setScale(1.2f);
		
		float closeXPosition = background.getX() + background.getWidth() + 14;
		float closeYPosition = background.getY() - 78;
		Sprite close_button = new Sprite(closeXPosition, closeYPosition, mGameResources.getmCloseButtonTextureRegion(), mVertexBufferObjectManager) {
			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent,float pTouchAreaLocalX, float pTouchAreaLocalY) {
				if(pSceneTouchEvent.isActionUp()) {
					if(mCloseButtonPressed) {
						this.setScale(0.7f);
						
						if(sfx_on) mGameResources.getButtonSFX().play();
						mCloseButtonPressed = false;
						mMenuScene.clearChildScene();
					}
				}
				if(pSceneTouchEvent.isActionDown()) {
					mCloseButtonPressed = true;
					this.setScale(0.6f);
				}
				return true;
			}
		};
		close_button.setScale(0.7f);
		
		Sprite calibrate_desc = new Sprite((CAMERA_WIDTH - mGameResources.getmCalibrationCustomHeaderTextureRegion().getWidth()) / 2, 150, mGameResources.getmCalibrationCustomHeaderTextureRegion(), mVertexBufferObjectManager);
		final PixelPerfectSprite calibrate_ring = new PixelPerfectSprite((CAMERA_WIDTH - mGameResources.getmCalibrationCustomRingTextureRegion().getWidth()) / 2, (CAMERA_HEIGHT - mGameResources.getmCalibrationCustomRingTextureRegion().getHeight()) / 2, mGameResources.getmCalibrationCustomRingTextureRegion(), mVertexBufferObjectManager);
		calibrate_ball = new Sprite((CAMERA_WIDTH - mGameResources.getmCalibrationCustomBallTextureRegion().getWidth()) / 2, (CAMERA_HEIGHT - mGameResources.getmCalibrationCustomBallTextureRegion().getHeight()) / 2, mGameResources.getmCalibrationCustomBallTextureRegion(), mVertexBufferObjectManager);
		
		Sprite calibrate_button = new Sprite((CAMERA_WIDTH - mGameResources.getmCalibrateButtonTextureRegion().getWidth()) / 2, 500, mGameResources.getmCalibrateButtonTextureRegion(), mVertexBufferObjectManager) {
			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent,float pTouchAreaLocalX, float pTouchAreaLocalY) {
				if(pSceneTouchEvent.isActionUp()) {
					if(mCalibrateButtonPressed) {
						this.setScale(0.85f);
						
						if(sfx_on) mGameResources.getButtonSFX().play();
						mCalibrateButtonPressed = false;

						//myTracker.setScreenName("Main Menu Screen");
						
						mEngine.unregisterUpdateHandler(updateCalibrateBallPosition);
						
						if(mMultiPlay) {
							mMultiPlay = false;
							mMainActivity.setMultiplayerActivity(true);
							myTracker.send(new HitBuilders.EventBuilder("UI Action", "Mulitplayer Calibrate Button Pressed - " + accelerometerSpeedY).build());
							mMainActivity.startQuickGame(accelerometerSpeedX, accelerometerSpeedY);
						} else if (mSoloPlay) {
							mSoloPlay = false;
							myTracker.send(new HitBuilders.EventBuilder("UI Action", "Singleplay Calibrate Button Pressed - " + accelerometerSpeedY).build());
							mSceneManager.loadGameScene(accelerometerSpeedX, accelerometerSpeedY, false);
						}
					}
				}
				if(pSceneTouchEvent.isActionDown()) {
					mCalibrateButtonPressed = true;
					this.setScale(0.75f);
				}
				return true;
			}
		};
		calibrate_button.setScale(0.85f);
		
		mCustomCalibrationScene.attachChild(new Sprite(0, 0, mGameResources.getmWashoutBackgroundTextureRegion(), mVertexBufferObjectManager));
		mCustomCalibrationScene.attachChild(background);
		mCustomCalibrationScene.attachChild(close_button);
		mCustomCalibrationScene.attachChild(calibrate_desc);
		mCustomCalibrationScene.attachChild(calibrate_ring);
		mCustomCalibrationScene.attachChild(calibrate_ball);
		mCustomCalibrationScene.attachChild(calibrate_button);

		mCustomCalibrationScene.registerTouchArea(close_button);
		mCustomCalibrationScene.registerTouchArea(calibrate_button);
		
		mCustomCalibrationScene.setBackgroundEnabled(false);

		updateCalibrateBallPosition = new IUpdateHandler() {
			public void onUpdate(float pSecondsElapsed) {
				if ((accelerometerSpeedZ != 0) || (accelerometerSpeedY != 0)) {
					
					sX = calibrate_ball.getX();
					sY = calibrate_ball.getY();
					
					// Set the Boundary limits
					float topLimit = calibrate_ring.getY() - 10;
					float leftLimit = calibrate_ring.getX() - 10;
					float rightLimit = calibrate_ring.getX() + calibrate_ring.getWidth() - 28;
					float bottomLimit = calibrate_ring.getY() + calibrate_ring.getHeight() - 28;
					
					// Calculate New X,Y Coordinates within Limits
					if (sX >= leftLimit) sX += accelerometerSpeedX;
					else sX = leftLimit;
					if (sX <= rightLimit) sX += accelerometerSpeedX;
					else sX = rightLimit;
					if (sY >= topLimit) sY += accelerometerSpeedY;
					else sY = topLimit;
					if (sY <= bottomLimit)	sY += accelerometerSpeedY;
					else sY = bottomLimit;

					// Double Check That New X,Y Coordinates are within Limits
					if (sX < leftLimit) sX = leftLimit;
					else if (sX > rightLimit) sX = rightLimit;
					if (sY < topLimit) sY = topLimit;
					else if (sY > bottomLimit) sY = bottomLimit;

					
					if(calibrate_ball != null && calibrate_ball.collidesWith(calibrate_ball))
						calibrate_ball.setPosition(sX, sY);
				}
			}
			public void reset() { }
		};		
		mEngine.registerUpdateHandler(updateCalibrateBallPosition);
	}
	
	public AnimatedSprite getmGoogleButtonSprite() {
		return mGoogleButtonSprite;
	}
	
	public AnimatedSprite getmFacebookButtonSprite() {
		return mFacebookButtonSprite;
	}

	@Override
	public void onSensorChanged(SensorEvent event) {
		synchronized (this) {
			switch (event.sensor.getType()) {
			case Sensor.TYPE_ACCELEROMETER:
				accelerometerSpeedX = event.values[1]; //x-axis
				accelerometerSpeedY = event.values[0]; //y-axis
				accelerometerSpeedZ = event.values[2]; //Z-axis
				break;
			}
		}
	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) { }
}
