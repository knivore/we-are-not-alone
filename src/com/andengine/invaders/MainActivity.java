/*
 * Copyright (C) 2013 Knivore Studios
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.andengine.invaders;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import javax.microedition.khronos.opengles.GL10;

import org.andengine.engine.camera.Camera;
import org.andengine.engine.handler.timer.ITimerCallback;
import org.andengine.engine.handler.timer.TimerHandler;
import org.andengine.engine.options.EngineOptions;
import org.andengine.engine.options.ScreenOrientation;
import org.andengine.engine.options.resolutionpolicy.RatioResolutionPolicy;
import org.andengine.entity.scene.IOnAreaTouchListener;
import org.andengine.entity.scene.IOnSceneTouchListener;
import org.andengine.entity.scene.ITouchArea;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.scene.background.Background;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.text.Text;
import org.andengine.entity.text.TextOptions;
import org.andengine.entity.util.FPSLogger;
import org.andengine.extension.physics.box2d.FixedStepPhysicsWorld;
import org.andengine.extension.physics.box2d.PhysicsConnector;
import org.andengine.extension.physics.box2d.PhysicsFactory;
import org.andengine.input.touch.TouchEvent;
import org.andengine.input.touch.controller.MultiTouch;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.texture.region.TextureRegion;
import org.andengine.opengl.view.RenderSurfaceView;
import org.andengine.util.HorizontalAlign;
import org.andengine.util.color.Color;
import org.andengine.util.math.MathUtils;
import org.json.JSONException;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.andengine.classes.AchievementContainer;
import com.andengine.classes.AsyncTaskLoader;
import com.andengine.classes.BaseGameActivity;
import com.andengine.classes.IAsyncCallback;
import com.andengine.classes.SaveGame;
import com.andengine.sharedpreferences.Options_SharedPreference;
import com.andengine.sharedpreferences.Score_SharedPreference;
import com.android.vending.billing.util.IabHelper;
import com.android.vending.billing.util.IabResult;
import com.android.vending.billing.util.Inventory;
import com.android.vending.billing.util.Purchase;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.basegameutils.GameHelper;
import com.basegameutils.GameHelper.GameHelperListener;
import com.facebook.FacebookException;
import com.facebook.FacebookOperationCanceledException;
import com.facebook.HttpMethod;
import com.facebook.Request;
import com.facebook.Request.Callback;
import com.facebook.RequestAsyncTask;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.model.GraphUser;
import com.facebook.widget.WebDialog;
import com.facebook.widget.WebDialog.OnCompleteListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.auth.GoogleAuthException;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.GamesActivityResultCodes;
import com.google.android.gms.games.GamesStatusCodes;
import com.google.android.gms.games.multiplayer.Invitation;
import com.google.android.gms.games.multiplayer.Multiplayer;
import com.google.android.gms.games.multiplayer.OnInvitationReceivedListener;
import com.google.android.gms.games.multiplayer.Participant;
import com.google.android.gms.games.multiplayer.realtime.RealTimeMessage;
import com.google.android.gms.games.multiplayer.realtime.RealTimeMessageReceivedListener;
import com.google.android.gms.games.multiplayer.realtime.Room;
import com.google.android.gms.games.multiplayer.realtime.RoomConfig;
import com.google.android.gms.games.multiplayer.realtime.RoomStatusUpdateListener;
import com.google.android.gms.games.multiplayer.realtime.RoomUpdateListener;
import com.google.android.gms.games.quest.Quest;
import com.google.android.gms.games.quest.QuestUpdateListener;
import com.google.android.gms.games.snapshot.Snapshot;
import com.google.android.gms.games.snapshot.SnapshotMetadataChange;
import com.google.android.gms.games.snapshot.Snapshots;
import com.google.android.gms.plus.Plus;
import com.vungle.publisher.AdConfig;
import com.vungle.publisher.VunglePub;


/**
 * @author Keh Chin Leong
 */
public class MainActivity extends BaseGameActivity implements GameHelperListener, IOnSceneTouchListener, SensorEventListener,// ConnectionCallbacks, OnConnectionFailedListener,
IOnAreaTouchListener, RealTimeMessageReceivedListener, RoomStatusUpdateListener, RoomUpdateListener, OnInvitationReceivedListener, QuestUpdateListener {

	//===========================================================
	// Constants
	//===========================================================
    static Context mContext;
    
	private static final int CAMERA_WIDTH = 1280;
	private static final int CAMERA_HEIGHT = 720;
	
	private GameResources mGameResources;
	private SceneManager mSceneManager = null;
	private Camera mCamera;
	private float mMultiXAngle = 0f;
	private float mMultiYAngle = 0f;

	private Options_SharedPreference options;
	
	private int connectionReTries = 0;
	
	private boolean multiplayerActivity = false;
	    
	//===========================================================
	// Google Play Game Services
	//===========================================================
    // The game helper object. This class is mainly a wrapper around this object.
    protected GameHelper mGameHelper;

    protected String accountID = "";
    protected String accountName = "";
	
    // We expose these constants here because we don't want users of this class
    // to have to know about GameHelper at all.
    public static final int CLIENT_GAMES = GameHelper.CLIENT_GAMES;
    public static final int CLIENT_APPSTATE = GameHelper.CLIENT_APPSTATE;
    public static final int CLIENT_PLUS = GameHelper.CLIENT_PLUS;
    public static final int CLIENT_ALL = GameHelper.CLIENT_ALL;

    // Requested clients. By default, that's just the games client.
    protected int mRequestedClients = CLIENT_GAMES | CLIENT_APPSTATE;

    protected boolean mDebugLog = false;
    
	//===========================================================
	// Google Save Games
	//===========================================================    
    // Members related to the conflict resolution chooser of Snapshots.
    final static int MAX_SNAPSHOT_RESOLVE_RETRIES = 3;
    
	//===========================================================
	// Google Multiplayer
	//===========================================================
    // Request codes for the UIs that we show with startActivityForResult:
    final static int RC_SELECT_PLAYERS = 10000;
    final static int RC_INVITATION_INBOX = 10001;
    final static int RC_WAITING_ROOM = 10002;

    MultiplayerPlayer1GameScene mMultiplayerPlayer1GameScene;
    MultiplayerPlayer2GameScene mMultiplayerPlayer2GameScene;
    String DESTINATED_MAP = "";
    
    // Room ID where the currently active game is taking place; null if we're
    // not playing.
    String mRoomId = null;

    // Are we playing in multiplayer mode?
    boolean mMultiplayer = false;

    // The participants in the currently active game
    ArrayList<Participant> mParticipants = null;

    // My participant ID in the currently active game
    String mMyId = null;

    // If non-null, this is the id of the invitation we received via the
    // invitation listener
    String mIncomingInvitationId = null;

    // Message buffer for sending messages
    byte[] mMsgBuf = new byte[2];

	//===========================================================
	// Google Analytics
	//===========================================================
    /**
     * Enum used to identify the tracker that needs to be used for tracking.
     *
     * A single tracker is usually enough for most purposes. In case you do need multiple trackers,
     * storing them all in Application object helps ensure that they are created only once per
     * application instance.
     */
    public enum TrackerName {
      APP_TRACKER, // Tracker used only in this app.
      //GLOBAL_TRACKER, // Tracker used by all the apps from a company. eg: roll-up tracking.
      ECOMMERCE_TRACKER, // Tracker used by all ecommerce transactions from a company.
    }

    HashMap<TrackerName, Tracker> mTrackers = new HashMap<TrackerName, Tracker>();
    
	private Tracker myTracker;

	//===========================================================
	// Google AdMob
	//===========================================================
	private AdView GoogleAdView;

	//===========================================================
	// Google Payment
	//===========================================================
    // The helper object
    IabHelper mIabHelper = null;
    boolean mIabHelperConnected = false;
    boolean mIsmPremiumNoAds = false;
    

	//===========================================================
	// VunglePub Video Ads
	//===========================================================
    // get the VunglePub instance
    final VunglePub vunglePub = VunglePub.getInstance();

	// create a new AdConfig object
	final AdConfig overrideConfig = new AdConfig();
	
	//===========================================================
	// SPLASH SCREEN
	//===========================================================
	private BitmapTextureAtlas mSplashTexture, mLoadingTexture, mTip1Texture, mTip2Texture, mTip3Texture;
	private ITextureRegion mSplashTextureRegion, mLoadingTextureRegion;
	private TextureRegion mTip1TextureRegion, mTip2TextureRegion, mTip3TextureRegion;
	
	private Scene mSplashScene;
	private Sprite KnivoreStudiosLogo, loadingSprite;
	private Text loadingText;
		
	//===========================================================
	// Game Screen
	//===========================================================
	private SensorManager sensorManager;
	private Handler mGameHandler;

    // whether we already loaded the state the first time (so we don't reload
    // every time the activity goes to the background and comes back to the foreground)
    boolean mAlreadyLoadedState = false;

	//===========================================================
	// Facebook
	//===========================================================
	protected FacebookManager facebookManager;
	protected boolean useFacebook;
	protected String fbUsername;
	
	//===========================================================
	// Methods for/from SuperClass/Interfaces
	//===========================================================

	public EngineOptions onCreateEngineOptions() {
		this.mGameHandler = new Handler();
		this.mCamera = new Camera(0, 0, CAMERA_WIDTH, CAMERA_HEIGHT);
		final EngineOptions engineOptions = new EngineOptions(true,ScreenOrientation.LANDSCAPE_FIXED, new RatioResolutionPolicy(CAMERA_WIDTH, CAMERA_HEIGHT), this.mCamera);
		engineOptions.getAudioOptions().setNeedsSound(true);
		engineOptions.getAudioOptions().setNeedsMusic(true);
		engineOptions.getTouchOptions().setNeedsMultiTouch(true);
		
		engineOptions.getTouchOptions().setTouchEventIntervalMilliseconds(5);
		if(MultiTouch.isSupported(this)) {
			if(!MultiTouch.isSupportedDistinct(this)) {
				//Toast.makeText(this, "MultiTouch detected, but your device has problems distinguishing between fingers.\n\nControls are placed at different vertical locations.", Toast.LENGTH_LONG).show();
				Toast.makeText(this, "MultiTouch detected, but your device has problems distinguishing between fingers.", Toast.LENGTH_LONG).show();
			}
		} else {
			//Toast.makeText(this, "Sorry your device does NOT support MultiTouch!\n\n(Falling back to SingleTouch.)\n\nControls are placed at different vertical locations.", Toast.LENGTH_LONG).show();
			Toast.makeText(this, "Sorry your device does NOT support MultiTouch!", Toast.LENGTH_LONG).show();
			this.finish();
		}
		return engineOptions;
	}

	public void onCreateResources(OnCreateResourcesCallback pOnCreateResourcesCallback)	throws Exception {
		// Load the resource class
		mGameResources = new GameResources(this.getTextureManager(), this.getFontManager(), this.getAssets(), this, this.getBaseContext(), this.getVertexBufferObjectManager(), this.mEngine, this.mCamera);
		mGameResources.setmSaveGame(new SaveGame(mGameResources));
		
		// Load the splash screen resources
		this.mSplashTexture = new BitmapTextureAtlas(this.getTextureManager(),1280,1000);
		this.mSplashTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mSplashTexture,this, "gfx/splash_screen.png", 0, 0);
		this.mSplashTexture.load();
		
		this.mLoadingTexture = new BitmapTextureAtlas(this.getTextureManager(),80,80);
		this.mLoadingTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mLoadingTexture,this, "gfx/loading.png", 0, 0);
		this.mLoadingTexture.load();

		this.mTip1Texture = new BitmapTextureAtlas(this.getTextureManager(),1163,680);
		this.mTip1TextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mTip1Texture,this, "gfx/Tip/GAME_TIPS_1.png", 0, 0);
		this.mTip1Texture.load();

		this.mTip2Texture = new BitmapTextureAtlas(this.getTextureManager(),1163,680);
		this.mTip2TextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mTip2Texture,this, "gfx/Tip/GAME_TIPS_2.png", 0, 0);
		this.mTip2Texture.load();

		this.mTip3Texture = new BitmapTextureAtlas(this.getTextureManager(),1163,680);
		this.mTip3TextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mTip3Texture,this, "gfx/Tip/GAME_TIPS_3.png", 0, 0);
		this.mTip3Texture.load();
		
		mGameResources.setmSplashTextureRegion(mSplashTextureRegion);
		mGameResources.setmLoadingTextureRegion(mLoadingTextureRegion);
		mGameResources.setmTip1TextureRegion(mTip1TextureRegion);
		mGameResources.setmTip2TextureRegion(mTip2TextureRegion);
		mGameResources.setmTip3TextureRegion(mTip3TextureRegion);
		pOnCreateResourcesCallback.onCreateResourcesFinished();
	}

	public void onCreateScene(OnCreateSceneCallback pOnCreateSceneCallback) throws Exception {        
		this.mEngine.registerUpdateHandler(new FPSLogger());
		
		// Declare the new splash scene
		mSplashScene = new Scene();

		// Center the splash on the camera.
		final float centerX = (CAMERA_WIDTH - mSplashTextureRegion.getWidth()) / 2;
		final float centerY = (CAMERA_HEIGHT - mSplashTextureRegion.getHeight()) / 2;

		// Declare the sensor manager that deals with the accelerometer for the GameScene
		sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
		
		// Create the sprite and add it to the scene.
		KnivoreStudiosLogo = new Sprite(centerX, centerY, mSplashTextureRegion, getVertexBufferObjectManager());
		KnivoreStudiosLogo.setBlendFunction(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);
		KnivoreStudiosLogo.setScale(0.5f);
		
		loadingSprite = new Sprite(1, CAMERA_HEIGHT - mLoadingTextureRegion.getHeight(), mLoadingTextureRegion, getVertexBufferObjectManager());
		loadingSprite.setScale(0.7f);
		
		loadingText = new Text(45, loadingSprite.getY() + 20, mGameResources.getmFont(), "", 1000, new TextOptions(HorizontalAlign.LEFT), getVertexBufferObjectManager());
		loadingText.setScale(0.6f);
		loadingText.setColor(Color.GREEN);
		mGameResources.setLoadingText(loadingText);
		
		FixedStepPhysicsWorld physicsWorld = new FixedStepPhysicsWorld(60, new Vector2(0, 0), true);
		this.mSplashScene.registerUpdateHandler(physicsWorld);
		final FixtureDef wallFixtureDef = PhysicsFactory.createFixtureDef(1, 0.5f, 2f);
		// Create dynamic body
		Body loadingSpriteBody = PhysicsFactory.createBoxBody(physicsWorld, loadingSprite, BodyType.DynamicBody, wallFixtureDef);
		// connect your sprite with physics body
		physicsWorld.registerPhysicsConnector(new PhysicsConnector(loadingSprite, loadingSpriteBody, true, true));
		loadingSpriteBody.applyAngularImpulse(5f);
        
		mSplashScene.setBackground(new Background(0f,0f,0f,0f));
		mSplashScene.attachChild(KnivoreStudiosLogo);
		mSplashScene.attachChild(loadingSprite);
		mSplashScene.attachChild(loadingText);
		
		options = new Options_SharedPreference(getBaseContext());
		
		new AsyncTaskLoader().execute(new IAsyncCallback() {
			@Override
			public void workToDo() {
				//Log.d(mGameResources.TAG, "Loading main menu & gameplay part 1 assets...");
				loadingText.setText("Loading Main Menu...");
				mGameResources.loadMainMenuResources();
				loadingText.setText("Loading Calibration...");
				mGameResources.loadCalibrationResources();

				//Load Tip 1
				final Sprite tip1 = new Sprite((CAMERA_WIDTH - mTip1TextureRegion.getWidth()) / 2, (CAMERA_HEIGHT - mTip1TextureRegion.getHeight()) / 2, mTip1TextureRegion, getVertexBufferObjectManager());
				tip1.setScale(0.85f);
				KnivoreStudiosLogo.detachSelf();
				mSplashScene.attachChild(tip1);
				mGameResources.loadGameResources_PART1();
				
				//Load Tip 2
				final Sprite tip2 = new Sprite((CAMERA_WIDTH - mTip2TextureRegion.getWidth()) / 2, (CAMERA_HEIGHT - mTip2TextureRegion.getHeight()) / 2, mTip2TextureRegion, getVertexBufferObjectManager());
				tip2.setScale(0.85f);
				tip1.detachSelf();
				mSplashScene.attachChild(tip2);
				mGameResources.loadGameResources_PART2();

				facebookManager = new FacebookManager(MainActivity.this);
				
				mSceneManager = new SceneManager(mEngine, sensorManager, mGameResources, getVertexBufferObjectManager(), mCamera, options, MainActivity.this, mContext);
				mSceneManager.loadMainMenuScene();
				mEngine.unregisterUpdateHandler(loadingSprite);
				mSplashScene.back();
				
				mGameResources.loadGameResources_PART3();
			}

			@Override
			public void onComplete() { }
		});

		pOnCreateSceneCallback.onCreateSceneFinished(mSplashScene);
	}

	public void onPopulateScene(Scene pScene, OnPopulateSceneCallback pOnPopulateSceneCallback) throws Exception {
		mEngine.start();

		// Google Analytics
		myTracker = getTracker(TrackerName.APP_TRACKER);
		
		pOnPopulateSceneCallback.onPopulateSceneFinished();
	}

	public GameHelper getGameHelper() {
		if (mGameHelper == null) {
			mGameHelper = new GameHelper(this, mRequestedClients);
			mGameHelper.enableDebugLog(mDebugLog);
		}
		return mGameHelper;
	}
	
	protected void enableDebugLog(boolean enabled) {
        mDebugLog = true;
        if (mGameHelper != null) {
        	mGameHelper.enableDebugLog(enabled);
        }
    }

	synchronized Tracker getTracker(TrackerName trackerId) {
		if (!mTrackers.containsKey(trackerId)) {

			GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
			//Tracker t = analytics.newTracker(R.string.analytics_tracking_id);
			Tracker t = analytics.newTracker("UA-40302183-2");
			mTrackers.put(trackerId, t);
		}
		return mTrackers.get(trackerId);
	}
	
	@Override
	public void onCreate(Bundle b) {
		super.onCreate(b);
		
		checkFaceBook();
		/*
		// get your App ID from the app's main page on the Vungle Dashboard
		// after setting up your app
		final String app_id = "54d32a0ba3401ecf3400000e";

		// initialize the Publisher SDK
		vunglePub.init(this, app_id);
		
		// get a handle on the global AdConfig object
		final AdConfig globalAdConfig = vunglePub.getGlobalAdConfig();

		// set any configuration options you like.
		// For a full description of available options, see the 'Configuration
		// Options' section.
		globalAdConfig.setSoundEnabled(true);
		globalAdConfig.setOrientation(Orientation.matchVideo);
		*/
		
		mContext = MainActivity.this;
        if (mGameHelper == null) {
            getGameHelper();
        }
        mGameHelper.setup(this);
	}
	
    @Override
    protected void onStart() {
        super.onStart();
        mGameHelper.onStart(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mGameHelper.onStop();
    }
    
    @Override
	public void onResume() {
		super.onResume();
        GooglePlayServicesUtil.isGooglePlayServicesAvailable(getApplicationContext());
	}

	@Override
	public void onPause() {
		if(mSceneManager != null) {
			if(mSceneManager.getCurrentScene().equals("GameScene")) {
				mSceneManager.pauseGame();
				mSceneManager.setCurrentScene("GamePauseScene");
			}
			if (options.getMusic() && mGameResources.getMainBGM().isPlaying())
				mGameResources.getMainBGM().pause();
		}
		super.onPause();
		GoogleAdView.pause();
	    //vunglePub.onPause();
	}
	
	@Override
	public void onResumeGame() {
		if(mSceneManager != null && mSceneManager.getCurrentScene().equals("GamePauseScene")) {
			super.onResumeGame();
		} else if(mSceneManager != null && !mSceneManager.getCurrentScene().equals("GamePauseScene")) {
			if (options.getMusic() && !mGameResources.getMainBGM().isPlaying())
				mGameResources.getMainBGM().play();
			super.onResumeGame();
		}
		GoogleAdView.resume();
	    //vunglePub.onResume();
	}

	@Override
	public void onPauseGame() {
		if(mSceneManager != null && mSceneManager.getCurrentScene().equals("GameScene")) {
			mSceneManager.pauseGame();
			mSceneManager.setCurrentScene("GamePauseScene");
			super.onPauseGame();
		}
	}
	
	@Override
	protected void onDestroy() {
	    super.onDestroy();
	    if (this.isGameLoaded()) {
	        System.exit(0);    
	    }
		GoogleAdView.destroy();
	}

	@Override
    public void onActivityResult(int requestCode, int responseCode, Intent intent) {
	    Log.d(mGameResources.TAG, "onActivityResult(" + requestCode + "," + responseCode + "," + intent);
	    /*
        if (requestCode == 1001) {
            int resultCode = intent.getIntExtra("RESPONSE_CODE", 0);
            String purchaseData = intent.getStringExtra("INAPP_PURCHASE_DATA");
            Log.d("INAPP_PURCHASE_DATA", ">>>" + purchaseData);
            String dataSignature = intent.getStringExtra("INAPP_DATA_SIGNATURE");
            Log.d("INAPP_DATA_SIGNATURE", ">>>" + dataSignature);
            String continuationToken = intent.getStringExtra("INAPP_CONTINUATION_TOKEN");
            Log.d("INAPP_CONTINUATION_TOKEN", ">>>" + continuationToken);
        }
	    */
	    
        if (mIabHelper == null) {
        	loadActivityResult(requestCode, responseCode, intent);
        	return;
        }
		// Pass on the activity result to the helper for handling
        if (!mIabHelper.handleActivityResult(requestCode, responseCode, intent)) {
            // not handled, so handle it ourselves (here's where you'd
            // perform any handling of activity results not related to in-app
            // billing...
        	loadActivityResult(requestCode, responseCode, intent);
    	}
        else {
            Log.d(mGameResources.TAG, "onActivityResult handled by IABUtil.");
        }
	}
	
	private void loadActivityResult(int requestCode, int responseCode, Intent intent) {
        super.onActivityResult(requestCode, responseCode, intent);
		if(mSceneManager != null) {
			if(mSceneManager.getCurrentScene().equals("FacebookScene")) {
				Session.getActiveSession().onActivityResult(this, requestCode, responseCode, intent);
			}
    	}
	    
        if (responseCode == GamesActivityResultCodes.RESULT_RECONNECT_REQUIRED)  {
        	mGameHelper.reconnectClient();
        } else {
        	mGameHelper.onActivityResult(requestCode, responseCode, intent);
        }
    	
    	if(multiplayerActivity) {
	    	switch (requestCode) {
	        case RC_SELECT_PLAYERS:
	            // we got the result from the "select players" UI -- ready to create the room
	            handleSelectPlayersResult(responseCode, intent);
	            break;
	        case RC_INVITATION_INBOX:
	            // we got the result from the "select invitation" UI (invitation inbox). We're
	            // ready to accept the selected invitation:
	            handleInvitationInboxResult(responseCode, intent);
	            break;
	        case RC_WAITING_ROOM:
	            // we got the result from the "waiting room" UI.
	            if (responseCode == Activity.RESULT_OK) {
	                // ready to start playing
	                Log.d(mGameResources.TAG, "Starting game (waiting room returned OK).");
	                
	                //Leader of this map will generate a random map and send to all participants
	                if (mParticipants != null) {
	                    if(mParticipants.get(0).getParticipantId().equals(mMyId)) {
	                    	byte[] message = new byte[] {'M', (byte) MathUtils.random(0,mGameResources.getCountryName().length - 1)} ;
	                    	sendReliableMessageRealTime(message);
	                    }
	                }

	                //Wait for 1 second to receive destinated map for all users
					mEngine.registerUpdateHandler(new TimerHandler(0.5f, false, new ITimerCallback() {
						@Override
						public void onTimePassed(final TimerHandler pTimerHandler) {
							mEngine.unregisterUpdateHandler(pTimerHandler);
							
							//If after 1 second and there's no destinated map, all users set to default map - Singapore
							if(DESTINATED_MAP.isEmpty())
								DESTINATED_MAP = mGameResources.getCountryName()[0];
							
							//Check again if there are participants in the room
			                if (mParticipants != null) {if(mParticipants.get(0).getParticipantId().equals(mMyId)) {
			                		mMultiplayerPlayer1GameScene = mSceneManager.loadMultiplayerPlayer1GameScene(mMultiXAngle, mMultiYAngle, DESTINATED_MAP);
			                	} else {
			                		mMultiplayerPlayer2GameScene = mSceneManager.loadMultiplayerPlayer2GameScene(mMultiXAngle, mMultiYAngle, DESTINATED_MAP);
			                	}
			                }
						}
					}));
	            } else if (responseCode == GamesActivityResultCodes.RESULT_LEFT_ROOM) {
	                // player indicated that they want to leave the room
	                leaveRoom();
	            } else if (responseCode == Activity.RESULT_CANCELED) {
	                // Dialog was cancelled (user pressed back key, for instance). In our game,
	                // this means leaving the room too. In more elaborate games, this could mean
	                // something else (like minimizing the waiting room UI).
	                leaveRoom();
	            }
	            break;
	    	}
    	}
	}
	
	@Override
	public boolean onSceneTouchEvent(Scene pScene, TouchEvent pSceneTouchEvent) {
		return false;
	}

	@Override                                                                
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
			Scene scene = this.mEngine.getScene();
			
			if(mSceneManager != null) {
				if(mSceneManager.getCurrentScene().equals("MainMenuScene")) {
					if(scene.hasChildScene()) {
						scene.clearChildScene();
					} else {
						AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
						builder.setTitle("Quit Game?")
								.setMessage("Are you sure you want to quit?")
								.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog, int which) {
								    	if(mGameHelper.isSignedIn() && mGameHelper.getApiClient().isConnected())
								    		saveSnapshot();
										endApplication();
									}
								})
								.setNegativeButton("No", null)
						.show();
					}
					
				} else if(mSceneManager.getCurrentScene().equals("MenuScrollerScene")) {
					if(scene.hasChildScene()) {
						scene.clearChildScene();
					} else {
						mSceneManager.loadMainMenuScene();
					}
					
				} else if(mSceneManager.getCurrentScene().equals("HangerScene")) {
					this.mCamera.setCenter(CAMERA_WIDTH / 2, CAMERA_HEIGHT / 2);
					if(scene.hasChildScene()) {
						scene.clearChildScene();
						mSceneManager.loadHangerScene();
					} else {						
						// Save data to Google
				    	if(mGameHelper.isSignedIn())
				    		saveSnapshot();
				    	
						mSceneManager.loadMainMenuScene();
						
				        //Log.d("Hanger - Google Purchase", "Destroying helper.");
				        if (mIabHelper != null) mIabHelper.dispose();
				        mIabHelper = null;
				        mIabHelperConnected = false;
					}
			        
				} else if(mSceneManager.getCurrentScene().equals("GameScene")) {
					mSceneManager.pauseGame();
					mSceneManager.setCurrentScene("GamePauseScene");
					
				} else if(mSceneManager.getCurrentScene().equals("GamePauseScene")) {
					mSceneManager.resumeGame();
					mSceneManager.setCurrentScene("GameScene");
				}
			}
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
	
	private void endApplication() {
		// Stop this main activity (End Application & Google Analytics)
		//myTracker.dispatch();
		MainActivity.this.finish();
	}
	
	@Override
	public boolean onAreaTouched(TouchEvent arg0, ITouchArea arg1, float arg2, float arg3) { return false; }

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) { }

	@Override
	public void onSensorChanged(SensorEvent event) { }


	//===========================================================
	//	Google Advertisment (AdMob)
	//===========================================================
    @Override
    protected void onSetContentView() {
        final FrameLayout frameLayout = new FrameLayout(this);
        final FrameLayout.LayoutParams frameLayoutLayoutParams = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT, Gravity.CENTER);

		GoogleAdView = new AdView(this);
		GoogleAdView.setAdUnitId("ca-app-pub-1418276812917770/5930664334");
		GoogleAdView.setAdSize(AdSize.BANNER);
		GoogleAdView.refreshDrawableState();

        final FrameLayout.LayoutParams adViewLayoutParams = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.WRAP_CONTENT, FrameLayout.LayoutParams.WRAP_CONTENT, Gravity.CENTER | Gravity.TOP);
        this.mRenderSurfaceView = new RenderSurfaceView(this);
        mRenderSurfaceView.setRenderer(mEngine, this);
        
        final android.widget.FrameLayout.LayoutParams surfaceViewLayoutParams = new FrameLayout.LayoutParams(super.createSurfaceViewLayoutParams());
        frameLayout.addView(this.mRenderSurfaceView, surfaceViewLayoutParams);
        frameLayout.addView(GoogleAdView, adViewLayoutParams);
        
        this.setContentView(frameLayout, frameLayoutLayoutParams);
    }
    
    public void showAdvertisement() {
    	mGameHandler.post(new Runnable() {
            @Override
            public void run() {
            	if(!mIsmPremiumNoAds) {
	                AdRequest.Builder adRequestBuilder = new AdRequest.Builder();
	                //adRequestBuilder.addTestDevice("************"); 
	                //adRequestBuilder.addTestDevice(AdRequest.DEVICE_ID_EMULATOR);
	                AdRequest adRequest = adRequestBuilder.build();
	                GoogleAdView.loadAd(adRequest);
	    		
	            	// If you want just hide the View. But it will retain space occupied by the View.
	            	GoogleAdView.setVisibility(AdView.VISIBLE);
	            	GoogleAdView.setEnabled(true);
	                //(AdView.GONE); //This will remove the View. and free the space occupied by the View 
            	}
            }
        });
    }

    public void hideAdvertisement() {
    	mGameHandler.post(new Runnable() {
            @Override
            public void run() {
            	if(GoogleAdView.getVisibility() == 0) {
	            	// If you want just hide the View. But it will retain space occupied by the View.
	            	GoogleAdView.setVisibility(AdView.INVISIBLE);
	                GoogleAdView.setEnabled(false);
	                //(AdView.GONE); //This will remove the View. and free the space occupied by the View
            	}
            }
        });
    }

    

    public void showVungleVideoAdvertisement() {
    	/*
    	mGameHandler.post(new Runnable() {
            @Override
            public void run() {
            	vunglePub.isCachedAdAvailable();
            	vunglePub.playAd(overrideConfig);
            }
        });
        */
    }
	
	//===========================================================
	// Facebook
	//===========================================================		
	public void facebookLogin() {
		if(mSceneManager != null)
	    	mSceneManager.setCurrentScene("FacebookScene");
	    
		Session.openActiveSession(this, true, new Session.StatusCallback() {
			@Override
			public void call(Session session, SessionState state, Exception exception) {
				if (session.isOpened()) {
					// make request to the /me API
			        Request.newMeRequest(session, new Request.GraphUserCallback() {
			        	// callback after Graph API response with user object
						@Override
						public void onCompleted(GraphUser user,	Response response) {
							if (user != null) {
								fbUsername = user.getUsername();
								useFacebook = true;
								if(mSceneManager != null)
									mSceneManager.updateFacebookButton(true);
		                    }
						}
			        }).executeAsync();
				}
			}
		});
	}
	
	public void facebookLoginThenPost(final String pRecord) {
		if(mSceneManager != null)
	    	mSceneManager.setCurrentScene("FacebookScene");
	    
		Session.openActiveSession(this, true, new Session.StatusCallback() {
			@Override
			public void call(Session session, SessionState state, Exception exception) {
				if (session.isOpened()) {
					// make request to the /me API
			        Request.newMeRequest(session, new Request.GraphUserCallback() {
			        	// callback after Graph API response with user object
						@Override
						public void onCompleted(GraphUser user,	Response response) {
							if (user != null) {
								fbUsername = user.getUsername();
								useFacebook = true;
								if(mSceneManager != null)
									mSceneManager.updateFacebookButton(true);
								
								facebookFeedDialog(pRecord);
		                    }
						}
			        }).executeAsync();
				}
			}
		});
	}
	
	public void facebookRequestDialog() {
        Bundle params = new Bundle();
        params.putString("message", fbUsername + " invited you to check out this game!");
 
		WebDialog requestsDialog = (new WebDialog.RequestsDialogBuilder(this, Session.getActiveSession(), params)).setOnCompleteListener(new OnCompleteListener() {
			@Override
			public void onComplete(Bundle values, FacebookException error) {
				if (error != null) {
					if (error instanceof FacebookOperationCanceledException) {     
		                        // Request cancelled
					} else {
		                        // Network Error
					}
		        } else {
		        	final String requestId = values.getString("request");
		        	if (requestId != null) {                       
		                        // Request sent
		        	} else {               
		                        // Request cancelled
		        	}
		        }
			}
		}).build();
		requestsDialog.show();
	}
	
	public boolean facebookFeedDialog(String pRecord) {
		Bundle params = new Bundle();
		//params.putString("name", fbUsername + " achieved a new record of " + pRecord + " at " + pWorld + " map");
		params.putString("name", "I have achieved a score of " + pRecord + "! Try beating that!");
		params.putString("caption", "We Are Not Alone");
		params.putString("link", "https://play.google.com/store/apps/details?id=com.andengine.invaders");
		params.putString("description", "Decide humanity fate now!");
		//params.putString("picture", "https://lh3.ggpht.com/gA1vmKasHsxk47L_6InKoakH5nvjSpePosEkVuuLL37-b_o20aqTbaKxLhDEn7tWO6yZ=w300-rw");

		final WebDialog feedDialog = (new WebDialog.FeedDialogBuilder(this, Session.getActiveSession(), params)).setOnCompleteListener(new OnCompleteListener() {
			@Override
			public void onComplete(Bundle values, FacebookException error) {
				if (error == null) {
					final String postId = values.getString("post_id");
					if (postId != null) {
						// POSTED
						runOnUiThread(new Runnable() {
	        	    		public void run() {
	        	        		mGameHelper.makeSimpleDialog("Earn 10 Gold!").show();
	        	    		}
	            		});
	            		Score_SharedPreference highScores = new Score_SharedPreference(mGameResources.getmBaseContext(),"");
	                	highScores.saveTotalObtainedGold(10);
	                	mSceneManager.setFacebookShared(true);
					} else {
						// POST CANCELLED
					}
				} else if (error instanceof FacebookOperationCanceledException) {
					// POST CANCELLED
				} else {
					// ERROR POSTING
				}
			}
		}).build();
		feedDialog.show();
		return mAlreadyLoadedState;
	}
	
	public void checkFaceBook() {
		Session.openActiveSession(this, false, new Session.StatusCallback() {
			@Override
			public void call(Session session, SessionState state, Exception exception) {
				if (session.isOpened()) {
					Request.newMeRequest(session, new Request.GraphUserCallback() {
						@Override
						public void onCompleted(GraphUser user, Response response) {
							if (user != null) {
								fbUsername = user.getUsername();
								useFacebook = true;
								if(mSceneManager != null)
									mSceneManager.updateFacebookButton(true);
							}
						}
					}).executeAsync();
				}
			}
		});
	}
	
	//Adding Open Graph features, Set up actions and objects & post on the timeline
	public void postOnTFacebookTimeline() {
		Bundle params = new Bundle();
		params.putString("your object", "your object url");

		Request request = new Request(Session.getActiveSession(), "me/<your app namespace>:<your action>", params, HttpMethod.POST, new Callback() {
			@Override
			public void onCompleted(Response response) {
				if (response.getError() == null) {
					// post successful
				} else {
					// Error occured
				}
			}
		});
		request.executeAndWait();
	}

	public void checkLikeStatus() {
		final Session session = Session.getActiveSession();
		if (session != null) {
			Request.Callback callback = new Request.Callback() {

				@Override
				public void onCompleted(Response response) {
					if (response.getGraphObject() != null) {
						try {
							if (!response.getGraphObject().getInnerJSONObject().getString("data").equalsIgnoreCase("[]")) {
								// Give credit, disable ads or whatever here                                                           
							}
						} catch (JSONException e) {
							Log.d("JSONException", e.getMessage());
						}
					}
				}
			};
			Request request = new Request(session, "me/likes/" + "YOURPAGEID", null, HttpMethod.GET, callback);
			RequestAsyncTask task = new RequestAsyncTask(request);
			task.execute();
		}
	}

	public void facebookLogout() {
		Session activeSession = Session.getActiveSession();
		if (activeSession != null) {
			activeSession.closeAndClearTokenInformation();
			useFacebook = false;
			if(mSceneManager != null)
				mSceneManager.updateFacebookButton(false);
		}
	}

	//===========================================================
	// Google Sign in & Save Game
	//===========================================================
	@Override
	public void onSignInFailed() {
        // Sign-in has failed. So show the user the sign-in button so they can click the "Sign-in" button.
		//Log.d(mGameResources.TAG, "Sign-in failed. Showing sign-in button.");
		if(mGameResources != null) mGameResources.setmGameHelper(mGameHelper);
		if(mSceneManager != null) mSceneManager.updateGoogleButton(false);
	}
	
	@Override
	public void onSignInSucceeded() {
        // register listener so we are notified if we receive an invitation to play while we are in the game
        Games.Invitations.registerInvitationListener(mGameHelper.getApiClient(), this);
		
        if(mGameResources == null) {
        	// Load the resource class
    		mGameResources = new GameResources(this.getTextureManager(), this.getFontManager(), this.getAssets(), this, this.getBaseContext(), this.getVertexBufferObjectManager(), this.mEngine, this.mCamera);
    		mGameResources.setmSaveGame(new SaveGame(mGameResources));    		
        }
		mGameResources.setmGameHelper(mGameHelper);
		mGameResources.setmAchievement(new AchievementContainer(mGameResources, false));
		
		loadIAB();
		
		//Log.d(mGameResources.TAG, "Sign-in successful! Loading game state from cloud.");
		if (!mAlreadyLoadedState && mGameHelper.isSignedIn() && mGameHelper.getApiClient().isConnected()) {
            loadFromSnapshot();
    	    Games.Quests.registerQuestUpdateListener(mGameHelper.getApiClient(), this);

    		new AsyncTaskLoader().execute(new IAsyncCallback() {
    			@Override
    			public void workToDo() {
    				try {
    					if(mGameHelper.getApiClient().isConnected()) {
    						accountName = Plus.AccountApi.getAccountName(mGameHelper.getApiClient());
    						accountID = GoogleAuthUtil.getAccountId(mGameResources.getmBaseContext(), accountName);
    					}
    				} catch (GoogleAuthException e) {
    					e.printStackTrace();
    				} catch (IOException e) {
    					e.printStackTrace();
    				}
    			}

    			@Override
    			public void onComplete() {
    				if(accountName != null || !accountName.isEmpty()) {
	    				mGameResources.setGoogleAccountID(accountID);
	    				mGameResources.setGoogleAccountName(accountName);
	
	    				/*
	    				// set any configuration options you like.
	    				// For a full description of available options, see the
	    				// 'Configuration Options' section.
	    				overrideConfig.setIncentivized(true);
	    				overrideConfig.setIncentivizedUserId(mGameResources.getGoogleAccountID());
	    				overrideConfig.setSoundEnabled(false);
	    				overrideConfig.setBackButtonImmediatelyEnabled(false);
	    				
	    				vunglePub.setEventListener(new EventListener(){
	
	    					@Override
	    					public void onAdEnd(boolean arg0) { }
	
	    					@Override
	    					public void onAdStart() { }
	
	    					@Override
	    					public void onAdUnavailable(String arg0) { }
	
	    					@Override
	    					public void onCachedAdAvailable() { }
	
	    					@Override
	    					public void onVideoView(boolean isCompletedView, int watchedMillis, int videoDurationMillis) {
	    						// Called each time an ad completes. isCompletedView is true if at least
	    				        // 80% of the video was watched, which constitutes a completed view.  
	    				        // watchedMillis is for the longest video view (if the user replayed the video).
	    						if(isCompletedView) {
	    		                	runOnUiThread(new Runnable() {
	    		        	    		public void run() {
	    		        	        		mGameHelper.makeSimpleDialog("Earn 20 Gold!").show();
	    		        	    		}
	    		            		});
	    		            		Score_SharedPreference highScores = new Score_SharedPreference(mGameResources.getmBaseContext(),"");
	    		                	highScores.saveTotalObtainedGold(20);
	    						}
	    					}
	    				});
	    				*/
    				}
    			}
    		});
        }
		
		// Sign-in worked!
		if(mSceneManager != null) mSceneManager.updateGoogleButton(true);
	}
	
    void loadFromSnapshot() {
        AsyncTask<Void, Void, Integer> task = new AsyncTask<Void, Void, Integer>() {
            @Override
            protected Integer doInBackground(Void... params) {
                Snapshots.OpenSnapshotResult result;
                
                if(mGameHelper.getApiClient().isConnected()) {
	                //Log.i(mGameResources.TAG, "Opening snapshot by name: " + mGameResources.currentSaveName);
	                result = Games.Snapshots.open(mGameHelper.getApiClient(), mGameResources.currentSaveName, true).await();
	                
	                int status = result.getStatus().getStatusCode();
	
	                Snapshot snapshot = null;
	                if (status == GamesStatusCodes.STATUS_OK) {
	                    snapshot = result.getSnapshot();
	                } else if (status == GamesStatusCodes.STATUS_SNAPSHOT_CONFLICT) {
	                    // if there is a conflict  - then resolve it.
	                    snapshot = processSnapshotOpenResult(result, 0);
	
	                    // if it resolved OK, change the status to Ok
	                    if (snapshot != null) {
	                        status = GamesStatusCodes.STATUS_OK;
	                    }
	                    else {
	                        Log.e(mGameResources.TAG,"Conflict was not resolved automatically");
	                    }
	                } else {
	                    Log.e(mGameResources.TAG, "Error while loading snapshot: " + status);
	                }
	
	                if (snapshot != null) {
	                    readSavedGame(snapshot);
	                }
	                return status;
                } else 
                	return 1;
            }

            @Override
            protected void onPostExecute(Integer status) {
                Log.i(mGameResources.TAG, "Snapshot loaded: " + status);

                if (status == GamesStatusCodes.STATUS_SNAPSHOT_NOT_FOUND) {
                    Log.i(mGameResources.TAG, "Error: Snapshot not found");
                } else if (status == GamesStatusCodes.STATUS_SNAPSHOT_CONTENTS_UNAVAILABLE) {
                    Log.i(mGameResources.TAG, "Error: Snapshot contents unavailable");
                } else if (status == GamesStatusCodes.STATUS_SNAPSHOT_FOLDER_UNAVAILABLE) {
                    Log.i(mGameResources.TAG, "Error: Snapshot folder unavailable");
                }
            }
        };
        task.execute();
    }

    private void readSavedGame(Snapshot snapshot) {
		try {
			mGameResources.setmSaveGame(new SaveGame(mGameResources, snapshot.getSnapshotContents().readFully()));
			mAlreadyLoadedState = true;
		} catch (IOException e) {
			e.printStackTrace();
		}
    }

    /**
     * Conflict resolution for when Snapshots are opened.
     *
     * @param requestCode - the request currently being processed.  This is used to forward on the
     *                    information to another activity, or to send the result intent.
     * @param result The open snapshot result to resolve on open.
     * @param retryCount - the current iteration of the retry.  The first retry should be 0.
     * @return The opened Snapshot on success; otherwise, returns null.
     */
    Snapshot processSnapshotOpenResult(Snapshots.OpenSnapshotResult result, int retryCount) {
        Snapshot mResolvedSnapshot = null;
    	retryCount++;
    	
    	if(result != null) {
	        int status = result.getStatus().getStatusCode();
	
	        //Log.i(mGameResources.TAG, "Save Result status: " + status);
	
	        //Games.Snapshots.delete(mGameResources.getmGameHelper().getApiClient(), result.getSnapshot().getMetadata());
	        //Games.Snapshots.delete(mGameResources.getmGameHelper().getApiClient(), result.getConflictingSnapshot().getMetadata());
	        
	        if (status == GamesStatusCodes.STATUS_OK) {
	            return result.getSnapshot();
	        } else if (status == GamesStatusCodes.STATUS_SNAPSHOT_CONTENTS_UNAVAILABLE) {
	            return result.getSnapshot();
	        } else if (status == GamesStatusCodes.STATUS_SNAPSHOT_CONFLICT) {
	            final Snapshot snapshot = result.getSnapshot();
	            final Snapshot conflictSnapshot = result.getConflictingSnapshot();
	
	            // Resolve between conflicts by selecting the newest of the conflicting snapshots.
	            mResolvedSnapshot = snapshot;
	        	
	            if(snapshot.getMetadata().getLastModifiedTimestamp() < conflictSnapshot.getMetadata().getLastModifiedTimestamp()) {
	            	mResolvedSnapshot = conflictSnapshot;
	            }
	
	            //Log.e(mGameResources.TAG, "Resolving conflicts " + retryCount);
	            Snapshots.OpenSnapshotResult resolveResult = Games.Snapshots.resolveConflict(mGameHelper.getApiClient(), mGameResources.currentSaveName, mResolvedSnapshot).await();
	        	if (retryCount < MAX_SNAPSHOT_RESOLVE_RETRIES) {
	                return processSnapshotOpenResult(resolveResult, retryCount);
	            } else {
	                //Log.e(mGameResources.TAG, "Could not resolve snapshot conflicts");
	                //Toast.makeText(getBaseContext(), message, Toast.LENGTH_LONG);
	            }
	        }
    	}
        // Fail, return null.
        return null;
    }
    
    /**
     * Prepares saving Snapshot to the user's synchronized storage, conditionally resolves errors,
     * and stores the Snapshot.
     */
    void saveSnapshot() {
        AsyncTask<Void, Void, Snapshots.OpenSnapshotResult> task = new AsyncTask<Void, Void, Snapshots.OpenSnapshotResult>() {
			@Override
			protected Snapshots.OpenSnapshotResult doInBackground(Void... params) {
				if(mGameHelper.getApiClient().isConnected())
					return Games.Snapshots.open(mGameHelper.getApiClient(), mGameResources.currentSaveName, true).await();
				else 
					return null;
			}

			@Override
			protected void onPostExecute(Snapshots.OpenSnapshotResult result) {
				Snapshot toWrite = processSnapshotOpenResult(result, 0);
				
				if(toWrite != null) {
					String snapshotResult = writeSnapshot(toWrite);
					Log.i(mGameResources.TAG, snapshotResult);
				} else {
					//myTracker.setScreenName("Main Activity");
					//myTracker.send(new HitBuilders.EventBuilder("Save Snapshot", "Snapshot null when attempting to save").build());
					
					//Presume to be no changes made.
					Log.i(mGameResources.TAG, "Snapshot null when attempting to save");
				}
			}
		};

        task.execute();
    }

    /**
     * Generates metadata, takes a screenshot, and performs the write operation for saving a
     * snapshot.
     */
    private String writeSnapshot(Snapshot snapshot) {
        // Set the data payload for the snapshot.
        snapshot.getSnapshotContents().writeBytes(mGameResources.getmSaveGame().toBytes());

        // Save the snapshot.
        SnapshotMetadataChange metadataChange = new SnapshotMetadataChange.Builder()
                //.setCoverImage(getScreenShot())
                .setDescription("Modified data at: " + Calendar.getInstance().getTime())
                .build();
        Games.Snapshots.commitAndClose(mGameHelper.getApiClient(), snapshot, metadataChange);
        return snapshot.toString();
    }

	//===========================================================
	// Multiplayer
	//===========================================================    
    public void setMultiplayerActivity(boolean multiplayerActivity) {
		this.multiplayerActivity = multiplayerActivity;
	}

	@Override
	public void onInvitationReceived(Invitation invitation) {
        // We got an invitation to play a game! So, store it in
        // mIncomingInvitationId and show the popup on the screen.
        mIncomingInvitationId = invitation.getInvitationId();
	}

	@Override
	public void onInvitationRemoved(String invitationId) {
        if (mIncomingInvitationId.equals(invitationId)) {
            mIncomingInvitationId = null;
        }
	}

	@Override
	public void onJoinedRoom(int statusCode, Room room) {
        Log.d(mGameResources.TAG, "onJoinedRoom(" + statusCode + ", " + room + ")");
        if (statusCode != GamesStatusCodes.STATUS_OK) {
            Log.e(mGameResources.TAG, "*** Error: onRoomConnected, status " + statusCode);
            showGameError();
            return;
        }

        // show the waiting room UI
        showWaitingRoom(room);
	}

	@Override
	public void onLeftRoom(int statusCode, String roomId) {
        // we have left the room; return to main screen.
        Log.d(mGameResources.TAG, "onLeftRoom, code " + statusCode);
        mSceneManager.loadMainMenuScene();
	}

	@Override
	public void onRoomConnected(int statusCode, Room room) {
        Log.d(mGameResources.TAG, "onRoomConnected(" + statusCode + ", " + room + ")");
        if (statusCode != GamesStatusCodes.STATUS_OK) {
            Log.e(mGameResources.TAG, "*** Error: onRoomConnected, status " + statusCode);
            showGameError();
            return;
        }
        updateRoom(room);
	}

	@Override
	public void onRoomCreated(int statusCode, Room room) {
        Log.d(mGameResources.TAG, "onRoomCreated(" + statusCode + ", " + room + ")");
        if (statusCode != GamesStatusCodes.STATUS_OK) {
            Log.e(mGameResources.TAG, "*** Error: onRoomCreated, status " + statusCode);
            showGameError();
            return;
        }

        // show the waiting room UI
        showWaitingRoom(room);
	}

	@Override
	public void onConnectedToRoom(Room room) {
		Log.d(mGameResources.TAG, "onConnectedToRoom.");

        // get room ID, participants and my ID:
        mRoomId = room.getRoomId();
        mParticipants = room.getParticipants();
        mMyId = room.getParticipantId(Games.Players.getCurrentPlayerId(mGameHelper.getApiClient()));

        // print out the list of participants (for debug purposes)
        Log.d(mGameResources.TAG, "Room ID: " + mRoomId);
        Log.d(mGameResources.TAG, "My ID " + mMyId);
        Log.d(mGameResources.TAG, "<< CONNECTED TO ROOM>>");
	}

	@Override
	public void onDisconnectedFromRoom(Room room) {
        mRoomId = null;
        showGameError();
	}

	@Override
	public void onP2PConnected(String participantId) { }

	@Override
	public void onP2PDisconnected(String participantId) { }

	@Override
	public void onPeerDeclined(Room room, List<String> arg1) {
        updateRoom(room);
	}

	@Override
	public void onPeerInvitedToRoom(Room room, List<String> arg1) {
        updateRoom(room);
	}

	@Override
	public void onPeerJoined(Room room, List<String> arg1) {
        updateRoom(room);
	}

	@Override
	public void onPeerLeft(Room room, List<String> arg1) {
        updateRoom(room);
	}

	@Override
	public void onPeersConnected(Room room, List<String> arg1) {
        updateRoom(room);
	}

	@Override
	public void onPeersDisconnected(Room room, List<String> arg1) {
        updateRoom(room);
	}

	@Override
	public void onRoomAutoMatching(Room room) {
        updateRoom(room);
	}

	@Override
	public void onRoomConnecting(Room room) {
        updateRoom(room);
	}

	@Override
	public void onRealTimeMessageReceived(RealTimeMessage message) {
		byte[] buf = message.getMessageData();
		String sender = message.getSenderParticipantId();

		if ((char)buf[0] == 'M') {
			DESTINATED_MAP = mGameResources.getCountryName()[(int)buf[1]];
		}
		else {
			if(mMultiplayerPlayer1GameScene != null) mMultiplayerPlayer1GameScene.onRealTimeMessageReceived(message);
			if(mMultiplayerPlayer2GameScene != null) mMultiplayerPlayer2GameScene.onRealTimeMessageReceived(message);
		}
	}

	public void sendReliableMessageRealTime(byte[] message) {
		for (Participant p : mParticipants) {
		    if (!p.getParticipantId().equals(mMyId) && mRoomId != null) {
		        Games.RealTimeMultiplayer.sendReliableMessage(mGameHelper.getApiClient(), null, message, mRoomId, p.getParticipantId());
		    }
		}
	}
	
	public void sendUnreliableMessageRealTime(byte[] message) {
		for (Participant p : mParticipants) {
		    if (!p.getParticipantId().equals(mMyId) && mRoomId != null) {
		        Games.RealTimeMultiplayer.sendUnreliableMessageToOthers(mGameHelper.getApiClient(), message, mRoomId);
		    }
		}
	}
	
    // Handle the result of the "Select players UI" we launched when the user clicked the
    // "Invite friends" button. We react by creating a room with those players.
    private void handleSelectPlayersResult(int response, Intent data) {
        if (response != Activity.RESULT_OK) {
            Log.w(mGameResources.TAG, "*** select players UI cancelled, " + response);
			mSceneManager.loadMainMenuScene();
            return;
        }

        Log.d(mGameResources.TAG, "Select players UI succeeded.");

        // get the invitee list
        final ArrayList<String> invitees = data.getStringArrayListExtra(Games.EXTRA_PLAYER_IDS);
        Log.d(mGameResources.TAG, "Invitee count: " + invitees.size());

        // get the automatch criteria
        Bundle autoMatchCriteria = null;
        int minAutoMatchPlayers = data.getIntExtra(Multiplayer.EXTRA_MIN_AUTOMATCH_PLAYERS, 1);
        int maxAutoMatchPlayers = data.getIntExtra(Multiplayer.EXTRA_MAX_AUTOMATCH_PLAYERS, 1);
        if (minAutoMatchPlayers > 0 || maxAutoMatchPlayers > 0) {
            autoMatchCriteria = RoomConfig.createAutoMatchCriteria(minAutoMatchPlayers, maxAutoMatchPlayers, 0);
            Log.d(mGameResources.TAG, "Automatch criteria: " + autoMatchCriteria);
        }

        // create the room
        Log.d(mGameResources.TAG, "Creating room...");
        RoomConfig.Builder rtmConfigBuilder = RoomConfig.builder(this);
        rtmConfigBuilder.addPlayersToInvite(invitees);
        rtmConfigBuilder.setMessageReceivedListener(this);
        rtmConfigBuilder.setRoomStatusUpdateListener(this);
        if (autoMatchCriteria != null) {
            rtmConfigBuilder.setAutoMatchCriteria(autoMatchCriteria);
        }
        
        mSceneManager.loadLoadingScene();
        keepScreenOn();

        if(mGameHelper.isSignedIn()) {
            Games.RealTimeMultiplayer.create(mGameHelper.getApiClient(), rtmConfigBuilder.build());
            Log.d(mGameResources.TAG, "Room created, waiting for it to be ready...");
        } else {
    		runOnUiThread(new Runnable() {
	    		public void run() {
	        		mGameHelper.makeSimpleDialog(getString(R.string.signin_to_join)).show();
	    		}
    		});
    	}
    }
    
	// Handle the result of the invitation inbox UI, where the player can pick an invitation
    // to accept. We react by accepting the selected invitation, if any.
    private void handleInvitationInboxResult(int response, Intent data) {
        if (response != Activity.RESULT_OK) {
            Log.w(mGameResources.TAG, "*** invitation inbox UI cancelled, " + response);
			mSceneManager.loadMainMenuScene();
            return;
        }

        Log.d(mGameResources.TAG, "Invitation inbox UI succeeded.");
        Invitation inv = data.getExtras().getParcelable(Multiplayer.EXTRA_INVITATION);

        // accept invitation
        acceptInviteToRoom(inv.getInvitationId());
    }
    
    // Show the waiting room UI to track the progress of other players as they enter the
    // room and get connected.
    void showWaitingRoom(Room room) {
        // minimum number of players required for our game
        // For simplicity, we require everyone to join the game before we start it
        // (this is signaled by Integer.MAX_VALUE).
        final int MIN_PLAYERS = Integer.MAX_VALUE;
        Intent i = Games.RealTimeMultiplayer.getWaitingRoomIntent(mGameHelper.getApiClient(), room, MIN_PLAYERS);

        // show waiting room UI
        if(mGameHelper.isSignedIn())
        	startActivityForResult(i, RC_WAITING_ROOM);
    	else {
    		runOnUiThread(new Runnable() {
	    		public void run() {
	        		mGameHelper.makeSimpleDialog(getString(R.string.signin_to_join)).show();
	    		}
    		});
    	}
    }
    
	void startQuickGame(float xAngle, float yAngle) {
		mMultiXAngle = xAngle;
		mMultiYAngle = yAngle;
        // quick-start a game with 1 randomly selected opponent
        final int MIN_OPPONENTS = 1, MAX_OPPONENTS = 1;
        Bundle autoMatchCriteria = RoomConfig.createAutoMatchCriteria(MIN_OPPONENTS, MAX_OPPONENTS, 0);
        RoomConfig.Builder rtmConfigBuilder = RoomConfig.builder(this);
        rtmConfigBuilder.setMessageReceivedListener(this);
        rtmConfigBuilder.setRoomStatusUpdateListener(this);
        rtmConfigBuilder.setAutoMatchCriteria(autoMatchCriteria);
        //keepScreenOn();

        if(mGameHelper.isSignedIn())
        	Games.RealTimeMultiplayer.create(mGameHelper.getApiClient(), rtmConfigBuilder.build());
    	else {
    		runOnUiThread(new Runnable() {
	    		public void run() {
	        		mGameHelper.makeSimpleDialog(getString(R.string.signin_to_join)).show();
	    		}
    		});
    	}
    }
	
	// Accept the given invitation.
    void acceptInviteToRoom(String invId) {
        // accept the invitation
        Log.d(mGameResources.TAG, "Accepting invitation: " + invId);
        RoomConfig.Builder roomConfigBuilder = RoomConfig.builder(this);
        roomConfigBuilder.setInvitationIdToAccept(invId)
                .setMessageReceivedListener(this)
                .setRoomStatusUpdateListener(this);
        keepScreenOn();
        
        if(mGameHelper.isSignedIn())
            Games.RealTimeMultiplayer.join(mGameHelper.getApiClient(), roomConfigBuilder.build());
    	else {
    		runOnUiThread(new Runnable() {
	    		public void run() {
	        		mGameHelper.makeSimpleDialog(getString(R.string.signin_to_join)).show();
	    		}
    		});
    	}
    }
    
    void updateRoom(Room room) {
        if (room != null) {
            mParticipants = room.getParticipants();
        }
        if (mParticipants != null) {
            //TODO: Update scores or ??
        	
        }
    }
    
    // Leave the room.
    void leaveRoom() {
        Log.d(mGameResources.TAG, "Leaving room.");
        stopKeepingScreenOn();
        if (mRoomId != null) {
            Games.RealTimeMultiplayer.leave(mGameHelper.getApiClient(), this, mRoomId);
            mRoomId = null;
        } else {
			mSceneManager.loadMainMenuScene();
        }
		multiplayerActivity = false;
    }
    
	// Sets the flag to keep this screen on. It's recommended to do that during
    // the handshake when setting up a game, because if the screen turns off, the
    // game will be cancelled.
    void keepScreenOn() {
        //getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    // Clears the flag that keeps the screen on.
    void stopKeepingScreenOn() {
        //getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }
    
    // Show error message about game being cancelled and return to main screen.
    void showGameError() {
		runOnUiThread(new Runnable() {
    		public void run() {
    	    	mGameHelper.makeSimpleDialog(getString(R.string.game_problem)).show();
    		}
		});
	
		mSceneManager.loadMainMenuScene();
    }

	//===========================================================
	// Quest
	//===========================================================
	@Override
	public void onQuestCompleted(Quest quest) {
		// TODO Auto-generated method stub
		
	}

	//===========================================================
	// Query Purchases
	//===========================================================
	public IabHelper loadIAB() {
		if(mIabHelper == null) {
	        mIabHelper = mGameResources.getmIabHelper();
	        
			// Start setup. This is asynchronous and the specified listener will be called once setup completes.
	        //Log.d(mGameResources.TAG, "Starting IAB setup.");
	        mIabHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
				@Override
				public void onIabSetupFinished(IabResult result) {
	                //Log.d(mGameResources.TAG, "IAB Setup finished.");
	
	                if (!result.isSuccess()) {
	                    // Problem occured
	                	//Log.e(mGameResources.TAG, "**** Hanger Purchase Error: Problem setting up in-app billing: " + result);
	                	mIabHelper = null;
	                    return;
	                }
	
	        		List<String> additionalSkuList = mGameResources.getAdditionalSkuList();
	        		
	                // Hooray, IAB is fully set up. Now, let's get an inventory of stuff we own.
	                //Log.d(mGameResources.TAG, "Setup successful. Querying inventory.");
	                mIabHelper.queryInventoryAsync(true, additionalSkuList, mGotInventoryListener);
	            }
	        });
		}
		return mIabHelper;
	}
	
	// Listener that's called when we finish querying the items we own
    IabHelper.QueryInventoryFinishedListener mGotInventoryListener = new IabHelper.QueryInventoryFinishedListener() {
		@Override
		public void onQueryInventoryFinished(IabResult result, Inventory inventory) {
            //Log.d(mGameResources.TAG, "Query inventory finished.");
            
            // Have we been disposed of in the meantime? If so, quit.
            if (mIabHelper == null) return;
            
            if (result.isFailure()) {
            	//Log.e(mGameResources.TAG, "**** Hanger Purchase Error: Failed to query inventory: " + result);
                return;
            }

            //Log.d(mGameResources.TAG, "Query inventory was successful.");
            
            //Check for items we own. Notice that for each purchase, we check
            //the developer payload to see if it's correct! See verifyDeveloperPayload().
            mGameResources.getProductDetailsList().clear();
            for(String product : mGameResources.getAdditionalSkuList()) {
            	mGameResources.getProductDetailsList().add(inventory.getSkuDetails(product));
                //Log.d(mGameResources.TAG, product);
                //Log.d(mGameResources.TAG, inventory.getSkuDetails(product).toString());
                //Log.d(mGameResources.TAG, mGameResources.getProductDetailsList().toString());

                Purchase purchase = inventory.getPurchase(product);
                if(product != mGameResources.getmPremiumNoAds()) {
	            	if(purchase != null && verifyDeveloperPayload(purchase)) {
	            		int goldPurchased = Integer.parseInt(product.replaceAll("[\\D]", ""));
	            		Score_SharedPreference highScores = new Score_SharedPreference(mGameResources.getmBaseContext(),"");
	                	highScores.saveTotalObtainedGold(-goldPurchased);
	            	}
                }
            }
            
            // Do we have the premium upgrade?
            Purchase premiumAdsFreePurchase = inventory.getPurchase(mGameResources.getmPremiumNoAds());
            mIsmPremiumNoAds = (premiumAdsFreePurchase != null && verifyDeveloperPayload(premiumAdsFreePurchase));
        	mGameResources.getmSaveGame().setPremiumNoAds(mIsmPremiumNoAds);
            Log.d(mGameResources.TAG, "User require " + (mIsmPremiumNoAds ? "NO ADVERTISMENT" : "ADVERTISMENT"));
            hideAdvertisement();
            showAdvertisement();
            mIabHelperConnected = true;
        }
    };

    /** Verifies the developer payload of a purchase. */
    boolean verifyDeveloperPayload(Purchase p) {
        String payload = p.getDeveloperPayload();
        
    	if(mGameHelper.isSignedIn()) {
            String storedPayload = mGameResources.getGoogleAccountID();
            //Log.d(mGameResources.TAG, "storedPayload : " + storedPayload);
            //Log.d(mGameResources.TAG, "payload : " + payload);
            //Log.d(mGameResources.TAG, "purchaseState : " + p.getPurchaseState());
            //Log.d(mGameResources.TAG, "storedPayload.trim() != payload.trim() : " + (storedPayload.trim() != payload.trim()));
            //Log.d(mGameResources.TAG, "p.getPurchaseState() != 0 : " + (p.getPurchaseState() != 0));
            //Log.d(mGameResources.TAG, "(storedPayload.trim() != payload.trim() || p.getPurchaseState() != 0) : " + (storedPayload.trim() != payload.trim() || p.getPurchaseState() != 0));
    		if(!storedPayload.trim().equals(payload.trim())) {
	    		//saveSnapshot();
	    		return false;
    		}
    		if(p.getPurchaseState() != 0) {
                //Log.d(mGameResources.TAG, "purchaseState : " + p.getPurchaseState());
	    		return false;
    		}
    	} else {
			connectionReTries++;
            //Log.d(mGameResources.TAG, "Not connected to GameHelper. Pending connection retries - " + connectionReTries);
    		if(connectionReTries < 4) {
    			mGameHelper.reconnectClient();
    			verifyDeveloperPayload(p);
    		} else {
    			connectionReTries = 0;
        		return false;
    		}
    	}
    	
        return true;
    }
}