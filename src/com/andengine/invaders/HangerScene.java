/*
 * Copyright (C) 2013 Knivore Studios
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.andengine.invaders;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import javax.microedition.khronos.opengles.GL10;

import org.andengine.engine.Engine;
import org.andengine.engine.camera.Camera;
import org.andengine.entity.modifier.AlphaModifier;
import org.andengine.entity.scene.IOnSceneTouchListener;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.sprite.AnimatedSprite;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.text.AutoWrap;
import org.andengine.entity.text.Text;
import org.andengine.entity.text.TextOptions;
import org.andengine.input.touch.TouchEvent;
import org.andengine.input.touch.detector.ScrollDetector;
import org.andengine.input.touch.detector.ScrollDetector.IScrollDetectorListener;
import org.andengine.input.touch.detector.SurfaceScrollDetector;
import org.andengine.opengl.texture.region.TiledTextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.HorizontalAlign;
import org.andengine.util.color.Color;
import org.andengine.util.math.MathUtils;

import android.hardware.SensorManager;
import android.opengl.GLES20;
import android.view.MotionEvent;

import com.andengine.invaders.MainActivity.TrackerName;
import com.andengine.sharedpreferences.Gadgets_SharedPreference;
import com.andengine.sharedpreferences.Options_SharedPreference;
import com.andengine.sharedpreferences.Score_SharedPreference;
import com.andengine.sharedpreferences.Spacecraft_SharedPreference;
import com.android.vending.billing.util.IabHelper;
import com.android.vending.billing.util.IabResult;
import com.android.vending.billing.util.Purchase;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.achievement.Achievement;
import com.google.android.gms.games.achievement.AchievementBuffer;
import com.google.android.gms.games.achievement.Achievements;
import com.google.android.gms.games.quest.Quests;
import com.makersf.andengine.extension.collisions.entity.sprite.PixelPerfectAnimatedSprite;

/**
 * @author Keh Chin Leong
 */
public class HangerScene implements IScrollDetectorListener, IOnSceneTouchListener {

	//===========================================================
	// Constants
	//===========================================================
	private static final int CAMERA_WIDTH = 1280;
	private static final int CAMERA_HEIGHT = 720;

	private static int PADDINGX = 380;
	private static int PADDINGY = 200;
	
	private boolean mBackButtonPressed = false;
	private boolean mUFOButtonPressed = false;
	private boolean mGadgetButtonPressed = false;
	private boolean mCashButtonPressed = false;
	private boolean mAchievementPressed = false;
	private boolean mProfilePressed = false;
	private boolean mCreditsPressed = false;
	private boolean mShopPressed = false;
	private boolean mMainMenuPressed = false;
	private boolean mHangerHelpPressed = false;
	private boolean mMissionPressed = false;
	private boolean mGetMoreGoldPressed = false;	
	
	private Tracker myTracker;
	private MainActivity mMainActivity;

	//private float afterScrollingDistanceY = 0;

	private SceneManager mSceneManager;
	private Scene mHangerScene, mProfileScene, mGadgetScene, mUFOScene, mAchievementScene, mSubAchievementScene, mPurchaseScene, mCreditsScene, mCashPurchaseScene, mMissionScene, mHelpScene;
	private Camera mCamera;
	private GameResources mGameResources;
	private VertexBufferObjectManager mVertexBufferObjectManager;

	//===========================================================
	// Hanger Screen
	//===========================================================
	private AnimatedSprite G_play_icon, UFO_Icon_Sprite, Gadget_Icon_Sprite, Cash_Icon_Sprite;
	private Sprite hangerBackground, washOutBackground, displayPanel, mBackButton, You_Have_Icon, subBackground, sub_SubBackground, subHeader, achievement_icon;
	private Text obtainedGold;

	// Options Sharedpreference
	private Options_SharedPreference options;
	private boolean sfx_on;

	// Scrolling and Item Clicked detector
	private SurfaceScrollDetector mScrollDetector;
	private boolean onItemClicked;
	
	private Score_SharedPreference highScores;
	private List<Sprite> purchaseButtonList, selectedToDisplayList, gadgetList;
	private Sprite[] equippedGadgetList;
	
	private float mMinY = 0;
	private float mMaxY = 0;
	private float mCurrentY = 0;
	
	private float spriteX = 0;
	private float spriteY = 0;
	
	
	//===========================================================
	// Billing
	//===========================================================    
    boolean mIsmPremiumNoAds = false;
    
    // (arbitrary) request code for the purchase flow
    static final int RC_REQUEST = 1001;
    
    // Current amount
    int mValue;
	
	//===========================================================
	// Constructor
	//===========================================================
	public HangerScene(Engine engine, SensorManager sensor, GameResources gr, VertexBufferObjectManager vbom, Camera c, Options_SharedPreference opSharedPreference, SceneManager sm, MainActivity mainActivity) {
		mGameResources = gr;
		mVertexBufferObjectManager = vbom;
		mCamera = c;
		mSceneManager = sm;
		mMainActivity = mainActivity;

		myTracker = mainActivity.getTracker(TrackerName.APP_TRACKER);
		
		mScrollDetector = new SurfaceScrollDetector(this);
		
		// Load Score Shared Preference
		highScores = new Score_SharedPreference(mGameResources.getmBaseContext(),"");
		
		options = new Options_SharedPreference(mGameResources.getmBaseContext());
		sfx_on = options.getSoundEffects();
	}

	public Scene getmHangerScene() {
		return mHangerScene;
	}

	public Scene getmProfileScene() {
		return mProfileScene;
	}

	public Scene getmGadgetScene() {
		return mGadgetScene;
	}

	public Scene getmUFOScene() {
		return mUFOScene;
	}

	public Scene getmAchievementScene() {
		return mAchievementScene;
	}

	public Scene getmMissionScene() {
		return mMissionScene;
	}	
	
	public Scene getmPurchaseScene() {
		return mPurchaseScene;
	}
	
	public Scene getmCashPurchaseScene() {
		return mCashPurchaseScene;
	}

	private void loadCommonDisplay(Scene mScene) {
		spriteX = CAMERA_WIDTH - mGameResources.getmCreditsScreenTextureRegion().getWidth();
		spriteY = (CAMERA_HEIGHT - mGameResources.getmProfileSceneBackgroundTextureRegion().getHeight()) / 2;
		
		subBackground = new Sprite(spriteX, spriteY, mGameResources.getmProfileSceneBackgroundTextureRegion(), mVertexBufferObjectManager);
		//subBackground.setScale(0.9f);		
		
		mBackButton = new Sprite(10, 10, mGameResources.getmCommonBackButtonTextureRegion(), mVertexBufferObjectManager) {
			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent,float pTouchAreaLocalX, float pTouchAreaLocalY) {
				if(pSceneTouchEvent.isActionDown()) {
					this.setScale(0.9f);
					mBackButtonPressed = true;
				}
				
				if(pSceneTouchEvent.isActionUp()) {
					if(mBackButtonPressed) {
						if(sfx_on) mGameResources.getButtonSFX().play();
						mBackButtonPressed = false;
						this.setScale(1f);
						mCamera.setCenter(CAMERA_WIDTH / 2, CAMERA_HEIGHT / 2);
						if(mHangerScene.hasChildScene()) {
							mHangerScene.clearChildScene();
							mSceneManager.loadHangerScene();
						} else {
							mSceneManager.loadMainMenuScene();
						}
					}
				}
				return true;
			}
		};
		
		washOutBackground = new Sprite(0, 0, mGameResources.getmWashoutBackgroundTextureRegion(), mVertexBufferObjectManager);
		
		mScene.attachChild(washOutBackground);
		mScene.attachChild(mBackButton);		
		mScene.registerTouchArea(mBackButton);
		
		mScene.setOnSceneTouchListener(this);
		mScene.setOnAreaTouchTraversalFrontToBack();
		mScene.setTouchAreaBindingOnActionDownEnabled(true);
		mScene.setOnSceneTouchListenerBindingOnActionDownEnabled(true);
	}
	
	public Scene loadHangerScene() {
		mMainActivity.showAdvertisement();
		
		mMinY = 0;
		mMaxY = 0;
		mCurrentY = 0;
		
		mHangerScene = new Scene();
		mHangerScene.setTouchAreaBindingOnActionDownEnabled(true);
		
		hangerBackground = new Sprite(0, 0, mGameResources.getmHangerBackgroundTextureRegion(), mVertexBufferObjectManager);
		
		AnimatedSprite ufo_window = new AnimatedSprite(310, 0, mGameResources.getmUFOWindowDisplayTextureRegion(), mVertexBufferObjectManager);
		ufo_window.setCurrentTileIndex(MathUtils.random(0,1));
		
		AnimatedSprite gadget_window = new AnimatedSprite(686, 0, mGameResources.getmGadgetWindowDisplayTextureRegion(), mVertexBufferObjectManager);
		gadget_window.setCurrentTileIndex(MathUtils.random(0,1));
		
		Sprite mission_window = new Sprite(1062, 70, mGameResources.getmMissionDisplayTextureRegion(), mVertexBufferObjectManager){
			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent,float pTouchAreaLocalX, float pTouchAreaLocalY) {
				if(pSceneTouchEvent.isActionDown()) {
					mMissionPressed = true;
				}
				
				if(pSceneTouchEvent.isActionUp()) {
					//if(sfx_on) mGameResources.getButtonSFX().play();
					if(mMissionPressed) {
						mMissionPressed = false;
						//mHangerScene.setChildScene(loadMissionScene());
					}
				}
				return true;
			}
		};		
		Sprite mission_spot1 = new Sprite(MathUtils.random(1060,1160), MathUtils.random(210,300), mGameResources.getmMissionSpotTextureRegion(), mVertexBufferObjectManager);
		mission_spot1.registerEntityModifier(new AlphaModifier(MathUtils.random(160,200), 0, 255));
		Sprite mission_spot2 = new Sprite(MathUtils.random(1080,1210), MathUtils.random(270,315), mGameResources.getmMissionSpotTextureRegion(), mVertexBufferObjectManager);
		mission_spot2.registerEntityModifier(new AlphaModifier(MathUtils.random(160,200), 0, 255));
		Sprite mission_spot3 = new Sprite(MathUtils.random(1120,1200), MathUtils.random(270,300), mGameResources.getmMissionSpotTextureRegion(), mVertexBufferObjectManager);
		mission_spot3.registerEntityModifier(new AlphaModifier(MathUtils.random(160,200), 0, 255));
		
		AnimatedSprite shop_icon = new AnimatedSprite(450, 450, mGameResources.getmHangerShopIconTextureRegion(), mVertexBufferObjectManager) {
			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent,float pTouchAreaLocalX, float pTouchAreaLocalY) {
				if(pSceneTouchEvent.isActionDown()) {
					this.setScale(0.9f);
					mShopPressed = true;
				}
				
				if(pSceneTouchEvent.isActionUp()) {
					if(sfx_on) mGameResources.getButtonSFX().play();
					if(mShopPressed) {
						//if(sfx_on) mGameResources.getButtonSFX().play();
						mShopPressed = false;
						this.setScale(1f);
						mHangerScene.setChildScene(loadPurchaseScene(false));
					}
				}
				return true;
			}
		};
		shop_icon.setCurrentTileIndex(MathUtils.random(1,3));

		// Achievement Scene
		achievement_icon = new Sprite(55, 208.5f, mGameResources.getmAchievementIconTextureRegion(), mVertexBufferObjectManager)	{
			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent,float pTouchAreaLocalX, float pTouchAreaLocalY) {
				if(pSceneTouchEvent.isActionDown()) {
					this.setScale(0.9f);
					mAchievementPressed = true;
				}
				
				if(pSceneTouchEvent.isActionUp()) {
					if(sfx_on) mGameResources.getButtonSFX().play();
					if(mAchievementPressed) {
						//if(sfx_on) mGameResources.getButtonSFX().play();
						mAchievementPressed = false;
						this.setScale(1f);
						if(mGameResources.getmGameHelper().isSignedIn()) {
							mHangerScene.setChildScene(loadAchievementScene(0,false));							
						} else {
							mMainActivity.runOnUiThread(new Runnable() {
								public void run() {
									mGameResources.getmGameHelper().makeSimpleDialog(mGameResources.getmBaseContext().getString(R.string.signin_for_achievements)).show();
								}
							});
				    	}
					}
				}
				return true;
			}
		};
		
		Sprite profile_icon = new Sprite(173.6f, 85, mGameResources.getmProfileIconTextureRegion(), mVertexBufferObjectManager)	{
			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent,float pTouchAreaLocalX, float pTouchAreaLocalY) {
				if(pSceneTouchEvent.isActionDown()) {
					this.setScale(0.9f);
					mProfilePressed = true;
				}
				
				if(pSceneTouchEvent.isActionUp()) {
					if(sfx_on) mGameResources.getButtonSFX().play();
					if(mProfilePressed) {
						//if(sfx_on) mGameResources.getButtonSFX().play();
						mProfilePressed = false;
						this.setScale(1f);
						mHangerScene.setChildScene(loadProfileScene(0,false));
					}
				}
				return true;
			}
		};
		
		AnimatedSprite credits_icon = new AnimatedSprite(300, 270, mGameResources.getmCreditsIconTextureRegion(), mVertexBufferObjectManager)	{
			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent,float pTouchAreaLocalX, float pTouchAreaLocalY) {
				if(pSceneTouchEvent.isActionDown()) {
					this.setScale(0.9f);
					mCreditsPressed = true;
				}
				
				if(pSceneTouchEvent.isActionUp()) {
					if(sfx_on) mGameResources.getButtonSFX().play();
					if(mCreditsPressed) {
						//if(sfx_on) mGameResources.getButtonSFX().play();
						mCreditsPressed = false;
						this.setScale(1f);
						mHangerScene.setChildScene(loadCreditsScene());
					}
				}
				return true;
			}
		};
		if(new Random().nextBoolean()) {
			credits_icon.setCurrentTileIndex(0);
			//credits_icon.animate(new long[] {MathUtils.random(555, 999), MathUtils.random(555, 999), MathUtils.random(555, 999)}, 0, 2, true);
			credits_icon.animate(new long[] {MathUtils.random(555, 999), MathUtils.random(555, 950)}, 0, 1, true);
		}
		else {
			credits_icon.setCurrentTileIndex(3);
			//credits_icon.animate(new long[] {MathUtils.random(555, 999), MathUtils.random(555, 999), MathUtils.random(555, 999)}, 3, 5, true);
			credits_icon.animate(new long[] {MathUtils.random(555, 999), MathUtils.random(555, 999)}, 3, 4, true);
		}
		
		/*
		// Leaderboard Scene
		Sprite LeaderboardButtonSprite = new Sprite(CAMERA_WIDTH - mGameResources.getmHighScoreButtonTextureRegion().getWidth(), CAMERA_HEIGHT - mGameResources.getmHighScoreButtonTextureRegion().getHeight() - 95, mGameResources.getmHighScoreButtonTextureRegion(), mVertexBufferObjectManager) {
			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent,float pTouchAreaLocalX, float pTouchAreaLocalY) {
				if(pSceneTouchEvent.getAction() == MotionEvent.ACTION_UP){
					loadLeaderboardScene();
					//mMenuScene.setChildScene(mLeaderboardScene, false, true, true);
				}
				return true;
			}
		};
		*/
		
		AnimatedSprite mBackToMainMenuButton = new AnimatedSprite(953.7f, 70, mGameResources.getmHangerBackIconTextureRegion(), mVertexBufferObjectManager) {
			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent,float pTouchAreaLocalX, float pTouchAreaLocalY) {
				if(pSceneTouchEvent.isActionDown()) {
					this.setScale(0.9f);
					mMainMenuPressed = true;
				}
				
				if(pSceneTouchEvent.isActionUp()) {
					if(sfx_on) mGameResources.getButtonSFX().play();
					if(mMainMenuPressed) {
						mMainMenuPressed = false;
						this.setScale(1f);
						
						mCamera.setCenter(CAMERA_WIDTH / 2, CAMERA_HEIGHT / 2);
						if(mHangerScene.hasChildScene()) {
							mHangerScene.clearChildScene();
							mSceneManager.loadHangerScene();
						} else {
							// Save data to Google
					    	if(mGameResources.getmGameHelper().isSignedIn())
					    		mMainActivity.saveSnapshot();
					    	
							mSceneManager.loadMainMenuScene();
							
					        //Log.d("Hanger - Google Purchase", "Destroying helper.");
					        if (mMainActivity.mIabHelper != null) mMainActivity.mIabHelper.dispose();
					        mMainActivity.mIabHelper = null;
					        mMainActivity.mIabHelperConnected = false;
						}
					}
				}
				return true;
			}
		};
		mBackToMainMenuButton.animate(650);

		Sprite NPC1 = new Sprite(810, 300, mGameResources.getNPCList().get(0), mVertexBufferObjectManager);
		Sprite NPC2 = new Sprite(940, 260, mGameResources.getNPCList().get(1), mVertexBufferObjectManager);
		Sprite NPC3 = new Sprite(850, 495, mGameResources.getNPCList().get(2), mVertexBufferObjectManager);
		Sprite NPC4 = new Sprite(970, 475, mGameResources.getNPCList().get(3), mVertexBufferObjectManager);
		Sprite NPC5 = new Sprite(210, 500, mGameResources.getNPCList().get(4), mVertexBufferObjectManager) {
			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent,float pTouchAreaLocalX, float pTouchAreaLocalY) {
				if(pSceneTouchEvent.isActionDown()) {
					this.setScale(0.9f);
					mHangerHelpPressed = true;
				}
				
				if(pSceneTouchEvent.isActionUp()) {
					if(sfx_on) mGameResources.getButtonSFX().play();
					if(mHangerHelpPressed) {
						mHangerHelpPressed = false;
						this.setScale(1f);

						mHangerScene.setChildScene(loadHelpScene());
					}
				}
				return true;
			}
		};
		Sprite NPC6 = new Sprite(780, 180, mGameResources.getNPCList().get(5), mVertexBufferObjectManager);
				
		mHangerScene.attachChild(hangerBackground);
		mHangerScene.attachChild(ufo_window);
		mHangerScene.attachChild(gadget_window);
		mHangerScene.attachChild(achievement_icon);
		mHangerScene.attachChild(profile_icon);
		mHangerScene.attachChild(credits_icon);
		mHangerScene.attachChild(shop_icon);
		mHangerScene.attachChild(mission_window);
		mHangerScene.attachChild(mission_spot1);
		mHangerScene.attachChild(mission_spot2);
		mHangerScene.attachChild(mission_spot3);
		mHangerScene.attachChild(NPC1);
		mHangerScene.attachChild(NPC2);
		mHangerScene.attachChild(NPC3);
		mHangerScene.attachChild(NPC4);
		mHangerScene.attachChild(NPC5);
		mHangerScene.attachChild(NPC6);
		mHangerScene.attachChild(mBackToMainMenuButton);

		mHangerScene.registerTouchArea(achievement_icon);
		mHangerScene.registerTouchArea(profile_icon);
		mHangerScene.registerTouchArea(shop_icon);
		mHangerScene.registerTouchArea(credits_icon);
		mHangerScene.registerTouchArea(mission_window);
		mHangerScene.registerTouchArea(NPC5);
		mHangerScene.registerTouchArea(mBackToMainMenuButton);
		
		mHangerScene.setBackgroundEnabled(false);
		return mHangerScene;
	}
	
	private Scene loadAchievementScene(float distanceY, boolean reload) {
		mMainActivity.hideAdvertisement();
		
		mAchievementScene = new Scene();
		mSubAchievementScene = new Scene();
		loadCommonDisplay(mAchievementScene);
		
		//Attached a google play icon and register the touch area to open google's achievements intent
		G_play_icon = new AnimatedSprite(CAMERA_WIDTH - (mGameResources.getmGooglePlayIconTextureRegion().getWidth() + 20), 44, mGameResources.getmGooglePlayIconTextureRegion(), mVertexBufferObjectManager) {
			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent,float pTouchAreaLocalX, float pTouchAreaLocalY) {					
				if(pSceneTouchEvent.getAction() == MotionEvent.ACTION_DOWN) {
					this.setCurrentTileIndex(1);
				} else if(pSceneTouchEvent.getAction() == MotionEvent.ACTION_UP) {
					this.setCurrentTileIndex(0);
					mMainActivity.runOnUiThread(new Runnable() {
					    public void run() {
					    	if(mGameResources.getmGameHelper().isSignedIn())
					    		mMainActivity.startActivityForResult(Games.Achievements.getAchievementsIntent(mGameResources.getmGameHelper().getApiClient()),5001);
			            	else
			            		mGameResources.getmGameHelper().makeSimpleDialog(mGameResources.getmBaseContext().getString(R.string.signin_for_achievements)).show();
					    }
					});
				}
				return true;
			}
		};
		G_play_icon.setScale(0.8f);

		sub_SubBackground = new Sprite(spriteX, (CAMERA_HEIGHT - mGameResources.getmProfileSubSceneBackgroundTextureRegion().getHeight()) / 2, mGameResources.getmProfileSubSceneBackgroundTextureRegion(), mVertexBufferObjectManager);
		subHeader = new Sprite(spriteX, spriteY + mGameResources.getmAchievementSubheaderTextureRegion().getHeight(), mGameResources.getmAchievementSubheaderTextureRegion(), mVertexBufferObjectManager);
		//subHeader.setScale(0.9f);
		
		mAchievementScene.attachChild(subBackground);
		AchievementBuffer buf = null;
		int bufSize = 0;
		if(mGameResources.getmGameHelper().isSignedIn()) {
			//Get number of achievements from G server
			boolean fullLoad = false;  // set to 'true' to reload all achievements (ignoring cache)
			long waitTime = 60;    // seconds to wait for achievements to load before timing out
			
			Achievements.LoadAchievementsResult r = (Achievements.LoadAchievementsResult)Games.Achievements.load(mGameResources.getmGameHelper().getApiClient(), fullLoad).await(waitTime, TimeUnit.SECONDS);
			buf = r.getAchievements();		
			bufSize = buf.getCount();
			
		} else {
    		mGameResources.getmGameHelper().makeSimpleDialog(mGameResources.getmBaseContext().getString(R.string.signin_for_achievements)).show();
    	}
		
		spriteX += 100;
		spriteY += 125;
		for(int i=0; i<bufSize; i++) {
			Achievement achievement = buf.get(i);

			// here you now have access to the achievement's data
			String id = achievement.getAchievementId();
			Long value = achievement.getXpValue();
			String name = achievement.getName();
			String description = achievement.getDescription();
			
			boolean unlocked = achievement.getState() == Achievement.STATE_UNLOCKED;  // is unlocked
			boolean incremental = achievement.getType() == Achievement.TYPE_INCREMENTAL;  // is incremental
			if (incremental) {
				int steps = achievement.getCurrentSteps();  // current incremental steps
			}
			
			if(value >= 0 && value <= 999) {
				AnimatedSprite bronze_trophy = new AnimatedSprite(spriteX, spriteY, mGameResources.getmAchievementTrophiesTextureRegion(), mVertexBufferObjectManager);
				if(unlocked)
					bronze_trophy.setCurrentTileIndex(1);
				else
					bronze_trophy.setCurrentTileIndex(0);
					
				mSubAchievementScene.attachChild(bronze_trophy);
				
			} else if(value >= 1000 && value <= 1499) {
				AnimatedSprite silver_trophy = new AnimatedSprite(spriteX, spriteY, mGameResources.getmAchievementTrophiesTextureRegion(), mVertexBufferObjectManager);
				if(unlocked)
					silver_trophy.setCurrentTileIndex(3);
				else
					silver_trophy.setCurrentTileIndex(2);
				
				mSubAchievementScene.attachChild(silver_trophy);
				
			} else if(value >= 1500 && value <= 1999) {
				AnimatedSprite gold_trophy = new AnimatedSprite(spriteX, spriteY, mGameResources.getmAchievementTrophiesTextureRegion(), mVertexBufferObjectManager);
				if(unlocked)
					gold_trophy.setCurrentTileIndex(5);
				else
					gold_trophy.setCurrentTileIndex(4);
				
				mSubAchievementScene.attachChild(gold_trophy);
			} else {
				//TODO: Create a platinum trophy image
				AnimatedSprite platinum_trophy = new AnimatedSprite(spriteX, spriteY, mGameResources.getmAchievementTrophiesTextureRegion(), mVertexBufferObjectManager);
				if(unlocked)
					platinum_trophy.setCurrentTileIndex(5);
				else
					platinum_trophy.setCurrentTileIndex(4);
				
				mSubAchievementScene.attachChild(platinum_trophy);
			}
			Text achievementName = new Text(spriteX + 210, spriteY + 15, mGameResources.getmFont(), name, 1000, new TextOptions(AutoWrap.WORDS, subBackground.getWidth() - (spriteX + 200), HorizontalAlign.LEFT), mVertexBufferObjectManager);
			achievementName.setScale(1.25f);
			achievementName.setColor(Color.PINK);
			mSubAchievementScene.attachChild(achievementName);
			
			Text achievementDescription = new Text(spriteX + 180, spriteY + 55, mGameResources.getmFont(), description, 1000, new TextOptions(AutoWrap.WORDS, subBackground.getWidth() - (spriteX + 200), HorizontalAlign.LEFT), mVertexBufferObjectManager);
			mSubAchievementScene.attachChild(achievementDescription);
			
			spriteY += 150;
			mMaxY = spriteY - CAMERA_HEIGHT;
		}
		
		if(buf != null)
			buf.close();
		
		mSubAchievementScene.setBackgroundEnabled(false);
		mAchievementScene.setBackgroundEnabled(false);
		mAchievementScene.attachChild(mSubAchievementScene);
		mAchievementScene.attachChild(sub_SubBackground);
		mAchievementScene.attachChild(subHeader);
		mAchievementScene.attachChild(G_play_icon);
		mAchievementScene.registerTouchArea(G_play_icon);
		
		return mAchievementScene;
	}
	
	private Scene loadProfileScene(float distanceY, boolean reload) {
		mMainActivity.hideAdvertisement();
		
		mProfileScene = new Scene();
		Scene mSubProfileScene = new Scene();
		loadCommonDisplay(mProfileScene);
		
		sub_SubBackground = new Sprite(spriteX, (CAMERA_HEIGHT - mGameResources.getmProfileSub2SceneBackgroundTextureRegion().getHeight()) / 2, mGameResources.getmProfileSub2SceneBackgroundTextureRegion(), mVertexBufferObjectManager);
		//Attached a google play icon and register the touch area to open google's achievements intent
		G_play_icon = new AnimatedSprite(CAMERA_WIDTH - (mGameResources.getmGooglePlayIconTextureRegion().getWidth() + 20), 40, mGameResources.getmGooglePlayIconTextureRegion(), mVertexBufferObjectManager) {
			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent,float pTouchAreaLocalX, float pTouchAreaLocalY) {					
				if(pSceneTouchEvent.getAction() == MotionEvent.ACTION_DOWN) {
					this.setCurrentTileIndex(1);
				} else if(pSceneTouchEvent.getAction() == MotionEvent.ACTION_UP) { 
					//if(sfx_on) mGameResources.getButtonSFX().play();
					this.setCurrentTileIndex(0);
					mMainActivity.runOnUiThread(new Runnable() {
					    public void run() {
					    	if(mGameResources.getmGameHelper().isSignedIn())
					    		mMainActivity.startActivityForResult(Games.Leaderboards.getAllLeaderboardsIntent(mGameResources.getmGameHelper().getApiClient()),5001);
			            		//mMainActivity.startActivityForResult(Games.Leaderboards.getLeaderboardIntent(mGameResources.getmGameHelper().getApiClient(), mGameResources.mainLeaderboardID),5001);
			            	else
			            		mGameResources.getmGameHelper().makeSimpleDialog(mGameResources.getmBaseContext().getString(R.string.signin_for_leaderboard)).show();
					    }
					});
				}
				return true;
			}
		};
		G_play_icon.setScale(0.8f);
				
		AnimatedSprite personal_best = new AnimatedSprite(spriteX, spriteY += 50, mGameResources.getmProfileSceneTextTextureRegion(), mVertexBufferObjectManager);
		personal_best.setCurrentTileIndex(0);

		Score_SharedPreference singaporeHighScores = new Score_SharedPreference(mGameResources.getmBaseContext(), "Singapore");
		Score_SharedPreference sydneyHighScores = new Score_SharedPreference(mGameResources.getmBaseContext(), "Sydney");
		
		Text bestScore = new Text(spriteX + 65, spriteY += 55, mGameResources.getmFont(), "BEST SCORE (PER GAME)", 1000, new TextOptions(AutoWrap.WORDS, subBackground.getWidth() - (spriteX + 200), HorizontalAlign.LEFT), mVertexBufferObjectManager);
		Text bestScoreValue = new Text(spriteX + 1000, spriteY, mGameResources.getmFont(), singaporeHighScores.loadMaxDistancePerCountry() > sydneyHighScores.loadMaxDistancePerCountry() ? "" + singaporeHighScores.loadMaxDistancePerCountry() + "m" : "" + sydneyHighScores.loadMaxDistancePerCountry() + "m", 1000, new TextOptions(AutoWrap.WORDS, subBackground.getWidth() - (spriteX + 200), HorizontalAlign.LEFT), mVertexBufferObjectManager);
		Text mostGoldCollected = new Text(spriteX + 65, spriteY += 55, mGameResources.getmFont(), "MOST GOLD COLLECTED (PER GAME)", 1000, new TextOptions(AutoWrap.WORDS, subBackground.getWidth() - (spriteX + 200), HorizontalAlign.LEFT), mVertexBufferObjectManager);
		Text mostGoldCollectedValue = new Text(spriteX + 1000, spriteY, mGameResources.getmFont(), singaporeHighScores.loadMaxGoldPerCountry() > sydneyHighScores.loadMaxGoldPerCountry() ? "" + singaporeHighScores.loadMaxGoldPerCountry() : "" + sydneyHighScores.loadMaxGoldPerCountry(), 1000, new TextOptions(AutoWrap.WORDS, subBackground.getWidth() - (spriteX + 200), HorizontalAlign.LEFT), mVertexBufferObjectManager);
		Text mostHumanAbsorbed = new Text(spriteX + 65, spriteY += 55, mGameResources.getmFont(), "MOST HUMAN ABSORBED (PER GAME)", 1000, new TextOptions(AutoWrap.WORDS, subBackground.getWidth() - (spriteX + 200), HorizontalAlign.LEFT), mVertexBufferObjectManager);
		Text mostHumanAbsorbedValue = new Text(spriteX + 1000, spriteY, mGameResources.getmFont(), singaporeHighScores.loadMaxAbsorbedHumansPerCountry() > sydneyHighScores.loadMaxAbsorbedHumansPerCountry() ? "" + singaporeHighScores.loadMaxAbsorbedHumansPerCountry() : "" + sydneyHighScores.loadMaxAbsorbedHumansPerCountry(), 1000, new TextOptions(AutoWrap.WORDS, subBackground.getWidth() - (spriteX + 200), HorizontalAlign.LEFT), mVertexBufferObjectManager);
		Text mostHumanAnnihilated = new Text(spriteX + 65, spriteY += 55, mGameResources.getmFont(), "MOST HUMAN ANNIHILATED (PER GAME)", 1000, new TextOptions(AutoWrap.WORDS, subBackground.getWidth() - (spriteX + 200), HorizontalAlign.LEFT), mVertexBufferObjectManager);
		Text mostHumanAnnihilatedValue = new Text(spriteX + 1000, spriteY, mGameResources.getmFont(), singaporeHighScores.loadMaxDisintegratedHumansPerCountry() > sydneyHighScores.loadMaxDisintegratedHumansPerCountry() ? "" + singaporeHighScores.loadMaxDisintegratedHumansPerCountry() : "" + sydneyHighScores.loadMaxDisintegratedHumansPerCountry(), 1000, new TextOptions(AutoWrap.WORDS, subBackground.getWidth() - (spriteX + 200), HorizontalAlign.LEFT), mVertexBufferObjectManager);
		
		//AnimatedSprite vehical_runs = new AnimatedSprite(spriteX, spriteY += 60, mGameResources.getmProfileSceneTextTextureRegion(), mVertexBufferObjectManager);
		//vehical_runs.setCurrentTileIndex(1);

		//AnimatedSprite total = new AnimatedSprite(spriteX, spriteY += 60, mGameResources.getmProfileSceneTextTextureRegion(), mVertexBufferObjectManager);
		//total.setCurrentTileIndex(2);
		
		//AnimatedSprite gadgets = new AnimatedSprite(spriteX, spriteY += 60, mGameResources.getmProfileSceneTextTextureRegion(), mVertexBufferObjectManager);
		//gadgets.setCurrentTileIndex(3);

		AnimatedSprite deaths = new AnimatedSprite(spriteX, spriteY += 60, mGameResources.getmProfileSceneTextTextureRegion(), mVertexBufferObjectManager);
		deaths.setCurrentTileIndex(4);
		
		Text deathCount = new Text(spriteX + 65, spriteY += 55, mGameResources.getmFont(), "TOTAL DEATH COUNT", 1000, new TextOptions(AutoWrap.WORDS, subBackground.getWidth() - (spriteX + 200), HorizontalAlign.LEFT), mVertexBufferObjectManager);
		int SGTotalDeathCount = singaporeHighScores.loadDeathByLaserStrike() + singaporeHighScores.loadDeathByMissile() + singaporeHighScores.loadDeathByScatterMissile();
		int SYTotalDeathCount = sydneyHighScores.loadDeathByLaserStrike() + sydneyHighScores.loadDeathByMissile() + sydneyHighScores.loadDeathByScatterMissile();
		Text deathCountValue = new Text(spriteX + 1000, spriteY, mGameResources.getmFont(), "" + (SGTotalDeathCount + SYTotalDeathCount), 1000, new TextOptions(AutoWrap.WORDS, subBackground.getWidth() - (spriteX + 200), HorizontalAlign.LEFT), mVertexBufferObjectManager);
		Text laserDeathCount = new Text(spriteX + 65, spriteY += 55, mGameResources.getmFont(), "LASER STRIKE DEATH COUNT", 1000, new TextOptions(AutoWrap.WORDS, subBackground.getWidth() - (spriteX + 200), HorizontalAlign.LEFT), mVertexBufferObjectManager);
		Text laserDeathCountValue = new Text(spriteX + 1000, spriteY, mGameResources.getmFont(), "" + (singaporeHighScores.loadDeathByLaserStrike() + sydneyHighScores.loadDeathByLaserStrike()), 1000, new TextOptions(AutoWrap.WORDS, subBackground.getWidth() - (spriteX + 200), HorizontalAlign.LEFT), mVertexBufferObjectManager);
		Text missileDeathCount = new Text(spriteX + 65, spriteY += 55, mGameResources.getmFont(), "MISSILE DEATH COUNT", 1000, new TextOptions(AutoWrap.WORDS, subBackground.getWidth() - (spriteX + 200), HorizontalAlign.LEFT), mVertexBufferObjectManager);
		Text missileDeathCountValue = new Text(spriteX + 1000, spriteY, mGameResources.getmFont(), "" + (singaporeHighScores.loadDeathByMissile() + sydneyHighScores.loadDeathByMissile()), 1000, new TextOptions(AutoWrap.WORDS, subBackground.getWidth() - (spriteX + 200), HorizontalAlign.LEFT), mVertexBufferObjectManager);
		//Text sMissileDeathCount = new Text(spriteX + 65, spriteY += 55, mGameResources.getmFont(), "SCATTER MISSILE DEATH COUNT", 1000, new TextOptions(AutoWrap.WORDS, subBackground.getWidth() - (spriteX + 200), HorizontalAlign.LEFT), mVertexBufferObjectManager);
		//Text sMissileDeathCountValue = new Text(spriteX + 1000, spriteY, mGameResources.getmFont(), "" + (singaporeHighScores.loadDeathByScatterMissile() + sydneyHighScores.loadDeathByScatterMissile()), 1000, new TextOptions(AutoWrap.WORDS, subBackground.getWidth() - (spriteX + 200), HorizontalAlign.LEFT), mVertexBufferObjectManager);
		
		AnimatedSprite multiplayer_runs = new AnimatedSprite(spriteX, spriteY += mGameResources.getmProfileSceneBackgroundTextureRegion().getHeight() / 5, mGameResources.getmProfileSceneTextTextureRegion(), mVertexBufferObjectManager);
		multiplayer_runs.setCurrentTileIndex(5);
				
		mSubProfileScene.setBackgroundEnabled(false);
		mSubProfileScene.attachChild(personal_best);
		mSubProfileScene.attachChild(bestScore);
		mSubProfileScene.attachChild(bestScoreValue);
		mSubProfileScene.attachChild(mostGoldCollected);
		mSubProfileScene.attachChild(mostGoldCollectedValue);
		mSubProfileScene.attachChild(mostHumanAbsorbed);
		mSubProfileScene.attachChild(mostHumanAbsorbedValue);
		mSubProfileScene.attachChild(mostHumanAnnihilated);
		mSubProfileScene.attachChild(mostHumanAnnihilatedValue);

		//mSubProfileScene.attachChild(vehical_runs);
		
		//mSubProfileScene.attachChild(total);
		
		//mSubProfileScene.attachChild(gadgets);
		
		mSubProfileScene.attachChild(deaths);
		mSubProfileScene.attachChild(deathCount);
		mSubProfileScene.attachChild(deathCountValue);
		mSubProfileScene.attachChild(laserDeathCount);
		mSubProfileScene.attachChild(laserDeathCountValue);
		mSubProfileScene.attachChild(missileDeathCount);
		mSubProfileScene.attachChild(missileDeathCountValue);
		//mSubProfileScene.attachChild(sMissileDeathCount);
		//mSubProfileScene.attachChild(sMissileDeathCountValue);		
		//mSubProfileScene.attachChild(multiplayer_runs);
		

		mProfileScene.attachChild(subBackground);
		mSubProfileScene.setBackgroundEnabled(false);
		mProfileScene.setBackgroundEnabled(false);
		mProfileScene.attachChild(mSubProfileScene);
		mProfileScene.attachChild(sub_SubBackground);
		mProfileScene.attachChild(G_play_icon);
		mProfileScene.registerTouchArea(G_play_icon);
		
		return mProfileScene;
	}

	private Scene loadCreditsScene() {
		mMainActivity.hideAdvertisement();
		
		mCreditsScene = new Scene();
		loadCommonDisplay(mCreditsScene);
		Sprite credits_image = new Sprite(CAMERA_WIDTH - mGameResources.getmCreditsScreenTextureRegion().getWidth(), (CAMERA_HEIGHT - mGameResources.getmCreditsScreenTextureRegion().getHeight()) / 2, mGameResources.getmCreditsScreenTextureRegion(), mVertexBufferObjectManager);
		//credits_image.setScale(0.9f);
		mCreditsScene.attachChild(credits_image);
		mCreditsScene.setBackgroundEnabled(false);
		return mCreditsScene;
	}

	private Scene loadHelpScene() {
		mHelpScene = new Scene();
		mHelpScene.setTouchAreaBindingOnActionDownEnabled(true);
		
		mHelpScene.attachChild(new Sprite(0, 0, mGameResources.getmWashoutBackgroundTextureRegion(), mVertexBufferObjectManager));
				
		AnimatedSprite shop_icon = new AnimatedSprite(450, 450, mGameResources.getmHangerShopIconTextureRegion(), mVertexBufferObjectManager) {
			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent,float pTouchAreaLocalX, float pTouchAreaLocalY) {
				if(pSceneTouchEvent.isActionDown()) {
					this.setScale(0.9f);
					mShopPressed = true;
				}
				
				if(pSceneTouchEvent.isActionUp()) {
					if(sfx_on) mGameResources.getButtonSFX().play();
					if(mShopPressed) {
						//if(sfx_on) mGameResources.getButtonSFX().play();
						mShopPressed = false;
						this.setScale(1f);
						mHangerScene.setChildScene(loadPurchaseScene(false));
					}
				}
				return true;
			}
		};
		shop_icon.setCurrentTileIndex(MathUtils.random(1,3));
		Text subDescription1 = new Text(630,600, mGameResources.getmFont(), "SHOP", 1000, new TextOptions(AutoWrap.WORDS, 500, HorizontalAlign.LEFT), mVertexBufferObjectManager);
		mHelpScene.attachChild(subDescription1);
		mHelpScene.attachChild(shop_icon);
		mHelpScene.registerTouchArea(shop_icon);		
		
		// Achievement Scene
		achievement_icon = new Sprite(55, 208.5f, mGameResources.getmAchievementIconTextureRegion(), mVertexBufferObjectManager)	{
			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent,float pTouchAreaLocalX, float pTouchAreaLocalY) {
				if(pSceneTouchEvent.isActionDown()) {
					this.setScale(0.9f);
					mAchievementPressed = true;
				}
				
				if(pSceneTouchEvent.isActionUp()) {
					if(sfx_on) mGameResources.getButtonSFX().play();
					if(mAchievementPressed) {
						//if(sfx_on) mGameResources.getButtonSFX().play();
						mAchievementPressed = false;
						this.setScale(1f);
						if(mGameResources.getmGameHelper().isSignedIn()) {
							mHangerScene.setChildScene(loadAchievementScene(0,false));							
						} else {
							mMainActivity.runOnUiThread(new Runnable() {
								public void run() {
									mGameResources.getmGameHelper().makeSimpleDialog(mGameResources.getmBaseContext().getString(R.string.signin_for_achievements)).show();
								}
							});
				    	}
					}
				}
				return true;
			}
		};		
		Text subDescription2 = new Text(30, 450, mGameResources.getmFont(), "ACHIVEMENTS", 1000, new TextOptions(AutoWrap.WORDS, 500, HorizontalAlign.LEFT), mVertexBufferObjectManager);
		mHelpScene.attachChild(subDescription2);
		mHelpScene.attachChild(achievement_icon);
		mHelpScene.registerTouchArea(achievement_icon);		
		
		Sprite profile_icon = new Sprite(173.6f, 85, mGameResources.getmProfileIconTextureRegion(), mVertexBufferObjectManager)	{
			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent,float pTouchAreaLocalX, float pTouchAreaLocalY) {
				if(pSceneTouchEvent.isActionDown()) {
					this.setScale(0.9f);
					mProfilePressed = true;
				}
				
				if(pSceneTouchEvent.isActionUp()) {
					if(sfx_on) mGameResources.getButtonSFX().play();
					if(mProfilePressed) {
						//if(sfx_on) mGameResources.getButtonSFX().play();
						mProfilePressed = false;
						this.setScale(1f);
						mHangerScene.setChildScene(loadProfileScene(0,false));
					}
				}
				return true;
			}
		};
		Text subDescription3 = new Text(260, 155, mGameResources.getmFont(), "PROFILE & HIGHSCORES", 1000, new TextOptions(AutoWrap.WORDS, 250, HorizontalAlign.LEFT), mVertexBufferObjectManager);
		mHelpScene.attachChild(subDescription3);
		mHelpScene.attachChild(profile_icon);
		mHelpScene.registerTouchArea(profile_icon);
		
		AnimatedSprite credits_icon = new AnimatedSprite(300, 270, mGameResources.getmCreditsIconTextureRegion(), mVertexBufferObjectManager)	{
			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent,float pTouchAreaLocalX, float pTouchAreaLocalY) {
				if(pSceneTouchEvent.isActionDown()) {
					this.setScale(0.9f);
					mCreditsPressed = true;
				}
				
				if(pSceneTouchEvent.isActionUp()) {
					if(sfx_on) mGameResources.getButtonSFX().play();
					if(mCreditsPressed) {
						//if(sfx_on) mGameResources.getButtonSFX().play();
						mCreditsPressed = false;
						this.setScale(1f);
						mHangerScene.setChildScene(loadCreditsScene());
					}
				}
				return true;
			}
		};
		Text subDescription4 = new Text(390, 395, mGameResources.getmFont(), "CREDITS", 1000, new TextOptions(AutoWrap.WORDS, 500, HorizontalAlign.LEFT), mVertexBufferObjectManager);
		mHelpScene.attachChild(credits_icon);
		mHelpScene.attachChild(subDescription4);
		mHelpScene.registerTouchArea(credits_icon);
		
		AnimatedSprite mBackToMainMenuButton = new AnimatedSprite(953.7f, 70, mGameResources.getmHangerBackIconTextureRegion(), mVertexBufferObjectManager) {
			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent,float pTouchAreaLocalX, float pTouchAreaLocalY) {
				if(pSceneTouchEvent.isActionDown()) {
					this.setScale(0.9f);
					mMainMenuPressed = true;
				}
				
				if(pSceneTouchEvent.isActionUp()) {
					if(sfx_on) mGameResources.getButtonSFX().play();
					if(mMainMenuPressed) {
						mMainMenuPressed = false;
						this.setScale(1f);
						
						mCamera.setCenter(CAMERA_WIDTH / 2, CAMERA_HEIGHT / 2);
						// Save data to Google
				    	if(mGameResources.getmGameHelper().isSignedIn())
				    		mMainActivity.saveSnapshot();
				    	
						mSceneManager.loadMainMenuScene();
						
				        //Log.d("Hanger - Google Purchase", "Destroying helper.");
				        if (mMainActivity.mIabHelper != null) mMainActivity.mIabHelper.dispose();
				        mMainActivity.mIabHelper = null;
				        mMainActivity.mIabHelperConnected = false;
					}
				}
				return true;
			}
		};
		mBackToMainMenuButton.animate(650);
		Text subDescription5 = new Text(900f, 128, mGameResources.getmFont(), "MAIN MENU", 1000, new TextOptions(AutoWrap.WORDS, 500, HorizontalAlign.LEFT), mVertexBufferObjectManager);
		mHelpScene.attachChild(mBackToMainMenuButton);
		mHelpScene.attachChild(subDescription5);
		mHelpScene.registerTouchArea(mBackToMainMenuButton);
				
		mHelpScene.setBackgroundEnabled(false);
		return mHelpScene;
	}
	
	private Scene loadMissionScene() {
		mMainActivity.hideAdvertisement();
		
		mMissionScene = new Scene();
		mMissionScene.setTouchAreaBindingOnActionDownEnabled(true);
		loadCommonDisplay(mMissionScene);

		//Attached a plus icon and register the touch area to load the quest from Google Play
		G_play_icon = new AnimatedSprite(CAMERA_WIDTH - (mGameResources.getmGooglePlayIconTextureRegion().getWidth() + 20), 17, mGameResources.getmGooglePlayIconTextureRegion(), mVertexBufferObjectManager) {
			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent,float pTouchAreaLocalX, float pTouchAreaLocalY) {					
				if(pSceneTouchEvent.getAction() == MotionEvent.ACTION_DOWN) {
					this.setCurrentTileIndex(1);
				} else if(pSceneTouchEvent.getAction() == MotionEvent.ACTION_UP) {
					//if(sfx_on) mGameResources.getButtonSFX().play();
					this.setCurrentTileIndex(0);
					mMainActivity.runOnUiThread(new Runnable() {
					    public void run() {
					    	if(mGameResources.getmGameHelper().isSignedIn())
					    		mMainActivity.startActivityForResult(Games.Quests.getQuestsIntent(mGameResources.getmGameHelper().getApiClient(), Quests.SELECT_ALL_QUESTS), 0);
			            	else
			            		mGameResources.getmGameHelper().makeSimpleDialog(mGameResources.getmBaseContext().getString(R.string.signin_for_quest)).show();
					    }
					});
				}
				return true;
			}
		};
		G_play_icon.setScale(0.8f);

		spriteY = PADDINGY;
		//TODO: Create all the mission/quest on to this area
		for(int i=0; i<1; i++) {
			

			spriteY += PADDINGY + 20;
			mMaxY = spriteY - CAMERA_HEIGHT;		
		}
		//~END
		
		mMissionScene.attachChild(subBackground);
		mMissionScene.attachChild(G_play_icon);
		mMissionScene.registerTouchArea(G_play_icon);

		mMissionScene.setBackgroundEnabled(false);
		return mMissionScene;
	}
	
	private Scene loadPurchaseScene(boolean reload) {
		mMainActivity.hideAdvertisement();
		
		mPurchaseScene = new Scene();
		mPurchaseScene.setTouchAreaBindingOnActionDownEnabled(true);
		loadCommonDisplay(mPurchaseScene);
		
		Sprite subBackground = new Sprite(0, 0, mGameResources.getmHangerSubBackgroundTextureRegion(), mVertexBufferObjectManager);
		displayPanel = new Sprite(CAMERA_WIDTH - mGameResources.getmHangerDisplayPanelTextureRegion().getWidth() - 41, CAMERA_HEIGHT - mGameResources.getmHangerDisplayPanelTextureRegion().getHeight() - 46, mGameResources.getmHangerDisplayPanelTextureRegion(), mVertexBufferObjectManager);
		
		UFO_Icon_Sprite = new AnimatedSprite(220, 200, mGameResources.getmUFOIconTextureRegion(), mVertexBufferObjectManager) {
			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent,float pTouchAreaLocalX, float pTouchAreaLocalY) {
				if(pSceneTouchEvent.isActionDown()) {
					this.setScale(0.9f);
					mUFOButtonPressed = true;
				}
				
				if(pSceneTouchEvent.isActionUp()) {
					//if(sfx_on) mGameResources.getButtonSFX().play();
					if(mUFOButtonPressed) {
						mUFOButtonPressed = false;
						this.setScale(1f);
						
						this.setCurrentTileIndex(1);
						Gadget_Icon_Sprite.setCurrentTileIndex(0);
						Cash_Icon_Sprite.setCurrentTileIndex(0);
						mPurchaseScene.setChildScene(loadUFOScene(false));
					}
				}
				return true;
			}
		};
		UFO_Icon_Sprite.setCurrentTileIndex(1);
		
		Gadget_Icon_Sprite = new AnimatedSprite(220, 350, mGameResources.getmGadgetIconTextureRegion(), mVertexBufferObjectManager) {
			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent,float pTouchAreaLocalX, float pTouchAreaLocalY) {
				if(pSceneTouchEvent.isActionDown()) {
					this.setScale(0.9f);
					mGadgetButtonPressed = true;
				}
				
				if(pSceneTouchEvent.isActionUp()) {
					//if(sfx_on) mGameResources.getButtonSFX().play();
					if(mGadgetButtonPressed) {
						mGadgetButtonPressed = false;
						this.setScale(1f);
						
						this.setCurrentTileIndex(1);
						UFO_Icon_Sprite.setCurrentTileIndex(0);
						Cash_Icon_Sprite.setCurrentTileIndex(0);
						mPurchaseScene.setChildScene(loadGadgetScene(false));
					}
				}
				return true;
			}
		};
		Gadget_Icon_Sprite.setCurrentTileIndex(0);

		Cash_Icon_Sprite = new AnimatedSprite(220, 500, mGameResources.getmPurchaseIconTextureRegion(), mVertexBufferObjectManager) {
			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent,float pTouchAreaLocalX, float pTouchAreaLocalY) {
				if(pSceneTouchEvent.isActionDown()) {
					this.setScale(0.9f);
					mCashButtonPressed = true;
				}
				
				if(pSceneTouchEvent.isActionUp()) {
					//if(sfx_on) mGameResources.getButtonSFX().play();
					if(mCashButtonPressed) {
						mCashButtonPressed = false;
						this.setScale(1f);
						
						this.setCurrentTileIndex(1);
						Gadget_Icon_Sprite.setCurrentTileIndex(0);
						UFO_Icon_Sprite.setCurrentTileIndex(0);
						mPurchaseScene.setChildScene(loadCashPurchaseScene(false));
					}
				}
				return true;
			}
		};
		Cash_Icon_Sprite.setCurrentTileIndex(0);
		
		Sprite Get_More_Gold = new Sprite(CAMERA_WIDTH - mGameResources.getmGetMoreGoldIconTextureRegion().getWidth() - 30, 10, mGameResources.getmGetMoreGoldIconTextureRegion(), mVertexBufferObjectManager) {
			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent,float pTouchAreaLocalX, float pTouchAreaLocalY) {
				if(pSceneTouchEvent.isActionDown()) {
					this.setScale(0.9f);
					mGetMoreGoldPressed = true;
				}
				
				if(pSceneTouchEvent.isActionUp()) {
					if(sfx_on) mGameResources.getButtonSFX().play();
					if(mGetMoreGoldPressed) {
						mGetMoreGoldPressed = false;
						this.setScale(1f);
						
						mMainActivity.showVungleVideoAdvertisement();
					}
				}
				return true;
			}
		};
		
		You_Have_Icon = new Sprite(350, 10, mGameResources.getmYouHaveIconTextureRegion(), mVertexBufferObjectManager);
		//You_Have_Icon = new Sprite(CAMERA_WIDTH - mGameResources.getmYouHaveIconTextureRegion().getWidth() - 30, 10, mGameResources.getmYouHaveIconTextureRegion(), mVertexBufferObjectManager);
		obtainedGold = new Text(515, 40, mGameResources.getmFont(), "0", 9, new TextOptions(HorizontalAlign.RIGHT), mVertexBufferObjectManager);
		//obtainedGold = new Text(1075, 40, mGameResources.getmFont(), "0", 9, new TextOptions(HorizontalAlign.RIGHT), mVertexBufferObjectManager);
		obtainedGold.setBlendFunction(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);
		obtainedGold.setAlpha(1f);
		obtainedGold.setText(""+highScores.loadObtainedDetuctableGold());
		
		mPurchaseScene.attachChild(subBackground);
		mPurchaseScene.attachChild(displayPanel);
		mPurchaseScene.attachChild(UFO_Icon_Sprite);
		mPurchaseScene.attachChild(Gadget_Icon_Sprite);
		mPurchaseScene.attachChild(Cash_Icon_Sprite);
		mPurchaseScene.attachChild(You_Have_Icon);
		mPurchaseScene.attachChild(obtainedGold);
		//mPurchaseScene.attachChild(Get_More_Gold);
		
		mPurchaseScene.registerTouchArea(UFO_Icon_Sprite);
		mPurchaseScene.registerTouchArea(Gadget_Icon_Sprite);
		mPurchaseScene.registerTouchArea(Cash_Icon_Sprite);
		//mPurchaseScene.registerTouchArea(Get_More_Gold);
		
		mPurchaseScene.setBackgroundEnabled(false);
		//Set the default screen to UFO screen
		mPurchaseScene.setChildScene(loadUFOScene(false));
		return mPurchaseScene;
	}

	private Scene loadUFOScene(boolean reload) {
		mMainActivity.hideAdvertisement();
		
		mUFOScene = new Scene();
		purchaseButtonList = new ArrayList<Sprite>();
		selectedToDisplayList = new ArrayList<Sprite>();
		
		// Load and Check Spacecraft Status (Locked / Unlocked) : False / True
		final Spacecraft_SharedPreference spacecraft_sharedpreference = new Spacecraft_SharedPreference(mGameResources.getmBaseContext());	
		
		//Attached a text description of the spacecraft model and the cost
		final Text modelDescription = new Text(945, 285, mGameResources.getmFont(), "", 1000, new TextOptions(AutoWrap.WORDS, displayPanel.getWidth() + 20, HorizontalAlign.LEFT), mVertexBufferObjectManager);
		modelDescription.setScale(0.8f);
		mUFOScene.attachChild(modelDescription);
		
		final Text modelCost = new Text(950, 568, mGameResources.getmFont(), "", 14, new TextOptions(AutoWrap.WORDS, displayPanel.getWidth() + 20, HorizontalAlign.LEFT), mVertexBufferObjectManager);
		modelCost.setScale(0.85f);
		mUFOScene.attachChild(modelCost);
		
		float spriteX = PADDINGX;
		float spriteY = PADDINGY;

		for (int x = 0; x < mGameResources.getUFOModelList().size(); x++) {
			final int spacecraft_costvalue = mGameResources.getSpacecraft_cost()[x];
			final int spacecraft_model_no = x;

			//Attached a box over the UFO
			Sprite UFOBox = new Sprite(spriteX, spriteY, mGameResources.getmUFOBoxTextureRegion(), mVertexBufferObjectManager);
			UFOBox.setScale(0.9f);			

			//Display image of the UFO when user select
			final PixelPerfectAnimatedSprite selectedUFOToDisplay = new PixelPerfectAnimatedSprite(0, 0, mGameResources.getUFOModelList().get(spacecraft_model_no), mVertexBufferObjectManager);
			selectedUFOToDisplay.setPosition(displayPanel.getX() + ((displayPanel.getWidth() - mGameResources.getUFOModelList().get(spacecraft_model_no).getWidth()) / 2), displayPanel.getY() + ((displayPanel.getHeight() - mGameResources.getUFOModelList().get(spacecraft_model_no).getHeight()) / 2) - 180);            
			selectedUFOToDisplay.setScale(0.8f);
			selectedUFOToDisplay.setVisible(false);
			selectedToDisplayList.add(selectedUFOToDisplay);

			//Attached a purchase icon for the UFOs
			final AnimatedSprite UFOPurchaseIcon = new AnimatedSprite(848, 590, mGameResources.getmEquipIconTextureRegion(), mVertexBufferObjectManager) {
				@Override
				public boolean onAreaTouched(TouchEvent pSceneTouchEvent,float pTouchAreaLocalX, float pTouchAreaLocalY) {
					if(pSceneTouchEvent.getAction() == MotionEvent.ACTION_DOWN && this.isVisible()) {
						onItemClicked = true;
					} else if(pSceneTouchEvent.getAction() == MotionEvent.ACTION_UP && this.isVisible()) { 
						if(sfx_on) mGameResources.getPurchaseSFX().play();
						if(onItemClicked) {							
							if(this.getCurrentTileIndex() == 0) {
								//Save the active spacecraft to the shared preference
								spacecraft_sharedpreference.saveActiveSpacecraft(spacecraft_model_no);
								this.setCurrentTileIndex(1);
							}
							
							if(this.getCurrentTileIndex() == 2) {
								//Remove the cost from shared_preference
								highScores.saveObtainedDetuctableGold(-spacecraft_costvalue);
								obtainedGold.setText(""+highScores.loadObtainedDetuctableGold());
								
								//Update the spacecraft status to available to the shared preference
								spacecraft_sharedpreference.saveSpacecraftStatus(spacecraft_model_no);
								
								this.setCurrentTileIndex(0);
							}
						}
						onItemClicked = false;
					}
					return true;
				}
			};
			UFOPurchaseIcon.setScale(0.53f);
			UFOPurchaseIcon.setVisible(false);
			purchaseButtonList.add(UFOPurchaseIcon);
			
			PixelPerfectAnimatedSprite UFOModel = new PixelPerfectAnimatedSprite((spriteX + (UFOBox.getWidth() - mGameResources.getUFOModelList().get(spacecraft_model_no).getWidth()) / 2) - 4, spriteY + (UFOBox.getHeight() - mGameResources.getUFOModelList().get(spacecraft_model_no).getHeight())/2, mGameResources.getUFOModelList().get(spacecraft_model_no), mVertexBufferObjectManager) {
				@Override
				public boolean onAreaTouched(TouchEvent pSceneTouchEvent,float pTouchAreaLocalX, float pTouchAreaLocalY) {
					if(pSceneTouchEvent.getAction() == MotionEvent.ACTION_DOWN) {
						for(Sprite purchaseSprite : purchaseButtonList) {
							purchaseSprite.setVisible(false);
							mUFOScene.unregisterTouchArea(purchaseSprite);
						}
						for(Sprite UFOSprite : selectedToDisplayList) {
							UFOSprite.setVisible(false);
						}
						
						if(spacecraft_model_no == 0) 
							modelDescription.setText(mGameResources.getmBaseContext().getString(R.string.ufo_01));
						else if(spacecraft_model_no == 1)
							modelDescription.setText(mGameResources.getmBaseContext().getString(R.string.ufo_02));
						else if(spacecraft_model_no == 2)
							modelDescription.setText(mGameResources.getmBaseContext().getString(R.string.ufo_03));
						else if(spacecraft_model_no == 3)
							modelDescription.setText(mGameResources.getmBaseContext().getString(R.string.ufo_04));
						else if(spacecraft_model_no == 4)
							modelDescription.setText(mGameResources.getmBaseContext().getString(R.string.ufo_05));
						else if(spacecraft_model_no == 5)
							modelDescription.setText(mGameResources.getmBaseContext().getString(R.string.ufo_06));
						else if(spacecraft_model_no == 6)
							modelDescription.setText(mGameResources.getmBaseContext().getString(R.string.ufo_07));
						else if(spacecraft_model_no == 7)
							modelDescription.setText(mGameResources.getmBaseContext().getString(R.string.ufo_08));
						else if(spacecraft_model_no == 8)
							modelDescription.setText(mGameResources.getmBaseContext().getString(R.string.ufo_09));
						else if(spacecraft_model_no == 9)
							modelDescription.setText(mGameResources.getmBaseContext().getString(R.string.ufo_10));
						
						modelCost.setText("Cost: " + spacecraft_costvalue);
						
						
						//Attached a equip & buy icon and register the touch area to allow purchasing n equiping of the spacecraft
						if(spacecraft_sharedpreference.getSpacecraftStatus(spacecraft_model_no)) {
							if(spacecraft_sharedpreference.getActiveSpacecraft() == spacecraft_model_no) {
								UFOPurchaseIcon.setCurrentTileIndex(1);
							} else {
								UFOPurchaseIcon.setCurrentTileIndex(0);
								mUFOScene.registerTouchArea(UFOPurchaseIcon);
							}
						} else {
							//Not enough $, therefore set buy button to grey
							if(highScores.loadObtainedDetuctableGold() < spacecraft_costvalue) {
								UFOPurchaseIcon.setCurrentTileIndex(3);
							} else {
								//Enough $ to make purchase, set buy button to blue
								UFOPurchaseIcon.setCurrentTileIndex(2);
								mUFOScene.registerTouchArea(UFOPurchaseIcon);
							}
						}
						UFOPurchaseIcon.setVisible(true);
						selectedUFOToDisplay.setVisible(true);
					}
					return true;
				}				
			};
			UFOModel.setCurrentTileIndex(0);
			UFOModel.setScale(0.8f);
			UFOModel.setX(UFOBox.getX() + (UFOBox.getWidth() - UFOModel.getWidth())/2);
			UFOModel.setY(UFOBox.getY() + (UFOBox.getHeight() - UFOModel.getHeight())/2);
			mUFOScene.attachChild(UFOBox);
			mUFOScene.attachChild(UFOModel);
			mUFOScene.attachChild(UFOPurchaseIcon);
			mUFOScene.attachChild(selectedUFOToDisplay);			
			mUFOScene.registerTouchArea(UFOModel);
			
			spriteX += UFOBox.getWidth() + 10;
			if(x != 0 && x % 3 == 0) {
				spriteX = PADDINGX;
				spriteY += UFOBox.getHeight() + 10;
			}
		}
		Sprite comingSoon = new Sprite(spriteX, spriteY, mGameResources.getmComingSoonIconTextureRegion(), mVertexBufferObjectManager);
		comingSoon.setScale(0.9f);		
		mUFOScene.attachChild(comingSoon);
		
		mMaxY = spriteY - CAMERA_HEIGHT;
		
		mUFOScene.setBackgroundEnabled(false);
		return mUFOScene;
	}
	
	private Scene loadGadgetScene(boolean reload) {
		mMainActivity.hideAdvertisement();
		
		mGadgetScene = new Scene();
		purchaseButtonList = new ArrayList<Sprite>();
		selectedToDisplayList = new ArrayList<Sprite>();
		gadgetList = new ArrayList<Sprite>();
		
		// Load and Gadgets shared preferences
		final Gadgets_SharedPreference gadget_sharedpreference = new Gadgets_SharedPreference(mGameResources.getmBaseContext());
		equippedGadgetList = new Sprite[gadget_sharedpreference.mEquippedGadgetConstant];
		
		final Text gadgetDescription = new Text(945, 315, mGameResources.getmFont(), "", 1000, new TextOptions(AutoWrap.WORDS, displayPanel.getWidth() + 20, HorizontalAlign.LEFT), mVertexBufferObjectManager);
		gadgetDescription.setScale(0.8f);
		mGadgetScene.attachChild(gadgetDescription);
		
		final Text gadgetCost = new Text(950, 568, mGameResources.getmFont(), "", 14, new TextOptions(AutoWrap.WORDS, displayPanel.getWidth() + 20, HorizontalAlign.LEFT), mVertexBufferObjectManager);
		gadgetCost.setScale(0.85f);
		mGadgetScene.attachChild(gadgetCost);
		
		//Attached 2 gadget boxes to the screen
		final Sprite equippedGadgetBox1 = new Sprite(660, 6, mGameResources.getmUFOBoxTextureRegion(), mVertexBufferObjectManager);
		equippedGadgetBox1.setScale(0.9f);
		equippedGadgetBox1.setBlendFunction(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);
		mGadgetScene.attachChild(equippedGadgetBox1);
		
		final Sprite equippedGadgetBox2 = new Sprite(795, 6, mGameResources.getmUFOBoxTextureRegion(), mVertexBufferObjectManager);
		equippedGadgetBox2.setScale(0.9f);
		equippedGadgetBox2.setBlendFunction(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);
		mGadgetScene.attachChild(equippedGadgetBox2);
				
		
		float spriteX = PADDINGX;
		float spriteY = PADDINGY;

		for (int x = 0; x < mGameResources.getGadgetList().size(); x++) {
			final int gadget_costvalue = mGameResources.getGadget_cost()[x];
			final int gadget_no = x;
			

			//Attached a gadget box over the gadgets
			final Sprite gadgetBox = new Sprite(spriteX, spriteY, mGameResources.getmGadgetCounterBoxTextureRegion(), mVertexBufferObjectManager);
			gadgetBox.setScale(0.9f);

			//Attached a textual count of the gadget brought and in stock
			final Text gadgetCount = new Text(0, spriteY + 82, mGameResources.getmFont(),  "", 2, new TextOptions(HorizontalAlign.LEFT), mVertexBufferObjectManager);
			//if the gadget count is less than 10 (meaning less than 2 digits), add a 0 infront
			gadgetCount.setText(gadget_sharedpreference.getGadgetCount(gadget_no) < 10 ? "0" + gadget_sharedpreference.getGadgetCount(gadget_no) : "" + gadget_sharedpreference.getGadgetCount(gadget_no));
			gadgetCount.setX((gadgetBox.getX() + gadgetBox.getWidth() - 8) - gadgetCount.getWidth() );
			gadgetCount.setScale(0.70f);

			//Display image of the gadget when user select
			mGameResources.getGadgetList().get(gadget_no).setCurrentTileIndex(0);
			TiledTextureRegion clone = mGameResources.getGadgetList().get(gadget_no).deepCopy();
			clone.set(clone.getTextureX() + 6, clone.getTextureY() + 6, clone.getWidth() - 12, clone.getHeight() - 12);
			final AnimatedSprite selectedGadgetToDisplay = new AnimatedSprite(displayPanel.getX() + ((displayPanel.getWidth() - mGameResources.getGadgetList().get(gadget_no).getWidth()) / 2), 210, clone, mVertexBufferObjectManager);
			selectedGadgetToDisplay.setScale(0.8f);
			selectedGadgetToDisplay.setVisible(false);
			selectedToDisplayList.add(selectedGadgetToDisplay);
			
			//Check preference for the equipped gadget
			if(gadget_sharedpreference.getEquippedGadget1() != 99) {				
				TiledTextureRegion clone2 = mGameResources.getGadgetList().get(gadget_sharedpreference.getEquippedGadget1()).deepCopy();
				clone2.set(clone2.getTextureX() + 6, clone2.getTextureY() + 6, clone2.getWidth() - 12, clone2.getHeight() - 12);
				Sprite selectedGadget = new Sprite(equippedGadgetBox1.getX() + 6, equippedGadgetBox1.getY() + 6, clone2, mVertexBufferObjectManager);
				selectedGadget.setScale(0.8f);
				mGadgetScene.attachChild(selectedGadget);
				equippedGadgetList[0] = selectedGadget;
			}
			if(gadget_sharedpreference.getEquippedGadget2() != 99) {				
				TiledTextureRegion clone2 = mGameResources.getGadgetList().get(gadget_sharedpreference.getEquippedGadget2()).deepCopy();
				clone2.set(clone2.getTextureX() + 6, clone2.getTextureY() + 6, clone2.getWidth() - 12, clone2.getHeight() - 12);
				Sprite selectedGadget = new Sprite(equippedGadgetBox2.getX() + 6, equippedGadgetBox2.getY() + 6, clone2, mVertexBufferObjectManager);
				selectedGadget.setScale(0.8f);
				mGadgetScene.attachChild(selectedGadget);
				equippedGadgetList[1] = selectedGadget;
			}
			
			//Attached a purchase icon for the gadgets
			final AnimatedSprite gadgetPurchaseIcon = new AnimatedSprite(848, 590, mGameResources.getmEquipIconTextureRegion(), mVertexBufferObjectManager) {
				@Override
				public boolean onAreaTouched(TouchEvent pSceneTouchEvent,float pTouchAreaLocalX, float pTouchAreaLocalY) {
					if(pSceneTouchEvent.getAction() == MotionEvent.ACTION_DOWN && this.isVisible()) {
						onItemClicked = true;
					} else if(pSceneTouchEvent.getAction() == MotionEvent.ACTION_UP && this.isVisible()) {
						if(sfx_on) mGameResources.getPurchaseSFX().play();
						if(onItemClicked && this.getCurrentTileIndex() == 2) {
							/*
							if(gadget_sharedpreference.getGadgetUnlockedStatus(gadget_no)) {
								
							}*/
							
							//Not enough $, therefore set buy button to grey
							if(highScores.loadObtainedDetuctableGold() < gadget_costvalue) {
								this.setCurrentTileIndex(3);
								//mGadgetScene.unregisterTouchArea(this);
							} else {
								//Enough $ to make purchase, set buy button to blue
								this.setCurrentTileIndex(2);
								//mGadgetScene.registerTouchArea(this);
								
								//Check whether inventory for particular gadget reached 99
								if(gadget_sharedpreference.getGadgetCount(gadget_no) < 99) {
									//Remove the cost from shared_preference
									highScores.saveObtainedDetuctableGold(-gadget_costvalue);
									obtainedGold.setText(""+highScores.loadObtainedDetuctableGold());
									gadget_sharedpreference.buyGadget(gadget_no, 1);
									gadgetCount.setText(gadget_sharedpreference.getGadgetCount(gadget_no) < 10 ? "0" + gadget_sharedpreference.getGadgetCount(gadget_no) : "" + gadget_sharedpreference.getGadgetCount(gadget_no));
									gadgetCount.setX((gadgetBox.getX() + gadgetBox.getWidth() - 8) - gadgetCount.getWidth());
								}
							}
							
							// Unlock Achievement
							mGameResources.getmAchievement().unlockPurchaseAchievement();
						}
						onItemClicked = false;
					}
					return true;
				}
			};
			gadgetPurchaseIcon.setScale(0.53f);
			gadgetPurchaseIcon.setVisible(false);
			purchaseButtonList.add(gadgetPurchaseIcon);
			

			//Attached the gadget icon and register the touch area
    		final Sprite gadgetIcon = new Sprite(spriteX, spriteY, mGameResources.getGadgetList().get(gadget_no), mVertexBufferObjectManager);
    		gadgetIcon.setScale(0.9f);
    		
    		
			//Display image of the gadget when user touch and hold to equip the gadget
			Sprite dragDropGadget = new Sprite(spriteX + 6, spriteY + 6, clone, mVertexBufferObjectManager) {
				@Override
				public boolean onAreaTouched(final TouchEvent pSceneTouchEvent, final float pTouchAreaLocalX, final float pTouchAreaLocalY) {
					
					if(pSceneTouchEvent.getAction() == MotionEvent.ACTION_DOWN) {						
						for(Sprite purchaseSprite : purchaseButtonList) {
							purchaseSprite.setVisible(false);
							mGadgetScene.unregisterTouchArea(purchaseSprite);
						}
						for(Sprite gadgetSprite : selectedToDisplayList) {
							gadgetSprite.setVisible(false);
						}
						
						//equippedGadgetBox1.setColor(Color.YELLOW);
						//equippedGadgetBox2.setColor(Color.YELLOW);
						equippedGadgetBox1.registerEntityModifier(new AlphaModifier(99, 0, 255));
						equippedGadgetBox2.registerEntityModifier(new AlphaModifier(99, 0, 255));
						
						//Attached a text description of the gadget and the cost
						if(gadget_no == 0) 
							gadgetDescription.setText(mGameResources.getmBaseContext().getString(R.string.gadget_01));
						else if(gadget_no == 1)
							gadgetDescription.setText(mGameResources.getmBaseContext().getString(R.string.gadget_02));
						else if(gadget_no == 2)
							gadgetDescription.setText(mGameResources.getmBaseContext().getString(R.string.gadget_03));
						else if(gadget_no == 3)
							gadgetDescription.setText(mGameResources.getmBaseContext().getString(R.string.gadget_04));
						else if(gadget_no == 4)
							gadgetDescription.setText(mGameResources.getmBaseContext().getString(R.string.gadget_05));
						else if(gadget_no == 5)
							gadgetDescription.setText(mGameResources.getmBaseContext().getString(R.string.gadget_06));
						else if(gadget_no == 6)
							gadgetDescription.setText(mGameResources.getmBaseContext().getString(R.string.gadget_07));
						else if(gadget_no == 7)
							gadgetDescription.setText(mGameResources.getmBaseContext().getString(R.string.gadget_08));
						else if(gadget_no == 8)
							gadgetDescription.setText(mGameResources.getmBaseContext().getString(R.string.gadget_09));
						else if(gadget_no == 9)
							gadgetDescription.setText(mGameResources.getmBaseContext().getString(R.string.gadget_10));
						
						gadgetCost.setText("Cost: " + gadget_costvalue);
						
						
						//Not enough $, therefore set buy button to grey
						if(highScores.loadObtainedDetuctableGold() < gadget_costvalue) {
							gadgetPurchaseIcon.setCurrentTileIndex(3);
						} else {
							//Enough $ to make purchase, set buy button to blue
							gadgetPurchaseIcon.setCurrentTileIndex(2);
							mGadgetScene.registerTouchArea(gadgetPurchaseIcon);
						}
						gadgetPurchaseIcon.setVisible(true);
						selectedGadgetToDisplay.setVisible(true);
					}
					
					if(pSceneTouchEvent.getAction() == MotionEvent.ACTION_MOVE) {
						if(gadget_sharedpreference.getGadgetCount(gadget_no) > 0) 
							setPosition(pSceneTouchEvent.getX() - getWidth() / 2, pSceneTouchEvent.getY() - getHeight() / 2);
					}
					
					if(pSceneTouchEvent.getAction() == MotionEvent.ACTION_UP) {
						// keep track on collision of the gadgets to determine whether user choose to equip the gadget or not
						float xCenterPoint = getX() + (getWidth()/2);
						float yCenterPoint = getY() + (getHeight()/2);
						
						//equippedGadgetBox1.reset();
						//equippedGadgetBox2.reset();
						equippedGadgetBox1.setAlpha(1.0f);
						equippedGadgetBox2.setAlpha(1.0f);
						equippedGadgetBox1.clearEntityModifiers();
						equippedGadgetBox2.clearEntityModifiers();
						
						if(collidesWith(equippedGadgetBox1) || collidesWith(equippedGadgetBox2)) {
							//The center point of the gadget image is within GadgetBox1
							if(xCenterPoint > equippedGadgetBox1.getX() && xCenterPoint < (equippedGadgetBox1.getWidth() + equippedGadgetBox1.getX())
								&& yCenterPoint > equippedGadgetBox1.getY() && yCenterPoint < (equippedGadgetBox1.getHeight() + equippedGadgetBox1.getY())) {
		
								TiledTextureRegion clone = mGameResources.getGadgetList().get(gadget_no).deepCopy();
								clone.set(clone.getTextureX() + 6, clone.getTextureY() + 6, clone.getWidth() - 12, clone.getHeight() - 12);
								
								/* Implement the following if decide to restrict each gadget can only be used once.
								//Check whether the gadget is already set in gadgetbox 2, if so, change the box2 gadget to box1
								if(gadget_sharedpreference.getEquippedGadget2() == gadget_no) {
									//mGadgetScene.detachChild(equippedGadgetList[1]);
									Log.d("getEquippedGadget2", "getEquippedGadget2");
									equippedGadgetList[1].setVisible(false);
									gadget_sharedpreference.setEquippedGadget2(99);
								}
								*/
								
								if(gadget_sharedpreference.getEquippedGadget1() != 99) {
									//mGadgetScene.detachChild(equippedGadgetList[0]);
									equippedGadgetList[0].setVisible(false);
								}

								Sprite selectedGadget = new Sprite(equippedGadgetBox1.getX() + 6, equippedGadgetBox1.getY() + 6, clone, mVertexBufferObjectManager);
								selectedGadget.setScale(0.8f);
								mGadgetScene.attachChild(selectedGadget);
								equippedGadgetList[0] = selectedGadget;
								
								//Update the shared preference
								gadget_sharedpreference.setEquippedGadget1(gadget_no);
							} 
							//The center point of the gadget image is within GadgetBox2
							else if(xCenterPoint > equippedGadgetBox2.getX() && xCenterPoint < (equippedGadgetBox2.getWidth() + equippedGadgetBox2.getX())
								&& yCenterPoint > equippedGadgetBox2.getY() && yCenterPoint < (equippedGadgetBox2.getHeight() + equippedGadgetBox2.getY())) {
		
								TiledTextureRegion clone = mGameResources.getGadgetList().get(gadget_no).deepCopy();
								clone.set(clone.getTextureX() + 6, clone.getTextureY() + 6, clone.getWidth() - 12, clone.getHeight() - 12);

								/* Implement the following if decide to restrict each gadget can only be used once.
								//Check whether the gadget is already set in gadgetbox 1, if so, change the box1 gadget to box2
								if(gadget_sharedpreference.getEquippedGadget1() == gadget_no) {
									//mGadgetScene.detachChild(equippedGadgetList[0]);
									Log.d("getEquippedGadget1", "getEquippedGadget1");
									equippedGadgetList[0].setVisible(false);
									gadget_sharedpreference.setEquippedGadget1(99);
								}
								*/
								
								if(gadget_sharedpreference.getEquippedGadget2() != 99) {
									//mGadgetScene.detachChild(equippedGadgetList[1]);
									equippedGadgetList[1].setVisible(false);
								}

								Sprite selectedGadget = new Sprite(equippedGadgetBox2.getX() + 6, equippedGadgetBox2.getY() + 6, clone, mVertexBufferObjectManager);
								selectedGadget.setScale(0.8f);
								mGadgetScene.attachChild(selectedGadget);
								equippedGadgetList[1] = selectedGadget;
								
								//Update the shared preference
								gadget_sharedpreference.setEquippedGadget2(gadget_no);
							}
						}
						
						//Reset the sprite back to its original position
				        this.setX(gadgetIcon.getX() + 6);
				        this.setY(gadgetIcon.getY() + 6);
					}
					
					
					return true;
				}
			};
			dragDropGadget.setScale(0.9f);
			gadgetList.add(dragDropGadget);
			
			mGadgetScene.attachChild(gadgetIcon);
			mGadgetScene.attachChild(dragDropGadget);
			mGadgetScene.attachChild(gadgetBox);
			mGadgetScene.attachChild(gadgetCount);
			mGadgetScene.attachChild(selectedGadgetToDisplay);
			mGadgetScene.attachChild(gadgetPurchaseIcon);
			mGadgetScene.registerTouchArea(dragDropGadget);
			mGadgetScene.setTouchAreaBindingOnActionDownEnabled(true);

			spriteX += gadgetIcon.getWidth() + 10;
			if(x != 0 && x % 3 == 0) {
				spriteX = PADDINGX;
				spriteY += gadgetIcon.getHeight() + 10;				
			}
		}
		
		Sprite comingSoon = new Sprite(spriteX, spriteY, mGameResources.getmComingSoonIconTextureRegion(), mVertexBufferObjectManager);
		comingSoon.setScale(0.9f);		
		mGadgetScene.attachChild(comingSoon);
		
		mMaxY = spriteY - CAMERA_HEIGHT;
		
		mGadgetScene.setBackgroundEnabled(false);
		return mGadgetScene;
	}
	
	private Scene loadCashPurchaseScene(boolean reload) {
		mMainActivity.hideAdvertisement();
		mMainActivity.loadIAB();
		
		mCashPurchaseScene = new Scene();
		purchaseButtonList = new ArrayList<Sprite>();
		selectedToDisplayList = new ArrayList<Sprite>();
		
		final Text cashPurchaseDescription = new Text(945, 315, mGameResources.getmFont(), "", 1000, new TextOptions(AutoWrap.WORDS, displayPanel.getWidth() + 20, HorizontalAlign.LEFT), mVertexBufferObjectManager);
		cashPurchaseDescription.setScale(0.8f);
		mCashPurchaseScene.attachChild(cashPurchaseDescription);
		
		final Text cashPurchaseCost = new Text(950, 568, mGameResources.getmFont(), "", 14, new TextOptions(AutoWrap.WORDS, displayPanel.getWidth() + 20, HorizontalAlign.LEFT), mVertexBufferObjectManager);
		cashPurchaseCost.setScale(0.85f);
		mCashPurchaseScene.attachChild(cashPurchaseCost);

		float spriteX = PADDINGX;
		float spriteY = PADDINGY;
		
		for (int x = 0; x < mGameResources.getmPurchaseItemsTextureRegion().getTileCount(); x++) {
			final int purchase_item_no = x;
			
    		//Attached a purchase icon for the cash purchase
    		final AnimatedSprite purchase_icon = new AnimatedSprite(850, 590, mGameResources.getmEquipIconTextureRegion(), mVertexBufferObjectManager) {
    			@Override
    			public boolean onAreaTouched(TouchEvent pSceneTouchEvent,float pTouchAreaLocalX, float pTouchAreaLocalY) {
    				if(pSceneTouchEvent.getAction() == MotionEvent.ACTION_DOWN) {
    					onItemClicked = true;
    				} else if(pSceneTouchEvent.getAction() == MotionEvent.ACTION_UP) {
    					if(onItemClicked && this.getCurrentTileIndex() == 2) {
						 	String payload = mGameResources.getGoogleAccountID();
				            //Log.d(mGameResources.TAG, "payload before making payment: " + payload);
							
    				    	if(mGameResources.getmGameHelper().isSignedIn()) {
    				    		//Log.d(mGameResources.TAG, "BUYING - " + mGameResources.getProductDetailsList().get(purchase_item_no).getSku());
    				    		if(mMainActivity.mIabHelperConnected)
    				    			mMainActivity.mIabHelper.launchPurchaseFlow(mMainActivity, mGameResources.getProductDetailsList().get(purchase_item_no).getSku(), RC_REQUEST, mPurchaseFinishedListener, payload);
    				    		else {
        				    		mMainActivity.runOnUiThread(new Runnable() {
    									public void run() {
    										mGameResources.getmGameHelper().makeSimpleDialog("Still trying to connect to Google. Please wait a moment before trying again.").show();
    									}
    								});
        				    	}
    				    	} else {
    				    		mMainActivity.runOnUiThread(new Runnable() {
									public void run() {
										mGameResources.getmGameHelper().makeSimpleDialog(mGameResources.getmBaseContext().getString(R.string.signin_to_purchase)).show();
									}
								});
    				    	}
    					}
    					onItemClicked = false;
    				}
    				return true;
    			}
    		};
    		purchase_icon.setScale(0.53f);
    		purchase_icon.setVisible(false);
    		purchaseButtonList.add(purchase_icon);
			
			//Attached the cash purchase icon and register the touch area
    		final AnimatedSprite cashPurchaseIcon = new AnimatedSprite(spriteX, spriteY, mGameResources.getmPurchaseItemsTextureRegion(), mVertexBufferObjectManager) {
				public boolean onAreaTouched(TouchEvent pSceneTouchEvent,float pTouchAreaLocalX, float pTouchAreaLocalY) {
					if(pSceneTouchEvent.getAction() == MotionEvent.ACTION_DOWN) {
						for(Sprite purchaseSprite : purchaseButtonList) {
							purchaseSprite.setVisible(false);
							mCashPurchaseScene.unregisterTouchArea(purchaseSprite);
						}
						
						if(mGameResources.getmGameHelper().isSignedIn()) {
							if(mGameResources.getProductDetailsList().size() > 0) {
				                //Log.d(mGameResources.TAG, mGameResources.getProductDetailsList().toString());
								if(purchase_item_no == 0)
									cashPurchaseDescription.setText(mGameResources.getProductDetailsList().get(purchase_item_no).getDescription());
								else if(purchase_item_no == 1)
									cashPurchaseDescription.setText(mGameResources.getProductDetailsList().get(purchase_item_no).getDescription());
								else if(purchase_item_no == 2)
									cashPurchaseDescription.setText(mGameResources.getProductDetailsList().get(purchase_item_no).getDescription());
								else if(purchase_item_no == 3)
									cashPurchaseDescription.setText(mGameResources.getProductDetailsList().get(purchase_item_no).getDescription());
								else if(purchase_item_no == 4)
									cashPurchaseDescription.setText(mGameResources.getProductDetailsList().get(purchase_item_no).getDescription());
								
								cashPurchaseCost.setText("Cost: " + mGameResources.getProductDetailsList().get(purchase_item_no).getPrice());
								
								//Attached a equip & buy icon and register the touch area to allow purchasing n equiping of the spacecraft
		                    	if(mMainActivity.mIsmPremiumNoAds && purchase_item_no == 4)
									purchase_icon.setCurrentTileIndex(3);
								else	
									purchase_icon.setCurrentTileIndex(2);
								
								mCashPurchaseScene.registerTouchArea(purchase_icon);
								
								purchase_icon.setVisible(true);
							}
						} else {
				    		mMainActivity.runOnUiThread(new Runnable() {
								public void run() {
									mGameResources.getmGameHelper().makeSimpleDialog(mGameResources.getmBaseContext().getString(R.string.signin_to_purchase)).show();
								}
							});
				    	}
					}
					return true;
				}
			};
			cashPurchaseIcon.setScale(0.9f);
			cashPurchaseIcon.setCurrentTileIndex(x);
			mCashPurchaseScene.registerTouchArea(cashPurchaseIcon);
    		mCashPurchaseScene.attachChild(cashPurchaseIcon);
    		mCashPurchaseScene.attachChild(purchase_icon);
    		
			spriteX += cashPurchaseIcon.getWidth() + 10;
			if(x != 0 && x % 3 == 0) {
				spriteX = PADDINGX;
				spriteY += cashPurchaseIcon.getHeight() + 10;				
			}
		}

		mCashPurchaseScene.setBackgroundEnabled(false);
		return mCashPurchaseScene;
	}
	
	@Override
	public boolean onSceneTouchEvent(Scene pScene, TouchEvent pSceneTouchEvent) {
		if(pScene.equals(mAchievementScene) || pScene.equals(mProfileScene)) {
			mScrollDetector.onTouchEvent(pSceneTouchEvent);
		}
		return true;
	}

	@Override
	public void onScroll(ScrollDetector pScollDetector, int pPointerID,	float pDistanceX, float pDistanceY) {
		//if(onItemClicked) onItemClickedThenScroll = true;
		
		//afterScrollingDistanceY = pDistanceY;
		
		// Return if ends are reached
		if (((mCurrentY - pDistanceY) < mMinY)) {
			return;
		} else if ((mCurrentY - pDistanceY) > mMaxY) {
			return;
		}

		// Center camera to the current point
		mCamera.offsetCenter(0, -pDistanceY);
		mCurrentY -= (pDistanceY);
		
		scrollingPoint(pDistanceY);
	}

	private void scrollingPoint(float pDistanceY) {
		if(hangerBackground != null)
			hangerBackground.setPosition(hangerBackground.getX(), (mCamera.getCenterY() - CAMERA_HEIGHT / 2 ));
		if(washOutBackground != null)
			washOutBackground.setPosition(hangerBackground.getX(), (mCamera.getCenterY() - CAMERA_HEIGHT / 2 ));
		if(mBackButton != null)
			mBackButton.setPosition(mBackButton.getX(), (mCamera.getCenterY() - CAMERA_HEIGHT / 2 ) + 10);
		if(sub_SubBackground != null)
			sub_SubBackground.setPosition(subBackground.getX(), (mCamera.getCenterY() - CAMERA_HEIGHT / 2 ) + ((CAMERA_HEIGHT - mGameResources.getmProfileSubSceneBackgroundTextureRegion().getHeight()) / 2));
		if(subBackground != null)
			subBackground.setPosition(subBackground.getX(), (mCamera.getCenterY() - CAMERA_HEIGHT / 2 ) + ((CAMERA_HEIGHT - mGameResources.getmProfileSceneBackgroundTextureRegion().getHeight()) / 2));
		if(subHeader != null)
			subHeader.setPosition(subHeader.getX(), (mCamera.getCenterY() - CAMERA_HEIGHT / 2 ) + subHeader.getHeight() + 10);
		if(G_play_icon != null)
			G_play_icon.setPosition(G_play_icon.getX(), (mCamera.getCenterY() - CAMERA_HEIGHT / 2 ) + 44);
		if(achievement_icon != null)
			achievement_icon.setPosition(achievement_icon.getX(), (mCamera.getCenterY() - CAMERA_HEIGHT / 2 ) + 208.5f);
		
		//You_Have_Icon.setPosition(CAMERA_WIDTH - mGameResources.getmYouHaveIconTextureRegion().getWidth() - 28, (mCamera.getCenterY() - CAMERA_HEIGHT / 2 ) + 10);
		//obtainedGold.setPosition(CAMERA_WIDTH - 169, (mCamera.getCenterY() - CAMERA_HEIGHT / 2 ) + 40);
				
		// Because Camera can have negative X values, so set to 0
		if (mCamera.getYMin() < 0) {
			mCamera.offsetCenter(0, 0);
			mCurrentY = 0;
		}
	}

	@Override
	public void onScrollStarted(ScrollDetector pScollDetector, int pPointerID, float pDistanceX, float pDistanceY) { }
	@Override
	public void onScrollFinished(ScrollDetector pScollDetector, int pPointerID,	float pDistanceX, float pDistanceY) { }

    // Callback for when a purchase is finished
    IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
        public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
            //Log.d(mGameResources.TAG, "Purchase finished: " + result + ", purchase: " + purchase);

            if(purchase != null) {
	        	// The purchase was processed. We will send the transaction and its associated line items to Google Analytics,
	        	// but only if the purchase has been confirmed. Build an item and send
	    	    myTracker.send(new HitBuilders.ItemBuilder()
	            .setTransactionId(purchase.getOrderId())
	            .setSku(purchase.getSku())
	            .setCategory(purchase.getItemType())
	            .setQuantity(1)
	            .set("Developer Payload", purchase.getDeveloperPayload())
	            .set("User Google ID", mGameResources.getGoogleAccountID())
	            .set("Purchase State", ""+purchase.getPurchaseState())
	            .set("Purchase Time", ""+purchase.getPurchaseTime())
	            .set("Purchasing Result", "" + result)
	            .set("IAB Null Status", "" + (mMainActivity.mIabHelper == null))
	            .setCurrencyCode("SGD")
	            .build());
            }
            
            // if we were disposed of in the meantime, quit.
            if (mMainActivity.mIabHelper == null) return;

            if (result.isFailure()) {
                complain("Error purchasing: " + result);
                return;
            }
            
            if (!mMainActivity.verifyDeveloperPayload(purchase)) {            
                complain("Error purchasing. Authenticity verification failed.");
                return;
            }

            //Log.d(mGameResources.TAG, "Purchase successful.");

            mMainActivity.mIabHelper.consumeAsync(purchase, mConsumeFinishedListener);
        }
    };

    // Called when consumption is complete
    IabHelper.OnConsumeFinishedListener mConsumeFinishedListener = new IabHelper.OnConsumeFinishedListener() {
        public void onConsumeFinished(Purchase purchase, IabResult result) {
            //Log.d(mGameResources.TAG, "Consumption finished. Purchase: " + purchase + ", result: " + result);

            // if we were disposed of in the meantime, quit.
            if (mMainActivity.mIabHelper == null) return;
            
            String message = "";
            if (result.isSuccess()) {
                // successfully consumed, so we apply the effects of the item in our
                // game world's logic, which in our case means filling up the humans captured

                for(String product : mGameResources.getAdditionalSkuList()) {
                	if (purchase.getSku().equals(product) && !product.equals(mGameResources.getmPremiumNoAds())) {
                		int goldPurchased = Integer.parseInt(product.replaceAll("[\\D]", ""));
                    	saveData(goldPurchased);
                    	message = goldPurchased + " Gold Consumption successful.";
                    } else if (purchase.getSku().equals(mGameResources.getmPremiumNoAds())) {
                    	mMainActivity.mIsmPremiumNoAds = true;
                    	mGameResources.getmSaveGame().setPremiumNoAds(mMainActivity.mIsmPremiumNoAds);
                    	message = "Ads Free Consumption successful.";
                    }
                }
            	mMainActivity.saveSnapshot();
            }
            else {
            	message = "Error while consuming: " + result;
                complain(message);
            }

            //Log.d(mGameResources.TAG, message);
            myTracker.send(new HitBuilders.EventBuilder()
            .setAction("InApp Purchase Consumption")
            .set("Consumption", "User " + purchase.getDeveloperPayload() + " : " + message)
            .build());
            
            //Log.d(mGameResources.TAG, "End consumption flow.");
        }
    };
        
    void complain(String message) {
        //Log.e(mGameResources.TAG, "Purchase Error: " + message);
    }
    
    void saveData(int value) {
    	//Save the gold purchased from google into the total obtained gold
    	Score_SharedPreference highScores = new Score_SharedPreference(mGameResources.getmBaseContext(),"");
    	highScores.saveTotalObtainedGold(value);
		obtainedGold.setText(""+highScores.loadObtainedDetuctableGold());
        //Log.d(TAG, "Purchase & Saved: " + String.valueOf(value));
        //Log.d(TAG, "Final Detuctable Gold: " + String.valueOf(highScores.loadObtainedDetuctableGold()));
        //Log.d(TAG, "Final Total Gold: " + String.valueOf(highScores.loadTotalObtainedGold()));
    }
}
