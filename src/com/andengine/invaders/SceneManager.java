/*
 * Copyright (C) 2013 Knivore Studios
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.andengine.invaders;

import org.andengine.engine.Engine;
import org.andengine.engine.camera.Camera;
import org.andengine.engine.handler.timer.ITimerCallback;
import org.andengine.engine.handler.timer.TimerHandler;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.scene.background.Background;
import org.andengine.entity.sprite.Sprite;
import org.andengine.extension.physics.box2d.FixedStepPhysicsWorld;
import org.andengine.extension.physics.box2d.PhysicsConnector;
import org.andengine.extension.physics.box2d.PhysicsFactory;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.math.MathUtils;

import android.content.Context;
import android.hardware.SensorManager;

import com.andengine.invaders.MainActivity.TrackerName;
import com.andengine.sharedpreferences.Options_SharedPreference;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;


/**
 * @author Keh Chin Leong
 */
public class SceneManager {

	private static final int CAMERA_WIDTH = 1280;
	private static final int CAMERA_HEIGHT = 720;
	
	private String currentScene = "SplashScreen";
	//private boolean loadmGameResourcesCallbackComplete = false;

	private Tracker myTracker;
	
	private Camera mCamera;
	private GameResources mGameResources;
	private VertexBufferObjectManager mVertexBufferObjectManager;
	private Engine mEngine;
	private SensorManager sensorManager;
	private Options_SharedPreference options;

	private MainActivity mMainActivity;
	private MainMenuScene mMainMenuSceneClass;
	private HangerScene mHangerSceneClass;
	private GameScene mGameSceneClass;
	private Scene mTipScene;
	private MultiplayerPlayer1GameScene mMultiplayerPlayer1GameScene;
	private MultiplayerPlayer2GameScene mMultiplayerPlayer2GameScene;
	
	public SceneManager(Engine engine, SensorManager sm, GameResources gr, VertexBufferObjectManager vbom, Camera cam, Options_SharedPreference opSharedPreference, MainActivity mainActivity, Context mContext) {
		this.mEngine = engine;
		this.sensorManager = sm;
		this.mGameResources = gr;
		this.mVertexBufferObjectManager = vbom;
		this.mCamera = cam;
		//this.options = opSharedPreference;
		this.options = new Options_SharedPreference(gr.getmBaseContext());
		this.mMainActivity = mainActivity;
		
		myTracker = mainActivity.getTracker(TrackerName.APP_TRACKER);		
	}
	
	public String getCurrentScene() {
		return currentScene;
	}
	
	public void setCurrentScene(String scene) {
		currentScene = scene;
	}
	
	public void loadLoadingScene() {
		Scene loadingScene = new Scene();
		loadingScene.setBackground(new Background(0f,0f,0f,0f));
				
		Sprite KnivoreStudiosLogo = new Sprite((CAMERA_WIDTH - mGameResources.getmSplashTextureRegion().getWidth()) / 2, (CAMERA_HEIGHT - mGameResources.getmSplashTextureRegion().getHeight()) / 2, mGameResources.getmSplashTextureRegion(), mVertexBufferObjectManager);
		KnivoreStudiosLogo.setScale(0.5f);
		loadingScene.attachChild(KnivoreStudiosLogo);
		
		Sprite loadingSprite = new Sprite(1, CAMERA_HEIGHT - mGameResources.getmLoadingTextureRegion().getHeight(), mGameResources.getmLoadingTextureRegion(), mVertexBufferObjectManager);
		loadingSprite.setScale(0.7f);
		loadingScene.attachChild(loadingSprite);
		
		loadingSprite.setRotation(5f);

		mGameResources.getLoadingText().detachSelf();
		loadingScene.attachChild(mGameResources.getLoadingText());	
		
		FixedStepPhysicsWorld physicsWorld = new FixedStepPhysicsWorld(60, new Vector2(0, 0), true);
		loadingScene.registerUpdateHandler(physicsWorld);
		final FixtureDef wallFixtureDef = PhysicsFactory.createFixtureDef(1, 0.5f, 2f);
		Body loadingSpriteBody = PhysicsFactory.createBoxBody(physicsWorld, loadingSprite, BodyType.DynamicBody, wallFixtureDef);
		physicsWorld.registerPhysicsConnector(new PhysicsConnector(loadingSprite, loadingSpriteBody, true, true));
		loadingSpriteBody.applyAngularImpulse(5f);

		mEngine.setScene(loadingScene);
		
		mMainActivity.showAdvertisement();
		
		currentScene = "LoadingScene";
	}

	public void loadTipScene(boolean restart) {
		mTipScene = new Scene();
		mTipScene.setBackground(new Background(0f,0f,0f,0f));
		
		Sprite tip1Image = new Sprite((CAMERA_WIDTH - mGameResources.getmTip1TextureRegion().getWidth()) / 2, (CAMERA_HEIGHT - mGameResources.getmTip1TextureRegion().getHeight()) / 2, mGameResources.getmTip1TextureRegion(), mVertexBufferObjectManager);
		tip1Image.setScale(0.8f);

		Sprite tip2Image = new Sprite((CAMERA_WIDTH - mGameResources.getmTip2TextureRegion().getWidth()) / 2, (CAMERA_HEIGHT - mGameResources.getmTip2TextureRegion().getHeight()) / 2, mGameResources.getmTip2TextureRegion(), mVertexBufferObjectManager);
		tip2Image.setScale(0.8f);

		Sprite tip3Image = new Sprite((CAMERA_WIDTH - mGameResources.getmTip3TextureRegion().getWidth()) / 2, (CAMERA_HEIGHT - mGameResources.getmTip3TextureRegion().getHeight()) / 2, mGameResources.getmTip3TextureRegion(), mVertexBufferObjectManager);
		tip3Image.setScale(0.8f);
		
		if(restart) {
			int random = MathUtils.random(1, 3);
			if(random == 1) {
				mTipScene.attachChild(tip1Image);
			} else if(random == 2) {
				mTipScene.attachChild(tip2Image);
			} else if(random == 3) {
				mTipScene.attachChild(tip3Image);
			}
		} else {
			mTipScene.attachChild(tip3Image);
		}
		
		Sprite loadingSprite = new Sprite(1, CAMERA_HEIGHT - mGameResources.getmLoadingTextureRegion().getHeight(), mGameResources.getmLoadingTextureRegion(), mVertexBufferObjectManager);
		loadingSprite.setScale(0.7f);
		mTipScene.attachChild(loadingSprite);
		
		loadingSprite.setRotation(5f);

		mGameResources.getLoadingText().detachSelf();
		mTipScene.attachChild(mGameResources.getLoadingText());	
		
		FixedStepPhysicsWorld physicsWorld = new FixedStepPhysicsWorld(60, new Vector2(0, 0), true);
		mTipScene.registerUpdateHandler(physicsWorld);
		final FixtureDef wallFixtureDef = PhysicsFactory.createFixtureDef(1, 0.5f, 2f);
		Body loadingSpriteBody = PhysicsFactory.createBoxBody(physicsWorld, loadingSprite, BodyType.DynamicBody, wallFixtureDef);
		physicsWorld.registerPhysicsConnector(new PhysicsConnector(loadingSprite, loadingSpriteBody, true, true));
		loadingSpriteBody.applyAngularImpulse(5f);
		
		mEngine.getScene().setChildScene(mTipScene);
		currentScene = "TipScene";
	}
	
	public void clearTipScene() {
		if(mEngine.getScene().hasChildScene() && mEngine.getScene().getChildScene().equals(mTipScene)) {
			mTipScene.back();
			currentScene = "MainMenuScene";
		}
	}

	public void loadMainMenuScene() {
		mCamera.setCenter(CAMERA_WIDTH / 2, CAMERA_HEIGHT / 2);
		
		mMainMenuSceneClass = new MainMenuScene(mEngine, sensorManager, mGameResources, mVertexBufferObjectManager, mCamera, options, SceneManager.this, mMainActivity);
		mMainMenuSceneClass.loadMainMenuScene();
		
		if(options.getMusic()) mGameResources.getMainBGM().play();
		
		mEngine.setScene(mMainMenuSceneClass.getmMenuScene());
		currentScene = "MainMenuScene";

		mMainActivity.showAdvertisement();
		
		myTracker.setScreenName("Main Menu Screen");
		myTracker.send(new HitBuilders.AppViewBuilder().build());
	}
	
	//true = login-ed, false = logout-ed
	public void updateGoogleButton(boolean login) {
        //if (currentScene != "MainMenuScene") return;
		if(mMainMenuSceneClass.getmGoogleButtonSprite() != null) {
			//Set the google button image on the main menu 
			if (login)
				mMainMenuSceneClass.getmGoogleButtonSprite().setCurrentTileIndex(0);
			else
				mMainMenuSceneClass.getmGoogleButtonSprite().setCurrentTileIndex(1);
		} else 
			mGameResources.setmGoogleButtonCurrentTileIndex(login);
	}
	
	//true = login-ed, false = logout-ed
	public void updateFacebookButton(boolean login) {
        if (login)
			mMainMenuSceneClass.getmFacebookButtonSprite().setCurrentTileIndex(0);
		else
			mMainMenuSceneClass.getmFacebookButtonSprite().setCurrentTileIndex(1);
	}
	
	public void loadHangerScene() {
		this.mCamera.setCenter(CAMERA_WIDTH / 2, CAMERA_HEIGHT / 2);
		mHangerSceneClass = new HangerScene(mEngine, sensorManager, mGameResources, mVertexBufferObjectManager, mCamera, options, SceneManager.this, mMainActivity);
		mHangerSceneClass.loadHangerScene();
		mEngine.setScene(mHangerSceneClass.getmHangerScene());
		currentScene = "HangerScene";
		
		//mMainActivity.hideAdvertisement();
		
		myTracker.setScreenName("Hanger Screen");
		myTracker.send(new HitBuilders.AppViewBuilder().build());
	}
	
	public void loadGameScene(final float xDegrees, final float yDegrees, boolean restart) {
		mGameResources.getMainBGM().seekTo(0);
		mGameResources.getMainBGM().pause();

		loadTipScene(restart);
		
		//Check whether resources has complete loading
		if(restart || mGameResources.gameResources_PART3_Loaded) {
			mEngine.registerUpdateHandler(new TimerHandler(2f, true, new ITimerCallback() {
				@Override
				public void onTimePassed(final TimerHandler pTimerHandler) {
					if(mGameResources.gameResources_PART3_Loaded) {
					mMainActivity.hideAdvertisement();
					
					final String DESTINATED_MAP = mGameResources.getCountryName()[MathUtils.random(0,mGameResources.getCountryName().length - 1)];
					mCamera.setCenter(CAMERA_WIDTH / 2, CAMERA_HEIGHT / 2);

					mGameSceneClass = new GameScene(mEngine, sensorManager, mGameResources, mVertexBufferObjectManager, mCamera, options, "", DESTINATED_MAP, xDegrees, yDegrees, SceneManager.this, mMainActivity);
					mGameSceneClass.loadGameScene();
					mEngine.setScene(mGameSceneClass.getmGameScene());
					currentScene = "GameScene";
					mEngine.unregisterUpdateHandler(pTimerHandler);
	
					myTracker.setScreenName("Game Screen - " + DESTINATED_MAP + " Map");
					myTracker.send(new HitBuilders.AppViewBuilder().build());
					}
				}
			}));
		} else {
			mEngine.registerUpdateHandler(new TimerHandler(3f, true, new ITimerCallback() {
				@Override
				public void onTimePassed(final TimerHandler pTimerHandler) {
					if(mGameResources.gameResources_PART3_Loaded) {
						mMainActivity.hideAdvertisement();
						
						final String DESTINATED_MAP = mGameResources.getCountryName()[MathUtils.random(0,mGameResources.getCountryName().length - 1)];
						mCamera.setCenter(CAMERA_WIDTH / 2, CAMERA_HEIGHT / 2);
	
						mGameSceneClass = new GameScene(mEngine, sensorManager, mGameResources, mVertexBufferObjectManager, mCamera, options, "", DESTINATED_MAP, xDegrees, yDegrees, SceneManager.this, mMainActivity);
						mGameSceneClass.loadGameScene();
						mEngine.setScene(mGameSceneClass.getmGameScene());
						currentScene = "GameScene";
						mEngine.unregisterUpdateHandler(pTimerHandler);
		
						myTracker.setScreenName("Game Screen - " + DESTINATED_MAP + " Map");
						myTracker.send(new HitBuilders.AppViewBuilder().build());
					}
				}
			}));
		}
	}

	public MultiplayerPlayer1GameScene loadMultiplayerPlayer1GameScene(float xDegrees, float yDegrees, String DESTINATED_MAP) {
		mGameResources.getMainBGM().seekTo(0);
		mGameResources.getMainBGM().pause();

		loadLoadingScene();

		mMainActivity.hideAdvertisement();

		if(options.getMusic()) mGameResources.getMainBGM().pause();
		
		mCamera.setCenter(CAMERA_WIDTH / 2, CAMERA_HEIGHT / 2);
		
		if(mMultiplayerPlayer1GameScene != null)
			mMultiplayerPlayer1GameScene = null;
		
		mMultiplayerPlayer1GameScene = new MultiplayerPlayer1GameScene(mEngine, sensorManager, mGameResources, mVertexBufferObjectManager, mCamera, options, "", DESTINATED_MAP, xDegrees, yDegrees, SceneManager.this, mMainActivity);
		mMultiplayerPlayer1GameScene.loadGameScene();
		mEngine.setScene(mMultiplayerPlayer1GameScene.getmGameScene());
		currentScene = "MultiplayerGameScene";

		myTracker.setScreenName("Multiplayer Player 1 Game Screen - " + DESTINATED_MAP + " Map");
		myTracker.send(new HitBuilders.AppViewBuilder().build());
		
		return mMultiplayerPlayer1GameScene;
	}
	
	public MultiplayerPlayer2GameScene loadMultiplayerPlayer2GameScene(float xDegrees, float yDegrees, String DESTINATED_MAP) {
		mGameResources.getMainBGM().seekTo(0);
		mGameResources.getMainBGM().pause();

		loadLoadingScene();

		mMainActivity.hideAdvertisement();

		if(options.getMusic()) mGameResources.getMainBGM().pause();
		
		mCamera.setCenter(CAMERA_WIDTH / 2, CAMERA_HEIGHT / 2);
		
		if(mMultiplayerPlayer2GameScene != null)
			mMultiplayerPlayer2GameScene = null;
		
		mMultiplayerPlayer2GameScene = new MultiplayerPlayer2GameScene(mEngine, sensorManager, mGameResources, mVertexBufferObjectManager, mCamera, options, "", DESTINATED_MAP, xDegrees, yDegrees, SceneManager.this, mMainActivity);
		mMultiplayerPlayer2GameScene.loadGameScene();
		mEngine.setScene(mMultiplayerPlayer2GameScene.getmGameScene());
		currentScene = "MultiplayerGameScene";

		myTracker.setScreenName("Multiplayer Player 2 Game Screen - " + DESTINATED_MAP + " Map");
		myTracker.send(new HitBuilders.AppViewBuilder().build());
		
		return mMultiplayerPlayer2GameScene;
	}
	
	public void pauseGame() {
		mGameSceneClass.pauseGame(true);
	}
	
	public void resumeGame() {
		if(mGameSceneClass.getmPauseScene().hasChildScene()) 
			mGameSceneClass.getmPauseScene().clearChildScene();
		else
			mGameSceneClass.resumeGame();
	}

	public void setFacebookShared(boolean result) {
		mGameSceneClass.setFacebookShared(result);
	}
}
