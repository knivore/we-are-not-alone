/*
 * Copyright (C) 2013 Knivore Studios
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.andengine.invaders;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Random;

import javax.microedition.khronos.opengles.GL10;

import org.andengine.audio.music.Music;
import org.andengine.engine.Engine;
import org.andengine.engine.camera.Camera;
import org.andengine.engine.camera.hud.controls.AnalogOnScreenControl;
import org.andengine.engine.camera.hud.controls.AnalogOnScreenControl.IAnalogOnScreenControlListener;
import org.andengine.engine.camera.hud.controls.BaseOnScreenControl;
import org.andengine.engine.handler.IUpdateHandler;
import org.andengine.engine.handler.timer.ITimerCallback;
import org.andengine.engine.handler.timer.TimerHandler;
import org.andengine.entity.IEntity;
import org.andengine.entity.modifier.AlphaModifier;
import org.andengine.entity.modifier.IEntityModifier;
import org.andengine.entity.modifier.MoveYModifier;
import org.andengine.entity.modifier.ScaleModifier;
import org.andengine.entity.scene.CameraScene;
import org.andengine.entity.scene.IOnAreaTouchListener;
import org.andengine.entity.scene.IOnSceneTouchListener;
import org.andengine.entity.scene.ITouchArea;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.sprite.AnimatedSprite;
import org.andengine.entity.sprite.AnimatedSprite.IAnimationListener;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.sprite.TiledSprite;
import org.andengine.entity.text.Text;
import org.andengine.entity.text.TextOptions;
import org.andengine.entity.util.FPSLogger;
import org.andengine.extension.physics.box2d.FixedStepPhysicsWorld;
import org.andengine.extension.physics.box2d.PhysicsConnector;
import org.andengine.extension.physics.box2d.PhysicsFactory;
import org.andengine.extension.physics.box2d.util.Vector2Pool;
import org.andengine.extension.physics.box2d.util.constants.PhysicsConstants;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.HorizontalAlign;
import org.andengine.util.color.Color;
import org.andengine.util.math.MathUtils;
import org.andengine.util.modifier.IModifier;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.opengl.GLES20;
import android.util.Log;
import android.view.MotionEvent;

import com.andengine.classes.CarPool;
import com.andengine.classes.CarSprite;
import com.andengine.classes.DisintegrateHumanPool;
import com.andengine.classes.DisintegrateHumanSprite;
import com.andengine.classes.ExplosionPool;
import com.andengine.classes.ExplosionSprite;
import com.andengine.classes.GoldPool;
import com.andengine.classes.GoldSprite;
import com.andengine.classes.HumanPool;
import com.andengine.classes.HumanSprite;
import com.andengine.classes.LampPool;
import com.andengine.classes.LampSprite;
import com.andengine.classes.LaserBeamAlertPool;
import com.andengine.classes.LaserBeamPool;
import com.andengine.classes.LaserBeamSprite;
import com.andengine.classes.Map;
import com.andengine.classes.MilitaryHumanPool;
import com.andengine.classes.MilitaryHumanSprite;
import com.andengine.classes.MilitaryMissileAlertPool;
import com.andengine.classes.MilitaryMissileAlertSprite;
import com.andengine.classes.MilitaryMissilePool;
import com.andengine.classes.MilitaryMissileSprite;
import com.andengine.classes.MissileAlertPool;
import com.andengine.classes.MissileHeadPool;
import com.andengine.classes.MissileHeadSprite;
import com.andengine.classes.MissileTailPool;
import com.andengine.classes.MissileTailSprite;
import com.andengine.classes.ScatterMissileAlertPool;
import com.andengine.classes.ScatterMissileHeadPool;
import com.andengine.classes.ScatterMissileHeadSprite;
import com.andengine.classes.ScatterMissileTailPool;
import com.andengine.classes.ScatterMissileTailSprite;
import com.andengine.classes.SmokePool;
import com.andengine.classes.SmokeSprite;
import com.andengine.classes.Spacecraft;
import com.andengine.classes.TrafficPool;
import com.andengine.classes.TrafficSprite;
import com.andengine.classes.WeaponAlertSprite;
import com.andengine.invaders.MainActivity.TrackerName;
import com.andengine.sharedpreferences.Gadgets_SharedPreference;
import com.andengine.sharedpreferences.Options_SharedPreference;
import com.andengine.sharedpreferences.Score_SharedPreference;
import com.andengine.sharedpreferences.Spacecraft_Meter_SharedPreference;
import com.andengine.sharedpreferences.Spacecraft_SharedPreference;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.multiplayer.realtime.RealTimeMessage;
import com.makersf.andengine.extension.collisions.entity.sprite.PixelPerfectAnimatedSprite;

/**
 * @author Keh Chin Leong
 */
public class MultiplayerPlayer1GameScene implements SensorEventListener, IOnAreaTouchListener, IOnSceneTouchListener  {

	//===========================================================
	// Constants
	//===========================================================
	private static final int CAMERA_WIDTH = 1280;
	private static final int CAMERA_HEIGHT = 720;	

	private float POSITION_X_ANGLE = 0f;
	private float POSITION_Y_ANGLE = 0f;
	private String DESTINATED_MAP = "";
	private int humanOnFirstLoadCount, lampOnFirstLoadCount = 0;
	private boolean UFO_BEAM_PREFERENCE = false; //Preference is for the evil/goodness meter (True = Evil, False = Good)

	private Tracker myTracker;
	
	private Score_SharedPreference highScores;
	private Spacecraft_SharedPreference spacecraft_sharedpreference;
	
	private MainActivity mMainActivity;
	
	private SceneManager sceneManager;
	private Camera mCamera;
	private GameResources mGameResources;
	private VertexBufferObjectManager mVertexBufferObjectManager;
	private Engine mEngine;
	
	//===========================================================
	// Fields
	//===========================================================
	private SensorManager sensorManager;

	private float accelerometerSpeedX;
	private float accelerometerSpeedY;
	private float accelerometerSpeedZ;
	private float sX, sY; // Sprite coordinates
    
	//----------------------------------------------
	//----------------- Game Scene -----------------
	private Scene mGameScene;

	// Background/SFX Sounds
	private Music gameBGM, laserBeamSFX, ufoHoverSFX, ufoBeamSFX, explosionSFX, countdownSFX, laserCountdownSFX, missileLaunchSFX, shieldHitSFX;	
	
	// Parallax Map
	private Map PARALLAX_MAP;

	// Physics World & Objects
	private FixedStepPhysicsWorld mEarthPhysicsWorld;
	//1st= ?? , 2nd is bounce, 3rd is friction?
	//private static final FixtureDef SPRITE_FIXTURE_DEF = PhysicsFactory.createFixtureDef(0, 0f, 0f);
	private static final FixtureDef FIXTURE_DEF = PhysicsFactory.createFixtureDef(0, 0f, 0f);

	// Handlers
	private IUpdateHandler updateSpritePosition;
	private TimerHandler updateDistanceHandler;
	private TimerHandler updateHumansHandler;
	private TimerHandler updateMilitaryHumansHandler;
	private TimerHandler updateSpeedHandler;
	private TimerHandler updateLampHandler;
	private TimerHandler updateCarHandler;
	private TimerHandler updateTrafficHandler;
	private TimerHandler displayLaserBeamAlertHandler;
	private TimerHandler displayMissileAlertHandler;
	private TimerHandler displayScatterMissileAlertHandler;
	//private TimerHandler updateBackgroundHandler;

	// Analog On Screen Controls textures
	private AnalogOnScreenControl velocityOnScreenControl;
	private Sprite angelButton, devilButton;
	//private PhysicsHandler playerPhysicsHandler, beamPhysicsHandler, windPhysicsHandler;

	// UFO textures
	private AnimatedSprite blackhole_Sprite;
	private float player1SpriteScaleMeter = 0.1f, player2SpriteScaleMeter = 0.1f;
	private float blackholeSpriteScaleMeter = 0.1f;
	private Body player1SpriteBody;
	private Spacecraft player1Spacecraft, player2Spacecraft;
	private PixelPerfectAnimatedSprite player1Sprite, player2Sprite;

	private HumanPool HUMAN_POOL;
	private LinkedList<HumanSprite> humanSpriteArray = new LinkedList<HumanSprite>();

	private DisintegrateHumanPool DISINTEGRATED_HUMAN_POOL;
	private LinkedList<DisintegrateHumanSprite> DisintegratedHumanSpriteArray = new LinkedList<DisintegrateHumanSprite>();

	private int militaryHumansCombo = 0;
	private MilitaryHumanPool MILITARY_POOL;
	private LinkedList<MilitaryHumanSprite> militaryHumanSpriteArray = new LinkedList<MilitaryHumanSprite>();
	private MilitaryMissileAlertPool MILITARY_MISSILE_ALERT_POOL;
	private LinkedList<MilitaryMissileAlertSprite> militaryMissileAlertSpriteArray = new LinkedList<MilitaryMissileAlertSprite>();
	private MilitaryMissilePool MILITARY_MISSILE_POOL;
	private LinkedList<MilitaryMissileSprite> militaryMissileSpriteArray = new LinkedList<MilitaryMissileSprite>();
	
	private LampPool LAMP_POOL;
	private LinkedList<LampSprite> lampSpriteArray = new LinkedList<LampSprite>();
	private CarPool CAR_POOL;
	private LinkedList<CarSprite> carSpriteArray = new LinkedList<CarSprite>();
	private TrafficPool TRAFFIC_POOL;
	private LinkedList<TrafficSprite> trafficSpriteArray = new LinkedList<TrafficSprite>();

	private LaserBeamAlertPool LASER_BEAM_ALERT_POOL;
	private LinkedList<WeaponAlertSprite> laserBeamAlertSpriteArray = new LinkedList<WeaponAlertSprite>();
	private LaserBeamPool LASER_BEAM_POOL;
	private LinkedList<LaserBeamSprite> laserBeamSpriteArray = new LinkedList<LaserBeamSprite>();

	private MissileAlertPool MISSILE_ALERT_POOL;
	private LinkedList<WeaponAlertSprite> missileAlertSpriteArray = new LinkedList<WeaponAlertSprite>();
	private MissileHeadPool MISSILE_HEAD_POOL;
	private LinkedList<MissileHeadSprite> missileHeadSpriteArray = new LinkedList<MissileHeadSprite>();
	private MissileTailPool MISSILE_TAIL_POOL;
	private LinkedList<MissileTailSprite> missileTailSpriteArray = new LinkedList<MissileTailSprite>();
	
	private ScatterMissileHeadPool SCATTER_MISSILE_HEAD_POOL;
	private LinkedList<ScatterMissileHeadSprite> scatterMissileHeadSpriteArray = new LinkedList<ScatterMissileHeadSprite>();
	private ScatterMissileTailPool SCATTER_MISSILE_TAIL_POOL;
	private LinkedList<ScatterMissileTailSprite> scatterMissileTailSpriteArray = new LinkedList<ScatterMissileTailSprite>();
	private ScatterMissileAlertPool SCATTER_MISSILE_ALERT_POOL;
	private LinkedList<WeaponAlertSprite> scatterMissileAlertSpriteArray = new LinkedList<WeaponAlertSprite>();
	
	private ExplosionPool EXPLOSION_POOL;
	private LinkedList<ExplosionSprite> explosionSpriteArray = new LinkedList<ExplosionSprite>();
	private SmokePool SMOKE_POOL;
	private LinkedList<SmokeSprite> smokeSpriteArray = new LinkedList<SmokeSprite>();
	private GoldPool GOLD_POOL;
	
	// Beam textures
	private PixelPerfectAnimatedSprite player1BeamSprite, player2BeamSprite;

	// Wind landing textures
	private PixelPerfectAnimatedSprite player1WindLandingSprite, player2WindLandingSprite;
	
	// Wind landing textures
	private Sprite forcefieldSprite;
	
	// Distance Font texture
	private Text distanceText, goldObtainedText, gameOverDistanceText, gameOverDistanceMeasurementText;
	private int distanceTravelled;
	private int goldObtained, humansAbsorbed, humansDisintegrated;

	//----------------------------------------------
	//------------------- Gadgets ------------------	
	private boolean Shield_on = false;
	private boolean ExtendTimer_on = false;
	private boolean MegaBeam_on = false;
	private boolean MissileJammer_on = false;
	private boolean HumanMagnet_on = false;
	private boolean Cloaking_on = false;
	private boolean Conversion_on = false;
	private boolean SOS_on = false;
	
	private HashMap<String, Float> leftOverTimerList = new HashMap<String, Float>();

	//----------------------------------------------
	//----------------- Magnet Scene ----------------	
	private Scene mMagnetScene;
	private float mMagnetPosX, mMagnetPosY;
	
	//----------------------------------------------
	//----------------- Pause Scene ----------------	
	private Scene mPauseScene;
	private boolean mPause = false;
	private Sprite pauseButton;
	private TiledSprite mSFXSprite;
	private TiledSprite mMusicSprite;
	
	//----------------------------------------------
	//-------------- Game Over Scene ---------------	
	private Scene mGameOverScene;
	private boolean mPlayerHit = false;
	private boolean mGameOver = false;

	// Game Over textures
	private Sprite mNewHighScoreSprite;
	
	//----------------------------------------------
	//-------------- Sharedpreference --------------	
	private Options_SharedPreference options;
	private boolean music_on;
	private boolean sfx_on;
	private boolean controlpads_on;

	private Spacecraft_Meter_SharedPreference meter_gauge;
	
	//----------------------------------------------
	//----------------- Multiplayer ----------------
	private boolean player2Loaded;
	
	
	
	//----------------------------------------------
	//------------------- Others -------------------
	private boolean gameStarted;
	private boolean player_on_flinch_mode = false, flinch_mode_activated = false;
	private boolean player_rotated = false;
	private float gameSpeed = (float) 0;
	private float beamPositionX, beamPositionY;
	private TimerHandler playerFlinchRotateTimer;
	//private HoldDetector onSceneTouchHold;

	//TO BE DELETED
	private FPSLogger debugFPS;
	private Text fpsText;
	private float tmp;

	
	//===========================================================
	// Constructor
	//===========================================================
	public MultiplayerPlayer1GameScene(Engine engine, SensorManager sensor, GameResources gr, VertexBufferObjectManager vbom, Camera c, Options_SharedPreference o, String weather, String destinatedMap, float yDegrees, float xDegrees, SceneManager sm, MainActivity mainActivity) {
		mEngine = engine;
		sensorManager = sensor;
		mGameResources = gr;
		mVertexBufferObjectManager = vbom;
		mCamera = c;
		options = o;
		DESTINATED_MAP = destinatedMap;
		//WEATHER = weather;
		POSITION_X_ANGLE = xDegrees;
		POSITION_Y_ANGLE = yDegrees;
		sceneManager = sm;
				
		PARALLAX_MAP = new Map(mCamera, CAMERA_HEIGHT, vbom, gr);
		
		LAMP_POOL = new LampPool(CAMERA_HEIGHT, CAMERA_WIDTH, vbom, gr, PARALLAX_MAP);
		CAR_POOL = new CarPool(CAMERA_HEIGHT, CAMERA_WIDTH, vbom, gr, PARALLAX_MAP);
		TRAFFIC_POOL = new TrafficPool(CAMERA_HEIGHT, CAMERA_WIDTH, vbom, gr, PARALLAX_MAP);
		
		HUMAN_POOL = new HumanPool(CAMERA_HEIGHT, CAMERA_WIDTH, vbom, gr, PARALLAX_MAP);
		MILITARY_POOL = new MilitaryHumanPool(CAMERA_HEIGHT, CAMERA_WIDTH, vbom, gr, PARALLAX_MAP);
		DISINTEGRATED_HUMAN_POOL = new DisintegrateHumanPool(CAMERA_HEIGHT, CAMERA_WIDTH, vbom, gr, PARALLAX_MAP);
		
		MISSILE_ALERT_POOL = new MissileAlertPool(vbom, gr);
		MISSILE_HEAD_POOL = new MissileHeadPool(CAMERA_HEIGHT, CAMERA_WIDTH, vbom, gr);
		MISSILE_TAIL_POOL = new MissileTailPool(CAMERA_HEIGHT, CAMERA_WIDTH, vbom, gr);
		
		SCATTER_MISSILE_ALERT_POOL = new ScatterMissileAlertPool(vbom, gr);
		SCATTER_MISSILE_HEAD_POOL = new ScatterMissileHeadPool(CAMERA_HEIGHT, CAMERA_WIDTH, vbom, gr);
		SCATTER_MISSILE_TAIL_POOL = new ScatterMissileTailPool(CAMERA_HEIGHT, CAMERA_WIDTH, vbom, gr);
		
		MILITARY_MISSILE_ALERT_POOL = new MilitaryMissileAlertPool(vbom, gr);
		MILITARY_MISSILE_POOL = new MilitaryMissilePool(CAMERA_HEIGHT, CAMERA_WIDTH, vbom, gr);
		
		LASER_BEAM_ALERT_POOL = new LaserBeamAlertPool(vbom, gr);
		LASER_BEAM_POOL = new LaserBeamPool(CAMERA_HEIGHT, CAMERA_WIDTH, vbom, gr);
				
		EXPLOSION_POOL = new ExplosionPool(vbom, gr);
		SMOKE_POOL = new SmokePool(vbom, gr);
		GOLD_POOL = new GoldPool(vbom, gr);
		
		gameBGM = gr.getGameBGM();
		laserBeamSFX = gr.getLaserBeamSFX();
		ufoHoverSFX = gr.getUfoHoverSFX();
		ufoBeamSFX = gr.getUfoBeamSFX();
		explosionSFX = gr.getExplosionSFX();
		countdownSFX = gr.getCountdownSFX();
		laserCountdownSFX = gr.getCountdownSFX();
		missileLaunchSFX = gr.getMissileLaunchSFX();
		shieldHitSFX = gr.getShieldHitSFX();

		myTracker = mainActivity.getTracker(TrackerName.APP_TRACKER);
		mMainActivity = mainActivity;
		
		highScores = new Score_SharedPreference(mGameResources.getmBaseContext(), DESTINATED_MAP);
		spacecraft_sharedpreference = new Spacecraft_SharedPreference(mGameResources.getmBaseContext());

		byte[] message = new byte[] {'S', (byte) spacecraft_sharedpreference.getActiveSpacecraft()} ;
		mMainActivity.sendReliableMessageRealTime(message);
	}

	public Scene getmGameScene() {
		return mGameScene;
	}

	public Scene getmPauseScene() {
		return mPauseScene;
	}

	public Scene getmGameOverScene() {
		return mGameOverScene;
	}

	@SuppressWarnings("static-access")
	public void loadGameScene() {		
		//------------------------------------------------------------------
		// PRINT FRAME PER SECOND - TO BE COMMENTED BEFORE RELEASE
		//------------------------------------------------------------------
		//debugFPS = new FPSLogger();
		//mEngine.registerUpdateHandler(debugFPS);
		//fpsText = new Text(CAMERA_WIDTH - mGameResources.getmPauseButtonTextureRegion().getWidth() - 110, 1, mGameResources.getmFont(), "0000000", 7, new TextOptions(HorizontalAlign.CENTER), mVertexBufferObjectManager);
		//fpsText.setBlendFunction(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);
		//fpsText.setAlpha(1f);
		//fpsText.setColor(Color.YELLOW);
		//mGameScene.attachChild(fpsText);
		//------------------------------------------------------------------
		//--------------------------- END ----------------------------------
		//------------------------------------------------------------------

		player1Spacecraft = new Spacecraft(mGameResources);
		player2Spacecraft = new Spacecraft(mGameResources);
		mGameScene = new Scene();		
		mGameScene.setOnAreaTouchListener(this);
		mGameScene.setOnSceneTouchListener(this);
		mGameScene.setTouchAreaBindingOnActionDownEnabled(true);
		
		gameStarted = false;
		distanceTravelled = 0;
		goldObtained = 0;
		humansAbsorbed = 0;
		humansDisintegrated = 0;

		// Options Shared Preference
		options = new Options_SharedPreference(mGameResources.getmBaseContext());
		music_on = options.getMusic();
		sfx_on = options.getSoundEffects();
		controlpads_on = options.getControlPads();

		// Spacecraft Meter Shared Preference
		meter_gauge = new Spacecraft_Meter_SharedPreference(mGameResources.getmBaseContext());

		mEarthPhysicsWorld = new FixedStepPhysicsWorld(40, new Vector2(0, SensorManager.GRAVITY_EARTH), false);
		
		// Add & Set background
		addGameBackGround();
		HUMAN_POOL.setHumanMapLocation(PARALLAX_MAP);
		MILITARY_POOL.setMilitaryHumanMapLocation(PARALLAX_MAP);
		LAMP_POOL.setMap(PARALLAX_MAP);
		CAR_POOL.setMap(PARALLAX_MAP);
		TRAFFIC_POOL.setMap(PARALLAX_MAP);
		
		// Initialize Gadgets
		initGadgets();

		// Create Pause Button & Load Pause Scene
		pauseButton = new Sprite(CAMERA_WIDTH - mGameResources.getmPauseButtonTextureRegion().getWidth() - 1, 1, mGameResources.getmPauseButtonTextureRegion(), mVertexBufferObjectManager) {
			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent,float pTouchAreaLocalX, float pTouchAreaLocalY) {
				if(pSceneTouchEvent.getAction()==MotionEvent.ACTION_DOWN) {
					pauseGame(true);
					sceneManager.setCurrentScene("GamePauseScene");
				}
				return true;
			}
		};
		pauseButton.setScale(0.8f);
		//mGameScene.attachChild(pauseButton);
		//mGameScene.registerTouchArea(pauseButton);
		loadPauseScene();
		
		// Load GameOver Scene & Detector
		loadGameOverScene();
		gameOverDetector();

		if(!controlpads_on) {
			sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), sensorManager.SENSOR_DELAY_GAME);
		}
		
		if(!player2Loaded) {
			mEngine.registerUpdateHandler(new TimerHandler(0.5f, true, new ITimerCallback() {
				@Override
				public void onTimePassed(final TimerHandler pTimerHandler) {
					if(player2Loaded) {
						mEngine.unregisterUpdateHandler(pTimerHandler);
						
						// Initialize player
						initPlayer();
						// Initialize Beam
						initBeam();
						// Update other player position
						updateOtherPlayerPosition();
						
						// Texts
						distanceText = new Text(35, 3, mGameResources.getmFont(), "00000", 5, new TextOptions(HorizontalAlign.RIGHT), mVertexBufferObjectManager);
						distanceText.setBlendFunction(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);
						distanceText.setAlpha(1f);
						distanceText.setScale(1.5f);
						distanceText.setColor(Color.WHITE);
						mGameScene.attachChild(distanceText);
						
						Text measurementText = new Text(176, 10, mGameResources.getmFont(), "m", 1, new TextOptions(HorizontalAlign.RIGHT), mVertexBufferObjectManager);
						measurementText.setBlendFunction(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);
						measurementText.setScale(0.9f);
						measurementText.setColor(Color.WHITE);
						mGameScene.attachChild(measurementText);
								
						goldObtainedText = new Text(40, 40, mGameResources.getmFont(), "0000", 4, new TextOptions(HorizontalAlign.RIGHT), mVertexBufferObjectManager);
						goldObtainedText.setBlendFunction(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);
						goldObtainedText.setAlpha(1f);
						goldObtainedText.setScale(1.2f);
						goldObtainedText.setColor(Color.YELLOW);
						mGameScene.attachChild(goldObtainedText);
				
						GoldSprite gold = new GoldSprite(105, 40, mGameResources.getmGoldTextureRegion(), mVertexBufferObjectManager);
						gold.setCurrentTileIndex(1);
						gold.setScale(0.4f);
						mGameScene.attachChild(gold);

						
						// Set the Boundary limits
						final float topLimit = 0;
						final float leftLimit = 77;
						final float rightLimit = CAMERA_WIDTH - player1Sprite.getWidth();
						//final float bottomLimit = CAMERA_HEIGHT - mParallaxLayerFront.getHeight() - 30;
						final float bottomLimit = CAMERA_HEIGHT - 150;
						
						if(!controlpads_on) {
							//===========================================================
							// Accelerometer - Update sprite positions
							//===========================================================
							
							updateSpritePosition = new IUpdateHandler() {
								public void onUpdate(float pSecondsElapsed) {
									if ((accelerometerSpeedZ != 0) || (accelerometerSpeedY != 0)) {
										
										/*
										if(sX > player1Sprite.getX()) { // Moving towards the back
											int reverseAnimationFrames[] = new int[spacecraft.getSpacecraftAnimationFrames()+1];
											for (int i = 0; i < reverseAnimationFrames.length; i++) {
												reverseAnimationFrames[i] = (spacecraft.getSpacecraftAnimationFrames()-i);
										    }
											player1Sprite.animate(spacecraft.getSpacecraftAnimationSpeed(), reverseAnimationFrames, true);
										} else { // Moving forward
											player1Sprite.animate(spacecraft.getSpacecraftAnimationSpeed(), 0, spacecraft.getSpacecraftAnimationFrames(), true);
										}
										*/
										
										sX = player1Sprite.getX();
										sY = player1Sprite.getY();
										
										// Calculate New X,Y Coordinates within Limits
										if (sX >= leftLimit) sX += (accelerometerSpeedX * player1Spacecraft.getSpacecraftMovingSpeed());
										else sX = leftLimit;
										if (sX <= rightLimit) sX += (accelerometerSpeedX * player1Spacecraft.getSpacecraftMovingSpeed());
										else sX = rightLimit;
										if (sY >= topLimit) sY += (accelerometerSpeedY * player1Spacecraft.getSpacecraftMovingSpeed());
										else sY = topLimit;
										if (sY <= bottomLimit - 50)	sY += (accelerometerSpeedY * player1Spacecraft.getSpacecraftMovingSpeed());
										else sY = bottomLimit - 50;
				
										// Double Check That New X,Y Coordinates are within Limits
										if (sX < leftLimit) sX = leftLimit;
										else if (sX > rightLimit) sX = rightLimit;
										if (sY < topLimit) sY = topLimit;
										else if (sY > bottomLimit - 50) sY = bottomLimit - 50;
				
										if(!mGameOver && !mPause && !mPlayerHit)
											player1Sprite.setPosition(sX, sY);
										
										forcefieldSprite.setPosition((sX + player1Sprite.getWidth()/2) - forcefieldSprite.getWidth()/2, (sY + player1Sprite.getHeight()/2) - forcefieldSprite.getHeight()/2);
				
										beamPositionX = player1Sprite.getX() + (player1Sprite.getWidth()/2) - (player1BeamSprite.getWidth()/2) - 2;
										beamPositionY = player1Sprite.getY() + player1Sprite.getHeight() - 10;
										
										//Play UFO Hover SFX
										if(sfx_on && !ufoHoverSFX.isPlaying() && beamPositionY < bottomLimit && !mPause && !mGameOver)
											ufoHoverSFX.play();
										
										//if sprite is reaching ground level, beam is not allowed
										if(beamPositionY > bottomLimit) {
											if(sfx_on && ufoHoverSFX.isPlaying()) ufoHoverSFX.pause();							
											if(sfx_on && ufoBeamSFX.isPlaying()) ufoBeamSFX.pause();
											
											//Remove the beam sprite
											if(player1BeamSprite.isVisible()) {
												if(sfx_on && ufoBeamSFX.isPlaying())
													ufoBeamSFX.pause();
												player1BeamSprite.setVisible(false);
											}
											
											//set the landing wind sprite
											player1WindLandingSprite.setPosition((player1Sprite.getX() + player1Sprite.getWidth()/2) - player1WindLandingSprite.getWidth()/2 - 10, beamPositionY - 40);
											if(!player1WindLandingSprite.isVisible() && !mPlayerHit) {
												player1WindLandingSprite.setVisible(true);
											}
										} else {
											player1BeamSprite.setPosition(beamPositionX, beamPositionY);
											player1WindLandingSprite.setVisible(false);
										}
									}
								}
								public void reset() { }
							};
							
						} else { //On screen control
							final float x1 = 65;
							final float y1 = CAMERA_HEIGHT - mGameResources.getmOnScreenControlBaseTextureRegion().getHeight() - 45;
							velocityOnScreenControl = new AnalogOnScreenControl(x1, y1, mCamera, mGameResources.getmOnScreenControlBaseTextureRegion(), mGameResources.getmOnScreenControlKnobTextureRegion(), 0.0001f, mVertexBufferObjectManager, new IAnalogOnScreenControlListener() {
								@Override
								public void onControlChange(final BaseOnScreenControl pBaseOnScreenControl, float pValueX, float pValueY) {
									sX = player1Sprite.getX();
									sY = player1Sprite.getY();
									
									// Double Check That New X,Y Coordinates are within Limits
									if(sX < leftLimit) player1SpriteBody.setTransform((leftLimit + player1Sprite.getWidth()/2) / PhysicsConstants.PIXEL_TO_METER_RATIO_DEFAULT, player1SpriteBody.getPosition().y, player1SpriteBody.getAngle());
									else if (sX > rightLimit) player1SpriteBody.setTransform((rightLimit + player1Sprite.getWidth()/2) / PhysicsConstants.PIXEL_TO_METER_RATIO_DEFAULT, player1SpriteBody.getPosition().y, player1SpriteBody.getAngle());
									if (sY < topLimit) player1SpriteBody.setTransform(player1SpriteBody.getPosition().x, (topLimit + player1Sprite.getHeight()/2) / PhysicsConstants.PIXEL_TO_METER_RATIO_DEFAULT, player1SpriteBody.getAngle());
									else if (sY > bottomLimit - 50) player1SpriteBody.setTransform(player1SpriteBody.getPosition().x, (bottomLimit - 50 + player1Sprite.getHeight()/2) / PhysicsConstants.PIXEL_TO_METER_RATIO_DEFAULT, player1SpriteBody.getAngle());
									
									//Move the ufo
									final Vector2 velocity = Vector2Pool.obtain(pValueX * 10f+(player1Spacecraft.getSpacecraftMovingSpeed()*1.5f), pValueY * 10f+(player1Spacecraft.getSpacecraftMovingSpeed()*1.5f));
									player1SpriteBody.setLinearVelocity(velocity);
														
									forcefieldSprite.setPosition((sX + player1Sprite.getWidth()/2) - forcefieldSprite.getWidth()/2, (sY + player1Sprite.getHeight()/2) - forcefieldSprite.getHeight()/2);

									beamPositionX = player1Sprite.getX() + (player1Sprite.getWidth()/2) - (player1BeamSprite.getWidth()/2) - 2;
									beamPositionY = player1Sprite.getY() + player1Sprite.getHeight() - 10;
									
									//Play UFO Hover SFX
									if(sfx_on && !ufoHoverSFX.isPlaying() && beamPositionY < bottomLimit && !mPause && !mGameOver)
										ufoHoverSFX.play();
									
									//if sprite is reaching ground level, beam is not allowed
									if(beamPositionY > bottomLimit) {
										if(sfx_on && ufoHoverSFX.isPlaying()) ufoHoverSFX.pause();						
										if(sfx_on && ufoBeamSFX.isPlaying()) ufoBeamSFX.pause();

										//Remove the beam sprite
										if(player1BeamSprite.isVisible()) {
											if(sfx_on && ufoBeamSFX.isPlaying())
												ufoBeamSFX.pause();
											
											player1BeamSprite.setVisible(false);
										}
										
										//set the landing wind sprite
										player1WindLandingSprite.setPosition((player1Sprite.getX() + player1Sprite.getWidth()/2) - player1WindLandingSprite.getWidth()/2 - 10, beamPositionY - 40);
										if(!player1WindLandingSprite.isVisible() && !mPlayerHit) {
											player1WindLandingSprite.setVisible(true);
										}
									} else {
										player1BeamSprite.setPosition(beamPositionX, beamPositionY);
										player1WindLandingSprite.setVisible(false);
									}
									Vector2Pool.recycle(velocity);
								}

								@Override
								public void onControlClick(final AnalogOnScreenControl pAnalogOnScreenControl) { }
							});
							velocityOnScreenControl.refreshControlKnobPosition();
							velocityOnScreenControl.getControlBase().setBlendFunction(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);
							velocityOnScreenControl.getControlBase().setAlpha(0.5f);

							mGameScene.setChildScene(velocityOnScreenControl);
							
							angelButton = new Sprite(CAMERA_WIDTH - 400, CAMERA_HEIGHT - 160, mGameResources.getmOnScreenControlAngelButtonTextureRegion(), mVertexBufferObjectManager) {
								@Override
								public boolean onAreaTouched(TouchEvent pTouchEvent,float pTouchAreaLocalX, float pTouchAreaLocalY) {
									if(pTouchEvent.isActionDown()) {
										UFO_BEAM_PREFERENCE = false; //GOODNESS ON!
										player2BeamSprite.animate(new long[] {50,50,50,50,50,50,50,50,50,50,50,50,50,50,50,50,50,50}, 0, 17, true);
										
										if (mEarthPhysicsWorld != null && !mGameOver && gameStarted && !player_on_flinch_mode) {
											//if sprite is above ground level, beam is allowed
											if(beamPositionY <= (CAMERA_HEIGHT - PARALLAX_MAP.getCurrentMapLayer1().getHeight() - 75)) {
												if(!mPlayerHit) {
													if(sfx_on)
														ufoBeamSFX.play();

													player2BeamSprite.setVisible(true);
												}
											}
										}
									}
									if(pTouchEvent.isActionUp()) {
										if(player2BeamSprite.isVisible()) {
											if(sfx_on && ufoBeamSFX.isPlaying())
												ufoBeamSFX.pause();
											
											player2BeamSprite.setVisible(false);
										}
									}
									return true;
								}
							};
							angelButton.setBlendFunction(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);
							angelButton.setAlpha(0.75f);
							angelButton.setScale(1.1f);
							velocityOnScreenControl.registerTouchArea(angelButton);
							velocityOnScreenControl.attachChild(angelButton);

							devilButton = new Sprite(CAMERA_WIDTH - 250, CAMERA_HEIGHT - 210, mGameResources.getmOnScreenControlDevilButtonTextureRegion(), mVertexBufferObjectManager) {
								@Override
								public boolean onAreaTouched(TouchEvent pTouchEvent,float pTouchAreaLocalX, float pTouchAreaLocalY) {
									if(pTouchEvent.isActionDown()) {
										UFO_BEAM_PREFERENCE = true; // EVIL ON & GOODNESS OFF!
										player2BeamSprite.animate(new long[] {50,50,50,50,50,50,50,50,50,50,50,50,50,50,50,50,50,50}, 18, 35, true);
										
										if (mEarthPhysicsWorld != null && !mGameOver && gameStarted && !player_on_flinch_mode) {
											//if sprite is above ground level, beam is allowed
											if(beamPositionY <= (CAMERA_HEIGHT - PARALLAX_MAP.getCurrentMapLayer1().getHeight() - 75)) {
												if(!mPlayerHit) {
													if(sfx_on)
														ufoBeamSFX.play();

													player2BeamSprite.setVisible(true);
												}
											}
										}
									}
									if(pTouchEvent.isActionUp()) {
										if(player2BeamSprite.isVisible()) {
											if(sfx_on && ufoBeamSFX.isPlaying())
												ufoBeamSFX.pause();
											
											player2BeamSprite.setVisible(false);
										}
									}
									return true;
								}
							};
							devilButton.setBlendFunction(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);
							devilButton.setAlpha(0.75f);
							devilButton.setScale(1.1f);
							velocityOnScreenControl.registerTouchArea(devilButton);
							velocityOnScreenControl.attachChild(devilButton);
						}
						
						if (mEarthPhysicsWorld != null) {
							if (!gameStarted) {
								startGameConfig();
							}
				
							// A update handler that detect humans, weapons etc and make decision whether to
							// to either change their humans status or check whether have it got out of screen
							IUpdateHandler detect = new IUpdateHandler() {
								@Override
								public void reset() { }
				
								@Override
								public void onUpdate(float pSecondsElapsed) {
									
									float height = CAMERA_HEIGHT - (player1Sprite.getY() + player1Sprite.getHeight() - 5) - (PARALLAX_MAP.getCurrentMapLayer1().getHeight() - 55);
									player1BeamSprite.setScaleCenterY((PARALLAX_MAP.getCurrentMapLayer1().getHeight() - 55));
									player1BeamSprite.setScaleY(1f + ((height - player1BeamSprite.getHeight()) / player1BeamSprite.getHeight()));
				
									for (int i = 0; i < humanSpriteArray.size(); i++) {
										final HumanSprite human = humanSpriteArray.get(i);
										/*
										//Collision detection for humans
										if(player1WindLandingSprite.collidesWith(human) && !player_on_flinch_mode && player1WindLandingSprite.isVisible() && !human.getHumanStatus().equals("ABSORBED") && human.isVisible()) {
											float windMiddle = player1WindLandingSprite.getX() + (player1WindLandingSprite.getWidth()/2);
											if (human.getX() > windMiddle) {
												human.setPosition(player1WindLandingSprite.getX() + player1WindLandingSprite.getWidth(), human.getY());
												if(!human.getHumanStatus().equals("BLOWN"))
													human.blown();
											}
											
										} else if(human.getHumanStatus().equals("BLOWN") && !player1WindLandingSprite.collidesWith(human)) {
											human.setRunningSpeed();
										}
										*/
										
										if (player1BeamSprite.collidesWith(human) && !player_on_flinch_mode && player1BeamSprite.isVisible() && !human.getHumanStatus().equals("ABSORBED") && human.isVisible()) {
											float beamFront = player1BeamSprite.getX() + 15;
											float beamEnd = player1BeamSprite.getX() + player1BeamSprite.getWidth();
											float humanFront = human.getX();
											float humanEnd = human.getX() + human.getWidth();
				
											if (beamFront < humanFront && humanFront < beamEnd && beamFront < humanEnd && humanEnd < beamEnd) {
												//TODO: Work on detect the beam and holds the human for 1.5sec then considered a successful absorb
												//if() {
												//}
												
												//Random chance to generate a new humans out
												if(MathUtils.random(0, 3) == 1) {
													HumanSprite hs = HUMAN_POOL.obtainPoolItem();
													humanSpriteArray.addLast(hs);
													if(!hs.hasParent())
														mGameScene.attachChild(hs);
												}
												
												if (UFO_BEAM_PREFERENCE) { //EVIL
													humansDisintegrated++;
													
													DisintegrateHumanSprite dhs = DISINTEGRATED_HUMAN_POOL.obtainPoolItem();
													dhs.setPosition(human.getX(),human.getY());
													dhs.disintegrate();
													
													if(human.getFacingDirection())
														dhs.switchDirection();
				
													if(player_on_flinch_mode)
														dhs.halt();
													
													DisintegratedHumanSpriteArray.addLast(dhs);
													if(!dhs.hasParent())
														mGameScene.attachChild(dhs);
													
													//25% chance of obtaining a gold
													if(MathUtils.random(0, 4) == 0) {
														final GoldSprite gold = GOLD_POOL.obtainPoolItem();
														gold.setPosition(human.getX() - 25, human.getY() + human.getHeight() / 4);
														gold.registerEntityModifier(new MoveYModifier(0.35f, gold.getY(), gold.getY() - 70, new IEntityModifier.IEntityModifierListener() {
															@Override
															public void onModifierStarted(IModifier<IEntity> pModifier, IEntity pItem) {
																pModifier.setAutoUnregisterWhenFinished(true);
																//gold.blink();
															}
				
															@Override
															public void onModifierFinished(IModifier<IEntity> pModifier, IEntity pItem) {
																recycleGold(gold);
																updateGoldCounter(MathUtils.random(2, 4));
															}
														}));
														
														if(!gold.hasParent())
															mGameScene.attachChild(gold);
													}
				
													human.setVisible(false);
													//recycleHuman(human);
													
													// Evil result in increase of evil meter & decrease of goodness meter 
													meter_gauge.incrementEvilMeter(0.1f);
													meter_gauge.decrementGoodnessMeter(0.05f);
												} else {
													human.absorb();
													humansAbsorbed++;
													
													human.registerEntityModifier(new AlphaModifier(2, 0, 255));
													human.registerEntityModifier(new MoveYModifier(0.15f, human.getY(), human.getY() - 35, new IEntityModifier.IEntityModifierListener() {
														@Override
														public void onModifierStarted(IModifier<IEntity> pModifier, IEntity pItem) {
															pModifier.setAutoUnregisterWhenFinished(true);
														}
				
														@Override
														public void onModifierFinished(IModifier<IEntity> pModifier, IEntity pItem) {
															//50% chance of obtaining a gold
															if(MathUtils.random(0, 1) == 0) {
																final GoldSprite gold = GOLD_POOL.obtainPoolItem();
																gold.setPosition(human.getX() - 25, human.getY() + human.getHeight() / 4);
																gold.registerEntityModifier(new MoveYModifier(0.35f, gold.getY(), gold.getY() - 70, new IEntityModifier.IEntityModifierListener() {
																	@Override
																	public void onModifierStarted(IModifier<IEntity> pModifier, IEntity pItem) {
																		pModifier.setAutoUnregisterWhenFinished(true);
																		//gold.blink();
																	}
				
																	@Override
																	public void onModifierFinished(IModifier<IEntity> pModifier, IEntity pItem) {
																		recycleGold(gold);
																		updateGoldCounter(MathUtils.random(1, 3));
																	}
																}));
																
																if(!gold.hasParent())
																	mGameScene.attachChild(gold);
															}
															
															human.setVisible(false);
															//recycleHuman(human);
														}
													}));
																							
													// Goodness result in increase of Goodness meter & decrease of evil meter 
													meter_gauge.incrementGoodnessMeter(0.1f);
													meter_gauge.decrementEvilMeter(0.05f);
												}
											}
											
											if(player1Sprite.getY() >= 150 && (player1Sprite.getX() - 250 <= human.getX()) && (player1Sprite.getX() + 250 >= human.getX())) {
												if(!human.getFacingDirection() && human.getHumanStatus().equals("WALK") && human.getStatusChanged() == false) {
													switch(MathUtils.random(0, 1)) {
														case 0: human.fear();
														case 1: human.shocked(player1Sprite);
													}
												}
												
												if(human.getFacingDirection() && human.getHumanStatus().equals("WALK") && human.getStatusChanged() == false) {
													human.shocked(player1Sprite);
												}
											}
										}
										
										if (human.offScreen(CAMERA_WIDTH, CAMERA_HEIGHT)) {
											recycleHuman(human);
										} 
									}
							
									for (int i = 0; i < militaryHumanSpriteArray.size(); i++) {
										final MilitaryHumanSprite military = militaryHumanSpriteArray.get(i);
									
										if(military.isVisible()) {
											//Collision detection for military humans
											if (player1BeamSprite.collidesWith(military) && !player_on_flinch_mode && player1BeamSprite.isVisible() && !military.getMilitaryStatus().equals("ABSORBED")) {
												float beamFront = player1BeamSprite.getX() + 15;
												float beamEnd = player1BeamSprite.getX() + player1BeamSprite.getWidth();
												float humanFront = military.getX();
												float humanEnd = military.getX() + military.getWidth();
				
												if (beamFront < humanFront && humanFront < beamEnd && beamFront < humanEnd && humanEnd < beamEnd) {
													//Work on detect the beam hold the human for 1.5sec then considered a successful absorb
													//if() {
													//}
				
													//Random chance to generate a new military human out
													if(MathUtils.random(0, 3) == 1) {
														MilitaryHumanSprite mhs = MILITARY_POOL.obtainPoolItem();
														militaryHumanSpriteArray.addLast(mhs);
														if(!mhs.hasParent())
															mGameScene.attachChild(mhs);
													}
				
													military.clearUpdateHandlers();
													military.setActionRegistered(false);
													
													if (UFO_BEAM_PREFERENCE) { //EVIL
														humansDisintegrated++;
														
														DisintegrateHumanSprite dhs = DISINTEGRATED_HUMAN_POOL.obtainPoolItem();
														dhs.setPosition(military.getX(),military.getY());
														dhs.disintegrate();
														
														if(military.getFacingDirection())
															dhs.switchDirection();
				
														if(player_on_flinch_mode)
															dhs.halt();
														
														DisintegratedHumanSpriteArray.addLast(dhs);
														if(!dhs.hasParent())
															mGameScene.attachChild(dhs);
														
														//25% chance of obtaining a gold
														if(MathUtils.random(0, 4) == 0) {
															final GoldSprite gold = GOLD_POOL.obtainPoolItem();
															gold.setPosition(military.getX() - 25, military.getY() + military.getHeight() / 4);
															gold.registerEntityModifier(new MoveYModifier(0.35f, gold.getY(), gold.getY() - 70, new IEntityModifier.IEntityModifierListener() {
																@Override
																public void onModifierStarted(IModifier<IEntity> pModifier, IEntity pItem) {
																	pModifier.setAutoUnregisterWhenFinished(true);
																	//gold.blink();
																}
				
																@Override
																public void onModifierFinished(IModifier<IEntity> pModifier, IEntity pItem) {
																	recycleGold(gold);
																	updateGoldCounter(MathUtils.random(2, 4));
																}
															}));
															
															if(!gold.hasParent())
																mGameScene.attachChild(gold);
														}
														
														military.setVisible(false);
														//recycleMilitaryHuman(military);
														
														// Evil result in increase of evil meter & decrease of goodness meter 
														meter_gauge.incrementEvilMeter(0.1f);
														meter_gauge.decrementGoodnessMeter(0.05f);
													} else {
														military.absorb();
														humansAbsorbed++;
				
														// Goodness result in increase of Goodness meter & decrease of evil meter 
														meter_gauge.incrementGoodnessMeter(0.1f);
														meter_gauge.decrementEvilMeter(0.05f);
														
														military.registerEntityModifier(new AlphaModifier(2, 0, 255));
														military.registerEntityModifier(new MoveYModifier(0.15f, military.getY(), military.getY() - 35, new IEntityModifier.IEntityModifierListener() {
															@Override
															public void onModifierStarted(IModifier<IEntity> pModifier, IEntity pItem) {
																pModifier.setAutoUnregisterWhenFinished(true);
															}
				
															@Override
															public void onModifierFinished(IModifier<IEntity> pModifier, IEntity pItem) {
																//50% chance of obtaining a gold
																if(MathUtils.random(0, 1) == 0) {
																	final GoldSprite gold = GOLD_POOL.obtainPoolItem();
																	gold.setPosition(military.getX() - 25, military.getY() + military.getHeight() / 4);
																	gold.registerEntityModifier(new MoveYModifier(0.35f, gold.getY(), gold.getY() - 70, new IEntityModifier.IEntityModifierListener() {
																		@Override
																		public void onModifierStarted(IModifier<IEntity> pModifier, IEntity pItem) {
																			pModifier.setAutoUnregisterWhenFinished(true);
																			//gold.blink();
																		}
				
																		@Override
																		public void onModifierFinished(IModifier<IEntity> pModifier, IEntity pItem) {
																			recycleGold(gold);
																			updateGoldCounter(MathUtils.random(1, 3));
																		}
																	}));
																	
																	if(!gold.hasParent())
																		mGameScene.attachChild(gold);
																}
																
																military.setVisible(false);
																//recycleMilitaryHuman(military);
															}
														}));
													}
												}
											}
											
											if(military.getMilitaryStatus().equals("WALK") && military.isActionRegistered() == false) {
												fireMilitaryMissile(military);
											}
											
											if(military.getMilitaryStatus().equals("FIRED")) {
												//if(player_on_flinch_mode) {
												//	fireMilitaryMissile(military);
												//} else {
												military.setWalkAnimationSpeed();
												//}
												
												if(player1Sprite.getX() < military.getX()) {
													if(!military.getFacingDirection()) {
														military.switchDirection();
														/*
														if(!military.getFacingDirection())
															military.moveLeftToRight((PARALLAX_MAP.getParallaxLayer().calculateSpeedInPixel % PARALLAX_MAP.getLayer1().getShapeWidthScaled()) * 3f);
														else if(military.getFacingDirection())
															military.moveRightToLeft((PARALLAX_MAP.getParallaxLayer().calculateSpeedInPixel % PARALLAX_MAP.getLayer1().getShapeWidthScaled()) * 3f);
														*/
													}
												} else if(player1Sprite.getX() > military.getX()) {
													if(military.getFacingDirection()) {
														military.switchDirection();
														/*
														if(!military.getFacingDirection())
															military.moveLeftToRight((PARALLAX_MAP.getParallaxLayer().calculateSpeedInPixel % PARALLAX_MAP.getLayer1().getShapeWidthScaled()) * 3f);
														else if(military.getFacingDirection())
															military.moveRightToLeft((PARALLAX_MAP.getParallaxLayer().calculateSpeedInPixel % PARALLAX_MAP.getLayer1().getShapeWidthScaled()) * 3f);
														*/
													}
												}
											}
										}
										if (military.offScreen(CAMERA_WIDTH, CAMERA_HEIGHT)) {
											recycleMilitaryHuman(military);
										} 
									}
									
									for (int i = 0; i < DisintegratedHumanSpriteArray.size(); i++) {
										final DisintegrateHumanSprite dhs = DisintegratedHumanSpriteArray.get(i);
										if (dhs.getCurrentTileIndex() >= 9) {
											recycleDisintegratedHuman(dhs);
										}
									}
									
									for (int i = 0; i < lampSpriteArray.size(); i++) {
										final LampSprite lamp = lampSpriteArray.get(i);
										
										//Collision detection for lamps
										if (player1BeamSprite.collidesWith(lamp) && !player_on_flinch_mode && player1BeamSprite.isVisible() && !lamp.getLampStatus().equals("STOP") && !lamp.getLampStatus().equals("EXPLODED") && lamp.isVisible()) {
											float beamFront = (player1BeamSprite.getX() + player1BeamSprite.getWidth()/2) - 42;
											float beamEnd = (player1BeamSprite.getX() + player1BeamSprite.getWidth()/2) + 42;
											float lampFront = lamp.getX();
											float lampEnd = lamp.getX() + lamp.getWidth();
											
											if (beamFront < lampFront && lampFront < beamEnd && beamFront < lampEnd && lampEnd < beamEnd) {
				
												// TODO : need to do some animations or shrinking thingy for lamp
												//tamp.setLampStatus("STOP");
												
												if (UFO_BEAM_PREFERENCE) { // EVIL BEAM
													for(int j=0; j<2; j++) {
														final int explode = j;
														
														final ExplosionSprite explosion = EXPLOSION_POOL.obtainPoolItem();
														explosion.setScale(0.8f);
														if(explode == 0)
															explosion.setPosition(lamp.getX() - explosion.getWidth()/2, (lamp.getY() + 10));
														else
															explosion.setPosition(lamp.getX() - explosion.getWidth()/2, (lamp.getY() + 45));
														explosion.animate(10, false, new IAnimationListener() {
															@Override
															public void onAnimationStarted(AnimatedSprite pAnimatedSprite, int pInitialLoopCount) {
																//lamp.setLampStatus("EXPLODED");
																//lamp.setZIndex(2);
																//explosion.setZIndex(explode);
																mGameScene.sortChildren();
																//if(sfx_on && !explosionSFX.isPlaying()) explosionSFX.play();
															}
				
															@Override
															public void onAnimationFrameChanged(AnimatedSprite pAnimatedSprite, int pOldFrameIndex, int pNewFrameIndex) {
																pAnimatedSprite.setPosition(lamp.getX() - pAnimatedSprite.getWidth()/2, (lamp.getY() - pAnimatedSprite.getHeight()/2) + lamp.getHeight()/2);
															}
				
															@Override
															public void onAnimationLoopFinished(AnimatedSprite pAnimatedSprite, int pRemainingLoopCount, int pInitialLoopCount) { }
				
															@Override
															public void onAnimationFinished(AnimatedSprite pAnimatedSprite) {
																if(explode==1) {
																	lamp.setVisible(false);
																	//recycleLamp(lamp);
																}
																pAnimatedSprite.setVisible(false);
															}
														});
														explosionSpriteArray.addLast(explosion);
														if(!explosion.hasParent())
															mGameScene.attachChild(explosion);
													}
												} else { // GOOD BEAM
													//Activate the flinch mode for the player
													if(player1Spacecraft.isSpacecraftFlinchMode() && !MegaBeam_on) {
														playerOnFlinchMode();
					
														lamp.clearEntityModifiers();
														lamp.registerEntityModifier(new AlphaModifier(2f, 0f, 255f));
														lamp.registerEntityModifier(new MoveYModifier(0.2f, lamp.getY(), lamp.getY() - 35f));
														lamp.registerEntityModifier(new ScaleModifier(0.2f, 1f, 0.5f, new IEntityModifier.IEntityModifierListener() {
															@Override
															public void onModifierStarted(IModifier<IEntity> pModifier,IEntity pItem) {	}
				
															@Override
															public void onModifierFinished(IModifier<IEntity> pModifier,IEntity pItem) {
															}
														}));
														endOfFlinchMode(lamp);
													}
												}
											}
										} 
										if (lamp.detectOffScreen()) {
											recycleLamp(lamp);
										}				
									}
				
									/*
									for (int i = 0; i < trafficSpriteArray.size(); i++) {
										final TrafficSprite traffic = trafficSpriteArray.get(i);
										
										//Collision detection for traffic light
										if (beamSprite.collidesWith(traffic) && !player_on_flinch_mode && beamSprite.isVisible() && !traffic.getTrafficStatus().equals("STOP") && !traffic.getTrafficStatus().equals("EXPLODED") && traffic.isVisible()) {
											float beamFront = (beamSprite.getX() + beamSprite.getWidth()/2) - 35;
											float beamEnd = (beamSprite.getX() + beamSprite.getWidth()/2) + 35;
											float trafficFront = traffic.getX();
											float trafficEnd = traffic.getX() + traffic.getWidth();
											
											if (beamFront < trafficFront && trafficFront < beamEnd && beamFront < trafficEnd && trafficEnd < beamEnd) {
				
												// TODO : need to do some animations or shrinking thingy for traffic lights
												//traffic.setTrafficsStatus("STOP");
												
												if (UFO_BEAM_PREFERENCE) { // EVIL BEAM
													for(int j=0; j<2; j++) {
														final int explode = j;
														
														final ExplosionSprite explosion = EXPLOSION_POOL.obtainPoolItem();
														explosion.setScale(0.8f);
														if(explode == 0)
															explosion.setPosition(traffic.getX() - explosion.getWidth()/2, (traffic.getY() - explosion.getHeight()/2) + traffic.getHeight()/2);
														else
															explosion.setPosition(traffic.getX() - explosion.getWidth()/2, (traffic.getY() - explosion.getHeight()/2));
														explosion.animate(15, false, new IAnimationListener() {
															@Override
															public void onAnimationStarted(AnimatedSprite pAnimatedSprite, int pInitialLoopCount) {
																//traffic.setTrafficStatus("EXPLODED");
																//traffic.setZIndex(2);
																//explosion.setZIndex(explode);
																mGameScene.sortChildren();
																//if(sfx_on && !explosionSFX.isPlaying()) explosionSFX.play();
															}
					
															@Override
															public void onAnimationFrameChanged(AnimatedSprite pAnimatedSprite, int pOldFrameIndex, int pNewFrameIndex) {
																pAnimatedSprite.setPosition(traffic.getX() - pAnimatedSprite.getWidth()/2, (traffic.getY() - pAnimatedSprite.getHeight()/2) + traffic.getHeight()/2);
															}
					
															@Override
															public void onAnimationLoopFinished(AnimatedSprite pAnimatedSprite, int pRemainingLoopCount, int pInitialLoopCount) { }
					
															@Override
															public void onAnimationFinished(AnimatedSprite pAnimatedSprite) {
																if(explode==1)
																	recycleTraffic(traffic);
																pAnimatedSprite.setVisible(false);
															}
														});
														explosionSpriteArray.addLast(explosion);
														if(!explosion.hasParent())
															mGameScene.attachChild(explosion);
													}
												} else { // GOOD BEAM
													//Activate the flinch mode for the player
													if(spacecraft.isSpacecraftFlinchMode() && !MegaBeam_on) {
														playerOnFlinchMode();
					
														traffic.clearEntityModifiers();
														traffic.registerEntityModifier(new AlphaModifier(2, 0, 255));
														traffic.registerEntityModifier(new MoveYModifier(0.2f, traffic.getY(), traffic.getY() - 35));
														traffic.registerEntityModifier(new ScaleModifier(1f, 1f, 0.5f));
					
														endOfFlinchMode(traffic);
													}
												}
											}
										}
										if (traffic.detectOffScreen()) {
											recycleTraffic(traffic);
										}
									}
									
									for (int i = 0; i < carSpriteArray.size(); i++) {
										final CarSprite car = carSpriteArray.get(i);
										
										//Collision detection for cars
										if (beamSprite.collidesWith(car) && !player_on_flinch_mode && beamSprite.isVisible() && !car.getCarStatus().equals("STOP") && !car.getCarStatus().equals("EXPLODED") && car.isVisible()) {
											float beamFront = (beamSprite.getX() + beamSprite.getWidth()/2) - 35;
											float beamEnd = (beamSprite.getX() + beamSprite.getWidth()/2) + 35;
											float carFront = car.getX();
											float carEnd = car.getX() + car.getWidth();
											
											if (beamFront < carFront && carFront < beamEnd && beamFront < carEnd && carEnd < beamEnd) {
				
												// TODO : need to do some animations or shrinking thingy for cars
												//car.setObstaclesStatus("STOP");
												
												if (UFO_BEAM_PREFERENCE) { // EVIL BEAM
													for(int j=0; j<2; j++) {
														final int explode = j;
														
														final ExplosionSprite explosion = EXPLOSION_POOL.obtainPoolItem();
														explosion.setScale(0.8f);
														if(explode == 0)
															explosion.setPosition(car.getX() - explosion.getWidth()/2, (traffic.getY() - explosion.getHeight()/2) + car.getHeight()/2);
														else
															explosion.setPosition(car.getX() - explosion.getWidth()/2, (traffic.getY() - explosion.getHeight()/2));
														explosion.animate(15, false, new IAnimationListener() {
															@Override
															public void onAnimationStarted(AnimatedSprite pAnimatedSprite, int pInitialLoopCount) {
																//car.setCarStatus("EXPLODED");
																car.setZIndex(2);
																explosion.setZIndex(explode);
																mGameScene.sortChildren();
																//if(sfx_on && !explosionSFX.isPlaying()) explosionSFX.play();
															}
					
															@Override
															public void onAnimationFrameChanged(AnimatedSprite pAnimatedSprite, int pOldFrameIndex, int pNewFrameIndex) {
																pAnimatedSprite.setPosition(car.getX() - pAnimatedSprite.getWidth()/2, (car.getY() - pAnimatedSprite.getHeight()/2) + car.getHeight()/2);
															}
					
															@Override
															public void onAnimationLoopFinished(AnimatedSprite pAnimatedSprite, int pRemainingLoopCount, int pInitialLoopCount) { }
					
															@Override
															public void onAnimationFinished(AnimatedSprite pAnimatedSprite) {
																if(explode==1)
																	recycleCar(car);
																pAnimatedSprite.setVisible(false);
															}
														});
														explosionSpriteArray.addLast(explosion);
														if(!explosion.hasParent())
															mGameScene.attachChild(explosion);									
													}
												} else { // GOOD BEAM
													//Activate the flinch mode for the player
													if(spacecraft.isSpacecraftFlinchMode() && !MegaBeam_on) {
														playerOnFlinchMode();
					
														car.clearEntityModifiers();
														car.registerEntityModifier(new AlphaModifier(2, 0, 255));
														car.registerEntityModifier(new MoveYModifier(0.2f, car.getY(), car.getY() - 35));
														car.registerEntityModifier(new ScaleModifier(1f, 1f, 0.5f));
					
														endOfFlinchMode(car);
													}
												}
											}
										}
										if (car.detectOffScreen()) {
											recycleCar(car);
										}				
									}
									*/
									
									for (int i = 0; i < laserBeamSpriteArray.size(); i++) {
										final LaserBeamSprite laserBeam = laserBeamSpriteArray.get(i);
										
										//Collision detection for laser beam
										if(laserBeam.isVisible()) {
											float laserBeamFront = laserBeam.getX() - 5;
											float laserBeamEnd = laserBeam.getX() + laserBeam.getWidth() + 3;
											
											if(laserBeam.collidesWith(player1Sprite) && !mPlayerHit) {
												float playerFront = player1Sprite.getX();
												float playerEnd = player1Sprite.getX() + player1Sprite.getWidth();
												if (laserBeamFront < playerFront && playerFront < laserBeamEnd && laserBeamFront < playerEnd && playerEnd < laserBeamEnd) {
													//for laserbeam, the player sprite must be touching the laser with a significant amt
													if(Shield_on) {
														shieldHit(player1Sprite, true);
														/*
														final AlphaModifier blink = new AlphaModifier(30, 0, 255);
														player1Sprite.registerEntityModifier(blink);
							
														mGameScene.registerUpdateHandler(new TimerHandler(3f, true, new ITimerCallback() {
															@Override
															public void onTimePassed(final TimerHandler pTimerHandler) {
																player1Sprite.unregisterEntityModifier(blink);
																player1Sprite.setAlpha(1.0f);
																mGameScene.unregisterUpdateHandler(pTimerHandler);
															}
														}));
														*/
													} else {
														player1Spacecraft.setSpacecraftLives(player1Spacecraft.getSpacecraftLives() - 1);
														
														if(player1Spacecraft.getSpacecraftLives() == 0) {
															playerHit(player1Sprite, true, laserBeam);
															highScores.saveDeathByLaserStrike();
														} else {
															playerHit(player1Sprite, false, laserBeam);
														}
													}
												}
											}
										
											for (int o = 0; o < lampSpriteArray.size(); o++) {
												final LampSprite lamp = lampSpriteArray.get(o);
												
												if(laserBeam.collidesWith(lamp) && !lamp.getDestroyedByLaserBeam() && lamp.isVisible()) {
													float lampFront = lamp.getX() - 5;
													float lampEnd = lamp.getX() + lamp.getWidth() + 5;
													
													if (laserBeamFront < lampFront && lampFront < laserBeamEnd && laserBeamFront < lampEnd && lampEnd < laserBeamEnd) {
														lamp.setDestroyedByLaserBeam(true);
														
														final ExplosionSprite explosion = EXPLOSION_POOL.obtainPoolItem();
														explosion.setScale(0.8f);
														explosion.setPosition(lamp.getX() - explosion.getWidth()/2, (lamp.getY() - explosion.getHeight()/2) + lamp.getHeight()/2);
														explosion.animate(15, false, new IAnimationListener() {
															@Override
															public void onAnimationStarted(AnimatedSprite pAnimatedSprite, int pInitialLoopCount) {
																//if(sfx_on && !explosionSFX.isPlaying()) explosionSFX.play();
															}
						
															@Override
															public void onAnimationFrameChanged(AnimatedSprite pAnimatedSprite, int pOldFrameIndex, int pNewFrameIndex) {
																pAnimatedSprite.setPosition(lamp.getX() - pAnimatedSprite.getWidth()/2, (lamp.getY() - pAnimatedSprite.getHeight()/2) + lamp.getHeight()/2);
															}
						
															@Override
															public void onAnimationLoopFinished(AnimatedSprite pAnimatedSprite, int pRemainingLoopCount, int pInitialLoopCount) { }
						
															@Override
															public void onAnimationFinished(AnimatedSprite pAnimatedSprite) {
																lamp.setVisible(false);
																//recycleLamp(lamp);
																pAnimatedSprite.setVisible(false);
															}
														});
														explosionSpriteArray.addLast(explosion);
														if(!explosion.hasParent())
															mGameScene.attachChild(explosion);									
													}
												}
											}
											/*
											for (int o = 0; o < carSpriteArray.size(); o++) {
												final CarSprite car = carSpriteArray.get(o);
												
												if(laserBeam.collidesWith(car) && !car.getDestroyedByLaserBeam() && car.isVisible()) {
													float carFront = car.getX();
													float carEnd = car.getX() + car.getWidth();
													if (laserBeamFront < carFront && carFront < laserBeamEnd && laserBeamFront < carEnd && carEnd < laserBeamEnd) {
														car.setDestroyedByLaserBeam(true);
														
														final ExplosionSprite explosion = EXPLOSION_POOL.obtainPoolItem();
														explosion.setScale(0.8f);
														explosion.setPosition(car.getX() - explosion.getWidth()/2, (car.getY() - explosion.getHeight()/2) + car.getHeight()/2);
														explosion.animate(15, false, new IAnimationListener() {
															@Override
															public void onAnimationStarted(AnimatedSprite pAnimatedSprite, int pInitialLoopCount) {
																//if(sfx_on && !explosionSFX.isPlaying()) explosionSFX.play();
															}
						
															@Override
															public void onAnimationFrameChanged(AnimatedSprite pAnimatedSprite, int pOldFrameIndex, int pNewFrameIndex) {
																pAnimatedSprite.setPosition(car.getX() - pAnimatedSprite.getWidth()/2, (car.getY() - pAnimatedSprite.getHeight()/2) + car.getHeight()/2);
															}
						
															@Override
															public void onAnimationLoopFinished(AnimatedSprite pAnimatedSprite, int pRemainingLoopCount, int pInitialLoopCount) { }
						
															@Override
															public void onAnimationFinished(AnimatedSprite pAnimatedSprite) {											
																recycleCar(car);
																pAnimatedSprite.setVisible(false);
															}
														});
														explosionSpriteArray.addLast(explosion);
														if(!explosion.hasParent())
															mGameScene.attachChild(explosion);									
													}
												}
											}
				
											for (int o = 0; o < trafficSpriteArray.size(); o++) {
												final TrafficSprite traffic = trafficSpriteArray.get(o);
												
												if(laserBeam.collidesWith(traffic) && !traffic.getDestroyedByLaserBeam() && traffic.isVisible()) {
													float trafficFront = traffic.getX();
													float trafficEnd = traffic.getX() + traffic.getWidth();
													if (laserBeamFront < trafficFront && trafficFront < laserBeamEnd && laserBeamFront < trafficEnd && trafficEnd < laserBeamEnd) {
														traffic.setDestroyedByLaserBeam(true);
														
														final ExplosionSprite explosion = EXPLOSION_POOL.obtainPoolItem();
														explosion.setScale(0.8f);
														explosion.setPosition(traffic.getX() - explosion.getWidth()/2, (traffic.getY() - explosion.getHeight()/2) + traffic.getHeight()/2);
														explosion.animate(15, false, new IAnimationListener() {
															@Override
															public void onAnimationStarted(AnimatedSprite pAnimatedSprite, int pInitialLoopCount) {
																//if(sfx_on && !explosionSFX.isPlaying()) explosionSFX.play();
															}
						
															@Override
															public void onAnimationFrameChanged(AnimatedSprite pAnimatedSprite, int pOldFrameIndex, int pNewFrameIndex) {
																pAnimatedSprite.setPosition(traffic.getX() - pAnimatedSprite.getWidth()/2, (traffic.getY() - pAnimatedSprite.getHeight()/2) + traffic.getHeight()/2);
															}
						
															@Override
															public void onAnimationLoopFinished(AnimatedSprite pAnimatedSprite, int pRemainingLoopCount, int pInitialLoopCount) { }
						
															@Override
															public void onAnimationFinished(AnimatedSprite pAnimatedSprite) {											
																recycleTraffic(traffic);
																pAnimatedSprite.setVisible(false);
															}
														});
														explosionSpriteArray.addLast(explosion);
														if(!explosion.hasParent())
															mGameScene.attachChild(explosion);									
													}
												}
											}
											*/
											for (int h = 0; h < humanSpriteArray.size(); h++) {
												HumanSprite human = humanSpriteArray.get(h);
												
												if(laserBeam.collidesWith(human) && human.isVisible()) {
													float humanFront = human.getX();
													float humanEnd = human.getX() + human.getWidth();
													if (laserBeamFront < humanFront && humanFront < laserBeamEnd && laserBeamFront < humanEnd && humanEnd < laserBeamEnd) {
														DisintegrateHumanSprite dhs = DISINTEGRATED_HUMAN_POOL.obtainPoolItem();
														dhs.setPosition(human.getX(), human.getY());
														dhs.disintegrate();
														
														if(human.getFacingDirection())
															dhs.switchDirection();
				
														if(player_on_flinch_mode)
															dhs.halt();
														
														DisintegratedHumanSpriteArray.addLast(dhs);
														if(!dhs.hasParent())
															mGameScene.attachChild(dhs);
														
														if(MathUtils.random(0, 2) == 1) {
															HumanSprite hs = HUMAN_POOL.obtainPoolItem();
															humanSpriteArray.addLast(hs);
															if(!hs.hasParent())
																mGameScene.attachChild(hs);
														}
														
														human.setVisible(false);
														//recycleHuman(human);
													}
												}
											}
											
											for (int m = 0; m < militaryHumanSpriteArray.size(); m++) {
												MilitaryHumanSprite militaryHuman = militaryHumanSpriteArray.get(m);
												
												if(laserBeam.collidesWith(militaryHuman) && militaryHuman.isVisible()) {
													float militaryHumanFront = militaryHuman.getX();
													float militaryHumanEnd = militaryHuman.getX() + militaryHuman.getWidth();
													if (laserBeamFront < militaryHumanFront && militaryHumanFront < laserBeamEnd && laserBeamFront < militaryHumanEnd && militaryHumanEnd < laserBeamEnd) {
														DisintegrateHumanSprite dhs = DISINTEGRATED_HUMAN_POOL.obtainPoolItem();
														dhs.setPosition(militaryHuman.getX(), militaryHuman.getY());
														dhs.disintegrate();
														
														if(militaryHuman.getFacingDirection())
															dhs.switchDirection();
				
														if(player_on_flinch_mode)
															dhs.halt();
														
														DisintegratedHumanSpriteArray.addLast(dhs);
														if(!dhs.hasParent())
															mGameScene.attachChild(dhs);
														
														if(MathUtils.random(0, 2) == 1) {
															MilitaryHumanSprite mhs = MILITARY_POOL.obtainPoolItem();
															militaryHumanSpriteArray.addLast(mhs);
															if(!mhs.hasParent())
																mGameScene.attachChild(mhs);
														}
														
														militaryHuman.setVisible(false);
														//recycleMilitaryHuman(militaryHuman);
													}
												}
											}
											
											for (int n = 0; n < missileHeadSpriteArray.size(); n++) {
												final MissileHeadSprite missileHead = missileHeadSpriteArray.get(n);
												final MissileTailSprite missileTail = missileTailSpriteArray.get(n);
												
												//Collision detection for missile head
												if(laserBeam.collidesWith(missileHead) && missileHead.isVisible()) {
													float missileHeadFront = missileHead.getX();
													float missileHeadEnd = missileHead.getX() + missileHead.getWidth();
													if (laserBeamFront < missileHeadFront && missileHeadFront < laserBeamEnd && laserBeamFront < missileHeadEnd && missileHeadEnd < laserBeamEnd) {
														
														final ExplosionSprite explosion = EXPLOSION_POOL.obtainPoolItem();
														explosion.setScale(0.8f);
														explosion.setPosition(missileHead.getX() - explosion.getWidth()/2, (missileHead.getY() - explosion.getHeight()/2) + missileHead.getHeight()/2);
														explosion.animate(15, false, new IAnimationListener() {
															@Override
															public void onAnimationStarted(AnimatedSprite pAnimatedSprite, int pInitialLoopCount) {
																missileHead.setVisible(false);
																missileTail.setVisible(false);
																
																//recycleMissile(missileHead, missileTail);
																if(sfx_on && missileLaunchSFX.isPlaying()) missileLaunchSFX.pause();
																//if(sfx_on && !explosionSFX.isPlaying()) explosionSFX.play();
															}
						
															@Override
															public void onAnimationFrameChanged(AnimatedSprite pAnimatedSprite, int pOldFrameIndex, int pNewFrameIndex) {
																pAnimatedSprite.setPosition(missileHead.getX() - pAnimatedSprite.getWidth()/2, (missileHead.getY() - pAnimatedSprite.getHeight()/2) + missileHead.getHeight()/2);
															}
						
															@Override
															public void onAnimationLoopFinished(AnimatedSprite pAnimatedSprite, int pRemainingLoopCount, int pInitialLoopCount) { }
						
															@Override
															public void onAnimationFinished(AnimatedSprite pAnimatedSprite) {
																pAnimatedSprite.setVisible(false);
															}
														});
														explosionSpriteArray.addLast(explosion);
														if(!explosion.hasParent())
															mGameScene.attachChild(explosion);
													}
												}
											}
											
											for (int o = 0; o < scatterMissileHeadSpriteArray.size(); o++) {
												final ScatterMissileHeadSprite scatterMissileHead = scatterMissileHeadSpriteArray.get(o);
												final ScatterMissileTailSprite scatterMissileTail = scatterMissileTailSpriteArray.get(o);
												
												//Collision detection for scatter missile head
												if(laserBeam.collidesWith(scatterMissileHead) && scatterMissileHead.isVisible()) {
													float scatterMissileHeadFront = scatterMissileHead.getX();
													float scatterMissileHeadEnd = scatterMissileHead.getX() + scatterMissileHead.getWidth();
													if (laserBeamFront < scatterMissileHeadFront && scatterMissileHeadFront < laserBeamEnd && laserBeamFront < scatterMissileHeadEnd && scatterMissileHeadEnd < laserBeamEnd) {
														
														final ExplosionSprite explosion = EXPLOSION_POOL.obtainPoolItem();
														explosion.setScale(0.8f);
														explosion.setPosition(scatterMissileHead.getX() - explosion.getWidth()/2, (scatterMissileHead.getY() - explosion.getHeight()/2) + scatterMissileHead.getHeight()/2);
														explosion.animate(15, false, new IAnimationListener() {
															@Override
															public void onAnimationStarted(AnimatedSprite pAnimatedSprite, int pInitialLoopCount) {
																//if(sfx_on && !explosionSFX.isPlaying()) explosionSFX.play();
																scatterMissileHead.setVisible(false);
																scatterMissileTail.setVisible(false);
																//recycleScatterMissile(scatterMissileHead, scatterMissileTail);
															}
						
															@Override
															public void onAnimationFrameChanged(AnimatedSprite pAnimatedSprite, int pOldFrameIndex, int pNewFrameIndex) {
																pAnimatedSprite.setPosition(scatterMissileHead.getX() - pAnimatedSprite.getWidth()/2, (scatterMissileHead.getY() - pAnimatedSprite.getHeight()/2) + scatterMissileHead.getHeight()/2);
															}
						
															@Override
															public void onAnimationLoopFinished(AnimatedSprite pAnimatedSprite, int pRemainingLoopCount, int pInitialLoopCount) { }
						
															@Override
															public void onAnimationFinished(AnimatedSprite pAnimatedSprite) {
																pAnimatedSprite.setVisible(false);
															}
														});
														explosionSpriteArray.addLast(explosion);
														if(!explosion.hasParent())
															mGameScene.attachChild(explosion);
													}
												}
											}
											
											for (int p = 0; p < militaryMissileSpriteArray.size(); p++) {
												final MilitaryMissileSprite militaryMissile = militaryMissileSpriteArray.get(p);
												
												//Collision detection for military missile
												if(laserBeam.collidesWith(militaryMissile) && militaryMissile.isVisible()) {										
													//float militaryMissileFront = militaryMissile.getX();
													//float militaryMissileEnd = militaryMissile.getX() + militaryMissile.getWidth();
													//if (laserBeamFront < militaryMissileFront && militaryMissileFront < laserBeamEnd && laserBeamFront < militaryMissileEnd && militaryMissileEnd < laserBeamEnd) {
														
													final ExplosionSprite explosion = EXPLOSION_POOL.obtainPoolItem();
													explosion.setScale(0.8f);
													
													if(militaryMissile.isVisible())
														explosion.setPosition(militaryMissile.getX() - explosion.getWidth()/2, (militaryMissile.getY() - explosion.getHeight()/2) + militaryMissile.getHeight()/2);
													else {
														if(militaryMissile.getFacingDirection())
															explosion.setX(explosion.getX() - ((PARALLAX_MAP.getParallaxLayer().calculateSpeedInPixel % PARALLAX_MAP.getLayer1().getShapeWidthScaled()) * 10f));
														else 
															explosion.setX(explosion.getX() + ((PARALLAX_MAP.getParallaxLayer().calculateSpeedInPixel % PARALLAX_MAP.getLayer1().getShapeWidthScaled()) * 10f));
													}
													
													explosion.animate(15, false, new IAnimationListener() {
														@Override
														public void onAnimationStarted(AnimatedSprite pAnimatedSprite, int pInitialLoopCount) {
															militaryMissile.setVisible(false);
															//if(sfx_on && !explosionSFX.isPlaying()) explosionSFX.play();
														}
					
														@Override
														public void onAnimationFrameChanged(AnimatedSprite pAnimatedSprite, int pOldFrameIndex, int pNewFrameIndex) {
															pAnimatedSprite.setPosition(militaryMissile.getX() - pAnimatedSprite.getWidth()/2, (militaryMissile.getY() - pAnimatedSprite.getHeight()/2) + militaryMissile.getHeight()/2);
														}
					
														@Override
														public void onAnimationLoopFinished(AnimatedSprite pAnimatedSprite, int pRemainingLoopCount, int pInitialLoopCount) { }
					
														@Override
														public void onAnimationFinished(AnimatedSprite pAnimatedSprite) {
															pAnimatedSprite.setVisible(false);
														}
													});
													explosionSpriteArray.addLast(explosion);
													if(!explosion.hasParent())
														mGameScene.attachChild(explosion);
													//}
												}
											}
										} else {
											recycleLaserBeam(laserBeam);
										}
									}
									
									for (int i = 0; i < missileHeadSpriteArray.size(); i++) {
										final MissileHeadSprite missileHead = missileHeadSpriteArray.get(i);
										final MissileTailSprite missileTail = missileTailSpriteArray.get(i);
				
										//missileTail.setX(missileHead.getX() + missileHead.getWidth() - 5f);
										missileTail.setY(missileHead.getY());
										
										if(MissileJammer_on) {
											final ExplosionSprite explosion = EXPLOSION_POOL.obtainPoolItem();
											explosion.setScale(0.8f);
											explosion.setPosition(missileHead.getX() - explosion.getWidth()/2, (missileHead.getY() - explosion.getHeight()/2) + missileHead.getHeight()/2);
											explosion.animate(15, false, new IAnimationListener() {
												@Override
												public void onAnimationStarted(AnimatedSprite pAnimatedSprite, int pInitialLoopCount) {
													if(sfx_on && missileLaunchSFX.isPlaying()) missileLaunchSFX.pause();
													//if(sfx_on && !explosionSFX.isPlaying()) explosionSFX.play();
													missileHead.setVisible(false);
													missileTail.setVisible(false);
													//recycleMissile(missileHead, missileTail);
												}
				
												@Override
												public void onAnimationFrameChanged(AnimatedSprite pAnimatedSprite, int pOldFrameIndex, int pNewFrameIndex) {
													pAnimatedSprite.setPosition(missileHead.getX() - pAnimatedSprite.getWidth()/2, (missileHead.getY() - pAnimatedSprite.getHeight()/2) + missileHead.getHeight()/2);
												}
				
												@Override
												public void onAnimationLoopFinished(AnimatedSprite pAnimatedSprite, int pRemainingLoopCount, int pInitialLoopCount) { }
				
												@Override
												public void onAnimationFinished(AnimatedSprite pAnimatedSprite) {
													pAnimatedSprite.setVisible(false);
												}
											});
											explosionSpriteArray.addLast(explosion);
											if(!explosion.hasParent())
												mGameScene.attachChild(explosion);
										}
										//Collision detection for missile head
										else if(!mPlayerHit) {
											if(missileHead.collidesWith(player1Sprite) && missileHead.isVisible()) {
												if(Shield_on) {
													shieldHit(missileHead, true);
												} else {
													if(sfx_on && missileLaunchSFX.isPlaying()) missileLaunchSFX.pause();
													player1Spacecraft.setSpacecraftLives(player1Spacecraft.getSpacecraftLives() - 1);
													
													if(player1Spacecraft.getSpacecraftLives() == 0) {
														playerHit(player1Sprite, true, missileHead);
														highScores.saveDeathByMissile();
													} else {
														playerHit(player1Sprite, false, missileHead);
													}
												}
												recycleMissile(missileHead, missileTail);
											}
											else if(player1BeamSprite.collidesWith(missileHead) && missileHead.isVisible() && player1BeamSprite.isVisible()) {
												if(Shield_on) {
													shieldHit(missileHead, true);
												} else {
													final TimerHandler beamMissileRotateTimer = new TimerHandler(0.05f, false, new ITimerCallback() {
														@Override
														public void onTimePassed(final TimerHandler pTimerHandler) {
															//missileHead.setY(missileHead.getY() - 3f);
															player1Sprite.setRotation(player1Sprite.getRotation() + 5f);
														}
													});
													mGameScene.registerUpdateHandler(beamMissileRotateTimer);
													
													TimerHandler beamMissileEndTimer = new TimerHandler(0.5f, false, new ITimerCallback() {
														@Override
														public void onTimePassed(final TimerHandler pTimerHandler) {
															mGameScene.unregisterUpdateHandler(beamMissileRotateTimer);
															mGameScene.unregisterUpdateHandler(pTimerHandler);
															
															if(sfx_on && missileLaunchSFX.isPlaying()) missileLaunchSFX.pause();
															player1Spacecraft.setSpacecraftLives(player1Spacecraft.getSpacecraftLives() - 1);
															
															if(player1Spacecraft.getSpacecraftLives() == 0) {
																playerHit(player1Sprite, true, missileHead);
																highScores.saveDeathByMissile();
															} else {
																playerHit(player1Sprite, false, missileHead);
															}
														}
													});
													mGameScene.registerUpdateHandler(beamMissileEndTimer);
												}
												recycleMissile(missileHead, missileTail);
											}
										}
										if (missileTail.offScreen(CAMERA_WIDTH, CAMERA_HEIGHT)) {
											recycleMissile(missileHead, missileTail);
										}
									}
									
									for (int i = 0; i < scatterMissileHeadSpriteArray.size(); i++) {
										final ScatterMissileHeadSprite scatterMissileHead = scatterMissileHeadSpriteArray.get(i);
										final ScatterMissileTailSprite scatterMissileTail = scatterMissileTailSpriteArray.get(i);
				
										//scatterMissileTail.setX(scatterMissileHead.getX() + scatterMissileHead.getWidth() - 5f);
										//scatterMissileTail.setY(scatterMissileHead.getY() - 8f);
										
										if(MissileJammer_on) {
											final ExplosionSprite explosion = EXPLOSION_POOL.obtainPoolItem();
											explosion.setScale(0.8f);
											explosion.setPosition(scatterMissileHead.getX() - explosion.getWidth()/2, (scatterMissileHead.getY() - explosion.getHeight()/2) + scatterMissileHead.getHeight()/2);
											explosion.animate(15, false, new IAnimationListener() {
												@Override
												public void onAnimationStarted(AnimatedSprite pAnimatedSprite, int pInitialLoopCount) {
													//if(sfx_on && !explosionSFX.isPlaying()) explosionSFX.play();
													scatterMissileHead.setVisible(false);
													scatterMissileTail.setVisible(false);
													//recycleScatterMissile(scatterMissileHead, scatterMissileTail);
												}
				
												@Override
												public void onAnimationFrameChanged(AnimatedSprite pAnimatedSprite, int pOldFrameIndex, int pNewFrameIndex) {
													pAnimatedSprite.setPosition(scatterMissileHead.getX() - pAnimatedSprite.getWidth()/2, (scatterMissileHead.getY() - pAnimatedSprite.getHeight()/2) + scatterMissileHead.getHeight()/2);
												}
				
												@Override
												public void onAnimationLoopFinished(AnimatedSprite pAnimatedSprite, int pRemainingLoopCount, int pInitialLoopCount) { }
				
												@Override
												public void onAnimationFinished(AnimatedSprite pAnimatedSprite) {
													pAnimatedSprite.setVisible(false);
												}
											});
											explosionSpriteArray.addLast(explosion);
											if(!explosion.hasParent())
												mGameScene.attachChild(explosion);
										}
										//Collision detection for scatter missile head
										else if(!mPlayerHit) {
											if(scatterMissileHead.collidesWith(player1Sprite) && scatterMissileHead.isVisible()) {
												if(Shield_on) {
													shieldHit(scatterMissileHead, true);
												} else {
													player1Spacecraft.setSpacecraftLives(player1Spacecraft.getSpacecraftLives() - 1);
													
													if(player1Spacecraft.getSpacecraftLives() == 0) {
														playerHit(player1Sprite, true, scatterMissileHead);
														highScores.saveDeathByScatterMissile();
													} else {
														playerHit(player1Sprite, false, scatterMissileHead);
													}
												}
												recycleScatterMissile(scatterMissileHead, scatterMissileTail);
											}
										}
										if (scatterMissileTail.offScreen(CAMERA_WIDTH, CAMERA_HEIGHT)) {
											recycleScatterMissile(scatterMissileHead, scatterMissileTail);
										}
									}
									
									for (int i = 0; i < militaryMissileSpriteArray.size(); i++) {
										final MilitaryMissileSprite militaryMissile = militaryMissileSpriteArray.get(i);
										
										//Collision detection for military missile
										if(!mPlayerHit) {
											if(militaryMissile.collidesWith(player1Sprite) && militaryMissile.isVisible()) {								
												if(Shield_on) {
													shieldHit(militaryMissile, false);
												} else {
													if(player1Spacecraft.isSpacecraftFlinchMode()) {
														highScores.saveHitByHumanRocketLauncher();
				
														//Activate the handler to start flinch mode for the player
														playerOnFlinchMode();	
					
														//Activate the handler to detect the end of flinch mode for the player
														endOfFlinchMode(militaryMissile);
													}
												}
												militaryMissile.setVisible(false);
												//recycleMilitaryMissile(militaryMissile);
											}
										}
										if (militaryMissile.offScreen(CAMERA_WIDTH, CAMERA_HEIGHT)) {
											recycleMilitaryMissile(militaryMissile);
										}
									}
									
									for (int i = 0; i < explosionSpriteArray.size(); i++) {
										final ExplosionSprite explosion = explosionSpriteArray.get(i);
										if(!explosion.isVisible()) {
											recycleExplosionSprite(explosion);
										}
									}
				
									/*
									for (int i = 0; i < smokeSpriteArray.size(); i++) {
										final SmokeSprite smoke = smokeSpriteArray.get(i);
										if(!smoke.isVisible()) {
										//if(smoke.getAlpha() > 0.2f) {
								        	recycleSmokeSprite(smoke);
								        	//smokeSpriteArray.remove(i);
											//SMOKE_POOL.recyclePoolItem(smoke);
										}
									}
									*/
									
									if(MegaBeam_on && player1BeamSprite.isVisible()) {
										player1BeamSprite.setScaleX(1.35f);
									} else if(!MegaBeam_on && player1BeamSprite.isVisible()) {
										player1BeamSprite.setScaleX(1f);
									} 
									
									if(mPlayerHit) {
										player1Sprite.setZIndex(1);
									} else {
										player2Sprite.setZIndex(7);
										player1Sprite.setZIndex(6);
										player1BeamSprite.setZIndex(5);
										player1WindLandingSprite.setZIndex(4);
										if(Shield_on) {
											player2Sprite.setZIndex(8);
											forcefieldSprite.setZIndex(7);
											player1Sprite.setZIndex(6);
											player1BeamSprite.setZIndex(5);
											player1WindLandingSprite.setZIndex(4);
										}
									}
									mGameScene.sortChildren();
								}
							};
							mEngine.registerUpdateHandler(detect);
						}
					}
				}
			}));
		}
	}
	
	@Override
	public boolean onSceneTouchEvent(Scene pScene, TouchEvent pTouchEvent) {
		if(pScene == mGameScene && !controlpads_on) {
			if (pTouchEvent.getX() >= CAMERA_WIDTH/2) {
				UFO_BEAM_PREFERENCE = true; // EVIL ON & GOODNESS OFF!
				player1BeamSprite.animate(new long[] {50,50,50,50,50,50,50,50,50,50,50,50,50,50,50,50,50,50}, 18, 35, true);
			} else {
				UFO_BEAM_PREFERENCE = false; // EVIL OFF & GOODNESS ON!
				player1BeamSprite.animate(new long[] {50,50,50,50,50,50,50,50,50,50,50,50,50,50,50,50,50,50}, 0, 17, true);
			}
			
			if (mEarthPhysicsWorld != null && !mGameOver && gameStarted && !player_on_flinch_mode) {
				//onSceneTouchHold.onSceneTouchEvent(pScene, pTouchEvent);
				if(pTouchEvent.isActionDown()) {
					//if sprite is above ground level, beam is allowed
					if(beamPositionY <= (CAMERA_HEIGHT - PARALLAX_MAP.getCurrentMapLayer1().getHeight() - 75)) {
						if(!mPlayerHit) {
							if(sfx_on)
								ufoBeamSFX.play();

							player1BeamSprite.setVisible(true);
						}
					}
				}
				if(pTouchEvent.isActionUp()) {
					if(player1BeamSprite.isVisible()) {
						if(sfx_on && ufoBeamSFX.isPlaying())
							ufoBeamSFX.pause();
						
						player1BeamSprite.setVisible(false);
					}
				}
				return true;
			}
		}
		return false;
	}
	
	@Override
	public boolean onAreaTouched(TouchEvent arg0, ITouchArea arg1, float arg2, float arg3) { return false; }

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) { }

	@Override
	public void onSensorChanged(SensorEvent event) {
		synchronized (this) {
			switch (event.sensor.getType()) {
			case Sensor.TYPE_ACCELEROMETER:
				accelerometerSpeedX = event.values[1] - POSITION_X_ANGLE; //x-axis
				accelerometerSpeedY = event.values[0] - POSITION_Y_ANGLE; //y-axis
				accelerometerSpeedZ = event.values[2]; //Z-axis
				break;
			}
		}
	}
	
	public void resumeGame() {
		mMainActivity.hideAdvertisement();
		
		mPause = false;
		pauseButton.setVisible(true);
		mGameScene.clearChildScene();

		if (controlpads_on) {
			mGameScene.setChildScene(velocityOnScreenControl);
		} else {
			if(gameStarted && !player_on_flinch_mode)
				mEngine.registerUpdateHandler(updateSpritePosition);
		}
		
		if (sfx_on && !ufoHoverSFX.isPlaying() && !mGameOver) 
			ufoHoverSFX.play();
		if (music_on && !gameBGM.isPlaying() && !mGameOver)
			gameBGM.resume();
	}
	
	public void pauseGame(boolean pauseScene) {
		if(pauseScene) {
			mMainActivity.showAdvertisement();			
			pauseButton.setVisible(false);
			player1BeamSprite.setVisible(false);
			mGameScene.setChildScene(mPauseScene, false, true, true);
		}		
		mPause = true;
		
		if(sfx_on && ufoHoverSFX.isPlaying())
			ufoHoverSFX.pause();
		if(sfx_on && ufoBeamSFX.isPlaying())
			ufoBeamSFX.pause();
		if(sfx_on && countdownSFX.isPlaying())
			countdownSFX.pause();
		if(sfx_on && laserCountdownSFX.isPlaying())
			laserCountdownSFX.pause();
		if(sfx_on && explosionSFX.isPlaying())
			explosionSFX.pause();
		if (sfx_on && laserBeamSFX.isPlaying()) 
			laserBeamSFX.pause();
		if (sfx_on && missileLaunchSFX.isPlaying()) 
			missileLaunchSFX.pause();
		if (sfx_on && shieldHitSFX.isPlaying()) 
			shieldHitSFX.pause();
		if (music_on && gameBGM.isPlaying())
			gameBGM.pause();
		
		if(!controlpads_on && gameStarted)
			mEngine.unregisterUpdateHandler(updateSpritePosition);
	}

	//Loading map based on the selected location from previous activity
	private void addGameBackGround() {
		//Add parallax background
		mGameScene.setBackground(PARALLAX_MAP.loadMap(DESTINATED_MAP));
		PARALLAX_MAP.pause();
	}

	
	// ======================================================================================================
	// ======================================================================================================
	//  Initialisation UFO, power-ups, humans and weapons
	// ======================================================================================================
	// ======================================================================================================
	
	private void initPlayer() {
		// Create blackhole and add it to the scene
		blackhole_Sprite = new AnimatedSprite(120, 40, mGameResources.getmBlackHoleTextureRegion(), mVertexBufferObjectManager);
		blackhole_Sprite.setScale(0);
		
		// Create character and add it to the scene
		player1Sprite = new PixelPerfectAnimatedSprite(0, 0, player1Spacecraft.loadSpacecraftTexture(spacecraft_sharedpreference.getActiveSpacecraft()), mVertexBufferObjectManager);
		player1Sprite.setPosition((blackhole_Sprite.getX() + blackhole_Sprite.getWidth()/2 + 10) - player1Sprite.getWidth()/2, (blackhole_Sprite.getY() + blackhole_Sprite.getHeight()/2) - player1Sprite.getHeight()/2);
		player1Sprite.setScale(0);
		player1Sprite.setBlendFunction(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);
		player1Sprite.animate(player1Spacecraft.getSpacecraftAnimationSpeed(), 0, player1Spacecraft.getSpacecraftAnimationFrames(),true);

		mGameScene.attachChild(blackhole_Sprite);
		mGameScene.attachChild(player1Sprite);

		// Attach the shadow player to the scene
		player2Sprite.setPosition((blackhole_Sprite.getX() + blackhole_Sprite.getWidth()/2 + 10) - player2Sprite.getWidth()/2, (blackhole_Sprite.getY() + blackhole_Sprite.getHeight()/2) - player2Sprite.getHeight()/2);
		player2Sprite.setScale(0);
		player2Sprite.setBlendFunction(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);
		player2Sprite.animate(player2Spacecraft.getSpacecraftAnimationSpeed(), 0, player2Spacecraft.getSpacecraftAnimationFrames(),true);

		//Increase in size
		blackhole_Sprite.registerUpdateHandler(new TimerHandler(0.01f, true, new ITimerCallback() {
			@Override
			public void onTimePassed(final TimerHandler pTimerHandler) {
				if(blackholeSpriteScaleMeter < 1f) {
					blackholeSpriteScaleMeter += 0.005f;
					blackhole_Sprite.setScale(blackholeSpriteScaleMeter);
				} else {
					blackhole_Sprite.unregisterUpdateHandler(pTimerHandler);
				}
			}
		}));
		
		blackhole_Sprite.animate(100, false, new IAnimationListener() {
			@Override
			public void onAnimationStarted(AnimatedSprite pAnimatedSprite, int pInitialLoopCount) { }
			@Override
			public void onAnimationFrameChanged(AnimatedSprite pAnimatedSprite, int pOldFrameIndex, int pNewFrameIndex) { }
			@Override
			public void onAnimationLoopFinished(AnimatedSprite pAnimatedSprite, int pRemainingLoopCount, int pInitialLoopCount) { }

			@Override
			public void onAnimationFinished(AnimatedSprite pAnimatedSprite) {
				blackhole_Sprite.animate(new long[] {100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100}, new int[] {23,22,21,20,19,18,17,16,15,14,13,12,11,10,9,8,7,6,5,4,3,2,1,0}, false, new IAnimationListener() {
					@Override
					public void onAnimationStarted(AnimatedSprite pAnimatedSprite, int pInitialLoopCount) {
						//Decrease in size
						blackhole_Sprite.registerUpdateHandler(new TimerHandler(0.01f, true, new ITimerCallback() {
							@Override
							public void onTimePassed(final TimerHandler pTimerHandler) {
								if(blackholeSpriteScaleMeter > 0f) {
									blackholeSpriteScaleMeter -= 0.005f;
									blackhole_Sprite.setScale(blackholeSpriteScaleMeter);
								} else {
									blackhole_Sprite.unregisterUpdateHandler(pTimerHandler);
								}
							}
						}));
					}
					
					@Override
					public void onAnimationFrameChanged(AnimatedSprite pAnimatedSprite, int pOldFrameIndex, int pNewFrameIndex) { }
					
					@Override
					public void onAnimationLoopFinished(AnimatedSprite pAnimatedSprite, int pRemainingLoopCount, int pInitialLoopCount) { }
					
					@Override
					public void onAnimationFinished(AnimatedSprite pAnimatedSprite) { }
				});
			}
		});
		
		player1Sprite.registerUpdateHandler(new TimerHandler(1f, true, new ITimerCallback() {
			@Override
			public void onTimePassed(final TimerHandler pTimerHandler1) {
				pTimerHandler1.setTimerSeconds(0.01f);
				if(player1SpriteScaleMeter < 1f) {
					player1SpriteScaleMeter += 0.005f;
					player1Sprite.setScale(player1SpriteScaleMeter);
				} else if(player1SpriteScaleMeter >= 1f) {
					player1Sprite.unregisterUpdateHandler(pTimerHandler1);
				}
			}
		}));
		
		player2Sprite.registerUpdateHandler(new TimerHandler(1f, true, new ITimerCallback() {
			@Override
			public void onTimePassed(final TimerHandler pTimerHandler1) {
				pTimerHandler1.setTimerSeconds(0.01f);
				if(player2SpriteScaleMeter < 1f) {
					player2SpriteScaleMeter += 0.005f;
					player2Sprite.setScale(player2SpriteScaleMeter);
				} else if(player2SpriteScaleMeter >= 1f) {
					player2Sprite.unregisterUpdateHandler(pTimerHandler1);
				}
			}
		}));
		
		// Create character's landing animation
		player1WindLandingSprite = new PixelPerfectAnimatedSprite(0, 0, mGameResources.getmWindLandingTextureRegion(), mVertexBufferObjectManager);
		player1WindLandingSprite.setPosition((player1Sprite.getX() + player1Sprite.getWidth()/2) - player1WindLandingSprite.getWidth()/2 - 10, beamPositionY - 40);
		player1WindLandingSprite.animate(50);
		player1WindLandingSprite.setVisible(false);
		mGameScene.attachChild(player1WindLandingSprite);

		// Create shadow character's landing animation
		player2WindLandingSprite = new PixelPerfectAnimatedSprite(0, 0, mGameResources.getmWindLandingTextureRegion(), mVertexBufferObjectManager);
		player2WindLandingSprite.setPosition((player2Sprite.getX() + player2Sprite.getWidth()/2) - player2WindLandingSprite.getWidth()/2 - 10, beamPositionY - 40);
		player2WindLandingSprite.animate(50);
		player2WindLandingSprite.setVisible(false);
		mGameScene.attachChild(player2WindLandingSprite);
		
		// Create character's forcefield shield
		forcefieldSprite = new Sprite(0, 0, mGameResources.getmForcefieldTextureRegion(), mVertexBufferObjectManager);
		forcefieldSprite.setPosition(player1Sprite.getX() - forcefieldSprite.getWidth()/4, player1Sprite.getY() - forcefieldSprite.getHeight()/5);
		forcefieldSprite.setScale(1f);
		forcefieldSprite.setVisible(false);
		forcefieldSprite.registerEntityModifier(new AlphaModifier(33, 0, 255));
		mGameScene.attachChild(forcefieldSprite);

		if(controlpads_on) {
			player1SpriteBody = PhysicsFactory.createBoxBody(mEarthPhysicsWorld, player1Sprite, BodyType.DynamicBody, FIXTURE_DEF);
			mEarthPhysicsWorld.registerPhysicsConnector(new PhysicsConnector(player1Sprite, player1SpriteBody, true, false));
			//playerSpriteBody.setTransform(playerSprite.getX()/ PhysicsConstants.PIXEL_TO_METER_RATIO_DEFAULT, playerSprite.getY()/ PhysicsConstants.PIXEL_TO_METER_RATIO_DEFAULT, playerSpriteBody.getAngle());
		}
	}
	
	private void initGadgets() {
		// Load Gadgets shared preferences
		final Gadgets_SharedPreference gadget_sharedpreference = new Gadgets_SharedPreference(mGameResources.getmBaseContext());	
		
		float spriteX = -10;

		for (int x = 0; x < mGameResources.getGadgetList().size(); x++) {
			if(gadget_sharedpreference.getEquippedGadget1() == x) {
				updateGadgets(gadget_sharedpreference, spriteX, 210, x);
			}
			if(gadget_sharedpreference.getEquippedGadget2() == x) {
				updateGadgets(gadget_sharedpreference, spriteX, 310, x);
			}
		}
		
		//If gadget box is not filled, just add a empty box
		if(gadget_sharedpreference.getEquippedGadget1() == 99) {
			Sprite gadgetIcon = new Sprite(spriteX, 210, mGameResources.getmUFOBoxTextureRegion(), mVertexBufferObjectManager);
			gadgetIcon.setScale(0.65f);
			mGameScene.attachChild(gadgetIcon);
		}
		if (gadget_sharedpreference.getEquippedGadget2() == 99) {
			Sprite gadgetIcon = new Sprite(spriteX, 310, mGameResources.getmUFOBoxTextureRegion(), mVertexBufferObjectManager);
			gadgetIcon.setScale(0.65f);
			mGameScene.attachChild(gadgetIcon);
		}		
	}
	
	private void updateGadgets(final Gadgets_SharedPreference sharedpreference, float spriteX, float spriteY, final int gadgetNumber) {
		/*
		//Attached the gadget count
		Text gadgetCount = new Text(spriteX + mGameResources.getGadgetList().get(x).getWidth() - 20, spriteY + mGameResources.getGadgetList().get(x).getHeight() - 25, mGameResources.getmFont(), "", 1000, new TextOptions(HorizontalAlign.LEFT), mVertexBufferObjectManager);
		if(gadget_sharedpreference.getGadgetCount(gadget_no) > Game_Gadget_Count) {
			gadgetCount.setText(""+Game_Gadget_Count);
		} else {
			gadgetCount.setText(""+gadget_sharedpreference.getGadgetCount(gadget_no));
		}
		mGameScene.attachChild(gadgetCount);
		*/
		
		//Attached a textual count of the leftover gadget timer 
		final Text gadgetTimeCounter = new Text(0, spriteY + 85, mGameResources.getmFont(),  "", 2, new TextOptions(HorizontalAlign.LEFT), mVertexBufferObjectManager);
		gadgetTimeCounter.setScale(0.8f);
		gadgetTimeCounter.setVisible(false);	

		//Attached the gadget icon and register the touch area
		AnimatedSprite gadgetIcon = new AnimatedSprite(spriteX, spriteY, mGameResources.getGadgetList().get(gadgetNumber), mVertexBufferObjectManager) {
			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent,float pTouchAreaLocalX, float pTouchAreaLocalY) {
				if(sharedpreference.getGadgetCount(gadgetNumber) != 0) {
					if(pSceneTouchEvent.isActionDown()) {
						this.setScale(0.55f);
							
						if(gadgetNumber == 0) {
							//create a time handler and set the shield boolean to true for 15sec.
							Shield_on = true;
							forcefieldSprite.setVisible(true);
							
							final float shieldTimer = 15f; //15 seconds shield
							leftOverTimerList.put("ShieldTimer", shieldTimer);
							
							gadgetTimeCounter.setText("" + Math.round(shieldTimer));
							gadgetTimeCounter.setX(this.getX() + this.getWidth() / 3);
							gadgetTimeCounter.setY(this.getY() + this.getHeight() / 2);
							gadgetTimeCounter.setVisible(true);
							
							//Last for 30 seconds
							mGameScene.registerUpdateHandler(new TimerHandler(1f, true, new ITimerCallback() {
								@Override
								public void onTimePassed(final TimerHandler pTimerHandler) {
									float leftoverTimer = leftOverTimerList.get("ShieldTimer");
									
									//if extend timer on, ignore the check for this timer
									if(!ExtendTimer_on) {
										if(leftoverTimer <= 0) {
											Shield_on = false;
											gadgetTimeCounter.setVisible(false);
											forcefieldSprite.setVisible(false);
											leftOverTimerList.put("ShieldTimer", shieldTimer);
											mGameScene.unregisterUpdateHandler(pTimerHandler);
										} else {
											if(Shield_on) {
												leftOverTimerList.put("ShieldTimer", leftoverTimer -= 1f);
												gadgetTimeCounter.setText("" + Math.round(leftoverTimer));
											} else {
												gadgetTimeCounter.setVisible(false);
												forcefieldSprite.setVisible(false);
												leftOverTimerList.put("ShieldTimer", shieldTimer);
												mGameScene.unregisterUpdateHandler(pTimerHandler);
											}
										}
									}
								}
							}));
							
						} else if(gadgetNumber == 1) {
							//create a time handler that extend all active time handler for 10sec
							ExtendTimer_on = true;
							
							final float extendTimer = 10f; //10 seconds
							leftOverTimerList.put("ExtendTimer", extendTimer);
							
							gadgetTimeCounter.setText("" + Math.round(extendTimer));
							gadgetTimeCounter.setX(this.getX() + this.getWidth() / 3);
							gadgetTimeCounter.setY(this.getY() + this.getHeight() / 2);
							gadgetTimeCounter.setVisible(true);
							
							mGameScene.registerUpdateHandler(new TimerHandler(1f, true, new ITimerCallback() {
								@Override
								public void onTimePassed(final TimerHandler pTimerHandler) {
									float leftoverTimer = leftOverTimerList.get("ExtendTimer");
									
									if(leftoverTimer <= 0) {
										ExtendTimer_on = false;
										gadgetTimeCounter.setVisible(false);
										leftOverTimerList.put("ExtendTimer", extendTimer);
										mGameScene.unregisterUpdateHandler(pTimerHandler);
									} else {
										leftOverTimerList.put("ExtendTimer", leftoverTimer -= 1f);
										gadgetTimeCounter.setText("" + Math.round(leftoverTimer));
									}
								}
							}));							
							
						} else if(gadgetNumber == 2) {
							//create a time handler and set the beam boolean to true for 15sec. 
							MegaBeam_on = true;

							final float beamTimer = 15f; //15 seconds
							leftOverTimerList.put("BeamTimer", beamTimer);
							
							gadgetTimeCounter.setText("" + Math.round(beamTimer));
							gadgetTimeCounter.setX(this.getX() + this.getWidth() / 3);
							gadgetTimeCounter.setY(this.getY() + this.getHeight() / 2);
							gadgetTimeCounter.setVisible(true);
							
							mGameScene.registerUpdateHandler(new TimerHandler(1f, true, new ITimerCallback() {
								@Override
								public void onTimePassed(final TimerHandler pTimerHandler) {
									float leftoverTimer = leftOverTimerList.get("BeamTimer");
									
									//if extend timer on, ignore the check for this timer
									if(!ExtendTimer_on) {
										if(leftoverTimer <= 0) {
											MegaBeam_on = false;
											gadgetTimeCounter.setVisible(false);
											leftOverTimerList.put("BeamTimer", beamTimer);
											mGameScene.unregisterUpdateHandler(pTimerHandler);
										} else {
											leftOverTimerList.put("BeamTimer", leftoverTimer -= 1f);
											gadgetTimeCounter.setText("" + Math.round(leftoverTimer));
										}
									}
								}
							}));
							
						} else if(gadgetNumber == 3) {
							//create a time handler and set the missile jammer boolean to true for 10sec. 
							MissileJammer_on = true;

							for (int i=0; i<missileAlertSpriteArray.size(); i++) {
								recycleMissileAlert(missileAlertSpriteArray.get(i));
							}
							for (int i=0; i<scatterMissileAlertSpriteArray.size(); i++) {
								recycleScatterMissileAlert(scatterMissileAlertSpriteArray.get(i));
							}
							
							final float missileJammerTimer = 10f; //10 seconds
							leftOverTimerList.put("MissileJammerTimer", missileJammerTimer);
							
							gadgetTimeCounter.setText("" + Math.round(missileJammerTimer));
							gadgetTimeCounter.setX(this.getX() + this.getWidth() / 3);
							gadgetTimeCounter.setY(this.getY() + this.getHeight() / 2);
							gadgetTimeCounter.setVisible(true);
							
							mGameScene.registerUpdateHandler(new TimerHandler(1f, true, new ITimerCallback() {
								@Override
								public void onTimePassed(final TimerHandler pTimerHandler) {
									float leftoverTimer = leftOverTimerList.get("MissileJammerTimer");
									
									//if extend timer on, ignore the check for this timer
									if(!ExtendTimer_on) {
										if(leftoverTimer <= 0) {
											MissileJammer_on = false;
											gadgetTimeCounter.setVisible(false);
											leftOverTimerList.put("MissileJammerTimer", missileJammerTimer);
											mGameScene.unregisterUpdateHandler(pTimerHandler);
										} else {
											leftOverTimerList.put("MissileJammerTimer", leftoverTimer -= 1f);
											gadgetTimeCounter.setText("" + Math.round(leftoverTimer));
										}
									}
								}
							}));
							
						} else if(gadgetNumber == 4) {
							//create a time handler and set the human magnet boolean to true for 15sec. 
							HumanMagnet_on = true;
							
							//TODO: Pause game and ask user to tap on the screen floor
							pauseGame(false);
							sceneManager.setCurrentScene("HumanMagnetScene");
							loadMagnetScene(gadgetTimeCounter, this);
						}
						
						sharedpreference.useGadget(gadgetNumber, 1);
						setCurrentTileIndex(1);
						mGameScene.unregisterTouchArea(this);
					}
					if(pSceneTouchEvent.isActionUp()) {
						this.setScale(0.65f);
					}
				}
				return true;
			}
		};
		gadgetIcon.setScale(0.65f);
		
		//On initialize, check whether the gadget has at least 1 gadget for usage
		if(sharedpreference.getGadgetCount(gadgetNumber) == 0) {
			gadgetIcon.setCurrentTileIndex(1);
		} else {
			gadgetIcon.setCurrentTileIndex(0);
			mGameScene.registerTouchArea(gadgetIcon);
		}	
		
		mGameScene.attachChild(gadgetIcon);
		mGameScene.attachChild(gadgetTimeCounter);
	}
	
	private void initBeam() {
		player1BeamSprite = new PixelPerfectAnimatedSprite(beamPositionX, beamPositionY, mGameResources.getmBeamTextureRegion().getWidth(), mGameResources.getmBeamTextureRegion().getHeight()/spacecraft_sharedpreference.getBeamLength(), mGameResources.getmBeamTextureRegion(), mVertexBufferObjectManager);
		player1BeamSprite.animate(50,true);
		player1BeamSprite.setVisible(false);
		player1BeamSprite.registerEntityModifier(new AlphaModifier(99, 0, 255));
		mGameScene.attachChild(player1BeamSprite);
	}

	// ======================================================================================================
	// ======================================================================================================
	//  Overall Game Methods
	// ======================================================================================================
	// ======================================================================================================
	
	private void startGameConfig() {
		mMainActivity.hideAdvertisement();
		
		// Batch Allocate Pool Items for all pools
		//LAMP_POOL.batchAllocatePoolItems(6);
		HUMAN_POOL.batchAllocatePoolItems(30);
		MILITARY_POOL.batchAllocatePoolItems(15);
		DISINTEGRATED_HUMAN_POOL.batchAllocatePoolItems(30);
		MISSILE_ALERT_POOL.batchAllocatePoolItems(5);
		MISSILE_HEAD_POOL.batchAllocatePoolItems(10);
		MISSILE_TAIL_POOL.batchAllocatePoolItems(10);
		SCATTER_MISSILE_ALERT_POOL.batchAllocatePoolItems(5);
		SCATTER_MISSILE_HEAD_POOL.batchAllocatePoolItems(10);
		SCATTER_MISSILE_TAIL_POOL.batchAllocatePoolItems(10);
		MILITARY_MISSILE_ALERT_POOL.batchAllocatePoolItems(20);
		MILITARY_MISSILE_POOL.batchAllocatePoolItems(20);
		LASER_BEAM_ALERT_POOL.batchAllocatePoolItems(3);
		LASER_BEAM_POOL.batchAllocatePoolItems(3);
		EXPLOSION_POOL.batchAllocatePoolItems(10);
		SMOKE_POOL.batchAllocatePoolItems(200);
		GOLD_POOL.batchAllocatePoolItems(15);
		
		updateHumansHandler = new TimerHandler(0.001f, true, new updateHumans());
		updateLampHandler = new TimerHandler(0.001f, true, new updateLamp());
		//updateCarHandler = new TimerHandler(1f, true, new updateCar());
		//updateTrafficHandler = new TimerHandler(1f, true, new updateTraffic());
		updateSpeedHandler = new TimerHandler(5f, true, new updateGameSpeed());
		displayMissileAlertHandler = new TimerHandler(MathUtils.random(10f, 15f), true, new displayMissileAlert());
		//displayScatterMissileAlertHandler = new TimerHandler(MathUtils.random(55f, 70f), true, new displayScatterMissileAlert());
		displayLaserBeamAlertHandler = new TimerHandler(MathUtils.random(25f, 35f), true, new displayLaserBeamAlert());
		updateMilitaryHumansHandler = new TimerHandler(MathUtils.random(15f, 20f), true, new updateMilitary());
		
		mGameScene.registerUpdateHandler(updateHumansHandler);
		mGameScene.registerUpdateHandler(displayMissileAlertHandler);
		//mGameScene.registerUpdateHandler(displayScatterMissileAlertHandler);
		mGameScene.registerUpdateHandler(displayLaserBeamAlertHandler);
		mGameScene.registerUpdateHandler(updateMilitaryHumansHandler);
		mGameScene.registerUpdateHandler(updateSpeedHandler);
		//TODO : FUTURE ENHANCEMENT of obstacles
		mGameScene.registerUpdateHandler(updateLampHandler);
		//mGameScene.registerUpdateHandler(updateCarHandler);
		//mGameScene.registerUpdateHandler(updateTrafficHandler);

		mGameScene.registerUpdateHandler(new TimerHandler(4.5f, false, new ITimerCallback() {
			@Override
			public void onTimePassed(TimerHandler pTimerHandler) {
				mGameScene.unregisterUpdateHandler(pTimerHandler);
				
				blackhole_Sprite.clearEntityModifiers();
				blackhole_Sprite.clearUpdateHandlers();
				blackhole_Sprite.setVisible(false);
				
				updateDistanceHandler = new TimerHandler(0.15f, true, new updateDistance());
				mGameScene.registerUpdateHandler(updateDistanceHandler);

				if (!controlpads_on) {
					mEngine.registerUpdateHandler(updateSpritePosition);
				} else {
					mGameScene.registerUpdateHandler(mEarthPhysicsWorld);
				}
				
				for (final HumanSprite human: humanSpriteArray) {
					human.animate(new long[] {200,350}, 25, 26, false, new IAnimationListener() {
						@Override
						public void onAnimationStarted(AnimatedSprite pAnimatedSprite, int pInitialLoopCount) { }
						@Override
						public void onAnimationFrameChanged(AnimatedSprite pAnimatedSprite, int pOldFrameIndex, int pNewFrameIndex) { }
						@Override
						public void onAnimationLoopFinished(AnimatedSprite pAnimatedSprite, int pRemainingLoopCount, int pInitialLoopCount) { }

						@Override
						public void onAnimationFinished(AnimatedSprite pAnimatedSprite) {
							human.setX(human.getX() + 15);
							if((human.getFacingDirection() == true && human.getX() >= (player1Sprite.getX() + 100)) || (human.getFacingDirection() == false && human.getX() <= (player1Sprite.getX() + 100))) {
								human.switchFacingDirection();
							} else if(human.getFacingDirection() == false && human.getX() >= (player1Sprite.getX() + 100)) {
								human.switchMovingDirection();
							} 
							human.setRunningSpeed();
						}
					});
					
				}
				PARALLAX_MAP.resume();
				if(music_on) gameBGM.play();
				gameStarted = true;
			}
		}));
	}
	
	//Speed of the Game
	private final class updateGameSpeed implements ITimerCallback {
		public void onTimePassed(final TimerHandler pTimerHandler) {
			if(gameSpeed < 1.23f)
				gameSpeed += 0.011f;
			
			if(updateDistanceHandler.getTimerSeconds() > 0.06)
				updateDistanceHandler.setTimerSeconds((updateDistanceHandler.getTimerSeconds() - 0.005f));
			
			if(!PARALLAX_MAP.isPause() && PARALLAX_MAP.getMapSpeed() < 23f) {
				PARALLAX_MAP.changeMapSpeed(PARALLAX_MAP.getMapSpeed() + 0.075f);
				//for(ObstaclesSprite os : obstaclesSpriteArray) {
				//	os.setObstaclesSpeed(os.getObstaclesSpeed() - 1.23f);
				//}
			}
			
			if(updateSpeedHandler.getTimerSeconds() > 4.5f)
				updateSpeedHandler.setTimerSeconds(3.5f);
			else if(updateSpeedHandler.getTimerSeconds() < 4f && updateSpeedHandler.getTimerSeconds() > 2.65f)
				updateSpeedHandler.setTimerSeconds((updateSpeedHandler.getTimerSeconds() - 0.01f));
		}
	}
	
	//Increment the distance travelled
	private final class updateDistance implements ITimerCallback {
		public void onTimePassed(final TimerHandler pTimerHandler) {
			//tmp = debugFPS.getFPS();
			//fpsText.setText(Float.parseFloat(new DecimalFormat("##").format(tmp)) + "fps");
			
			distanceTravelled++;
			
			if(distanceTravelled < 10) {
				distanceText.setText("0000" + distanceTravelled);
			}
			else if(distanceTravelled >= 10 && distanceTravelled < 100) {
				distanceText.setText("000" + distanceTravelled);
			}
			else if(distanceTravelled >= 100 && distanceTravelled < 1000) {
				distanceText.setText("00" + distanceTravelled);
			}
			else if(distanceTravelled >= 1000 && distanceTravelled < 10000) {
				distanceText.setText("0" + distanceTravelled);
			}
			else if(distanceTravelled >= 10000 && distanceTravelled < 99999) {
				distanceText.setText("" + distanceTravelled);
			}
			else {
				distanceText.setText("99999");
			}
		}
	}

	
	// ======================================================================================================
	// ======================================================================================================
	//  HUMANS & MILITARY Update Handlers 
	// ======================================================================================================
	// ======================================================================================================
	
	//Update on the appearing of the little humans
	private final class updateHumans implements ITimerCallback {
		public void onTimePassed(final TimerHandler pTimerHandler) {
			if (gameStarted && !player_on_flinch_mode) {
				updateHumansHandler.setTimerSeconds(MathUtils.random(0.2f, 1f));
				
				HUMAN_POOL.setOnFirstLoad(false);
				
				HumanSprite hs = HUMAN_POOL.obtainPoolItem();
				humanSpriteArray.addLast(hs);
				
				if(!hs.hasParent()) {
					mGameScene.attachChild(hs);
				}
				hs.setZIndex(1);
				mGameScene.sortChildren();
				
			} else if(!gameStarted && humanOnFirstLoadCount < 16) {
				humanOnFirstLoadCount++;
				
				HUMAN_POOL.setOnFirstLoad(true);
				
				HumanSprite hs = HUMAN_POOL.obtainPoolItem();
				humanSpriteArray.addLast(hs);				
				if(!hs.hasParent())
					mGameScene.attachChild(hs);
			}
		}
	}

	//Update on the appearing of the little military humans
	private final class updateMilitary implements ITimerCallback {
		public void onTimePassed(final TimerHandler pTimerHandler) {
			if (gameStarted) {
				if (updateMilitaryHumansHandler.getTimerSeconds() < 2f) {
					militaryHumansCombo++;
				}
				
				if(militaryHumanSpriteArray.size() < 8) {
					if(militaryHumansCombo > 3 || militaryHumanSpriteArray.size() > 5) {
						militaryHumansCombo = 0;
						updateMilitaryHumansHandler.setTimerSeconds(MathUtils.random(3.45f, 6.78f) - gameSpeed);
					} else 
						updateMilitaryHumansHandler.setTimerSeconds(MathUtils.random(1.3f, 3.45f) - gameSpeed);
					
					militaryHumanSpriteArray.addLast(MILITARY_POOL.obtainPoolItem());
					if(!militaryHumanSpriteArray.getLast().hasParent())
						mGameScene.attachChild(militaryHumanSpriteArray.getLast());
				}
			}
		}
	}
	
	// ======================================================================================================
	// ======================================================================================================
	//  WEAPONS Alert/Update Handlers 
	// ======================================================================================================
	// ======================================================================================================
	
	// Missile Alert Handler
	private final class displayMissileAlert implements ITimerCallback {
		public void onTimePassed(final TimerHandler pTimerHandler) {
			if(!MissileJammer_on) {
				WeaponAlertSprite missileAlert = MISSILE_ALERT_POOL.obtainPoolItem();
				missileAlert.setPosition(CAMERA_WIDTH - missileAlert.getWidth() - 4, player1Sprite.getY());				
				missileAlert.setCurrentTileIndex(0);
				missileAlertSpriteArray.addLast(missileAlert);
				if(!missileAlert.hasParent())
					mGameScene.attachChild(missileAlert);
				
				for (int i=0; i<missileAlertSpriteArray.size(); i++) {
					missileAlert.setY(missileAlert.getY() + missileAlert.getHeight());
					//if(missileAlert.getY() >= missileAlertSpriteArray.get(i).getY() && missileAlert.getY() <= missileAlertSpriteArray.get(i).getY() + missileAlert.getHeight()) {
						if(missileAlert.getY() > CAMERA_HEIGHT - 155)
							missileAlert.setY(MathUtils.random(10, CAMERA_HEIGHT - 155 - missileAlert.getHeight()));
					//}
				}
				
				TimerHandler missileAlertHandler = new TimerHandler(MathUtils.random(0.123f, 0.175f), true, new updateMissileAlert(missileAlert, new Random().nextBoolean()));
				mGameScene.registerUpdateHandler(missileAlertHandler);
				
				//displayMissileAlertHandler.setTimerSeconds(MathUtils.random(3.5f, 4.5f) - gameSpeed);
				displayMissileAlertHandler.setTimerSeconds(MathUtils.random(3f, 4f) - gameSpeed);
			}
		}
	}
	
	//Update Missile Alert
	private final class updateMissileAlert implements ITimerCallback {
		WeaponAlertSprite missileAlertSprite;
		boolean moveStatus = true;
		float timeTaken;
		
		public updateMissileAlert(WeaponAlertSprite missileAlertSprite, boolean moveStatus) {
			this.missileAlertSprite = missileAlertSprite;
			//this.moveStatus = moveStatus;
		}
		
		public void onTimePassed(final TimerHandler pTimerHandler) {
			if(MissileJammer_on) {
				mGameScene.unregisterUpdateHandler(pTimerHandler);
			} else {
				timeTaken += pTimerHandler.getTimerSecondsElapsed() + 0.1f;
	
				if(moveStatus) {
					if(player1Sprite.getY() - missileAlertSprite.getY() > 0) 
						missileAlertSprite.setY(missileAlertSprite.getY() + (5 + gameSpeed));
					else 
						missileAlertSprite.setY(missileAlertSprite.getY() - (5 + gameSpeed) < 0 ? 0 : missileAlertSprite.getY() - (5 + gameSpeed));
				}
				
				float timeTakenSpeed = (3.55f - gameSpeed) < 2.65f ? 2.65f : 3.55f - gameSpeed;
				if(timeTaken >= timeTakenSpeed-1.11f) {
					if(sfx_on) countdownSFX.play();
					missileAlertSprite.setCurrentTileIndex(2);
				} else if(timeTaken >= (timeTakenSpeed-1.11f)/2) {
					missileAlertSprite.setCurrentTileIndex(1);
				} 
				
				if (timeTaken >= timeTakenSpeed) {
					//Generate Missile Head w Collision
					MISSILE_HEAD_POOL.setXPosition(missileAlertSprite.getX() + 4);
					MISSILE_HEAD_POOL.setYPosition(missileAlertSprite.getY());
	
		            MissileHeadSprite mhs = MISSILE_HEAD_POOL.obtainPoolItem();
		            missileHeadSpriteArray.addLast(mhs);
					if(!mhs.hasParent())
						mGameScene.attachChild(mhs);
	
					//Generate Missile Tail	w No Collision
		            MISSILE_TAIL_POOL.setXPosition(mhs.getX() + mhs.getWidth() + 3);
		            MISSILE_TAIL_POOL.setYPosition(mhs.getY());
		            
					MissileTailSprite mts = MISSILE_TAIL_POOL.obtainPoolItem();
					missileTailSpriteArray.addLast(mts);
					if(!mts.hasParent())
						mGameScene.attachChild(mts);
					
					//Generate smoke trails behind the missile
					generateSmokeHandler(mts);
					
					float speed = (1.9f - gameSpeed) < 1.5f ? 1.5f : 1.9f - gameSpeed;
					mhs.moveRightToLeft(speed);
					mts.moveRightToLeft(speed);
					
					//Detach the alert sprite and pause the sound effect
					if(sfx_on && countdownSFX.isPlaying()) {
						countdownSFX.pause();
						countdownSFX.seekTo(0);
					}
					if(sfx_on) {
						missileLaunchSFX.seekTo(0);
						missileLaunchSFX.play();
					}
					recycleMissileAlert(missileAlertSprite);
					
					//Unregister this timerhandler
					timeTaken = 0;
					mGameScene.unregisterUpdateHandler(pTimerHandler);
				}
			}
		}
	}
	
	// Scatter Missile Alert Handler
	private final class displayScatterMissileAlert implements ITimerCallback {
		public void onTimePassed(final TimerHandler pTimerHandler) {
			if(!MissileJammer_on) {
				WeaponAlertSprite scatterMissileAlert = SCATTER_MISSILE_ALERT_POOL.obtainPoolItem();
				scatterMissileAlert.setPosition(CAMERA_WIDTH - scatterMissileAlert.getWidth() - 4, player1Sprite.getY());				
				scatterMissileAlert.setCurrentTileIndex(0);
				scatterMissileAlertSpriteArray.addLast(scatterMissileAlert);
				if(!scatterMissileAlert.hasParent())
					mGameScene.attachChild(scatterMissileAlert);
					
				for (int i=0; i<scatterMissileAlertSpriteArray.size(); i++) {
					//if(scatterMissileAlert.getY() >= missileAlertSpriteArray.get(i).getY() && scatterMissileAlert.getY() <= missileAlertSpriteArray.get(i).getY() + scatterMissileAlert.getHeight()) {
						scatterMissileAlert.setY(scatterMissileAlert.getY() + scatterMissileAlert.getHeight());
						if(scatterMissileAlert.getY() > CAMERA_HEIGHT - 155)
							scatterMissileAlert.setY(MathUtils.random(10, CAMERA_HEIGHT - 155 - scatterMissileAlert.getHeight()));
					//}
				}
				
				TimerHandler scatterMissileAlertHandler = new TimerHandler(MathUtils.random(0.123f, 0.175f), true, new updateScatterMissileAlert(scatterMissileAlert, new Random().nextBoolean()));
				mGameScene.registerUpdateHandler(scatterMissileAlertHandler);
				
				displayScatterMissileAlertHandler.setTimerSeconds(MathUtils.random(7.77f, 11.11f) - gameSpeed);
			}
		}
	}
	
	//Update Scatter Missile Alert
	private final class updateScatterMissileAlert implements ITimerCallback {
		WeaponAlertSprite scatterMissileAlert;
		boolean moveStatus = true;
		float timeTaken;
		
		public updateScatterMissileAlert(WeaponAlertSprite scatterMissileAlert, boolean moveStatus) {
			this.scatterMissileAlert = scatterMissileAlert;
			//this.moveStatus = moveStatus;
		}
		
		public void onTimePassed(final TimerHandler pTimerHandler) {
			if(MissileJammer_on) {
				mGameScene.unregisterUpdateHandler(pTimerHandler);
			} else {
				timeTaken += pTimerHandler.getTimerSecondsElapsed() + 0.1f;
				
				if(moveStatus) {
					if(player1Sprite.getY() - scatterMissileAlert.getY() > 0) 
						scatterMissileAlert.setY(scatterMissileAlert.getY() + (5 + gameSpeed));
					else 
						scatterMissileAlert.setY(scatterMissileAlert.getY() - (5 + gameSpeed) < 0 ? 0 : scatterMissileAlert.getY() - (5 + gameSpeed));
				}
				
				float timeTakenSpeed = (3.85f - gameSpeed) < 2.85f ? 2.85f : 3.85f - gameSpeed;
				if(timeTaken >= timeTakenSpeed-1.11f) {
					if(sfx_on) countdownSFX.play();
					scatterMissileAlert.setCurrentTileIndex(2);
				} else if(timeTaken >= (timeTakenSpeed-1.11f)/2) {
					scatterMissileAlert.setCurrentTileIndex(1);
				}
				
				if (timeTaken >= timeTakenSpeed) {
					SCATTER_MISSILE_HEAD_POOL.setXPosition(scatterMissileAlert.getX() + 4);
					SCATTER_MISSILE_HEAD_POOL.setYPosition(scatterMissileAlert.getY());
	
					for(int i=0; i<1; i++) {
						final int missile = i;
						
						//Launch Missile (Missile Head) Comes with Collision
			            final ScatterMissileHeadSprite smhs = SCATTER_MISSILE_HEAD_POOL.obtainPoolItem();
						scatterMissileHeadSpriteArray.addLast(smhs);
			            smhs.setScaleCenterX(0 + smhs.getWidth());
			            smhs.setScale(0.7f);
						if(!smhs.hasParent())
							mGameScene.attachChild(smhs);
		
						//Generate Missile Tail	w No Collision
						SCATTER_MISSILE_TAIL_POOL.setXPosition(smhs.getX() + smhs.getWidth() - 4);
						SCATTER_MISSILE_TAIL_POOL.setYPosition(smhs.getY() - 8f);
			            
						final ScatterMissileTailSprite smts = SCATTER_MISSILE_TAIL_POOL.obtainPoolItem();
						scatterMissileTailSpriteArray.addLast(smts);
						smhs.setMissileTail(smts);
						smts.setScaleCenterX(0);
						smts.setScale(0.6f);
						if(!smts.hasParent())
							mGameScene.attachChild(smts);
		
						//Generate smoke trails behind the missile
						generateSmokeHandler(smts);
						
						final float xDist = CAMERA_WIDTH / 2f;//MathUtils.random(300f, 300f);
						final float speed = (1.9f - gameSpeed) < 1.35f ? 1.35f : 1.9f - gameSpeed;
						
						//Register first action
						scatterMissileMovement(smhs, smts, missile, xDist, speed, false, 0);
						
						smhs.clearUpdateHandlers();
						final float time = (1.5f - gameSpeed) < 0.75f ? 0.75f : 1.5f - gameSpeed;
						smhs.registerUpdateHandler(new TimerHandler(time, true, new ITimerCallback() {
							@Override
							public void onTimePassed(final TimerHandler pTimerHandler) {
								//Tried, 0.30625f, 1.425, -10.7f, 
								//DESC : -100 means slower, -10 means the distance between tail n head is that head position 
								//will be higher than tail when moving down.
								scatterMissileMovement(smhs, smts, missile, xDist, speed, true, -10.7f);
							}
						}));
	
						//Detach the alert sprite and pause the sound effect after 1st missile generated
						if(missile==0) {
							if(sfx_on && countdownSFX.isPlaying()) {
								countdownSFX.pause();
								countdownSFX.seekTo(0);
							}
						
							recycleScatterMissileAlert(scatterMissileAlert);
						
							//Unregister this timerhandler
							timeTaken = 0;
							mGameScene.unregisterUpdateHandler(pTimerHandler);
						}
					}
				}
			}
		}
	}
	
	private void scatterMissileMovement(ScatterMissileHeadSprite smhs, ScatterMissileTailSprite smts, int missile, float xDist, float speed, boolean switchPos, float moveCount) {
		if(smhs.getY() <= 65f) {
			float yDist = smhs.getY();
			
			if (switchPos) {
				if(smhs.getMoveStatus().equals("UP")) {
					yDist += MathUtils.random(325f, 400f);
					smhs.moveDownRightToLeft(speed, xDist, yDist > CAMERA_HEIGHT - 150 ? CAMERA_HEIGHT - 150 : yDist);
					//smts.moveDownRightToLeft(speed, xDist, yDist > CAMERA_HEIGHT - 150 ? CAMERA_HEIGHT - 150 : yDist+(1f*moveCount));
					//smts.moveDownRightToLeft(speed, xDist, 0);
				} else {
					smhs.moveUpRightToLeft(speed, xDist, 0f);
					//smts.moveUpRightToLeft(speed, xDist, 0f);
				}				
			} else {
				if(missile == 0) {
					smhs.moveUpRightToLeft(speed, xDist, 0f);
					//smts.moveUpRightToLeft(speed, xDist, 0f);
				} else {
					yDist += MathUtils.random(325f, 400f);
					smhs.moveDownRightToLeft(speed, xDist, yDist > CAMERA_HEIGHT - 150 ? CAMERA_HEIGHT - 150 : yDist);
					//smts.moveDownRightToLeft(speed, xDist, yDist > CAMERA_HEIGHT - 150 ? CAMERA_HEIGHT - 150 : yDist);
					//smts.moveDownRightToLeft(speed, xDist, 0);
				} 
			}			
		} else if(smhs.getY() > 65f && smhs.getY() <= 150f) {
			float yDist = smhs.getY() - 55f;
			if (switchPos) {
				if(smhs.getMoveStatus().equals("UP")) {
					yDist += MathUtils.random(275f, 350f);
					smhs.moveDownRightToLeft(speed, xDist, yDist > CAMERA_HEIGHT - 150 ? CAMERA_HEIGHT - 150 : yDist);
					//smts.moveDownRightToLeft(speed, xDist, yDist > CAMERA_HEIGHT - 150 ? CAMERA_HEIGHT - 150 : yDist+(1f*moveCount));
					//smts.moveDownRightToLeft(speed, xDist, 0);
				} else {
					smhs.moveUpRightToLeft(speed, xDist, smhs.getY() - yDist < 60 ? -(60 - smhs.getY()) : yDist);
					//smts.moveUpRightToLeft(speed, xDist, smhs.getY() - yDist < 60 ? -(60 - smhs.getY()) : yDist+(1f*moveCount));
					//smts.moveUpRightToLeft(speed, xDist, 0);
				}				
			} else {
				if(missile==0) {
					smhs.moveUpRightToLeft(speed, xDist, smhs.getY() - yDist < 60 ? -(60 - smhs.getY()) : yDist);
					//smts.moveUpRightToLeft(speed, xDist, smhs.getY() - yDist < 60 ? -(60 - smhs.getY()) : yDist);
					//smts.moveUpRightToLeft(speed, xDist, 0);
				} else {
					yDist += MathUtils.random(275f, 350f);
					smhs.moveDownRightToLeft(speed, xDist, yDist > CAMERA_HEIGHT - 150 ? CAMERA_HEIGHT - 150 : yDist);
					//smts.moveDownRightToLeft(speed, xDist, yDist > CAMERA_HEIGHT - 150 ? CAMERA_HEIGHT - 150 : yDist);
					//smts.moveDownRightToLeft(speed, xDist, 0);
				}
			}
		} else if(smhs.getY() > 150f && smhs.getY() <= 250f) {
			float yDist = MathUtils.random(200f, 300f);
			
			if (switchPos) {
				if(smhs.getMoveStatus().equals("UP")) {
					yDist += MathUtils.random(90f, 150f);
					smhs.moveDownRightToLeft(speed, xDist, yDist > CAMERA_HEIGHT - 150 ? CAMERA_HEIGHT - 150 : yDist);
					//smts.moveDownRightToLeft(speed, xDist, yDist > CAMERA_HEIGHT - 150 ? CAMERA_HEIGHT - 150 : yDist+(1f*moveCount));
					//smts.moveDownRightToLeft(speed, xDist, 0);
				} else {
					smhs.moveUpRightToLeft(speed, xDist, smhs.getY() - yDist < 60 ? -(60 - smhs.getY()) : yDist);
					//smts.moveUpRightToLeft(speed, xDist, smhs.getY() - yDist < 60 ? -(60 - smhs.getY()) : yDist+(1f*moveCount));
					//smts.moveUpRightToLeft(speed, xDist, 0);
				}				
			} else {
				if(missile==0) {
					smhs.moveUpRightToLeft(speed, xDist, smhs.getY() - yDist < 60 ? -(60 - smhs.getY()) : yDist);
					//smts.moveUpRightToLeft(speed, xDist, smhs.getY() - yDist < 60 ? -(60 - smhs.getY()) : yDist);
					//smts.moveUpRightToLeft(speed, xDist, 0);
				} else {
					yDist += MathUtils.random(90f, 150f);
					smhs.moveDownRightToLeft(speed, xDist, yDist > CAMERA_HEIGHT - 150 ? CAMERA_HEIGHT - 150 : yDist);
					//smts.moveDownRightToLeft(speed, xDist, yDist > CAMERA_HEIGHT - 150 ? CAMERA_HEIGHT - 150 : yDist);
					//smts.moveDownRightToLeft(speed, xDist, 0);
				}
			}
		} else if(smhs.getY() > 250f && smhs.getY() <= 350f) {
			float yDist = MathUtils.random(200f, 275f);
			
			if (switchPos) {
				if(smhs.getMoveStatus().equals("UP")) {
					smhs.moveDownRightToLeft(speed, xDist, yDist > CAMERA_HEIGHT - 150 ? CAMERA_HEIGHT - 150 : yDist);
					//smts.moveDownRightToLeft(speed, xDist, yDist > CAMERA_HEIGHT - 150 ? CAMERA_HEIGHT - 150 : yDist+(1f*moveCount));
					//smts.moveDownRightToLeft(speed, xDist, 0);
				} else {
					smhs.moveUpRightToLeft(speed, xDist, smhs.getY() - yDist < 60 ? -(60 - smhs.getY()) : yDist);
					//smts.moveUpRightToLeft(speed, xDist, smhs.getY() - yDist < 60 ? -(60 - smhs.getY()) : yDist+(1f*moveCount));
					//smts.moveUpRightToLeft(speed, xDist, 0);
				}				
			} else {
				if(missile==0) {
					smhs.moveUpRightToLeft(speed, xDist, smhs.getY() - yDist < 60 ? -(60 - smhs.getY()) : yDist);
					//smts.moveUpRightToLeft(speed, xDist, smhs.getY() - yDist < 60 ? -(60 - smhs.getY()) : yDist);
					//smts.moveUpRightToLeft(speed, xDist, 0);
				} else {
					smhs.moveDownRightToLeft(speed, xDist, yDist > CAMERA_HEIGHT - 150 ? CAMERA_HEIGHT - 150 : yDist);
					//smts.moveDownRightToLeft(speed, xDist, yDist > CAMERA_HEIGHT - 150 ? CAMERA_HEIGHT - 150 : yDist);
					//smts.moveDownRightToLeft(speed, xDist, 0);
				}
			}
		} else if(smhs.getY() > 350f && smhs.getY() <= 450f) {
			float yDist = MathUtils.random(150f, 250f);
			
			if (switchPos) {
				if(smhs.getMoveStatus().equals("UP")) {
					smhs.moveDownRightToLeft(speed, xDist, yDist > CAMERA_HEIGHT - 150 ? CAMERA_HEIGHT - 150 : yDist);
					//smts.moveDownRightToLeft(speed, xDist, yDist > CAMERA_HEIGHT - 150 ? CAMERA_HEIGHT - 150 : yDist+(1f*moveCount));
					//smts.moveDownRightToLeft(speed, xDist, 0);
				} else {
					yDist += MathUtils.random(80f, 120f);
					smhs.moveUpRightToLeft(speed, xDist, smhs.getY() - yDist < 60 ? -(60 - smhs.getY()) : yDist);
					//smts.moveUpRightToLeft(speed, xDist, smhs.getY() - yDist < 60 ? -(60 - smhs.getY()) : yDist+(1f*moveCount));
					//smts.moveUpRightToLeft(speed, xDist, 0);
				}				
			} else {
				if(missile==0) {
					yDist += MathUtils.random(100f, 150f);
					smhs.moveUpRightToLeft(speed, xDist, smhs.getY() - yDist < 60 ? -(60 - smhs.getY()) : yDist);
					//smts.moveUpRightToLeft(speed, xDist, smhs.getY() - yDist < 60 ? -(60 - smhs.getY()) : yDist);
					//smts.moveUpRightToLeft(speed, xDist, 0);
				} else {
					smhs.moveDownRightToLeft(speed, xDist, yDist > CAMERA_HEIGHT - 150 ? CAMERA_HEIGHT - 150 : yDist);
					//smts.moveDownRightToLeft(speed, xDist, yDist > CAMERA_HEIGHT - 150 ? CAMERA_HEIGHT - 150 : yDist);
					//smts.moveDownRightToLeft(speed, xDist, 0);
				}
			}
		} else if(smhs.getY() > 450f && smhs.getY() <= CAMERA_HEIGHT - 100) {
			float yDist = MathUtils.random(300f, 400f);
			
			if (switchPos) {
				if(smhs.getMoveStatus().equals("UP")) {
					smhs.moveDownRightToLeft(speed, xDist, 0);
					//smts.moveDownRightToLeft(speed, xDist, 0);
				} else {
					smhs.moveUpRightToLeft(speed, xDist, smhs.getY() - yDist < 60 ? -(60 - smhs.getY()) : yDist);
					//smts.moveUpRightToLeft(speed, xDist, smhs.getY() - yDist < 60 ? -(60 - smhs.getY()) : yDist+(1f*moveCount));
				}				
			} else {
				if(missile==0) {
					smhs.moveUpRightToLeft(speed, xDist, smhs.getY() - yDist < 60 ? -(60 - smhs.getY()) : yDist);
					//smts.moveUpRightToLeft(speed, xDist, smhs.getY() - yDist < 60 ? -(60 - smhs.getY()) : yDist);
				} else {
					smhs.moveDownRightToLeft(speed, xDist, 0);
					//smts.moveDownRightToLeft(speed, xDist, 0);
				}
			}
		}
		
		//if(switchPos) 
		//	moveCount = moveCount + moveCount;
	}

	//Laser Beam Alert Handler
	private final class displayLaserBeamAlert implements ITimerCallback {
		public void onTimePassed(final TimerHandler pTimerHandler) {
			WeaponAlertSprite laserBeamAlert = LASER_BEAM_ALERT_POOL.obtainPoolItem();
			
			if(MathUtils.random(0, 1) == 0)
				laserBeamAlert.setPosition(player1Sprite.getX(), 1);
			else 
				laserBeamAlert.setPosition(MathUtils.random(100f, CAMERA_WIDTH - 100f), 1);
				
			//laserBeamAlert.animate(180);
			laserBeamAlert.setCurrentTileIndex(0);
			laserBeamAlertSpriteArray.addLast(laserBeamAlert);
			if(!laserBeamAlert.hasParent())
				mGameScene.attachChild(laserBeamAlert);
			
			TimerHandler laserBeamAlertHandler = new TimerHandler(MathUtils.random(0.123f, 0.175f), true, new updateLaserBeamAlert(laserBeamAlert));
			mGameScene.registerUpdateHandler(laserBeamAlertHandler);
			
			displayLaserBeamAlertHandler.setTimerSeconds(MathUtils.random(6f, 8.5f) - gameSpeed);
		}
	}

	//Update Laser Beam Alert
	private final class updateLaserBeamAlert implements ITimerCallback {
		WeaponAlertSprite laserBeamAlert;
		float timeTaken;
		public updateLaserBeamAlert(WeaponAlertSprite laserBeamAlert) {
			this.laserBeamAlert = laserBeamAlert;
		}
		public void onTimePassed(final TimerHandler pTimerHandler) {
			timeTaken += pTimerHandler.getTimerSecondsElapsed() + 0.1f;
			
			if(player1Sprite.getX() - laserBeamAlert.getX() > 0) 
				laserBeamAlert.setX(laserBeamAlert.getX() + gameSpeed + 1f);
			else 
				laserBeamAlert.setX(laserBeamAlert.getX() - gameSpeed - 1f);

			float timeTakenSpeed = (4.5f - gameSpeed) < 3.5f ? 3.5f : 4.5f - gameSpeed;
			
			
			if(timeTaken >= timeTakenSpeed-1.11f) {
				if(sfx_on) laserCountdownSFX.play();
				laserBeamAlert.setCurrentTileIndex(2);
			} else if(timeTaken >= (timeTakenSpeed-1.11f)/2) {
				laserBeamAlert.setCurrentTileIndex(1);
			}
			
			if (timeTaken >= timeTakenSpeed) {
				//Create Laser Beam and set the positions etc
				LaserBeamSprite lbs = LASER_BEAM_POOL.obtainPoolItem();
				lbs.setPosition(laserBeamAlert.getX(), 0);
				lbs.setScaleCenterY(0);
				lbs.setScaleY(1f + (((CAMERA_HEIGHT - 40) - lbs.getHeight()) / lbs.getHeight()));
				lbs.setScaleX(1.6f);
				//lbs.setHeight(CAMERA_HEIGHT-39);
	            lbs.animate(10, 45, new IAnimationListener() {
					@Override
					public void onAnimationStarted(AnimatedSprite pAnimatedSprite, int pInitialLoopCount) { }
					@Override
					public void onAnimationFrameChanged(AnimatedSprite pAnimatedSprite, int pOldFrameIndex, int pNewFrameIndex) { }
					@Override
					public void onAnimationLoopFinished(AnimatedSprite pAnimatedSprite, int pRemainingLoopCount, int pInitialLoopCount) { }
					@Override
					public void onAnimationFinished(AnimatedSprite pAnimatedSprite) {
						//pAnimatedSprite.setPosition(-200, 0);
						if(sfx_on && laserBeamSFX.isPlaying()) laserBeamSFX.pause();
						pAnimatedSprite.setVisible(false);
						pAnimatedSprite.clearUpdateHandlers();
					}
				});
	            laserBeamSpriteArray.addLast(lbs);

				float speed = (8.5f - gameSpeed) < 5.5f ? 5.5f : 8.5f - gameSpeed;
				float distance = (456f * gameSpeed) < 300f ? 300f : 456f * gameSpeed;
				if(player1Sprite.getX() - laserBeamAlert.getX() > 0) 
					lbs.moveLeftToRight(speed, distance);
				else 
					lbs.moveRightToLeft(speed, distance);

				//Detach the alert sprite and pause the sound effect
				if(sfx_on && laserCountdownSFX.isPlaying()) {
					laserCountdownSFX.pause();
					laserCountdownSFX.seekTo(0);
				}
				if(sfx_on && !laserBeamSFX.isPlaying()) {
					laserBeamSFX.seekTo(0);
					laserBeamSFX.play();
				}
				recycleLaserBeamAlert(laserBeamAlert);
				
				//Unregister this timerhandler
				timeTaken = 0;
				mGameScene.unregisterUpdateHandler(pTimerHandler);
				
				//Attach the laser beam to the scene
				if(!lbs.hasParent()) {
		            mGameScene.attachChild(lbs);
				}
			}
		}
	}
	
	private void fireMilitaryMissile(final MilitaryHumanSprite military) {
		military.setActionRegistered(true);
		//float randomTime = MathUtils.random(1f, 2.5f);
		//float time = randomTime - gameSpeed < 0.25f ? 0.25f : randomTime - gameSpeed;
		military.registerUpdateHandler(new TimerHandler(MathUtils.random(0.5f, 2f), false, new ITimerCallback() {
            @Override
            public void onTimePassed(final TimerHandler pTimerHandler) {
				military.unregisterUpdateHandler(pTimerHandler);
				
            	if(player1Sprite.getY() < CAMERA_HEIGHT/3) {
            		military.aim2(player1Sprite);
            	} else {
            		military.aim1(player1Sprite);
            	}
            	
            	final MilitaryMissileAlertSprite militaryMissileAlert = MILITARY_MISSILE_ALERT_POOL.obtainPoolItem();
            	militaryMissileAlert.setAssignedMilitaryHuman(military);
            	militaryMissileAlert.animate(300, false, new IAnimationListener() {
					@Override
					public void onAnimationStarted(AnimatedSprite pAnimatedSprite, int pInitialLoopCount) {													
						military.setMilitaryStatus("AIMING");
						if(!military.getMilitaryStatus().contains("AIM")) {
							recycleMilitaryMissileAlert(militaryMissileAlert);
						}
		            	militaryMissileAlert.setAssignedMilitaryHuman(military);
					}
					
					@Override
					public void onAnimationFrameChanged(AnimatedSprite pAnimatedSprite, int pOldFrameIndex, int pNewFrameIndex) {
						if(!military.getMilitaryStatus().contains("AIM")) {
							recycleMilitaryMissileAlert(militaryMissileAlert);
						}
		            	militaryMissileAlert.setAssignedMilitaryHuman(military);
					}
					
					@Override
					public void onAnimationLoopFinished(AnimatedSprite pAnimatedSprite, int pRemainingLoopCount, int pInitialLoopCount) { }
					
					@Override
					public void onAnimationFinished(AnimatedSprite pAnimatedSprite) {
						if(military.getMilitaryStatus().contains("AIM") && militaryMissileAlert.getAssignedMilitaryHuman().equals(military)) {
							MilitaryMissileSprite militaryMissile = MILITARY_MISSILE_POOL.obtainPoolItem();
							militaryMissileSpriteArray.addLast(militaryMissile);
							
							militaryMissile.setFlippedHorizontal(military.getFacingDirection());
							
							if(military.getFacingDirection())
								militaryMissile.setPosition(military.getX() - 23f, military.getY());
							else 
								militaryMissile.setPosition(military.getX() + 19f, military.getY());
							
							float militaryMissileSpeed = (4.0f - gameSpeed) < 2.9f ? 2.9f : 4.0f - gameSpeed;

							
							if(military.getAimAngle().equals("AIM1")) {
								militaryMissile.setCurrentTileIndex(1);
								militaryMissile.fire1(militaryMissileSpeed, CAMERA_WIDTH);
							} else if(military.getAimAngle().equals("AIM2")) {
								militaryMissile.setCurrentTileIndex(2);
								militaryMissile.fire2(militaryMissileSpeed, CAMERA_WIDTH, CAMERA_HEIGHT);
							}

							//Generate smoke trails behind the missile
							generateSmokeHandler(militaryMissile);
							
							if(!militaryMissile.hasParent())
								mGameScene.attachChild(militaryMissile);
							
							military.setMilitaryStatus("FIRED");
						}
						recycleMilitaryMissileAlert(militaryMissileAlert);						
					}
				});

            	militaryMissileAlertSpriteArray.addLast(militaryMissileAlert);
				if(!militaryMissileAlert.hasParent())
					mGameScene.attachChild(militaryMissileAlert);				
            }
        }));
	}

	// ======================================================================================================
	// ======================================================================================================
	//  Obstacles Update Handlers (Lamp, Traffic, Car etc.)
	// ======================================================================================================
	// ======================================================================================================
	
	private final class updateLamp implements ITimerCallback {
		public void onTimePassed(final TimerHandler pTimerHandler) {
			if(!gameStarted) {
				if(lampOnFirstLoadCount >= 2) {
					//mGameScene.unregisterUpdateHandler(pTimerHandler);
				} else {
					lampOnFirstLoadCount++;
					
					LAMP_POOL.setOnFirstLoad(true);
	
					LampSprite ls = LAMP_POOL.obtainPoolItem();
			        lampSpriteArray.addLast(ls);
					if(!ls.hasParent())
						mGameScene.attachChild(ls);
				}
			} else {
				LAMP_POOL.setOnFirstLoad(false);
				LampSprite ls = LAMP_POOL.obtainPoolItem();
		        lampSpriteArray.addLast(ls);
				if(!ls.hasParent())
					mGameScene.attachChild(ls);
				
				pTimerHandler.setTimerSeconds(3f);
			}
		}
	}

	private final class updateCar implements ITimerCallback {
		public void onTimePassed(final TimerHandler pTimerHandler) {
			CarSprite cs = CAR_POOL.obtainPoolItem();
	        carSpriteArray.addLast(cs);
			if(!cs.hasParent())
				mGameScene.attachChild(cs);
			
			updateCarHandler.setTimerSeconds(MathUtils.random(5f, 10f));
		}
	}

	private final class updateTraffic implements ITimerCallback {
		public void onTimePassed(final TimerHandler pTimerHandler) {
			TrafficSprite ts = TRAFFIC_POOL.obtainPoolItem();
			trafficSpriteArray.addLast(ts);
			if(!ts.hasParent())
				mGameScene.attachChild(ts);
			
			updateTrafficHandler.setTimerSeconds(MathUtils.random(5f, 9f));
		}
	}
	
	// ======================================================================================================
	// ======================================================================================================
	//  Shared Methods
	// ======================================================================================================
	// ======================================================================================================
	
	private void playerOnFlinchMode() {
		if (!player_on_flinch_mode) {			
			player_on_flinch_mode = true;
			
			//If gameplay is using accelerometer, unregister the updating of the player location
			if(!controlpads_on) {
				mEngine.unregisterUpdateHandler(updateSpritePosition);
			} else {
				mGameScene.unregisterUpdateHandler(mEarthPhysicsWorld);
			}
				
			//Pause the map from moving
			PARALLAX_MAP.pause();
			
			//Unregister the distance from increasing
			mGameScene.unregisterUpdateHandler(updateDistanceHandler);
			mGameScene.unregisterUpdateHandler(updateLampHandler);
			
			//Unregister the obstacles from moving
			//mGameScene.unregisterUpdateHandler(updateObstaclesHandler);
			
			//flinch the UFO and delay its ability to absorb for awhile
			playerFlinchRotateTimer = new TimerHandler(0.1f, true, new ITimerCallback() {
				@Override
				public void onTimePassed(final TimerHandler pTimerHandler) {
					if(player_rotated) {
						player_rotated = false;
						player1Sprite.setRotation(player1Sprite.getRotation() + 5f);
					} else {
						player_rotated = true;
						player1Sprite.setRotation(player1Sprite.getRotation() - 5f);
					}
				}
			});
			//Register the timer handler that shakes the player sprite
			mGameScene.registerUpdateHandler(playerFlinchRotateTimer);
			
			//Loop and change all humans moving direction
			for (int i = 0; i < humanSpriteArray.size(); i++) {
				HumanSprite human = humanSpriteArray.get(i);
				human.setMovingDirection(human.getFacingDirection());
				human.playerInFlinchMode();
			}
			
			for (int i = 0; i < lampSpriteArray.size(); i++) {
				lampSpriteArray.get(i).setLampStatus("STOP");
			}
			
			for (int i = 0; i < trafficSpriteArray.size(); i++) {
				trafficSpriteArray.get(i).setTrafficStatus("STOP");
			}
			
			//Loop and stop all military humans from moving and aim at player
			for (int i = 0; i < militaryHumanSpriteArray.size(); i++) {
				MilitaryHumanSprite military = militaryHumanSpriteArray.get(i); 
				military.haltMilitary();
				//military.setActionRegistered(false);
				
				//if(MathUtils.random(0, 1) == 0) {
				//	military.setWalkAnimationSpeed();
				//	if(!military.getFacingDirection())
				//		military.moveLeftToRight((PARALLAX_MAP.getParallaxLayer().calculateSpeedInPixel % PARALLAX_MAP.getLayer1().getShapeWidthScaled()) * 5f);
				//	else
				//		military.moveRightToLeft((PARALLAX_MAP.getParallaxLayer().calculateSpeedInPixel % PARALLAX_MAP.getLayer1().getShapeWidthScaled()) * 5f);
				//} 
				//else {
				//	if(!military.getMilitaryStatus().contains("AIM"))
				//		fireMilitaryMissile(militaryHumanSpriteArray.get(i));
				//}
			}
			
			//Loop and stop all disintegrated humans from moving
			for (int i = 0; i < DisintegratedHumanSpriteArray.size(); i++) {
				DisintegratedHumanSpriteArray.get(i).halt();
			}
			
			//for (int i = 0; i < militaryMissileAlertSpriteArray.size(); i++) {
			//	recycleMilitaryMissileAlert(militaryMissileAlertSpriteArray.get(i));
			//}
			
			//Check whether beam sprite is attached to the scene
			//If so, pause the sound effects and unregister all collision handler and remove from scene
			if(player1BeamSprite.isVisible()) {
				if(sfx_on && ufoBeamSFX.isPlaying()) ufoBeamSFX.pause();
				//beamSprite.setVisible(false);
			}
		}
	}
	
	private void endOfFlinchMode(final AnimatedSprite sprite) {
		if(player_on_flinch_mode && !flinch_mode_activated) {
			flinch_mode_activated = true;
			mGameScene.registerUpdateHandler(new TimerHandler(0.1f, true, new playerFlinchFinished(sprite)));
		}
	}

	private final class playerFlinchFinished implements ITimerCallback {
		AnimatedSprite sprite;
		float timeTaken;
		
		public playerFlinchFinished(AnimatedSprite sprite) {
			this.sprite = sprite;
		}
		
		public void onTimePassed(TimerHandler pTimerHandler) {
			timeTaken += pTimerHandler.getTimerSecondsElapsed() + 0.1f;
			
			if(!MilitaryMissileSprite.class.isInstance(sprite)) {
				if(timeTaken >= 0.6f && timeTaken < 1f)
					player1BeamSprite.setVisible(false);
			}

			if(timeTaken >= 0.6f && timeTaken < 1f) {
				sprite.setVisible(false);
				/*
				//Register a handler to revert all unregistered handlers back to scene
				if (LampSprite.class.isInstance(sprite)) {
					recycleLamp((LampSprite)sprite);
				}
				if (CarSprite.class.isInstance(sprite)) {
					recycleCar((CarSprite)sprite);
				}
				if (TrafficSprite.class.isInstance(sprite)) {
					recycleTraffic((TrafficSprite)sprite);
				}*/
			} else if(timeTaken >= 1.2f) {
				flinch_mode_activated = false;
			
				//Unregister the handler that was use to rotate the player
				mGameScene.unregisterUpdateHandler(playerFlinchRotateTimer);

				//Set the player back to original rotation
				player1Sprite.setRotation(0f);
				
				//Re-register the handlers that was paused 
				PARALLAX_MAP.resume();
				mGameScene.registerUpdateHandler(updateDistanceHandler);
				mGameScene.registerUpdateHandler(updateLampHandler);

				if (!controlpads_on) {
					mEngine.registerUpdateHandler(updateSpritePosition);
				} else {
					mGameScene.registerUpdateHandler(mEarthPhysicsWorld);
				}

				//Change random number of humans direction back 
				for (int i = 0; i < humanSpriteArray.size(); i++) {
					HumanSprite human = humanSpriteArray.get(i);
					human.setMovingDirection(!human.getFacingDirection());
					human.playerOutOfFlinchMode();
				}
				
				for (int i = 0; i < lampSpriteArray.size(); i++) {
					lampSpriteArray.get(i).setLampStatus("MOVE");
				}
				
				for (int i = 0; i < trafficSpriteArray.size(); i++) {
					trafficSpriteArray.get(i).setTrafficStatus("MOVE");
				}
				
				//Loop and recover all military humans from stop
				for (int i = 0; i < militaryHumanSpriteArray.size(); i++) {
					militaryHumanSpriteArray.get(i).haltRecover();
				}

				//Loop and stop all disintegrated humans from moving
				for (int i = 0; i < DisintegratedHumanSpriteArray.size(); i++) {
					DisintegratedHumanSpriteArray.get(i).haltRecover();
				}
				
				//Set indicators back to false
				player_on_flinch_mode = false;
				
				//Unregister this timer handler
				mGameScene.unregisterUpdateHandler(pTimerHandler);
			}
		}
	}
	
	private void shieldHit(AnimatedSprite sprite, boolean shieldDestroyed) {
		if(shieldDestroyed) {
			//TODO: Decide whether shield is on for a period of time or used once only
			//Shield_on = false;
			//forcefieldSprite.setVisible(false);
		}
		
		
		final ExplosionSprite explosion = EXPLOSION_POOL.obtainPoolItem();
		explosion.setPosition(sprite.getX() - explosion.getWidth()/2, (sprite.getY() - explosion.getHeight()/2) + 15);
		explosion.animate(15, false, new IAnimationListener() {
			@Override
			public void onAnimationStarted(AnimatedSprite pAnimatedSprite, int pInitialLoopCount) {
				if(sfx_on && missileLaunchSFX.isPlaying()) missileLaunchSFX.pause();
				//if(sfx_on && !explosionSFX.isPlaying()) explosionSFX.play();
				if(sfx_on && !shieldHitSFX.isPlaying()) shieldHitSFX.play();
			}

			@Override
			public void onAnimationFrameChanged(AnimatedSprite pAnimatedSprite, int pOldFrameIndex, int pNewFrameIndex) { }

			@Override
			public void onAnimationLoopFinished(AnimatedSprite pAnimatedSprite, int pRemainingLoopCount, int pInitialLoopCount) { }

			@Override
			public void onAnimationFinished(AnimatedSprite pAnimatedSprite) {
				pAnimatedSprite.setVisible(false);
			}
		});
		explosionSpriteArray.addLast(explosion);
		if(!explosion.hasParent())
			mGameScene.attachChild(explosion);
	}
	
	
	private void playerHit(AnimatedSprite sprite, boolean playerDestroyed, PixelPerfectAnimatedSprite destroyer) {
		
		if(playerDestroyed) {
			mPlayerHit = true;

			if(player1BeamSprite.hasParent()) 
				player1BeamSprite.setVisible(false);
			
			if(player1WindLandingSprite.hasParent()) 
				player1WindLandingSprite.setVisible(false);
			
			if(controlpads_on) 
				mGameScene.clearChildScene();
			
			float posX = player1Sprite.getX();// - ((spacecraft.loadSpacecraftExplosionTexture().getWidth(0) - player1Sprite.getWidth())/4);
			float posY = player1Sprite.getY();// - ((spacecraft.loadSpacecraftExplosionTexture().getHeight(0) - player1Sprite.getHeight())/2);
			float player1SpriteWidth = player1Sprite.getWidth(), player1SpriteHeight = player1Sprite.getHeight();

			mGameScene.detachChild(player1Sprite);	
			
			player1Sprite = new PixelPerfectAnimatedSprite(posX, posY, player1Spacecraft.loadSpacecraftExplosionTexture(spacecraft_sharedpreference.getActiveSpacecraft()), mVertexBufferObjectManager);
			player1Sprite.setPosition(posX - (player1Sprite.getWidth() - player1SpriteWidth)/2, posY - (player1Sprite.getHeight() - player1SpriteHeight)/2);
			player1Sprite.animate(player1Spacecraft.getSpacecraftExplosionSpeed(), 0, player1Spacecraft.getSpacecraftExplosionFrames(), false, new IAnimationListener() {
				@Override
				public void onAnimationStarted(AnimatedSprite pAnimatedSprite, int pInitialLoopCount) { }
				@Override
				public void onAnimationFrameChanged(AnimatedSprite pAnimatedSprite, int pOldFrameIndex, int pNewFrameIndex) { }
				@Override
				public void onAnimationLoopFinished(AnimatedSprite pAnimatedSprite, int pRemainingLoopCount, int pInitialLoopCount) { }
	
				@Override
				public void onAnimationFinished(AnimatedSprite pAnimatedSprite) {
					mGameOver = true;
				}
			});
			//player1Sprite.setScale(1.05f);
			mGameScene.attachChild(player1Sprite);
		}
		
		//create explosion sprite
		for (int i = 0; i < 2; i++) {
			final int explosionCount = i;
			
			ExplosionSprite explosionSprite = EXPLOSION_POOL.obtainPoolItem();
			if(explosionCount == 0) {
				explosionSprite.setPosition((sprite.getX() + sprite.getWidth()/2) - explosionSprite.getWidth()/2, sprite.getY() + sprite.getHeight()/2 - explosionSprite.getHeight()/2);
			} else if(explosionCount == 1) {
				explosionSprite.setPosition(sprite.getX() - explosionSprite.getWidth()/2, sprite.getY() + sprite.getHeight()/2 - explosionSprite.getHeight()/2);
			} else if(explosionCount == 2) {
				explosionSprite.setPosition(sprite.getX() + sprite.getWidth()/2, sprite.getY() + sprite.getHeight() - explosionSprite.getHeight()/2);
			} else {
				explosionSprite.setPosition(sprite.getX() + sprite.getWidth()/2, sprite.getY() - explosionSprite.getHeight()/2);
			}
			//explosionSprite.setScale(0.95f);
			
			explosionSprite.animate(50, false, new IAnimationListener() {
				@Override
				public void onAnimationStarted(AnimatedSprite pAnimatedSprite, int pInitialLoopCount) {
					if(explosionCount == 0) { //Register the sound effects only for the first explosion
						if(sfx_on && !explosionSFX.isPlaying()) explosionSFX.play();
						if(sfx_on && ufoHoverSFX.isPlaying()) ufoHoverSFX.pause();
						if(sfx_on && ufoBeamSFX.isPlaying()) ufoBeamSFX.pause();
						if(sfx_on && countdownSFX.isPlaying()) countdownSFX.pause();
					}
				}
	
				@Override
				public void onAnimationFrameChanged(AnimatedSprite pAnimatedSprite, int pOldFrameIndex, int pNewFrameIndex) { }
	
				@Override
				public void onAnimationLoopFinished(AnimatedSprite pAnimatedSprite, int pRemainingLoopCount, int pInitialLoopCount) { }
	
				@Override
				public void onAnimationFinished(AnimatedSprite pAnimatedSprite) {
					pAnimatedSprite.setVisible(false);
					
					if(explosionCount == 0) { //Only for the first explosion
						if(explosionSFX.isPlaying()) explosionSFX.pause();
						if(!controlpads_on) 
							mEngine.unregisterUpdateHandler(updateSpritePosition);
						else 
							mGameScene.unregisterUpdateHandler(mEarthPhysicsWorld);
					}
				}
			});
			explosionSpriteArray.addLast(explosionSprite);
			if(!explosionSprite.hasParent())
				mGameScene.attachChild(explosionSprite);
		}
	}
	
	private void generateSmokeHandler(final AnimatedSprite sprite) {
		TimerHandler generateSmokeHandler = new TimerHandler(0.1f, true, new ITimerCallback() {
			@Override
			public void onTimePassed(final TimerHandler pTimerHandler) {
				if(sprite.isVisible()) {
					final SmokeSprite smoke = SMOKE_POOL.obtainPoolItem();

					float fadeOutSpeed = (0.25f + gameSpeed) > 0.45f ? 0.45f : 0.25f + gameSpeed;					
					AlphaModifier fadeOut = new AlphaModifier(fadeOutSpeed, 0.95f, 0.1f);
					
					if(MilitaryMissileSprite.class.isInstance(sprite)) {
						pTimerHandler.setTimerSeconds(0.05f);
						smoke.setScale(0.3f);
						
						if(((MilitaryMissileSprite)sprite).getFireAngle().equals("FIRE1")) {
							if(!sprite.isFlippedHorizontal()) //military missile flying right
								smoke.setPosition(sprite.getX() - sprite.getWidth() - 8.5f, (sprite.getY() + sprite.getHeight() + 7f) - smoke.getHeight()/2);
							else //military missile flying left
								smoke.setPosition(sprite.getX() + sprite.getWidth() - 14.5f, (sprite.getY() + sprite.getHeight() + 4f) - smoke.getHeight()/2);
						} else {
							if(!sprite.isFlippedHorizontal()) //military missile flying right
								smoke.setPosition(sprite.getX() - sprite.getWidth(), (sprite.getY() + sprite.getHeight() + 7f) - smoke.getHeight()/2);
							else //military missile flying left
								smoke.setPosition(sprite.getX() + sprite.getWidth() - 34, (sprite.getY() + sprite.getHeight() + 4f) - smoke.getHeight()/2);
						}
						
						
					} else if(ScatterMissileTailSprite.class.isInstance(sprite)) {
						pTimerHandler.setTimerSeconds(0.06f);
						smoke.setScale(MathUtils.random(0.4f, 0.5f));
						smoke.setPosition((sprite.getX() + sprite.getWidth()/2) - 22, (sprite.getY() + sprite.getHeight()/2) - smoke.getHeight()/2);
						
					} else if(MissileTailSprite.class.isInstance(sprite)) {
						pTimerHandler.setTimerSeconds(0.04f);
						smoke.setScale(MathUtils.random(0.6f, 0.7f));
						smoke.setPosition((sprite.getX() + sprite.getWidth()/2) - 16, (sprite.getY() + sprite.getHeight()/2) - smoke.getHeight()/2);
						
					}
					smoke.clearEntityModifiers();
					smoke.clearUpdateHandlers();
					smoke.registerUpdateHandler(new TimerHandler(fadeOut.getDuration()+0.01f, false, new ITimerCallback() {
						@Override
						public void onTimePassed(final TimerHandler pTimerHandler) {
							//smoke.setVisible(false);
							recycleSmokeSprite(smoke);
						}
					}));
					smoke.registerEntityModifier(fadeOut);
					smokeSpriteArray.addLast(smoke);
					if(!smoke.hasParent())
						mGameScene.attachChild(smoke);
				}
			}
		});
		sprite.clearUpdateHandlers();
		sprite.registerUpdateHandler(generateSmokeHandler);
	}	
	
	private void updateGoldCounter(int gold) {
		if(MegaBeam_on || HumanMagnet_on || Conversion_on)
			goldObtained += (gold * (MathUtils.random(2, 3)));
		else
			goldObtained += gold;
		
		if(goldObtained < 10) {
			goldObtainedText.setText("000" + goldObtained);
		}
		else if(goldObtained >= 10 && goldObtained < 100) {
			goldObtainedText.setText("00" + goldObtained);
		}
		else if(goldObtained >= 100 && goldObtained < 1000) {
			goldObtainedText.setText("0" + goldObtained);
		}
		else if(goldObtained >= 1000 && goldObtained < 9999) {
			goldObtainedText.setText("" + goldObtained);
		}
	}

	// ======================================================================================================
	// ======================================================================================================
	//  RECYCLING
	// ======================================================================================================
	// ======================================================================================================

	private void recycleHuman(final HumanSprite human) {
		mEngine.runOnUpdateThread(new Runnable() {
			@Override
			public void run() {
				humanSpriteArray.remove(human);
				HUMAN_POOL.recyclePoolItem(human);
			}
		});
	}

	private void recycleMilitaryHuman(final MilitaryHumanSprite militaryHuman) {
		mEngine.runOnUpdateThread(new Runnable() {
			@Override
			public void run() {
				militaryHumanSpriteArray.remove(militaryHuman);
				MILITARY_POOL.recyclePoolItem(militaryHuman);
			}
		});
	}

	private void recycleDisintegratedHuman(final DisintegrateHumanSprite disintegrateHuman) {
		mEngine.runOnUpdateThread(new Runnable() {
			@Override
			public void run() {
				DisintegratedHumanSpriteArray.remove(disintegrateHuman);
				DISINTEGRATED_HUMAN_POOL.recyclePoolItem(disintegrateHuman);
			}
		});
	}

	private void recycleLamp(final LampSprite obstacle) {
		mEngine.runOnUpdateThread(new Runnable() {
			@Override
			public void run() {
				lampSpriteArray.remove(obstacle);
				LAMP_POOL.recyclePoolItem(obstacle);
			}
		});
	}

	private void recycleCar(final CarSprite obstacle) {
		mEngine.runOnUpdateThread(new Runnable() {
			@Override
			public void run() {
				carSpriteArray.remove(obstacle);
				CAR_POOL.recyclePoolItem(obstacle);
			}
		});
	}

	private void recycleTraffic(final TrafficSprite obstacle) {
		mEngine.runOnUpdateThread(new Runnable() {
			@Override
			public void run() {
				trafficSpriteArray.remove(obstacle);
				TRAFFIC_POOL.recyclePoolItem(obstacle);
			}
		});
	}

	private void recycleMissileAlert(final WeaponAlertSprite missileAlert) {
		mEngine.runOnUpdateThread(new Runnable() {
			@Override
			public void run() {
				missileAlertSpriteArray.remove(missileAlert);
				MISSILE_ALERT_POOL.recyclePoolItem(missileAlert);
			}
		});
	}
	
	private void recycleMissile(final MissileHeadSprite missileHead, final MissileTailSprite missileTail) {
		mEngine.runOnUpdateThread(new Runnable() {
			@Override
			public void run() {
				missileHeadSpriteArray.remove(missileHead);
				missileTailSpriteArray.remove(missileTail);
				MISSILE_HEAD_POOL.recyclePoolItem(missileHead);
				MISSILE_TAIL_POOL.recyclePoolItem(missileTail);
			}
		});
	}

	private void recycleScatterMissileAlert(final WeaponAlertSprite scatterMissileAlert) {
		mEngine.runOnUpdateThread(new Runnable() {
			@Override
			public void run() {
				scatterMissileAlertSpriteArray.remove(scatterMissileAlert);
				SCATTER_MISSILE_ALERT_POOL.recyclePoolItem(scatterMissileAlert);
			}
		});
	}
	
	private void recycleScatterMissile(final ScatterMissileHeadSprite missileHead, final ScatterMissileTailSprite missileTail) {
		mEngine.runOnUpdateThread(new Runnable() {
			@Override
			public void run() {
				scatterMissileHeadSpriteArray.remove(missileHead);
				scatterMissileTailSpriteArray.remove(missileTail);
				SCATTER_MISSILE_HEAD_POOL.recyclePoolItem(missileHead);
				SCATTER_MISSILE_TAIL_POOL.recyclePoolItem(missileTail);
			}
		});
	}

	private void recycleMilitaryMissileAlert(final MilitaryMissileAlertSprite militaryMissileAlert) {
		mEngine.runOnUpdateThread(new Runnable() {
			@Override
			public void run() {
				militaryMissileAlertSpriteArray.remove(militaryMissileAlert);
				MILITARY_MISSILE_ALERT_POOL.recyclePoolItem(militaryMissileAlert);
			}
		});
	}
	
	private void recycleMilitaryMissile(final MilitaryMissileSprite militaryMissile) {
		mEngine.runOnUpdateThread(new Runnable() {
			@Override
			public void run() {
				militaryMissileSpriteArray.remove(militaryMissile);
				MILITARY_MISSILE_POOL.recyclePoolItem(militaryMissile);
			}
		});
	}

	private void recycleLaserBeamAlert(final WeaponAlertSprite laserBeamAlert) {
		mEngine.runOnUpdateThread(new Runnable() {
			@Override
			public void run() {
				laserBeamAlertSpriteArray.remove(laserBeamAlert);
				LASER_BEAM_ALERT_POOL.recyclePoolItem(laserBeamAlert);
			}
		});
	}
	
	private void recycleLaserBeam(final LaserBeamSprite laserBeam) {
		mEngine.runOnUpdateThread(new Runnable() {
			@Override
			public void run() {
				laserBeamSpriteArray.remove(laserBeam);
				LASER_BEAM_POOL.recyclePoolItem(laserBeam);
			}
		});
	}

	private void recycleSmokeSprite(final SmokeSprite smokeSprite) {
		mEngine.runOnUpdateThread(new Runnable() {
			@Override
			public void run() {
				smokeSprite.clearUpdateHandlers();
				smokeSpriteArray.remove(smokeSprite);
				SMOKE_POOL.recyclePoolItem(smokeSprite);
			}
		});
	}

	private void recycleGold(final GoldSprite goldSprite) {
		mEngine.runOnUpdateThread(new Runnable() {
			@Override
			public void run() {
				GOLD_POOL.recyclePoolItem(goldSprite);
			}
		});
	} 

	private void recycleExplosionSprite(final ExplosionSprite explosionSprite) {
		mEngine.runOnUpdateThread(new Runnable() {
			@Override
			public void run() {
				explosionSpriteArray.remove(explosionSprite);
				EXPLOSION_POOL.recyclePoolItem(explosionSprite);
			}
		});
	}
	// ======================================================================================================
	// ======================================================================================================
	//  Load other scenes (Pause, GameOver, etc)
	// ======================================================================================================
	// ======================================================================================================
	private void loadMagnetScene(final Text gadgetTimeCounter, AnimatedSprite gadgetIcon) {
		mMagnetScene = new CameraScene(mCamera);
		mMagnetScene.setOnSceneTouchListener(this);
		mMagnetScene.setTouchAreaBindingOnActionDownEnabled(true);
		
		mMagnetPosX = 0;
		mMagnetPosY = 0;
		
		
		mMagnetScene.attachChild(new Sprite(0, 0, mGameResources.getmWashoutBackgroundTextureRegion(), mVertexBufferObjectManager));
		

		
		
		final float humanMagnetTimer = 15f; //15 seconds
		leftOverTimerList.put("HumanMagnetTimer", humanMagnetTimer);
		
		gadgetTimeCounter.setText("" + Math.round(humanMagnetTimer));
		gadgetTimeCounter.setX(gadgetIcon.getX() + gadgetIcon.getWidth() / 3);
		gadgetTimeCounter.setY(gadgetIcon.getY() + gadgetIcon.getHeight() / 2);
		gadgetTimeCounter.setVisible(true);
		
		mGameScene.registerUpdateHandler(new TimerHandler(1f, true, new ITimerCallback() {
			@Override
			public void onTimePassed(final TimerHandler pTimerHandler) {
				float leftoverTimer = leftOverTimerList.get("HumanMagnetTimer");
				
				//if extend timer on, ignore the check for this timer
				if(!ExtendTimer_on) {
					if(leftoverTimer <= 0) {
						HumanMagnet_on = false;
						gadgetTimeCounter.setVisible(false);
						leftOverTimerList.put("HumanMagnetTimer", humanMagnetTimer);
						mGameScene.unregisterUpdateHandler(pTimerHandler);
					} else {
						leftOverTimerList.put("HumanMagnetTimer", leftoverTimer -= 1f);
						gadgetTimeCounter.setText("" + Math.round(leftoverTimer));
					}
				}
			}
		}));
		
		mMagnetScene.setBackgroundEnabled(false);
	}

	private void loadPauseScene() {
		//Options Shared Preference
		options = new Options_SharedPreference(mGameResources.getmBaseContext());
		music_on = options.getMusic();
		sfx_on = options.getSoundEffects();
		controlpads_on = options.getControlPads();
		
		mPauseScene = new CameraScene(mCamera);
		mPauseScene.setOnSceneTouchListener(this);
		mPauseScene.setTouchAreaBindingOnActionDownEnabled(true);
		
		float centerX = (CAMERA_WIDTH - mGameResources.getmCalibrationCustomScreenTextureRegion().getWidth()) / 2;
		float centerY = (CAMERA_HEIGHT - mGameResources.getmCalibrationCustomScreenTextureRegion().getHeight()) / 2;
		
		Sprite background = new Sprite(centerX, centerY, mGameResources.getmCalibrationCustomScreenTextureRegion(), mVertexBufferObjectManager);
		background.setScale(1f);
		
		float posX = CAMERA_WIDTH/2 - mGameResources.getmGameplayPauseMenuButtonTextureRegion().getWidth()/2;
		float posY = 200;
		AnimatedSprite resumeButtonSprite = new AnimatedSprite(posX, posY, mGameResources.getmGameplayPauseMenuButtonTextureRegion(), mVertexBufferObjectManager) {
			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent,float pTouchAreaLocalX, float pTouchAreaLocalY) {
				if(pSceneTouchEvent.getAction()==MotionEvent.ACTION_UP) {
					this.setScale(0.55f);
					resumeGame();
					sceneManager.setCurrentScene("GameScene");
				}
				if(pSceneTouchEvent.isActionDown()) {
					this.setScale(0.45f);
				}
				return true;
			}
		};
		resumeButtonSprite.setCurrentTileIndex(0);
		resumeButtonSprite.setScale(0.55f);
		
		/*
		AnimatedSprite restartButtonSprite = new AnimatedSprite(posX, posY+=75, mGameResources.getmGameplayPauseMenuButtonTextureRegion(), mVertexBufferObjectManager) {
			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent,float pTouchAreaLocalX, float pTouchAreaLocalY) {
				if(pSceneTouchEvent.getAction()==MotionEvent.ACTION_UP) {
					this.setScale(0.55f);
					sceneManager.loadGameScene(POSITION_X_ANGLE, POSITION_Y_ANGLE, false);
				}
				if(pSceneTouchEvent.isActionDown()) {
					this.setScale(0.45f);
				}
				return true;
			}
		};
		restartButtonSprite.setCurrentTileIndex(1);
		restartButtonSprite.setScale(0.55f);
		*/
		
		AnimatedSprite MainMenuButtonSprite = new AnimatedSprite(posX, posY+=75, mGameResources.getmGameplayPauseMenuButtonTextureRegion(), mVertexBufferObjectManager) {
			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent,float pTouchAreaLocalX, float pTouchAreaLocalY) {
				if(pSceneTouchEvent.getAction()==MotionEvent.ACTION_UP) {
					this.setScale(0.55f);
					sceneManager.loadMainMenuScene();
					mMainActivity.leaveRoom();
				}
				if(pSceneTouchEvent.isActionDown()) {
					this.setScale(0.45f);
				}
				return true;
			}
		};
		MainMenuButtonSprite.setCurrentTileIndex(2);
		MainMenuButtonSprite.setScale(0.55f);
		
		
		/* Add 2 / 3 music & SFX icon */
		mMusicSprite = new TiledSprite((CAMERA_WIDTH/2 - mGameResources.getmMusicTextureRegion().getWidth()), 400, mGameResources.getmMusicTextureRegion(), mVertexBufferObjectManager) {
			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent,float pTouchAreaLocalX, float pTouchAreaLocalY) {
				if(pSceneTouchEvent.getAction()==MotionEvent.ACTION_UP) {
					this.setScale(0.5f);
					if(mMusicSprite.getCurrentTileIndex() == 1) {
						mMusicSprite.setCurrentTileIndex(0);
						music_on = true;
						options.saveMusic(music_on);
					} else {
						mMusicSprite.setCurrentTileIndex(1);
						music_on = false;
						options.saveMusic(music_on);
					}
				}
				if(pSceneTouchEvent.isActionDown()) {
					this.setScale(0.4f);
				}
				return true;
			}
		};
		mMusicSprite.setScale(0.5f);
		if(music_on) mMusicSprite.setCurrentTileIndex(0);
		else mMusicSprite.setCurrentTileIndex(1);
		
		mSFXSprite = new TiledSprite(CAMERA_WIDTH/2, 400, mGameResources.getmSFXTextureRegion(), mVertexBufferObjectManager) {
			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent,float pTouchAreaLocalX, float pTouchAreaLocalY) {
				if(pSceneTouchEvent.getAction()==MotionEvent.ACTION_UP) {
					this.setScale(0.5f);
					if(mSFXSprite.getCurrentTileIndex() == 1) {
						mSFXSprite.setCurrentTileIndex(0);
						sfx_on = true;
						options.saveSFX(sfx_on);
					} else {
						mSFXSprite.setCurrentTileIndex(1);
						sfx_on = false;
						options.saveSFX(sfx_on);
					}
				}
				if(pSceneTouchEvent.isActionDown()) {
					this.setScale(0.4f);
				}
				return true;
			}
		};
		mSFXSprite.setScale(0.5f);
		if(sfx_on) mSFXSprite.setCurrentTileIndex(0);
		else mSFXSprite.setCurrentTileIndex(1);
		
		mPauseScene.attachChild(new Sprite(0, 0, mGameResources.getmWashoutBackgroundTextureRegion(), mVertexBufferObjectManager));
		mPauseScene.attachChild(background);
		mPauseScene.attachChild(mMusicSprite);
		mPauseScene.attachChild(mSFXSprite);
		
		mPauseScene.registerTouchArea(mMusicSprite);
		mPauseScene.registerTouchArea(mSFXSprite);

		mPauseScene.attachChild(resumeButtonSprite);
		//mPauseScene.attachChild(restartButtonSprite);
		mPauseScene.attachChild(MainMenuButtonSprite);

		mPauseScene.registerTouchArea(resumeButtonSprite);
		//mPauseScene.registerTouchArea(restartButtonSprite);
		mPauseScene.registerTouchArea(MainMenuButtonSprite);
		
		mPauseScene.setBackgroundEnabled(false);
	}

	private void loadGameOverScene() {
		mGameOverScene = new CameraScene(mCamera);
		mGameOverScene.setOnSceneTouchListener(this);
		mGameOverScene.setTouchAreaBindingOnActionDownEnabled(true);
		float posY = 55;
		posY += 237;

		mNewHighScoreSprite = new Sprite(915, 36, mGameResources.getmNewHighScoreTextTextureRegion(), mVertexBufferObjectManager);
		
		Sprite GameOverSprite = new Sprite(210, 277, mGameResources.getmGameOverTextureRegion(), mVertexBufferObjectManager);
		
		/*
		AnimatedSprite restartButtonSprite = new AnimatedSprite((CAMERA_WIDTH - mGameResources.getmGameplayPauseMenuButtonTextureRegion().getWidth())/2 + 130, posY += 237, mGameResources.getmGameplayPauseMenuButtonTextureRegion(), mVertexBufferObjectManager) {
			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent,float pTouchAreaLocalX, float pTouchAreaLocalY) {
				if(pSceneTouchEvent.getAction()==MotionEvent.ACTION_UP) {
					//sceneManager.loadGameScene(POSITION_X_ANGLE, POSITION_Y_ANGLE, true);
				}
				return true;
			}
		};
		restartButtonSprite.setCurrentTileIndex(1);
		restartButtonSprite.setScale(0.7f);
		*/
		
		AnimatedSprite mainMenuButtonSprite = new AnimatedSprite((CAMERA_WIDTH - mGameResources.getmGameplayPauseMenuButtonTextureRegion().getWidth())/2 + 130, posY += 85, mGameResources.getmGameplayPauseMenuButtonTextureRegion(), mVertexBufferObjectManager) {
			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent,float pTouchAreaLocalX, float pTouchAreaLocalY) {
				if(pSceneTouchEvent.getAction()==MotionEvent.ACTION_UP) {
					mMainActivity.leaveRoom();
					sceneManager.loadMainMenuScene();
				}
				return true;
			}
		};
		mainMenuButtonSprite.setCurrentTileIndex(2);
		mainMenuButtonSprite.setScale(0.7f);

		gameOverDistanceText = new Text(CAMERA_WIDTH/2, 160, mGameResources.getmFont(), "00000", 5, new TextOptions(HorizontalAlign.RIGHT), mVertexBufferObjectManager);
		gameOverDistanceText.setScale(5.65f);
		mGameOverScene.attachChild(gameOverDistanceText);
		
		gameOverDistanceMeasurementText = new Text(100, 198, mGameResources.getmFont(), "m", 1, new TextOptions(HorizontalAlign.RIGHT), mVertexBufferObjectManager);
		gameOverDistanceMeasurementText.setScale(3f);
		mGameOverScene.attachChild(gameOverDistanceMeasurementText);
		
		mGameOverScene.attachChild(new Sprite(0, 0, mGameResources.getmWashoutBackgroundTextureRegion(), mVertexBufferObjectManager));
		mGameOverScene.attachChild(GameOverSprite);
		//mGameOverScene.attachChild(restartButtonSprite);
		mGameOverScene.attachChild(mainMenuButtonSprite);

		//mGameOverScene.registerTouchArea(restartButtonSprite);
		mGameOverScene.registerTouchArea(mainMenuButtonSprite);

		mGameOverScene.setBackgroundEnabled(false);
	}

	private void gameOverDetector() {
		mGameScene.registerUpdateHandler(new IUpdateHandler() {
			@Override
			public void reset() {  }

			@Override
			public void onUpdate(float arg0) {
				if(mGameOver) {
					//myTracker.setScreenName("Game Screen");
					myTracker.send(new HitBuilders.EventBuilder()
			        .setAction("Game Over")
			        .set("User", "User " + mGameResources.getGoogleAccountName() + "(" + mGameResources.getGoogleAccountID() + ")")
			        .set("Distance", ""+distanceTravelled)
			        .set("Gold Earn", ""+goldObtained)
			        .set("Absorbed Humans", ""+humansAbsorbed)
			        .set("Disintegrated Humans", ""+humansDisintegrated)
			        .build());
			        
					 
					//Update Score SharedPreference
					highScores.saveGamesCompleted();
					
					gameOverDistanceText.setText(distanceTravelled + "");
					gameOverDistanceMeasurementText.setX((gameOverDistanceText.getScaleX() * gameOverDistanceText.getWidth()) / 1.3f + gameOverDistanceText.getX());
					
					//Save the new max high scores
					if(highScores.loadMaxDistancePerCountry() < distanceTravelled) {
						highScores.saveMaxDistancePerCountry(distanceTravelled);
						mGameOverScene.attachChild(mNewHighScoreSprite);
						//NewHighScoreSprite.setX((gameOverDistanceText.getScaleX() * gameOverDistanceText.getWidth()) + gameOverDistanceText.getX() + 30);
					}
					if(highScores.loadMaxGoldPerCountry() < goldObtained) {
						highScores.saveMaxGoldPerCountry(goldObtained);
					}
					if(highScores.loadMaxAbsorbedHumansPerCountry() < humansAbsorbed) {
						highScores.saveMaxAbsorbedHumansPerCountry(humansAbsorbed);
					}
					if(highScores.loadMaxDisintegratedHumansPerCountry() < humansDisintegrated) {
						highScores.saveMaxDisintegratedHumansPerCountry(humansDisintegrated);
					}
					
					//Save overall scores
					highScores.saveAbsorbedHumansPerCountry(humansAbsorbed);
					highScores.saveDisintegratedHumansPerCountry(humansDisintegrated);
					highScores.saveTotalObtainedGold(goldObtained);
					highScores.saveCombinedDistance(distanceTravelled);
					
					//Check whether all music is stop
					if(ufoHoverSFX.isPlaying()) ufoHoverSFX.pause();
					if(ufoBeamSFX.isPlaying()) ufoBeamSFX.pause();
					if(countdownSFX.isPlaying()) countdownSFX.pause();
					if(laserCountdownSFX.isPlaying()) laserCountdownSFX.pause();
					if(explosionSFX.isPlaying()) explosionSFX.pause();
					if(gameBGM.isPlaying()) gameBGM.pause();
					if(laserBeamSFX.isPlaying()) laserBeamSFX.pause();
					if(missileLaunchSFX.isPlaying()) missileLaunchSFX.pause();
					if(shieldHitSFX.isPlaying()) shieldHitSFX.pause();
					
					//Unregister all handlers if gameover
					mGameScene.unregisterUpdateHandler(displayMissileAlertHandler);
					mGameScene.unregisterUpdateHandler(updateDistanceHandler);
					mGameScene.unregisterUpdateHandler(updateHumansHandler);
					mGameScene.unregisterUpdateHandler(updateSpeedHandler);
					
					mGameScene.setChildScene(mGameOverScene, false, true, true);
					
					sceneManager.setCurrentScene("GameOverScene");

					mMainActivity.showAdvertisement();
					
			    	if(mGameResources.getmGameHelper().isSignedIn()) {
			    		// Submit new score to Google Leaderboards
			    		
			    		Score_SharedPreference singaporeHighScores = new Score_SharedPreference(mGameResources.getmBaseContext(), "Singapore");
			    		Score_SharedPreference sydneyHighScores = new Score_SharedPreference(mGameResources.getmBaseContext(), "Sydney");
			    		
			    		Games.Leaderboards.submitScore(mGameResources.getmGameHelper().getApiClient(), mGameResources.mainLeaderboardID, distanceTravelled);
			    		Games.Leaderboards.submitScore(mGameResources.getmGameHelper().getApiClient(), mGameResources.humansAbsorbedLeaderboardID, singaporeHighScores.loadAbsorbedHumansPerCountry() + sydneyHighScores.loadAbsorbedHumansPerCountry());
			    		Games.Leaderboards.submitScore(mGameResources.getmGameHelper().getApiClient(), mGameResources.humansDisintegratedLeaderboardID, singaporeHighScores.loadDisintegratedHumansPerCountry() + sydneyHighScores.loadDisintegratedHumansPerCountry());
			    		
			    		// Unlock Achievement
			    		mGameResources.getmAchievement().unlockGameAchievement();
			    		
			    		// Save data to Google
			    		//if(MathUtils.random(1, 3) == 1)
			    		mMainActivity.saveSnapshot();
			    	}
                }
         	}
        });
    }
	
	// ======================================================================================================
	// ======================================================================================================
	//  MULTIPLAYER STUFF HERE
	// ======================================================================================================
	// ======================================================================================================
	
	public void onRealTimeMessageReceived(RealTimeMessage message) {
		byte[] buf = message.getMessageData();
		String sender = message.getSenderParticipantId();
		Log.d(mGameResources.TAG, "Player 1 receive message : " + Arrays.toString(buf));

		if ((char)buf[0] == 'S') {
			// Create character and add it to the scene
			player2Sprite = new PixelPerfectAnimatedSprite(0, 0, player2Spacecraft.loadSpacecraftTexture(buf[1]), mVertexBufferObjectManager);
			
			player2Loaded = true;
			mGameScene.attachChild(player2Sprite);
		}
		if ((char)buf[0] == 'P') {
			if ((char)buf[1] == '2') {
				Log.d(mGameResources.TAG, "Player " + (char)buf[1] + " position received: " + (float)buf[2] + "/" + (float)buf[3]);
				player2Sprite.setPosition((float)buf[2], (float)buf[3]);
			}
		}
	}
	
	public void updateOtherPlayerPosition() {
		mGameScene.registerUpdateHandler(new TimerHandler(0.1f, true, new ITimerCallback() {
			@Override
			public void onTimePassed(final TimerHandler pTimerHandler) {
				byte[] message = new byte[] {(byte) 'P', (byte) '2', (byte) player2Sprite.getX(), (byte) player2Sprite.getY()};
				mMainActivity.sendUnreliableMessageRealTime(message);
			}
		}));
	}
}
