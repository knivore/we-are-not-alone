/*
 * Copyright (C) 2013 Knivore Studios
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.andengine.classes;

import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.adt.pool.GenericPool;
import org.andengine.util.math.MathUtils;

import com.andengine.invaders.GameResources;

/**
 * @author Keh Chin Leong
 */
public class GoldPool extends GenericPool<GoldSprite> {
	
	private GameResources gameResource;
	private VertexBufferObjectManager vbom;

	public GoldPool(VertexBufferObjectManager vertexBufferObjM, GameResources gr) {
		vbom = vertexBufferObjM;
		gameResource = gr;
	}

	/**
	 * Called when a Missile Head is required but there isn't one in the pool
	 */
	@Override
	protected GoldSprite onAllocatePoolItem() {
		//create missiles at the right end of the screen
		GoldSprite gold = new GoldSprite(0, 0, gameResource.getmGoldTextureRegion(), vbom);
		gold.setCurrentTileIndex(1);
		//gold.setCurrentTileIndex(MathUtils.random(0, 2));
		return gold;
	}

	/**
	 * Called when a Missile Head is sent to the pool
	 */
	@Override
	protected void onHandleRecycleItem(final GoldSprite goldSprite) {
		goldSprite.setVisible(false);
	}

	/**
	 * Called just before a Missile Head is returned to the caller, this is where you
	 * write your initialize code i.e. set location, rotation, etc.
	 */
	@Override
	protected void onHandleObtainItem(final GoldSprite goldSprite) {
		goldSprite.reset();
	}
}
