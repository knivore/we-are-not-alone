/*
 * Copyright (C) 2013 Knivore Studios
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.andengine.classes;

import org.andengine.engine.handler.physics.PhysicsHandler;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.math.MathUtils;

import com.makersf.andengine.extension.collisions.entity.sprite.PixelPerfectAnimatedSprite;
import com.makersf.andengine.extension.collisions.opengl.texture.region.PixelPerfectTiledTextureRegion;

/**
 * @author Keh Chin Leong
 */
public class MilitaryHumanSprite extends PixelPerfectAnimatedSprite {

	// ===========================================================
	// Constants
	// ===========================================================
	private static final int CAMERA_WIDTH = 1280, CAMERA_HEIGHT = 720;
	private boolean actionRegistered = false, isHalt = false;
	private long spriteAnimationSpeed;
	private float spriteMovingSpeed;
	private String militaryStatus, aimAngle;
	private PhysicsHandler mPhysicsHandler;
	private Map mMap;
	
	public MilitaryHumanSprite(float pX, float pY, PixelPerfectTiledTextureRegion pTiledTextureRegion, VertexBufferObjectManager vbom, Map map) {
		super(pX, pY, pTiledTextureRegion, vbom);
		mPhysicsHandler = new PhysicsHandler(this);
		mMap = map;
	}

	public void reset() {
		//if(MathUtils.random(0, 1) == 0) {
			setPosition(CAMERA_WIDTH + getWidth(), CAMERA_HEIGHT - getHeight() - MathUtils.random(25, 48));
			setFacingDirection(true);
		//} else {
		//	setPosition(0 - getWidth(), CAMERA_HEIGHT - getHeight() - MathUtils.random(25, 48));
		//	setFacingDirection(false);
		//}
		registerUpdateHandler(mPhysicsHandler);
		setWalkAnimationSpeed();
		setVisible(true);
		setAlpha(1);
		isHalt = false;
	}
	
	@Override
	protected void onManagedUpdate(float pSecondsElapsed) {
		//if(this.isVisible()) {
			if(!isHalt) {
				if(getMilitaryStatus().contains("AIM")) {
					mPhysicsHandler.setVelocityX(0);
					this.setX(this.getX() - ((mMap.getParallaxLayer().calculateSpeedInPixel % mMap.getLayer1().getShapeWidthScaled()) * 10f));
				} else {
					if(!getFacingDirection())
						moveLeftToRight((mMap.getParallaxLayer().calculateSpeedInPixel % mMap.getLayer1().getShapeWidthScaled()) * 3f);
					else
						moveRightToLeft((mMap.getParallaxLayer().calculateSpeedInPixel % mMap.getLayer1().getShapeWidthScaled()) * 3f);
				}
			} else {
				mPhysicsHandler.setVelocityX(0);
				if(!getMilitaryStatus().contains("AIM")) {
					if(!getFacingDirection())
						moveLeftToRight(MathUtils.random(110f, 135f));
					else
						moveRightToLeft(MathUtils.random(110f, 135f));
				}
			}
			super.onManagedUpdate(pSecondsElapsed);
		//}
	}

	//-----------------------------OTHER METHODS-----------------------------//
	//-----------------------------------------------------------------------//
	public float getHumanMovingSpeed() {
		return spriteMovingSpeed;
	}

	public void setHumanMovingSpeed(float spriteMovingSpeed) {
		this.spriteMovingSpeed = spriteMovingSpeed;
	}

	public boolean getFacingDirection() {
		//FALSE = face >> (RIGHT) || TRUE = face << (LEFT)
		return isFlippedHorizontal();
	}

	public void setFacingDirection(boolean d) {
		//FALSE = face >> (RIGHT) || TRUE = face << (LEFT)
		setFlippedHorizontal(d);
	}
	
	public void switchDirection() {
		if(getFacingDirection() == false) {
			setFlippedHorizontal(true);
		} else {
			setFlippedHorizontal(false);
		}
	}
	
	//Absorb is tile index of 8 - 10
	public void absorb() {
		animate(new long[] {100,100,100}, 8, 10, false);
		setMilitaryStatus("ABSORBED");
	}
	
	public void aim1(PixelPerfectAnimatedSprite playerSprite) { //Aim is tile index of 4 - 5/6
		setMilitaryStatus("AIM");
		setAimAngle("AIM1");
		animate(new long[] {100,100}, new int[] {4,5}, false);
	}
	
	public void aim2(PixelPerfectAnimatedSprite playerSprite) { //Aim is tile index of 4 - /6
		setMilitaryStatus("AIM");
		setAimAngle("AIM2");
		animate(new long[] {100,100}, new int[] {4,6}, false);
	}
	
	public void blown() { //Blowned is tile index of 12 - 14
		setMilitaryStatus("BLOWN");
		animate(new long[] {100,100}, new int[] {12,14}, false);
	}

	public boolean isActionRegistered() {
		return actionRegistered;
	}

	public void setActionRegistered(boolean action) {
		actionRegistered = action;
	}
	
	public long getAnimationSpeed() {
		return spriteAnimationSpeed;
	}
	
	public void setAnimationSpeed(long speed) {
		this.animate(new long[] {speed,speed,speed,speed}, 0, 3, true);
	}
	
	public String getMilitaryStatus() {
		return militaryStatus;
	}

	public void setMilitaryStatus(String status) {
		militaryStatus = status;
	}

	public String getAimAngle() {
		return aimAngle;
	}

	public void setAimAngle(String aimAngle) {
		this.aimAngle = aimAngle;
	}

	public void setWalkAnimationSpeed() {
		setActionRegistered(false);
		long speed = (long) MathUtils.random(100, 120);
		animate(new long[] {speed,speed,speed,speed}, 0, 3, true);
		militaryStatus = "WALK";
	}
	
	public void moveRightToLeft(float speed) {
		if(!isHalt) {
			if(militaryStatus.equals("WALK")) {
				spriteMovingSpeed = -1 * (MathUtils.random(180, 200) + speed);
			} else {
				spriteMovingSpeed = -1 * (MathUtils.random(200, 220) + speed);
			}
			mPhysicsHandler.setVelocityX(spriteMovingSpeed);
		} else {
			mPhysicsHandler.setVelocityX(-speed);
		}
	}
	
	public void moveLeftToRight(float speed) {
		if(!isHalt) {
			if(militaryStatus.equals("WALK")) {
				spriteMovingSpeed = 20f + speed;
				mPhysicsHandler.setVelocityX(spriteMovingSpeed);
			} else {
				spriteMovingSpeed = 30f + speed;
				mPhysicsHandler.setVelocityX(spriteMovingSpeed);
			}
		} else {
			mPhysicsHandler.setVelocityX(speed);
		}
	}

	public void setRunAnimationSpeed() {
		setActionRegistered(false);
		long speed = (long) MathUtils.random(75, 100);		
		animate(new long[] {speed,speed,speed,speed}, 0, 3, true);
		militaryStatus = "RUN";
	}
	
	public void haltMilitary() {
		isHalt = true;
	}
	
	public void haltRecover() {
		isHalt = false;
	}

	public boolean offScreen(int cameraWidth, int cameraHeight) {
		if ((getX() > cameraWidth + getWidth() + 100) || (getX() < -(200 + getWidth()))) {
			return true;
		} else {
			return false;			
		}
	}
}
