/*
 * Copyright (C) 2013 Knivore Studios
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.andengine.classes;

import org.andengine.opengl.texture.region.TiledTextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

/**
 * @author Keh Chin Leong
 */
public class MilitaryMissileAlertSprite extends WeaponAlertSprite {

	// ===========================================================
	// Constants
	// ===========================================================
	private MilitaryHumanSprite mhs;
	
	public MilitaryMissileAlertSprite(float pX, float pY, TiledTextureRegion tiledTextureRegion, VertexBufferObjectManager vbom) {
		super(pX, pY, tiledTextureRegion, vbom);
	}

	public void reset() {
    	setAssignedMilitaryHuman(null);
		setCurrentTileIndex(0);
		setScale(0.8f);
		setVisible(true);
	}

	@Override
	protected void onManagedUpdate(float pSecondsElapsed) {
		if(isVisible() && mhs != null && mhs.isVisible()) {
	    	if(mhs.getFacingDirection()) {
	    		setPosition(mhs.getX() + 3, mhs.getY());
	    	} else {
	    		setPosition(mhs.getX() - 20, mhs.getY());
	    	}
	    	if(!mhs.getMilitaryStatus().equals("AIM") && !mhs.getMilitaryStatus().equals("AIMING")) {
	    		setVisible(false);
	    		//stopAnimation();
			}
		}
    	super.onManagedUpdate(pSecondsElapsed);
	}

	public MilitaryHumanSprite getAssignedMilitaryHuman() {
		return mhs;
	}
	
	public void setAssignedMilitaryHuman(MilitaryHumanSprite mhs) {
		this.mhs = mhs;
	}
}
