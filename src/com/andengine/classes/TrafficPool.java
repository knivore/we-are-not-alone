/*
 * Copyright (C) 2013 Knivore Studios
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.andengine.classes;

import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.adt.pool.GenericPool;

import com.andengine.invaders.GameResources;

/**
 * @author Keh Chin Leong
 */
public class TrafficPool extends GenericPool<TrafficSprite> {
	
	private Map currentMap;
	private GameResources gr;
	private VertexBufferObjectManager vbom;
	private float cameraHeight;
	private float cameraWidth;

	public TrafficPool(float CAMERA_HEIGHT, float CAMERA_WIDTH, VertexBufferObjectManager vertexBufferObjM, GameResources gameResources, Map map) {
		vbom = vertexBufferObjM;
		cameraHeight = CAMERA_HEIGHT;
		cameraWidth = CAMERA_WIDTH;
		gr = gameResources;
		currentMap = map;
	}
	
	/**
	 * Called when a Traffic Light is required but there isn't one in the pool
	 */
	@Override
	protected TrafficSprite onAllocatePoolItem() {
		TrafficSprite sprite = new TrafficSprite(cameraWidth, cameraHeight - gr.getmLampPostTextureRegion().getHeight()  - 67, gr.getmTrafficLightTextureRegion(), vbom, currentMap);
		return sprite;
	}

	/**
	 * Called when a Traffic Light is sent to the pool
	 */
	@Override
	protected void onHandleRecycleItem(final TrafficSprite traffic) {
		traffic.clearEntityModifiers();
		traffic.setVisible(false);
	}

	/**
	 * Called just before a Traffic Light is returned to the caller, this is where you
	 * write your initialize code i.e. set location, rotation, etc.
	 */
	@Override
	protected void onHandleObtainItem(final TrafficSprite traffic) {
		traffic.reset();
	}
	
	public void setMap(Map map) {
		currentMap = map;
	}
}
