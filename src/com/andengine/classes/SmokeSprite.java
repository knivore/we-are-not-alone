/*
 * Copyright (C) 2013 Knivore Studios
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.andengine.classes;

import org.andengine.entity.sprite.AnimatedSprite;
import org.andengine.opengl.texture.region.TiledTextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.math.MathUtils;

/**
 * @author Keh Chin Leong
 */
public class SmokeSprite extends AnimatedSprite {

	// ===========================================================
	// Constants
	// ===========================================================
	
	public SmokeSprite(float pX, float pY, TiledTextureRegion pTiledTextureRegion, VertexBufferObjectManager vbom) {
		super(pX, pY, pTiledTextureRegion, vbom);
	}

	public void reset() {
		setCurrentTileIndex(MathUtils.random(0, 1));
		setRotation(MathUtils.random(90f, 270f));
		setAlpha(1.0f);
		setVisible(true);
		clearUpdateHandlers();
		clearEntityModifiers();
	}

	//-----------------------------OTHER METHODS-----------------------------//
	//-----------------------------------------------------------------------//

}
