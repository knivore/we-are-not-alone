/*
 * Copyright (C) 2013 Knivore Studios
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.andengine.classes;

import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.adt.pool.GenericPool;

import com.andengine.invaders.GameResources;

/**
 * @author Keh Chin Leong
 */
public class MilitaryMissileAlertPool extends GenericPool<MilitaryMissileAlertSprite> {
	
	private GameResources gr;
	private VertexBufferObjectManager vbom;

	public MilitaryMissileAlertPool(VertexBufferObjectManager vertexBufferObjM, GameResources gameResources) {
		vbom = vertexBufferObjM;
		gr = gameResources;
	}
	
	/**
	 * Called when a MilitaryMissileAlert is required but there isn't one in the pool
	 */
	@Override
	protected MilitaryMissileAlertSprite onAllocatePoolItem() {
		MilitaryMissileAlertSprite militaryMissileAlert = new MilitaryMissileAlertSprite(0, 0, gr.getmMilitaryAlertTextureRegion().deepCopy(), vbom);
		return militaryMissileAlert;
	}

	/**
	 * Called when a MilitaryMissileAlert is sent to the pool
	 */
	@Override
	protected void onHandleRecycleItem(final MilitaryMissileAlertSprite pMissileAlert) {
		pMissileAlert.setVisible(false);
	}

	/**
	 * Called just before a MilitaryMissileAlert is returned to the caller, this is where you
	 * write your initialize code i.e. set location, rotation, etc.
	 */
	@Override
	protected void onHandleObtainItem(final MilitaryMissileAlertSprite pMissileAlert) {
		pMissileAlert.reset();
	}
}
