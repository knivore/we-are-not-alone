/*
 * Copyright (C) 2013 Knivore Studios
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.andengine.classes;

import java.util.ArrayList;

import org.andengine.engine.camera.Camera;
import org.andengine.entity.shape.IAreaShape;
import org.andengine.entity.sprite.Sprite;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.math.MathUtils;

import com.andengine.classes.ParallaxLayer.ParallaxEntity;
import com.andengine.invaders.GameResources;
//import org.andengine.entity.scene.background.ParallaxBackground.ParallaxEntity;

/**
 * @author Keh Chin Leong
 */
public class Map {
	private ParallaxLayer parallaxLayerBackground;
	//private SequentialParallaxBackground sequentialParallaxBackground;
	private ParallaxEntity Layer1;
	
	protected ITextureRegion currentLayer1;
	private GameResources gr;
	
	private float CAMERA_HEIGHT;
	private Camera CAMERA;
	private VertexBufferObjectManager vbom;
	private String currentMap;
	private float mapSpeed;	
	private boolean pause;
	
	public Map(Camera camera, float camera_height, VertexBufferObjectManager vertexBufferObjM, GameResources gameResources) {
		CAMERA_HEIGHT = camera_height;
		CAMERA = camera;
		vbom = vertexBufferObjM;
		mapSpeed = 17.5f;
		gr = gameResources;
	}
	
	public void loadSingaporeMap() {
		getRandomLayer1();
		parallaxLayerBackground = new ParallaxLayer(CAMERA, false);
		//parallaxLayerBackground = new AutoParallaxBackground(0, 0, 0, 5);
		parallaxLayerBackground.setParallaxChangePerSecond(mapSpeed);
		
		parallaxLayerBackground.attachParallaxEntity(new ParallaxEntity(0f, new Sprite(0, 0, gr.getmSingaporeParallaxLayer5(), vbom)));
		//parallaxLayerBackground.attachParallaxEntity(new ParallaxEntity(-0.4f, new Sprite(0, 0, gr.getmSingaporeParallaxLayer4_2(), vbom)));
		parallaxLayerBackground.attachParallaxEntity(new ParallaxEntity(-0.001f, new Sprite(0, CAMERA_HEIGHT - gr.getmSingaporeParallaxLayer3().getHeight() - (getCurrentMapLayer1().getHeight()*1.6f), gr.getmSingaporeParallaxLayer3(), vbom)));
		//parallaxLayerBackground.attachParallaxEntity(new ParallaxEntities(-0.1f, randomizeMidgroundMaps()));
		//parallaxLayerBackground.attachParallaxEntity(new ParallaxEntity(-0.6f, new Sprite(0, 0, gr.getmSingaporeParallaxLayer4_1(), vbom)));
		Layer1 = new ParallaxEntity(-10f, new Sprite(0, CAMERA_HEIGHT - getCurrentMapLayer1().getHeight(), getCurrentMapLayer1(), vbom));
				
		parallaxLayerBackground.attachParallaxEntity(Layer1);
		parallaxLayerBackground.attachParallaxEntity(new ParallaxEntity(-10f, new Sprite(0, (CAMERA_HEIGHT - gr.getmSingaporeParallaxLayer2_1().getHeight() - getCurrentMapLayer1().getHeight()) + 4, gr.getmSingaporeParallaxLayer2_1(), vbom)));
		
		/*
		sequentialParallaxBackground = new SequentialParallaxBackground(CAMERA, true);
		ArrayList<IAreaShape> layers = new ArrayList<IAreaShape>();
		
		layers.add(new Sprite(0, CAMERA_HEIGHT - gr.getmSingaporeParallaxLayer5().getHeight() - 90, gr.getmSingaporeParallaxLayer5(), vbom));
		sequentialParallaxBackground.attachParallaxEntities(new ParallaxEntities(0f, layers));
		
		layers.clear();
		layers.add(new Sprite(0, 0, gr.getmSingaporeParallaxLayer4_2(), vbom));
		sequentialParallaxBackground.attachParallaxEntities(new ParallaxEntities(-0.4f, layers));
		
		sequentialParallaxBackground.attachParallaxEntities(new ParallaxEntities(-0.1f, randomizeMidgroundMaps()));
		
		layers.clear();
		layers.add(new Sprite(0, CAMERA_HEIGHT - gr.getmSingaporeParallaxLayer2_1().getHeight() - getCurrentMapLayer1().getHeight() + 12, gr.getmSingaporeParallaxLayer2_1(), vbom));
		sequentialParallaxBackground.attachParallaxEntities(new ParallaxEntities(-1f, layers));

		layers.clear();
		layers.add(new Sprite(0, CAMERA_HEIGHT - getCurrentMapLayer1().getHeight(), getCurrentMapLayer1(), vbom));
		sequentialParallaxBackground.attachParallaxEntities(new ParallaxEntities(0f, layers));		
		
		*/
		currentMap = "Singapore";
	}
	
	public void loadSydneyMap() {
		getRandomLayer1();
		parallaxLayerBackground = new ParallaxLayer(CAMERA, false);
		//parallaxLayerBackground = new AutoParallaxBackground(0, 0, 0, 05);
		parallaxLayerBackground.setParallaxChangePerSecond(mapSpeed);
		
		parallaxLayerBackground.attachParallaxEntity(new ParallaxEntity(0f, new Sprite(0, -30, gr.getmSydneyParallaxLayer6(), vbom)));
		parallaxLayerBackground.attachParallaxEntity(new ParallaxEntity(-(MathUtils.random(0.1f, 1f)), new Sprite(0, 60, gr.getmSydneyParallaxLayer5A(), vbom)));
		parallaxLayerBackground.attachParallaxEntity(new ParallaxEntity(-(MathUtils.random(0.6f, 1.5f)), new Sprite(0, 125, gr.getmSydneyParallaxLayer5B(), vbom)));
		parallaxLayerBackground.attachParallaxEntity(new ParallaxEntity(-0.0005f, new Sprite(-15, 270, gr.getmSydneyParallaxLayer4(), vbom)));
		parallaxLayerBackground.attachParallaxEntity(new ParallaxEntity(-4f, new Sprite(0, CAMERA_HEIGHT - gr.getmSydneyParallaxLayer3_1().getHeight() - 40, gr.getmSydneyParallaxLayer3_1(), vbom)));
		parallaxLayerBackground.attachParallaxEntity(new ParallaxEntity(-6f, new Sprite(0, CAMERA_HEIGHT - gr.getmSydneyParallaxLayer3_2().getHeight() - 40, gr.getmSydneyParallaxLayer3_2(), vbom)));
		Sprite Layer2_1 = new Sprite(MathUtils.random(50, 800), CAMERA_HEIGHT - gr.getmSydneyParallaxLayer2_1().getHeight() - MathUtils.random(55, 95), gr.getmSydneyParallaxLayer2_1(), vbom);
		Layer2_1.setScale(0.6f);
		parallaxLayerBackground.attachParallaxEntity(new ParallaxEntity(-(MathUtils.random(0.1f, 0.5f)), Layer2_1, false, 2));
		Sprite Layer2_2 = new Sprite(MathUtils.random(50, 800), CAMERA_HEIGHT - gr.getmSydneyParallaxLayer2_2().getHeight() - MathUtils.random(55, 95), gr.getmSydneyParallaxLayer2_2(), vbom);
		Layer2_2.setScale(0.5f);
		parallaxLayerBackground.attachParallaxEntity(new ParallaxEntity(-(MathUtils.random(0.3f, 0.8f)), Layer2_2, false, 2));
		Sprite Layer2_3 = new Sprite(MathUtils.random(50, 800), CAMERA_HEIGHT - gr.getmSydneyParallaxLayer2_3().getHeight() - MathUtils.random(55, 95), gr.getmSydneyParallaxLayer2_3(), vbom);
		Layer2_3.setScale(0.7f);
		parallaxLayerBackground.attachParallaxEntity(new ParallaxEntity(-(MathUtils.random(0.2f, 0.8f)), Layer2_3, false, 2));
		Sprite Layer2_4 = new Sprite(-MathUtils.random(50, 800), CAMERA_HEIGHT - gr.getmSydneyParallaxLayer2_4().getHeight() - MathUtils.random(55, 95), gr.getmSydneyParallaxLayer2_4(), vbom);
		Layer2_4.setScale(0.4f);
		parallaxLayerBackground.attachParallaxEntity(new ParallaxEntity(MathUtils.random(0.1f, 0.35f), Layer2_4, false, 2));
		Layer1 = new ParallaxEntity(-10f, new Sprite(0, CAMERA_HEIGHT - getCurrentMapLayer1().getHeight(), getCurrentMapLayer1(), vbom));
		parallaxLayerBackground.attachParallaxEntity(Layer1);
		parallaxLayerBackground.attachParallaxEntity(new ParallaxEntity(-10f, new Sprite(0, (CAMERA_HEIGHT - getCurrentMapLayer1().getHeight() - gr.getmSydneyParallaxLayer1().getHeight()) + 3, gr.getmSydneyParallaxLayer1(), vbom)));
		currentMap = "Sydney";
	}
	
	public ParallaxLayer loadMap(String destinatedMap) {
		if(destinatedMap.toLowerCase().contains("singapore")) {
			loadSingaporeMap();
		}
		if(destinatedMap.toLowerCase().contains("sydney")) {
			loadSydneyMap();
		}
		return parallaxLayerBackground;
	}
	
	public ParallaxEntity getLayer1() {
		return Layer1;
	}

	public float getMapSpeed() {
		return mapSpeed;	
	}
	
	public void changeMapSpeed(float value) {
		mapSpeed = value;
		parallaxLayerBackground.setParallaxChangePerSecond(mapSpeed);
	}

	public boolean isPause() {
		return pause;
	}
	
	public void pause() {
		parallaxLayerBackground.setParallaxChangePerSecond(0f);	
		pause = true;
	}
	
	public void resume() {
		parallaxLayerBackground.setParallaxChangePerSecond(mapSpeed);
		pause = false;
	}

	public void addParallaxLayerObjects(ParallaxEntity object) {
		parallaxLayerBackground.attachParallaxEntity(object);
	}
	
	public void removeParallaxLayerObjects(ParallaxEntity object) {
		parallaxLayerBackground.detachParallaxEntity(object);
	}
	
	public String getCurrentMap() {
		return currentMap;
	}

	public ParallaxLayer getParallaxLayer() {
		return parallaxLayerBackground;
	}
	
	public ITextureRegion getCurrentMapLayer1() {
		return currentLayer1;
	}
	
	public ITextureRegion getRandomLayer1() {
		switch(MathUtils.random(1, 2)) {
		case 1: currentLayer1 = gr.getmCommonParallaxLayer1_1();
		case 2: currentLayer1 = gr.getmCommonParallaxLayer1_2();
		//case 3: currentLayer1 = mCommonParallaxLayer1_3;
		}
		return currentLayer1;
	}
	
	public ArrayList<IAreaShape> randomizeMidgroundMaps() {
		ArrayList<IAreaShape> randomizeMidgroundMaps = new ArrayList<IAreaShape>();
		
		Sprite singaporeLayer2_1Sprite = new Sprite(0, CAMERA_HEIGHT - gr.getmSingaporeParallaxLayer2_1().getHeight() - getRandomLayer1().getHeight(), gr.getmSingaporeParallaxLayer2_1(), vbom);
		//Sprite singaporeLayer2_2Sprite = new Sprite(0, CAMERA_HEIGHT - gr.getmSingaporeParallaxLayer2_2().getHeight() - getRandomLayer1().getHeight(), gr.getmSingaporeParallaxLayer2_1(), vbom);
		
		for(int i=0; i<MathUtils.random(3,6); i++) {
			randomizeMidgroundMaps.add(singaporeLayer2_1Sprite);
		}
		
		//for(int i=0; i<MathUtils.random(2,4); i++) {
		//	randomizeMidgroundMaps.add(singaporeLayer2_2Sprite);
		//} 

		randomizeMidgroundMaps.add(singaporeLayer2_1Sprite);
		//randomizeMidgroundMaps.add(singaporeLayer2_2Sprite);
		
		return randomizeMidgroundMaps;
	}
}

/*
package com.andengine.classes;

import java.util.ArrayList;

import org.andengine.engine.camera.Camera;
import org.andengine.entity.scene.background.AutoParallaxBackground;
import org.andengine.entity.shape.IAreaShape;
import org.andengine.entity.sprite.Sprite;
import org.andengine.opengl.texture.TextureManager;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.math.MathUtils;

import com.andengine.classes.ParallaxLayer.ParallaxEntity;
import com.andengine.classes.ParallaxLayer.ParallaxEntities;


import android.content.Context;

public class Map {
	// Parallax Background textures
	private BitmapTextureAtlas mStartParallaxBackgroundTexture, mSingaporeParallaxBackgroundTexture, 
	mMalaysiaParallaxBackgroundTexture, mTaiwanParallaxBackgroundTexture;

	private ParallaxLayer parallaxLayerBackground;
	private AutoParallaxBackground autoParallaxBackground;
	private SequentialParallaxBackground sequentialParallaxBackground;
	
	private ITextureRegion mStartParallaxLayerBack, mStartParallaxLayerMiddle, mStartParallaxLayerFront;
	private ITextureRegion mSingaporeParallaxLayerBack, mSingaporeParallaxLayerMiddle, mSingaporeParallaxLayerFront;
	private ITextureRegion mMalaysiaParallaxLayerBack, mMalaysiaParallaxLayerMiddle, mMalaysiaParallaxLayerFront;
	private ITextureRegion mTaiwanParallaxLayerBack, mTaiwanParallaxLayerMiddle, mTaiwanParallaxLayerFront;
	private ITextureRegion mEasterIslandParallaxLayerBack, mEasterIslandParallaxLayerMiddle, mEasterIslandParallaxLayerFront;
	
	private float CAMERA_HEIGHT;
	private Camera CAMERA;
	private VertexBufferObjectManager vbom;
	private String currentMap;
	private int random1, random2;
	
	public Map(Camera camera, TextureManager textureManager, Context context,float CAMERA_HEIGHT, VertexBufferObjectManager vbom){
		// Load parallax background entities
		mStartParallaxBackgroundTexture = new BitmapTextureAtlas(textureManager,2048, 1024, TextureOptions.DEFAULT);
		mStartParallaxLayerFront = BitmapTextureAtlasTextureRegionFactory.createFromAsset(mStartParallaxBackgroundTexture, context, "gfx/Game/floor2.png", 0, 0);
		mStartParallaxLayerMiddle = BitmapTextureAtlasTextureRegionFactory.createFromAsset(mStartParallaxBackgroundTexture, context, "gfx/Game/start_map.png", 0, 120);
		mStartParallaxLayerBack = BitmapTextureAtlasTextureRegionFactory.createFromAsset(mStartParallaxBackgroundTexture, context, "gfx/Game/background.png", 0, 376);
		mStartParallaxBackgroundTexture.load();
		
		mSingaporeParallaxBackgroundTexture = new BitmapTextureAtlas(textureManager,2048, 1024, TextureOptions.DEFAULT);
		mSingaporeParallaxLayerFront = BitmapTextureAtlasTextureRegionFactory.createFromAsset(mSingaporeParallaxBackgroundTexture, context, "gfx/Game/floor2.png", 0, 0);
		mSingaporeParallaxLayerMiddle = BitmapTextureAtlasTextureRegionFactory.createFromAsset(mSingaporeParallaxBackgroundTexture, context, "gfx/Game/foreground.png", 0, 120);
		mSingaporeParallaxLayerBack = BitmapTextureAtlasTextureRegionFactory.createFromAsset(mSingaporeParallaxBackgroundTexture, context, "gfx/Game/background.png", 0, 376);
		mSingaporeParallaxBackgroundTexture.load();
		
		mMalaysiaParallaxBackgroundTexture = new BitmapTextureAtlas(textureManager,2048, 1024, TextureOptions.DEFAULT);
		mMalaysiaParallaxLayerFront = BitmapTextureAtlasTextureRegionFactory.createFromAsset(mMalaysiaParallaxBackgroundTexture, context, "gfx/Game/floor2.png", 0, 0);
		mMalaysiaParallaxLayerMiddle = BitmapTextureAtlasTextureRegionFactory.createFromAsset(mMalaysiaParallaxBackgroundTexture, context, "gfx/Game/malaysiaForeground.png", 0, 120);
		mMalaysiaParallaxLayerBack = BitmapTextureAtlasTextureRegionFactory.createFromAsset(mMalaysiaParallaxBackgroundTexture, context, "gfx/Game/background.png", 0, 376);
		mMalaysiaParallaxBackgroundTexture.load();
		
		CAMERA_HEIGHT = CAMERA_HEIGHT;
		CAMERA = camera;
		vbom = vbom;
	}

	public ArrayList<IAreaShape> randomizeBackgroundMaps(int randomNo) {
		ArrayList<IAreaShape> randomizeBackgroundMaps = new ArrayList<IAreaShape>();
		
		Sprite startingMapBackgroundSprite = new Sprite(0, CAMERA_HEIGHT - mStartParallaxLayerBack.getHeight(), mStartParallaxLayerBack, vbom);
		Sprite singaporeBackgroundSprite = new Sprite(0, CAMERA_HEIGHT - mSingaporeParallaxLayerBack.getHeight(), mSingaporeParallaxLayerBack, vbom);
		Sprite malaysiaBackgroundSprite = new Sprite(0, CAMERA_HEIGHT - mMalaysiaParallaxLayerBack.getHeight(), mMalaysiaParallaxLayerBack, vbom);

		randomizeBackgroundMaps.add(startingMapBackgroundSprite);		

		switch(randomNo){
		case 1:
			randomizeBackgroundMaps.add(singaporeBackgroundSprite);
			randomizeBackgroundMaps.add(malaysiaBackgroundSprite);
		case 2:
			randomizeBackgroundMaps.add(malaysiaBackgroundSprite);
			randomizeBackgroundMaps.add(singaporeBackgroundSprite);
		}

		return randomizeBackgroundMaps;
	}
	
	public ArrayList<IAreaShape> randomizeMidgroundMaps(int randomNo1, int randomNo2) {
		ArrayList<IAreaShape> randomizeMidgroundMaps = new ArrayList<IAreaShape>();
		
		Sprite startingMapMidgroundSprite = new Sprite(0, CAMERA_HEIGHT - mStartParallaxLayerFront.getHeight() - mStartParallaxLayerMiddle.getHeight() , mStartParallaxLayerMiddle, vbom);
		Sprite singaporeMidgroundSprite = new Sprite(0, CAMERA_HEIGHT - mSingaporeParallaxLayerFront.getHeight() - mSingaporeParallaxLayerMiddle.getHeight() , mSingaporeParallaxLayerMiddle, vbom);
		Sprite malaysiaMidgroundSprite = new Sprite(0, CAMERA_HEIGHT - mMalaysiaParallaxLayerFront.getHeight() - mMalaysiaParallaxLayerMiddle.getHeight() , mMalaysiaParallaxLayerMiddle, vbom);

		for(int i=0; i<3; i++){
			randomizeMidgroundMaps.add(startingMapMidgroundSprite);		
		}
		
		switch(randomNo1){
		case 1:
			for(int i=0; i<randomNo2; i++){
				randomizeMidgroundMaps.add(singaporeMidgroundSprite);			
			}
			for(int i=0; i<randomNo2; i++){
				randomizeMidgroundMaps.add(malaysiaMidgroundSprite);		
			}
		case 2:
			for(int i=0; i<randomNo2; i++){
				randomizeMidgroundMaps.add(malaysiaMidgroundSprite);		
			}
			for(int i=0; i<randomNo2; i++){
				randomizeMidgroundMaps.add(singaporeMidgroundSprite);			
			}
		}
		
		return randomizeMidgroundMaps;
	}
	
	public ArrayList<IAreaShape> randomizeForegroundMaps(int randomNo1, int randomNo2) {
		ArrayList<IAreaShape> randomizeForegroundMaps = new ArrayList<IAreaShape>();
		
		Sprite startingMapForegroundSprite = new Sprite(0, CAMERA_HEIGHT - mStartParallaxLayerFront.getHeight(),mStartParallaxLayerFront, vbom);
		Sprite singaporeForegroundSprite = new Sprite(0, CAMERA_HEIGHT - mSingaporeParallaxLayerFront.getHeight(), mSingaporeParallaxLayerFront, vbom);
		Sprite malaysiaForegroundSprite = new Sprite(0, CAMERA_HEIGHT - mMalaysiaParallaxLayerFront.getHeight(), mMalaysiaParallaxLayerFront, vbom);

		randomizeForegroundMaps.add(startingMapForegroundSprite);
			
		switch(randomNo1){
		case 1:
			for(int i=0; i<randomNo2; i++){
				randomizeForegroundMaps.add(singaporeForegroundSprite);
			}
			for(int i=0; i<randomNo2; i++){
				randomizeForegroundMaps.add(malaysiaForegroundSprite);
			}
		case 2:
			for(int i=0; i<randomNo2; i++){
				randomizeForegroundMaps.add(malaysiaForegroundSprite);
			}
			for(int i=0; i<randomNo2; i++){
				randomizeForegroundMaps.add(singaporeForegroundSprite);
			}
		}
		return randomizeForegroundMaps;
	}
	
	public ParallaxLayer loadRandomizeMap() {
		random1 = MathUtils.random(1, 2);
		random2 = MathUtils.random(5, 8);
		sequentialParallaxBackground = new ParallaxLayer(CAMERA, true);
		sequentialParallaxBackground.setParallaxChangePerSecond(8);
		
		sequentialParallaxBackground.attachParallaxEntities(new ParallaxEntities(0, randomizeBackgroundMaps(random1)));
		sequentialParallaxBackground.attachParallaxEntities(new ParallaxEntities(-10, randomizeMidgroundMaps(random1,random2)));
		sequentialParallaxBackground.attachParallaxEntities(new ParallaxEntities(-20, randomizeForegroundMaps(random1,random2)));
		currentMap = "Start";
		
		return sequentialParallaxBackground;
	}
	
	public String getCurrentMap() {
		if(sequentialParallaxBackground.getCurrentMap() == 0)
			//Current at the starting map
			currentMap = "current";
		else if(sequentialParallaxBackground.getCurrentMap() > 3 && sequentialParallaxBackground.getCurrentMap() < (sequentialParallaxBackground.getCurrentMap() + random2))
			if(random1 == 1)
				currentMap = "singapore";
			else
				currentMap = "malaysia";
		else
			if(random1 == 1)
				currentMap = "malaysia";
			else
				currentMap = "singapore";
		
		return currentMap;
	}
	
	public float getCurrentMapFrontParallaxHeight(){
		if(currentMap.equalsIgnoreCase("singapore")){
			return mSingaporeParallaxLayerFront.getHeight();
		} else if(currentMap.equalsIgnoreCase("malaysia")){
			return mMalaysiaParallaxLayerFront.getHeight();			
		}
		return 0;
	}
	
}
*/
