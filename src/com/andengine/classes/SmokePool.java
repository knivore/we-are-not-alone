/*
 * Copyright (C) 2013 Knivore Studios
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.andengine.classes;

import javax.microedition.khronos.opengles.GL10;

import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.adt.pool.GenericPool;
import org.andengine.util.math.MathUtils;

import com.andengine.invaders.GameResources;

/**
 * @author Keh Chin Leong
 */
public class SmokePool extends GenericPool<SmokeSprite> {
	
	private GameResources gameResource;
	private VertexBufferObjectManager vbom;

	public SmokePool(VertexBufferObjectManager vertexBufferObjM, GameResources gr) {
		vbom = vertexBufferObjM;
		gameResource = gr;
	}

	/**
	 * Called when a Smoke is required but there isn't one in the pool
	 */
	@Override
	protected SmokeSprite onAllocatePoolItem() {
		//create missiles at the right end of the screen
		SmokeSprite smoke = new SmokeSprite(0, 0, gameResource.getmSmokeTextureRegion(), vbom);
		smoke.setBlendFunction(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);
		smoke.setCurrentTileIndex(MathUtils.random(0, 1));
		return smoke;
	}

	/**
	 * Called when a Smoke is sent to the pool
	 */
	@Override
	protected void onHandleRecycleItem(final SmokeSprite smokeSprite) {
		smokeSprite.setVisible(false);
	}

	/**
	 * Called just before a Smoke is returned to the caller, this is where you
	 * write your initialize code i.e. set location, rotation, etc.
	 */
	@Override
	protected void onHandleObtainItem(final SmokeSprite smokeSprite) {
		smokeSprite.reset();
	}
}
