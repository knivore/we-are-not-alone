package com.andengine.classes;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import com.andengine.invaders.GameResources;
import com.andengine.invaders.R;
import com.andengine.sharedpreferences.Gadgets_SharedPreference;
import com.andengine.sharedpreferences.Score_SharedPreference;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.achievement.Achievement;
import com.google.android.gms.games.achievement.AchievementBuffer;
import com.google.android.gms.games.achievement.Achievements;

public class AchievementContainer {

	public static String ACTIVE_IF_GREATER_THAN = ">";
	public static String ACTIVE_IF_LESS_THAN = "<";

	private ArrayList<AchievementProperty> propertyList;
	private GameResources mGameResources;

	public AchievementContainer(GameResources gameResources, boolean alert) {
		mGameResources = gameResources;
		propertyList = new ArrayList<AchievementProperty>();
		
		if(mGameResources.getmGameHelper().isSignedIn()) {
			new AsyncTaskLoader().execute(new IAsyncCallback() {
				@Override
				public void workToDo() {
					//Get number of achievements from G server
					boolean fullLoad = false;  // set to 'true' to reload all achievements (ignoring cache)
					long waitTime = 60;    // seconds to wait for achievements to load before timing out
					
					Achievements.LoadAchievementsResult r = (Achievements.LoadAchievementsResult)Games.Achievements.load(mGameResources.getmGameHelper().getApiClient(), fullLoad).await(waitTime, TimeUnit.SECONDS);
					AchievementBuffer buf = r.getAchievements();		
					int bufSize = buf.getCount();
				
					for(int i=0; i<bufSize; i++) {
						Achievement achievement = buf.get(i);
						
						// here you now have access to the achievement's data
						String id = achievement.getAchievementId();
						Long value = achievement.getXpValue();
						String name = achievement.getName();
						String description = achievement.getDescription();
						
						boolean unlocked = achievement.getState() == Achievement.STATE_UNLOCKED;  // is unlocked
						boolean incremental = achievement.getType() == Achievement.TYPE_INCREMENTAL;  // is incremental
						if (incremental) {
							int steps = achievement.getCurrentSteps();  // current incremental steps
						}
						
						String tag = description.substring(0, description.indexOf(" "));
						//Log.d(mGameResources.TAG, tag);
						if(description.toLowerCase().contains("gadget")) {
							if(tag.equalsIgnoreCase("purchase"))
								tag = "Purchase Gadget";
							else if(tag.equalsIgnoreCase("use")) 
								tag = "Use Gadget";
						}
						propertyList.add(new AchievementProperty(id, name, tag, description, Integer.parseInt(description.replaceAll("[\\D]", "")), Integer.parseInt(value.toString()) / 50, unlocked));

						//Log.d(mGameResources.TAG, propertyList.get(propertyList.size()-1).getmTag());
					}
					buf.close();
				}
	
				@Override
				public void onComplete() {
					//Do a run thru checking whether has the user complete some of the achievement.
					unlockAchievement();					
				}
			});
		} else {
			if(alert)
				mGameResources.getmGameHelper().makeSimpleDialog(mGameResources.getmBaseContext().getString(R.string.signin_for_achievements)).show();
		}
	}
	
	public void unlockAchievement() {
		unlockGameAchievement();
		unlockPurchaseAchievement();
		//unlockMultiplayerAchievement();
	}
	
	public void unlockGameAchievement() {
		if(mGameResources.getmGameHelper().isSignedIn()) {
			Score_SharedPreference generalHighScores = new Score_SharedPreference(mGameResources.getmBaseContext(), "");
			Score_SharedPreference singaporeHighScores = new Score_SharedPreference(mGameResources.getmBaseContext(), "Singapore");
			Score_SharedPreference sydneyHighScores = new Score_SharedPreference(mGameResources.getmBaseContext(), "Sydney");
			
			int bestScoreValue = singaporeHighScores.loadMaxDistancePerCountry() > sydneyHighScores.loadMaxDistancePerCountry() ? singaporeHighScores.loadMaxDistancePerCountry() : sydneyHighScores.loadMaxDistancePerCountry();
			int totalHumanAbsorbedValue = singaporeHighScores.loadAbsorbedHumansPerCountry() + sydneyHighScores.loadAbsorbedHumansPerCountry();
			int tootalHumanAnnihilatedValue = singaporeHighScores.loadDisintegratedHumansPerCountry() + sydneyHighScores.loadDisintegratedHumansPerCountry();
			int totalObtainedGold = generalHighScores.loadTotalObtainedGold();
	
			for(AchievementProperty p : propertyList) {
				if(p.getmTag().toLowerCase().contains("Travel".toLowerCase())) {
					if(bestScoreValue > p.getmConditions() && !p.getmStatus()) {
						Games.Achievements.unlock(mGameResources.getmGameHelper().getApiClient(), p.getId());
						
					}
				} else if(p.getmTag().toLowerCase().contains("Earn".toLowerCase())) {
					if(totalObtainedGold > p.getmConditions() && !p.getmStatus()) {
						Games.Achievements.unlock(mGameResources.getmGameHelper().getApiClient(), p.getId());
					}
				} else if(p.getmTag().toLowerCase().contains("Absorb".toLowerCase())) {
					if(totalHumanAbsorbedValue > p.getmConditions() && !p.getmStatus()) {
						Games.Achievements.unlock(mGameResources.getmGameHelper().getApiClient(), p.getId());
					}
				} else if(p.getmTag().toLowerCase().contains("Annihilate".toLowerCase())) {
					if(tootalHumanAnnihilatedValue > p.getmConditions() && !p.getmStatus()) {
						Games.Achievements.unlock(mGameResources.getmGameHelper().getApiClient(), p.getId());
					}
				}
			}
		}
	}
	
	public void unlockPurchaseAchievement() {
		if(mGameResources.getmGameHelper().isSignedIn()) {
			Gadgets_SharedPreference gadget_sharedpreference = new Gadgets_SharedPreference(mGameResources.getmBaseContext());
			
			for(AchievementProperty p : propertyList) {
				if(p.getmTag().equalsIgnoreCase("Purchase Gadget")) {
					if(gadget_sharedpreference.loadTotalGadgetBrought() > p.getmConditions() && !p.getmStatus()) {
						Games.Achievements.unlock(mGameResources.getmGameHelper().getApiClient(), p.getId());
					}
				} else if(p.getmTag().equalsIgnoreCase("Use Gadget")) {
					if(gadget_sharedpreference.loadTotalGadgetUsed() > p.getmConditions() && !p.getmStatus()) {
						Games.Achievements.unlock(mGameResources.getmGameHelper().getApiClient(), p.getId());
					}
				}
			}
		}
	}
	
	public void unlockMultiplayerAchievement() {
		if(mGameResources.getmGameHelper().isSignedIn()) {
			for(AchievementProperty p : propertyList) {
				if(p.getmTag().toLowerCase().contains("Multiplayer")) {
					
				} 
			}
		}
	}
}
