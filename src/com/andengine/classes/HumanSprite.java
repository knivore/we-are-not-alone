/*
 * Copyright (C) 2013 Knivore Studios
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.andengine.classes;

import java.util.Random;

import org.andengine.engine.handler.physics.PhysicsHandler;
import org.andengine.entity.sprite.AnimatedSprite;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.math.MathUtils;

import com.makersf.andengine.extension.collisions.entity.sprite.PixelPerfectAnimatedSprite;
import com.makersf.andengine.extension.collisions.opengl.texture.region.PixelPerfectTiledTextureRegion;

/**
 * @author Keh Chin Leong
 */
public class HumanSprite extends PixelPerfectAnimatedSprite {

	// ===========================================================
	// Constants
	// ===========================================================
	private static final int CAMERA_WIDTH = 1280, CAMERA_HEIGHT = 720;
	private boolean statusChanged = false;
	private boolean onFirstLoad = false;
	private boolean movingDirection = false;
	private boolean playerFlinchMode = false;
	private float humanMovingSpeed = 0f;
	private String humanStatus;
	private PhysicsHandler mPhysicsHandler;
	private Map mMap;
	
	public HumanSprite(float pX, float pY, PixelPerfectTiledTextureRegion pTiledTextureRegion, VertexBufferObjectManager vbom, Map map) {
		super(pX, pY, pTiledTextureRegion, vbom);
		mPhysicsHandler = new PhysicsHandler(this);
		registerUpdateHandler(mPhysicsHandler);
		mMap = map;
	}

	public void onFirstLoad() {
		onFirstLoad = true;
		setPosition(MathUtils.random(5, 1275), CAMERA_HEIGHT - getHeight() - MathUtils.random(25, 45));
		if (new Random().nextBoolean()) {
			setFacingDirection(true);
			movingDirection = true;
		} else {
			setFacingDirection(false);
			movingDirection = false;
		}
		setVisible(true);
		setAlpha(1);
		setWalkingSpeed();
		humanMovingSpeed = 90;
	}
	
	public void reset() {
		onFirstLoad = false;
		this.setPosition(CAMERA_WIDTH + this.getWidth(), CAMERA_HEIGHT - this.getHeight() - MathUtils.random(25, 45));
		
		if (MathUtils.RANDOM.nextBoolean()) {
			if (MathUtils.RANDOM.nextBoolean()) {
				setFacingDirection(true);
			} else {
				setFacingDirection(false);
			}
			setRunningSpeed();
		} else {
			if (MathUtils.RANDOM.nextBoolean()) {
				setFacingDirection(true);
			} else {
				setFacingDirection(false);
			}
			setWalkingSpeed();
		}
		
		setFacingDirection(false);
		movingDirection = true;
		setRunningSpeed();
		
		setVisible(true);
		setAlpha(1);
	}
	
	@Override
	protected void onManagedUpdate(float pSecondsElapsed) {
		//if(this.isVisible()) {
			if(!movingDirection)
				moveLeftToRight(humanMovingSpeed + MathUtils.random(30f, 55f));
			else
				moveRightToLeft(humanMovingSpeed);
			super.onManagedUpdate(pSecondsElapsed);
		//}
	}
	
	//-----------------------------OTHER METHODS-----------------------------//
	//-----------------------------------------------------------------------//
	public float getHumanMovingSpeed() {
		return humanMovingSpeed;
	}

	public void setHumanMovingSpeed(float humanMovingSpeed) {
		this.humanMovingSpeed = humanMovingSpeed;
	}

	public boolean getFacingDirection() {
		//FALSE = face >> (RIGHT) || TRUE = face << (LEFT)
		return isFlippedHorizontal();
	}

	public void setFacingDirection(boolean direction) {
		//FALSE = face >> (RIGHT) || TRUE = face << (LEFT)
		setFlippedHorizontal(direction);
	}
	
	public boolean getMovingDirection() {
		// False => moving right, True => moving left
		return movingDirection;
	}

	public void setMovingDirection(boolean direction) {
		// False => moving right, True => moving left
		movingDirection = direction;
	}
	
	public void blown() {
		//mPhysicsHandler.setVelocityX(0);
		animate(new long[] {100,100}, 22, 23, false); //Blown away is tile index of 22 - 24
		humanStatus = "BLOWN";
	}
	
	public void absorb() {
		mPhysicsHandler.setVelocityX(0);
		animate(new long[] {100,100,100}, 16, 18, true); //Absorb is tile index of 16 & 18
		humanStatus = "ABSORBED";
	}
	
	public void fear() { //Fear is tile index of 19 & 20
		mPhysicsHandler.setVelocityX(0);
		//moveRightToLeft(MathUtils.random(10f, 15f));
		animate(new long[] {50,50}, 19, 20, true);
		humanStatus = "FEAR";
	}
	
	public void shocked(final PixelPerfectAnimatedSprite playerSprite) { //Shock is tile index of 25 & 26
		mPhysicsHandler.setVelocityX(0);
		humanStatus = "SHOCK";
		animate(new long[] {200,350}, 25, 26, false, new IAnimationListener() {
			@Override
			public void onAnimationStarted(AnimatedSprite pAnimatedSprite, int pInitialLoopCount) {
				if(getFacingDirection())
					setX(getX() + 12f);
				else
					setX(getX() - 12f);
			}
			@Override
			public void onAnimationFrameChanged(AnimatedSprite pAnimatedSprite, int pOldFrameIndex, int pNewFrameIndex) { }
			@Override
			public void onAnimationLoopFinished(AnimatedSprite pAnimatedSprite, int pRemainingLoopCount, int pInitialLoopCount) { }

			@Override
			public void onAnimationFinished(AnimatedSprite pAnimatedSprite) {
				//setX(getX() + 15);
				if((getFacingDirection() == true && getX() >= (playerSprite.getX() + 100)) || (getFacingDirection() == false && getX() <= (playerSprite.getX() + 100))) {
					//switch(MathUtils.random(0, 1)) {
						//case 0: {
							switchFacingDirection();
							//if facing left <
							if(getFacingDirection()) {
								switchMovingDirection();
							}
							//moveLeftToRight(MathUtils.random(humanMovingSpeed + MathUtils.random(0.5f, 2f), humanMovingSpeed + MathUtils.random(0.5f, 3.5f)));
						//}
						//case 1: {
							//moveRightToLeft(MathUtils.random(humanMovingSpeed - MathUtils.random(0.5f, 2f), humanMovingSpeed - MathUtils.random(1.5f, 4f)));
						//}
					//}
				} else if(getFacingDirection() == false && getX() >= (playerSprite.getX() + 100)) {
					//moveLeftToRight(MathUtils.random(humanMovingSpeed + MathUtils.random(0.5f, 2f), humanMovingSpeed + MathUtils.random(0.5f, 3.5f)));	
				} else if(getFacingDirection() == true && getX() <= (playerSprite.getX() + 100)) {
					//moveRightToLeft(MathUtils.random(humanMovingSpeed - MathUtils.random(0.5f, 2f), humanMovingSpeed - MathUtils.random(1.5f, 4f)));
				}
				setRunningSpeed();
			}
		});
	}
	
	public void playerInFlinchMode() {
		playerFlinchMode = true;
	}
	
	public void playerOutOfFlinchMode() {
		playerFlinchMode = false;
	}
	
	public void switchMovingDirection() {
		if(getFacingDirection() == true) {
			movingDirection = false;
		} else {
			movingDirection = true;
		}
	}
	
	public void switchFacingDirection() {
		if(getFacingDirection() == true) {
			setFacingDirection(false);
		} else {
			setFacingDirection(true);
		}
	}
	
	public void moveRightToLeft(float speed) {
		mPhysicsHandler.setVelocityX(-1 * speed);
	}

	public void moveLeftToRight(float speed) {
		mPhysicsHandler.setVelocityX(1 * speed);
	}
	
	public void setWalkingSpeed() {
		long speed = 0;
		
		if(!mMap.isPause() || !playerFlinchMode) {
			if(!movingDirection) {
				humanMovingSpeed = MathUtils.random(90, 110);
				speed = (long) MathUtils.random(95, 115);
			} else {
				if(getFacingDirection()) {
					//Facing < and walking <
					humanMovingSpeed = MathUtils.random(140, 160);
				} else {
					//Facing > and walking <
					humanMovingSpeed = MathUtils.random(130, 150);
				}
				speed = (long) MathUtils.random(70, 90);
			}
		} else {
			if(onFirstLoad) {
				humanMovingSpeed = MathUtils.random(100, 150);
			} else {
				humanMovingSpeed = MathUtils.random(150, 170);
			}
			speed = (long) MathUtils.random(80, 100);
		}
		
		animate(new long[] {speed,speed,speed,speed,speed,speed,speed,speed}, 0, 7, true);
		humanStatus = "WALK";
	}

	public void setRunningSpeed() {
		long speed = 0;
		
		if(!mMap.isPause() || !playerFlinchMode) {
			if(!movingDirection) {
				//Facing > and running >
				humanMovingSpeed = MathUtils.random(115, 145);
				speed = (long) MathUtils.random(70, 85);
			} else {
				if(getFacingDirection()) {
					//Facing < and running <
					humanMovingSpeed = MathUtils.random(160, 180);
				} else {
					//Facing > and running <
					humanMovingSpeed = MathUtils.random(130, 150);
				}
				speed = (long) MathUtils.random(55, 80);
			}		
		} else {
			humanMovingSpeed = MathUtils.random(185, 245);
			speed = (long) MathUtils.random(55, 80);
		}		
		animate(new long[] {speed,speed,speed,speed,speed,speed,speed,speed}, 8, 15, true);
		humanStatus = "RUN";
	}

	public void humanStopMoving() {
		humanStatus = "STOP";
		mPhysicsHandler.setVelocityX(0);
		this.setCurrentTileIndex(0);
	}
	
	public void setHumanStatus(String status) {
		humanStatus = status;
		statusChanged = true;
	}
	
	public String getHumanStatus() {
		return humanStatus;
	}
	
	public boolean getStatusChanged() {
		return statusChanged;
	}	
	
	public boolean offScreen(int cameraWidth, int cameraHeight) {
		if ((getX() > cameraWidth + getWidth() + 30) || (getX() < 0 - getWidth() - 30)) {
			return true;
		} else {
			return false;			
		}
	}
}
