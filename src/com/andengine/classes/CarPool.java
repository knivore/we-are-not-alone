/*
 * Copyright (C) 2013 Knivore Studios
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.andengine.classes;

import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.adt.pool.GenericPool;
import org.andengine.util.math.MathUtils;

import com.andengine.invaders.GameResources;

/**
 * @author Keh Chin Leong
 */
public class CarPool extends GenericPool<CarSprite> {
	
	private Map currentMap;
	private GameResources gr;
	private VertexBufferObjectManager vbom;
	private float cameraHeight;
	private float cameraWidth;

	public CarPool(float CAMERA_HEIGHT, float CAMERA_WIDTH, VertexBufferObjectManager vertexBufferObjM, GameResources gameResources, Map map) {
		vbom = vertexBufferObjM;
		cameraHeight = CAMERA_HEIGHT;
		cameraWidth = CAMERA_WIDTH;
		gr = gameResources;
		currentMap = map;
	}
	
	/**
	 * Called when a car is required but there isn't one in the pool
	 */
	@Override
	protected CarSprite onAllocatePoolItem() {
		CarSprite sprite = new CarSprite(cameraWidth, cameraHeight - gr.getmLampPostTextureRegion().getHeight() - MathUtils.random(45f, 66f), gr.getmCarsTextureRegion(), vbom, currentMap);
		sprite.setScale(0.75f);
		return sprite;
	}

	/**
	 * Called when a car is sent to the pool
	 */
	@Override
	protected void onHandleRecycleItem(final CarSprite car) {
		car.clearEntityModifiers();
		car.setVisible(false);
	}

	/**
	 * Called just before a car is returned to the caller, this is where you
	 * write your initialize code i.e. set location, rotation, etc.
	 */
	@Override
	protected void onHandleObtainItem(final CarSprite car) {
		car.reset();
	}
	
	public void setMap(Map map) {
		currentMap = map;
	}
}
