package com.andengine.classes;


public class AchievementProperty {

	private String id;
	private String mName;
	private String mTag;
	private String mDescription;
	private int mConditions;
	private int mRewards;
	private boolean mStatus;

	public AchievementProperty(String id, String name, String tag, String description, int conditions, int rewards, boolean status) {
		setId(id);
		setmName(name);
		setmTag(tag);
		setmDescription(description);
		setmConditions(conditions);
		setmRewards(rewards);
		setmStatus(status);
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public String getmName() {
		return mName;
	}

	public void setmName(String mName) {
		this.mName = mName;
	}

	public String getmTag() {
		return mTag;
	}

	public void setmTag(String mTag) {
		this.mTag = mTag;
	}
	
	public String getmDescription() {
		return mDescription;
	}

	public void setmDescription(String mDescription) {
		this.mDescription = mDescription;
	}

	public int getmConditions() {
		return mConditions;
	}

	public void setmConditions(int mConditions) {
		this.mConditions = mConditions;
	}

	public int getmRewards() {
		return mRewards;
	}

	public void setmRewards(int mRewards) {
		this.mRewards = mRewards;
	}

	public void setmStatus(boolean status) { 
		mStatus = status; 
	}

	public boolean getmStatus() {
		return mStatus;
	}
}
