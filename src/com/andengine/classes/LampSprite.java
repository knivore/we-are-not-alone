/*
 * Copyright (C) 2013 Knivore Studios
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.andengine.classes;

import org.andengine.engine.handler.physics.PhysicsHandler;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

import com.makersf.andengine.extension.collisions.entity.sprite.PixelPerfectAnimatedSprite;
import com.makersf.andengine.extension.collisions.opengl.texture.region.PixelPerfectTiledTextureRegion;

/**
 * @author Keh Chin Leong
 */
public class LampSprite extends PixelPerfectAnimatedSprite {

	// ===========================================================
	// Constants
	// ===========================================================
	private static final int CAMERA_WIDTH = 1280;
	private static final int CAMERA_HEIGHT = 720;
	private PhysicsHandler mPhysicsHandler;
	private String lampStatus;
	//private float lampSpeed = -174.6f;
	private boolean destroyedByLaserBeam;
	private Map mMap;
	
	public LampSprite(float pX, float pY, PixelPerfectTiledTextureRegion tiledTextureRegion, VertexBufferObjectManager vbom, Map map) {
		super(pX, pY, tiledTextureRegion, vbom);
		mMap = map;
		lampStatus = "MOVE";
	}

	public void reset() {
		//setPosition(CAMERA_WIDTH, CAMERA_HEIGHT - getHeight() - 67);
		setVisible(true);
		setAlpha(1f);
		setScale(1f);
		lampStatus = "MOVE";
	}

	@Override
	protected void onManagedUpdate(float pSecondsElapsed) {
		//if(this.isVisible()) {
			if(lampStatus.equals("MOVE")) {
				this.setX(this.getX() - ((mMap.getParallaxLayer().calculateSpeedInPixel % mMap.getLayer1().getShapeWidthScaled()) * 10f));
				//this.setX(CAMERA_WIDTH + 10 - (mMap.getParallaxLayer().getParallaxValue() * 10) % mMap.getLayer1().getShapeWidthScaled());
				//move(lampSpeed + (-1 * (mMap.getParallaxLayer().calculateSpeedInPixel % mMap.getLayer1().shapeWidthScaled)));
			} else if (lampStatus.equals("FIRSTLOADMOVE")) {
				this.setX(this.getX() - ((mMap.getParallaxLayer().calculateSpeedInPixel % mMap.getLayer1().getShapeWidthScaled()) * 10f));
			} else if(lampStatus.equals("STOP")) {
				//stopMoving();
			}
			super.onManagedUpdate(pSecondsElapsed);
		//}
	}
	
	//-----------------------------OTHER METHODS-----------------------------//
	//-----------------------------------------------------------------------//
	/*
	public float getLampSpeed() {
		return lampSpeed;
	}

	public void setLampSpeed(float speed) {
		this.lampSpeed = speed;
	}
	*/
	public boolean detectOffScreen() {
		if (getX() < 0 - getWidth() - 10 || getX() > CAMERA_WIDTH + getWidth() + 30) {
			return true;
		} else
			return false;
	}
	
	public void move(float speed) {
		mPhysicsHandler.setVelocityX(speed);
	}
	
	public void stopMoving() {
		mPhysicsHandler.setVelocityX(0);
	}
	
	public String getLampStatus() {
		return lampStatus;
	}
	
	public void setLampStatus(String status) {
		lampStatus = status;
	}
	
	public boolean getDestroyedByLaserBeam() {
		return destroyedByLaserBeam;
	}
	
	public void setDestroyedByLaserBeam(boolean status) {
		destroyedByLaserBeam = status;
	}
}
