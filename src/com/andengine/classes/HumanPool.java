/*
 * Copyright (C) 2013 Knivore Studios
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.andengine.classes;

import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.adt.pool.GenericPool;
import org.andengine.util.math.MathUtils;

import com.andengine.invaders.GameResources;
import com.makersf.andengine.extension.collisions.opengl.texture.region.PixelPerfectTiledTextureRegion;

/**
 * @author Keh Chin Leong
 */
public class HumanPool extends GenericPool<HumanSprite> {

	private Map currentMap;
	private GameResources gr;
	private VertexBufferObjectManager vbom;
	private float positionY, cameraHeight, cameraWidth;
	public int humansCaptured;
	private boolean onFirstLoad;

	public HumanPool(float CAMERA_HEIGHT, float CAMERA_WIDTH, VertexBufferObjectManager vertexBufferObjM, GameResources gameResources, Map map) {
		vbom = vertexBufferObjM;
		cameraHeight = CAMERA_HEIGHT;
		cameraWidth = CAMERA_WIDTH;
		gr = gameResources;
		currentMap = map;
	}

	/**
	 * Called when a Human is required but there isn't one in the pool
	 */
	@Override
	protected HumanSprite onAllocatePoolItem() {
		HumanSprite hs = null;
		PixelPerfectTiledTextureRegion humanTexture = null;
		
		//Random in getting the gender of humans
		//If its a male, shrink alitte bit as the image of male is abit larger in size compared to female
		//With no gender discrimination in anyway. Just the graphics is shrink in an uneven manner
		if(MathUtils.RANDOM.nextBoolean()) {
			humanTexture = gr.createFemaleHumans().deepCopy();
			if(onFirstLoad) {
				hs = new HumanSprite(MathUtils.random(50, 850), positionY, humanTexture, vbom, currentMap);
			} else {
				hs = new HumanSprite(cameraWidth + humanTexture.getWidth() + MathUtils.random(10, 20), positionY, humanTexture, vbom, currentMap);
			}
			hs.setScale(0.7f);
		} else {
			humanTexture = gr.createMaleHumans().deepCopy();
			if(onFirstLoad) {
				hs = new HumanSprite(MathUtils.random(50, 850), positionY, humanTexture, vbom, currentMap);
			} else {
				hs = new HumanSprite(cameraWidth + humanTexture.getWidth() + MathUtils.random(10, 20), positionY, humanTexture, vbom, currentMap);
			}
			hs.setScale(0.7f);
		}
		return hs;
	}

	/**
	 * Called when a Human is sent to the pool
	 */
	protected void onHandleRecycleItem(final HumanSprite pHuman) {
		pHuman.clearEntityModifiers();
		pHuman.setVisible(false);
		pHuman.setFacingDirection(true);
	}

	/**
	 * Called just before a Human is returned to the caller, this is where you
	 * write your initialize code i.e. set location, rotation, etc.
	 */
	protected synchronized void onHandleObtainItem(final HumanSprite pHuman) {
		if(onFirstLoad) {
			pHuman.onFirstLoad();
		} else {
			pHuman.reset();
		}
	}
	
	public void setHumanMapLocation(Map pMap) {
		//currentMap = pMap;
		//if(currentMap.getCurrentMap() == "") { 
		// DO SOMETHING IN THE FUTURE?~
		//}
		
		positionY = cameraHeight - MathUtils.random(-20, 5) - gr.createHumans().getHeight();
	}
	
	public void setOnFirstLoad(boolean value) {
		onFirstLoad = value;
	}
}
