package com.andengine.classes;

public abstract class IAsyncCallback {

	public abstract void workToDo();

	public abstract void onComplete();

}