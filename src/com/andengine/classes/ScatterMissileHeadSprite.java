/*
 * Copyright (C) 2013 Knivore Studios
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.andengine.classes;

import org.andengine.entity.modifier.MoveModifier;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

import com.makersf.andengine.extension.collisions.entity.sprite.PixelPerfectAnimatedSprite;
import com.makersf.andengine.extension.collisions.opengl.texture.region.PixelPerfectTiledTextureRegion;

/**
 * @author Keh Chin Leong
 */
public class ScatterMissileHeadSprite extends PixelPerfectAnimatedSprite {

	// ===========================================================
	// Constants
	// ===========================================================
	//private static final int CAMERA_WIDTH = 1280;
	//private static final int CAMERA_HEIGHT = 720;
	private MoveModifier moveModifier;
	//private PhysicsHandler mPhysicsHandler;
	private ScatterMissileTailSprite missileTail;
	private float speed;
	private String moveStatus = "";
	
	public ScatterMissileHeadSprite(float pX, float pY, PixelPerfectTiledTextureRegion pTiledTextureRegion, VertexBufferObjectManager vbom) {
		super(pX, pY, pTiledTextureRegion, vbom);
		//mPhysicsHandler = new PhysicsHandler(this);
		//registerUpdateHandler(mPhysicsHandler);
	}

	public void reset() {
		//this.mPhysicsHandler.setVelocityY(0f);
		//setScale(1f);
		//setPosition(0, 0);
		setVisible(true);
		//clearEntityModifiers();
		//clearUpdateHandlers();
	}
	
	@Override
	protected void onManagedUpdate(float pSecondsElapsed) {
		if(this.isVisible()) {
			if(moveStatus.equals("UP"))
				missileTail.setY(this.getY() - 8f);
			else
				missileTail.setY(this.getY() + 8f);
			
			/*
			if(moveStatus.equals("UP")) {
				if(getY() > 100)
					this.mPhysicsHandler.setVelocity(-100f, -100f);
			} else if(moveStatus.equals("DOWN")) {
				if(getY() > 0 && getY() < 650)
					this.mPhysicsHandler.setVelocity(-100f, 100f);
			}
			*/
			super.onManagedUpdate(pSecondsElapsed);
		}
	}	
	
	//-----------------------------OTHER METHODS-----------------------------//
	//-----------------------------------------------------------------------//

	public boolean offScreen(int cameraWidth, int cameraHeight) {
		if (getX() <= 0 - getWidth() - 86) return true;
		else return false;
	}
	
	public void moveUpRightToLeft(float speed, float xPos, float yPos) {
		moveStatus = "UP";
		this.speed = speed;
		
		clearEntityModifiers();
		missileTail.clearEntityModifiers();
		
		if(moveModifier != null)
			moveModifier.reset(this.speed, getX(), getX() - xPos, getY(), getY() - yPos);
		else 
			moveModifier = new MoveModifier(this.speed, getX(), getX() - xPos, getY(), getY() - yPos);
		
		registerEntityModifier(moveModifier);
		missileTail.registerEntityModifier(moveModifier);
	}
	
	public void moveDownRightToLeft(float speed, float xPos, float yPos) {
		moveStatus = "DOWN";
		this.speed = speed;
		
		clearEntityModifiers();
		missileTail.clearEntityModifiers();
		
		if(moveModifier != null)
			moveModifier.reset(this.speed, getX(), getX() - xPos, getY(), getY() + yPos);
		else 
			moveModifier = new MoveModifier(this.speed, getX(), getX() - xPos, getY(), getY() + yPos);
		
		registerEntityModifier(moveModifier);
		missileTail.registerEntityModifier(moveModifier);
	}

	public float getSpeed() {
		return speed;
	}
	
	public void setSpeed(float mSpeed) {
		speed = mSpeed;
	}

	public String getMoveStatus() {
		return moveStatus;
	}

	public void setMoveStatus(String moveStatus) {
		this.moveStatus = moveStatus;
	}

	public ScatterMissileTailSprite getMissileTail() {
		return missileTail;
	}

	public void setMissileTail(ScatterMissileTailSprite missileTail) {
		this.missileTail = missileTail;
	}
}
