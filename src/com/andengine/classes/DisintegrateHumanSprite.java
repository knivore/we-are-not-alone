/*
 * Copyright (C) 2013 Knivore Studios
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.andengine.classes;

import org.andengine.engine.handler.physics.PhysicsHandler;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.math.MathUtils;

import com.makersf.andengine.extension.collisions.entity.sprite.PixelPerfectAnimatedSprite;
import com.makersf.andengine.extension.collisions.opengl.texture.region.PixelPerfectTiledTextureRegion;

/**
 * @author Keh Chin Leong
 */
public class DisintegrateHumanSprite extends PixelPerfectAnimatedSprite {

	// ===========================================================
	// Constants
	// ===========================================================
	private static final int CAMERA_WIDTH = 1280;
	private boolean facingDirection = true, isHalt = false; //TRUE = face >> (RIGHT) || FALSE = face << (LEFT)
	private static final int CAMERA_HEIGHT = 720;
	private PhysicsHandler mPhysicsHandler;
	private Map mMap;
	
	public DisintegrateHumanSprite(float pX, float pY, PixelPerfectTiledTextureRegion pTiledTextureRegion, VertexBufferObjectManager vbom, Map map) {
		super(pX, pY, pTiledTextureRegion, vbom);
		mPhysicsHandler = new PhysicsHandler(this);
		registerUpdateHandler(mPhysicsHandler);
		mMap = map;
	}

	//public void reset(float speedOfHumans) {
	public void reset() {
		isHalt = false;
		setPosition(CAMERA_WIDTH + getWidth() + MathUtils.random(10, 20), CAMERA_HEIGHT);
		setVisible(true);
		setFlippedHorizontal(false);
		setAlpha(1);
		setCurrentTileIndex(0);
	}
	
	@Override
	protected void onManagedUpdate(float pSecondsElapsed) {
		if(this.isVisible()) {
			if(!isHalt) {
				moveRightToLeft(pSecondsElapsed);
				//this.setX(this.getX() - ((mMap.getParallaxLayer().calculateSpeedInPixel % mMap.getLayer1().shapeWidthScaled) / 3f));
				//this.setX(this.getX() - ((mMap.getParallaxLayer().getParallaxValue() * 10) % mMap.getLayer1().shapeWidthScaled));
			}
			super.onManagedUpdate(pSecondsElapsed);
		}
	}
	

	//-----------------------------OTHER METHODS-----------------------------//
	//-----------------------------------------------------------------------//
	public boolean getFacingDirection() {
		//TRUE = face >> (RIGHT) || FALSE = face << (LEFT)
		return facingDirection;
	}

	public void setFacingDirection(boolean direction) {
		//TRUE = face >> (RIGHT) || FALSE = face << (LEFT)
		facingDirection = direction;
	}
	
	public void disintegrate() {
		animate(new long[] {200,200,200,200,125,100,75,75,75,50}, 0, 9, false);
	}

	public void switchDirection() {
		//TRUE = face >> (RIGHT) || FALSE = face << (LEFT)
		if(!isFlippedHorizontal()) {
			setFlippedHorizontal(true);
			setFacingDirection(false);
		} else {
			setFlippedHorizontal(false);
			setFacingDirection(true);
		}
	}
	
	public void moveRightToLeft(float pSecondsElapsed) {
		float spriteMovingSpeed = -175f + (-1 * (mMap.getParallaxLayer().calculateSpeedInPixel % mMap.getLayer1().getShapeWidthScaled()));
		mPhysicsHandler.setVelocityX(spriteMovingSpeed);
	}
	
	public void halt() {
		mPhysicsHandler.setVelocityX(0);
		isHalt = true;
	}
	
	public void haltRecover() {
		isHalt = false;
	}
}
