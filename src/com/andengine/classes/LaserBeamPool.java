/*
 * Copyright (C) 2013 Knivore Studios
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.andengine.classes;

import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.adt.pool.GenericPool;

import com.andengine.invaders.GameResources;

/**
 * @author Keh Chin Leong
 */
public class LaserBeamPool extends GenericPool<LaserBeamSprite> {
	
	private GameResources gameResource;
	private VertexBufferObjectManager vbom;
	//private float cameraHeight;
	private float cameraWidth;

	public LaserBeamPool(float CAMERA_HEIGHT, float CAMERA_WIDTH, VertexBufferObjectManager vertexBufferObjM, GameResources gr) {
		vbom = vertexBufferObjM;
		//cameraHeight = CAMERA_HEIGHT;
		cameraWidth = CAMERA_WIDTH;
		gameResource = gr;
	}

	/**
	 * Called when a Laser Beam is required but there isn't one in the pool
	 */
	@Override
	protected LaserBeamSprite onAllocatePoolItem() {
		//create Laser Beam at the Top of the screen
		LaserBeamSprite laserBeam = new LaserBeamSprite(cameraWidth, 0, gameResource.getmLaserBeamTextureRegion(), vbom);
		return laserBeam;
	}

	/**
	 * Called when a Laser Beam is sent to the pool
	 */
	@Override
	protected void onHandleRecycleItem(final LaserBeamSprite pLaserBeam) {
		pLaserBeam.clearEntityModifiers();
		pLaserBeam.setVisible(false);
	}

	/**
	 * Called just before a Laser Beam is returned to the caller, this is where you
	 * write your initialize code i.e. set location, rotation, etc.
	 */
	@Override
	protected void onHandleObtainItem(final LaserBeamSprite pLaserBeam) {
		pLaserBeam.reset();
	}
}
