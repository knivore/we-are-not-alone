/*
 * Copyright (C) 2013 Knivore Studios
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.andengine.classes;

import org.andengine.entity.modifier.MoveXModifier;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

import com.makersf.andengine.extension.collisions.entity.sprite.PixelPerfectAnimatedSprite;
import com.makersf.andengine.extension.collisions.opengl.texture.region.PixelPerfectTiledTextureRegion;

/**
 * @author Keh Chin Leong
 */
public class MissileHeadSprite extends PixelPerfectAnimatedSprite {

	// ===========================================================
	// Constants
	// ===========================================================
	//private static final int CAMERA_WIDTH = 1280;
	//private static final int CAMERA_HEIGHT = 720;
	private MoveXModifier moveX;
	private float speed;
	
	public MissileHeadSprite(float pX, float pY, PixelPerfectTiledTextureRegion pTiledTextureRegion, VertexBufferObjectManager vbom) {
		super(pX, pY, pTiledTextureRegion, vbom);
	}

	public void reset() {
		setPosition(0, 0);
		setVisible(true);
	}

	//-----------------------------OTHER METHODS-----------------------------//
	//-----------------------------------------------------------------------//

	public boolean offScreen(int cameraWidth, int cameraHeight) {
		if (getX() <= 0 - getWidth() - 86) return true;
		else return false;
	}
	
	public void moveRightToLeft(float speed) {
		this.speed = speed;
		moveX = new MoveXModifier(this.speed, getX(), 0 - getWidth() - 98);
		registerEntityModifier(moveX);
	}

	public float getSpeed() {
		return speed;
	}
	
	public void setSpeed(float mSpeed) {
		speed = mSpeed;
	}
}
