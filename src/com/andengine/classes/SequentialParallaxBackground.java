/*
 * Copyright (C) 2013 Knivore Studios
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.andengine.classes;

import java.util.ArrayList;

import org.andengine.engine.camera.Camera;
import org.andengine.entity.Entity;
import org.andengine.entity.scene.background.IBackground;
import org.andengine.entity.shape.IAreaShape;
import org.andengine.opengl.util.GLState;
import org.andengine.util.modifier.IModifier;

/**
 * @author Keh Chin Leong
 */
public class SequentialParallaxBackground extends Entity implements IBackground {
	// ===========================================================
	// Constants
	// ===========================================================

	// ===========================================================
	// Fields
	// ===========================================================

	private final ArrayList<IParallaxEntity> mParallaxEntities = new ArrayList<IParallaxEntity>();
	private int mParallaxEntityCount;

	protected float mParallaxValue;
	protected float mParallaxScrollValue;

	protected float mParallaxChangePerSecond;

	protected float mParallaxScrollFactor = 0.2f;

	private Camera mCamera;

	private float mCameraPreviousX;
	private float mCameraOffsetX;

	private boolean mIsScrollable = false;
	private static int index;

	// ===========================================================
	// Constructors
	// ===========================================================
	public SequentialParallaxBackground() {
	}

	public SequentialParallaxBackground(final Camera camera, final boolean mIsScrollable){
		mCamera = camera;
		this.mIsScrollable = mIsScrollable;

		mCameraPreviousX = camera.getCenterX();
	}


	// ===========================================================
	// Getter & Setter
	// ===========================================================

	public void setParallaxValue(final float pParallaxValue) {
		mParallaxValue = pParallaxValue;
	}

	public void setParallaxChangePerSecond(final float pParallaxChangePerSecond) {
		mParallaxChangePerSecond = pParallaxChangePerSecond;
	}

	public void setParallaxScrollFactor(final float pParallaxScrollFactor){
		mParallaxScrollFactor = pParallaxScrollFactor;
	}

	// ===========================================================
	// Methods for/from SuperClass/Interfaces
	// ===========================================================
	@Override
	public void onManagedDraw(GLState pGLState, Camera pCamera) {
		super.preDraw(pGLState, pCamera);

		final float parallaxValue = mParallaxValue;
		final ArrayList<IParallaxEntity> parallaxEntities = mParallaxEntities;

		for(int i = 0; i < mParallaxEntityCount; i++) {
			parallaxEntities.get(i).onDraw(pGLState, pCamera, parallaxValue);
		}
	}

	@Override
	protected void onManagedUpdate(float pSecondsElapsed) {
		if(mIsScrollable && mCameraPreviousX != mCamera.getCenterX()){
			mCameraOffsetX = mCameraPreviousX - mCamera.getCenterX();
			mCameraPreviousX = mCamera.getCenterX();

			mParallaxScrollValue += mCameraOffsetX * mParallaxScrollFactor;
			mCameraOffsetX = 0;
		}

		mParallaxValue += mParallaxChangePerSecond * pSecondsElapsed;
		super.onManagedUpdate(pSecondsElapsed);
	}

	// ===========================================================
	// Methods
	// ===========================================================	
	public void attachParallaxEntities(final ParallaxEntities parallaxEntity) {
		mParallaxEntities.add(parallaxEntity);
		mParallaxEntityCount++;
	}

	public boolean detachParallaxEntity(final ParallaxEntities pParallaxEntity) {
		mParallaxEntityCount--;
		final boolean success = mParallaxEntities.remove(pParallaxEntity);
		if(!success) {
			mParallaxEntityCount++;
		}
		return success;
	}

	/*
	 * Interface for ParallaxEntity objects. Only requires onDraw method.
	 */
	 public static interface IParallaxEntity {
		 public void onDraw(final GLState pGLState, final Camera pCamera, final float pParallaxValue);
	 }

	/*
	 * ParallaxEntity code copied from AndEngine, written by Nicolas Gramlich.
	 * Modified to implement IParallaxEntity (functionality intact)
	 */
	public static class ParallaxEntities implements IParallaxEntity {
		final float mParallaxFactor;
		final ArrayList<IAreaShape> mAreaShapes;

		public ParallaxEntities(final float pParallaxFactor, final ArrayList<IAreaShape> pAreaShapes) {
			mParallaxFactor = pParallaxFactor;
			mAreaShapes = pAreaShapes;
		}

		public void onDraw(final GLState pGLState, final Camera pCamera, final float pParallaxValue) {
			if (mAreaShapes.size() > 0) {
				pGLState.pushModelViewGLMatrix();
				{
					index = 0;
					final float cameraWidth = pCamera.getWidth();

					float shapeWidthScaled = 0;

					for (IAreaShape shape : mAreaShapes)
						shapeWidthScaled += shape.getWidthScaled();

					float baseOffset = (pParallaxValue * mParallaxFactor) % shapeWidthScaled;

					shapeWidthScaled = mAreaShapes.get(index).getWidthScaled();

					while (baseOffset > 0)
						baseOffset -= shapeWidthScaled;

					pGLState.translateModelViewGLMatrixf(baseOffset, 0, 0);

					float currentMaxX = baseOffset;

					do {
						mAreaShapes.get(index).onDraw(pGLState, pCamera);
						pGLState.translateModelViewGLMatrixf(shapeWidthScaled, 0, 0);
						currentMaxX += shapeWidthScaled;

						index = (index + 1) % mAreaShapes.size();
						shapeWidthScaled = mAreaShapes.get(index).getWidthScaled();
					} while (currentMaxX < cameraWidth);
				}
				pGLState.popModelViewGLMatrix();
			}
		}
	}

	// ===========================================================
	// Inner and Anonymous Classes
	// ===========================================================
	public int getCurrentMap(){
		return index;
	}

	
	@Override
	public void registerBackgroundModifier(IModifier<IBackground> pBackgroundModifier) {}

	@Override
	public boolean unregisterBackgroundModifier(IModifier<IBackground> pBackgroundModifier) { return false; }

	@Override
	public void clearBackgroundModifiers() {}

	@Override
	public boolean isColorEnabled() { return false; }

	@Override
	public void setColorEnabled(boolean pColorEnabled) {}

}