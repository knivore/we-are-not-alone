/*
 * Copyright (C) 2013 Knivore Studios
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.andengine.classes;

import com.andengine.invaders.GameResources;
import com.makersf.andengine.extension.collisions.opengl.texture.region.PixelPerfectTiledTextureRegion;

/**
 * @author Keh Chin Leong
 */
public class Spacecraft {

	// ===========================================================
	// Constants
	// ===========================================================
	private float spacecraftMovingSpeed;
	private long[] spacecraftAnimationSpeed, spacecraftExplosionSpeed;
	private int spacecraftAnimationFrames, spacecraftExplosionFrames, spacecraftLives;
	private boolean spacecraftFlinchMode;
	private GameResources gameResources;
	
	public Spacecraft(GameResources gr) {
		gameResources = gr;
	}

	//-----------------------------OTHER METHODS-----------------------------//
	//-----------------------------------------------------------------------//
	public PixelPerfectTiledTextureRegion loadSpacecraftTexture(int spacecraftNumber) {
		PixelPerfectTiledTextureRegion spacecraftTextureRegion = gameResources.getUFOModelList().get(spacecraftNumber);
		
		if (spacecraftNumber == 0) {
			setSpacecraftMovingSpeed(1f);
			setSpacecraftAnimationFrames(29); //0-29
			setSpacecraftAnimationSpeed(15);
			setSpacecraftFlinchMode(true);
			setSpacecraftLives(1);
		} else if (spacecraftNumber == 1) {
			setSpacecraftMovingSpeed(1.45f);
			setSpacecraftAnimationFrames(39); //0-39
			setSpacecraftAnimationSpeed(20);
			setSpacecraftFlinchMode(true);
			setSpacecraftLives(1);
		} else if (spacecraftNumber == 2) {
			setSpacecraftMovingSpeed(1.9f);
			setSpacecraftAnimationFrames(39); //0-39
			setSpacecraftAnimationSpeed(20);
			setSpacecraftFlinchMode(true);
			setSpacecraftLives(1);
		} else if (spacecraftNumber == 3) {
			setSpacecraftMovingSpeed(0.7f);
			setSpacecraftAnimationFrames(39); //0-39
			setSpacecraftAnimationSpeed(20);
			setSpacecraftFlinchMode(false);
			setSpacecraftLives(1);
		} else if (spacecraftNumber == 4) {
			setSpacecraftMovingSpeed(1.25f);
			setSpacecraftAnimationFrames(29); //0-29
			setSpacecraftAnimationSpeed(15);
			setSpacecraftFlinchMode(true);
			setSpacecraftLives(2);
		} /*
		else if (spacecraftNumber == 5) {
			setSpacecraftMovingSpeed(0f);
			setSpacecraftAnimationFrames(29); //0-29
			setSpacecraftAnimationSpeed(30);
		} 
		else if (spacecraftNumber == 6) {
			setSpacecraftMovingSpeed(0f);
			setSpacecraftAnimationFrames(29); //0-29
			setSpacecraftAnimationSpeed(30);
		}
		*/

		return spacecraftTextureRegion;
	}
	
	//-----------------------------OTHER METHODS-----------------------------//
	//-----------------------------------------------------------------------//
	public PixelPerfectTiledTextureRegion loadSpacecraftExplosionTexture(int spacecraftNumber) {
		PixelPerfectTiledTextureRegion explosionTextureRegion = gameResources.getUFOModelExplosionList().get(spacecraftNumber);
		
		if (spacecraftNumber == 0) {
			setSpacecraftMovingSpeed(1.1f);
			setSpacecraftExplosionFrames(37);
			setSpacecraftExplosionSpeed(37);
		} else if (spacecraftNumber == 1) {
			setSpacecraftMovingSpeed(2.0f);
			setSpacecraftExplosionFrames(37);
			setSpacecraftExplosionSpeed(37);
		} else if (spacecraftNumber == 2) {
			setSpacecraftMovingSpeed(0.7f);
			setSpacecraftExplosionFrames(37);
			setSpacecraftExplosionSpeed(37);
		} else if (spacecraftNumber == 3) {
			setSpacecraftMovingSpeed(1.35f);
			setSpacecraftExplosionFrames(35);
			setSpacecraftExplosionSpeed(35);
		} else if (spacecraftNumber == 4) {
			setSpacecraftMovingSpeed(1.7f);
			setSpacecraftExplosionFrames(35);
			setSpacecraftExplosionSpeed(35);
		} /*
		else if (spacecraftNumber == 5) {
			setSpacecraftMovingSpeed(0f);
			setSpacecraftExplosionFrames(30); //30-65
			setSpacecraftExplosionSpeed(35);
		} 
		else if (spacecraftNumber == 6) {
			setSpacecraftMovingSpeed(0f);
			setSpacecraftExplosionFrames(30); //30-65
			setSpacecraftExplosionSpeed(35);
		}
		*/

		return explosionTextureRegion;
	}


	public float getSpacecraftMovingSpeed() {
		return spacecraftMovingSpeed;
	}
	
	public void setSpacecraftMovingSpeed(float speed) {
		spacecraftMovingSpeed = speed;
	}

	public long[] getSpacecraftAnimationSpeed() {
		return spacecraftAnimationSpeed;
	}
	
	public void setSpacecraftAnimationSpeed(int speed) {
		long animationSpeed[] = new long[getSpacecraftAnimationFrames()+1];
		for(int i = 0; i < animationSpeed.length; i++) {
			animationSpeed[i] = speed;
	    }
		spacecraftAnimationSpeed = animationSpeed;
	}
	
	public long[] getSpacecraftExplosionSpeed() {
		return spacecraftExplosionSpeed;
	}
	
	public void setSpacecraftExplosionSpeed(int speed) {
		long explosionSpeed[] = new long[getSpacecraftExplosionFrames()+1];
		for(int i = 0; i < explosionSpeed.length; i++) {
			explosionSpeed[i] = speed;
	    }
		spacecraftExplosionSpeed = explosionSpeed;
	}

	public int getSpacecraftAnimationFrames() {
		return spacecraftAnimationFrames;
	}
	
	public void setSpacecraftAnimationFrames(int animationFrames) {
		spacecraftAnimationFrames = animationFrames;
	}

	public int getSpacecraftExplosionFrames() {
		return spacecraftExplosionFrames;
	}
	
	public int getSpacecraftLives() {
		return spacecraftLives;
	}

	public void setSpacecraftLives(int spacecraftLives) {
		this.spacecraftLives = spacecraftLives;
	}

	public boolean isSpacecraftFlinchMode() {
		return spacecraftFlinchMode;
	}

	public void setSpacecraftFlinchMode(boolean spacecraftFlinchMode) {
		this.spacecraftFlinchMode = spacecraftFlinchMode;
	}

	public void setSpacecraftExplosionFrames(int explosionFrames) {
		spacecraftExplosionFrames = explosionFrames;
	}
}
