/*
 * Copyright (C) 2013 Knivore Studios
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.andengine.classes;

import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.adt.pool.GenericPool;
import org.andengine.util.math.MathUtils;

import com.andengine.invaders.GameResources;

/**
 * @author Keh Chin Leong
 */
public class MilitaryHumanPool extends GenericPool<MilitaryHumanSprite> {

	private Map currentMap;
	private GameResources gr;
	private VertexBufferObjectManager vbom;
	private float positionY, cameraHeight, cameraWidth;
	public int humansCaptured;

	public MilitaryHumanPool(float CAMERA_HEIGHT, float CAMERA_WIDTH, VertexBufferObjectManager vertexBufferObjM, GameResources gameResources, Map map) {
		vbom = vertexBufferObjM;
		cameraHeight = CAMERA_HEIGHT;
		cameraWidth = CAMERA_WIDTH;
		gr = gameResources;
		currentMap = map;
	}

	/**
	 * Called when a Human is required but there isn't one in the pool
	 */
	@Override
	protected MilitaryHumanSprite onAllocatePoolItem() {
		//create animated sprite at the right end
		MilitaryHumanSprite mhs = new MilitaryHumanSprite(cameraWidth + gr.createMilitary().getWidth() + MathUtils.random(10, 20), positionY, gr.createMilitary().deepCopy(), vbom, currentMap);
		mhs.setScale(0.75f);
		return mhs;
	}

	/**
	 * Called when a Human is sent to the pool
	 */
	@Override
	protected void onHandleRecycleItem(final MilitaryHumanSprite pMilitaryHuman) {
		pMilitaryHuman.clearEntityModifiers();
		pMilitaryHuman.clearUpdateHandlers();
		pMilitaryHuman.setVisible(false);
	}

	/**
	 * Called just before a Military Human is returned to the caller, this is where you
	 * write your initialize code i.e. set location, rotation, etc.
	 */
	@Override
	protected void onHandleObtainItem(final MilitaryHumanSprite pMilitaryHuman) {
		pMilitaryHuman.reset();
	}
	
	public void setMilitaryHumanMapLocation(Map pMap) {
		//currentMap = pMap;
		//if(currentMap.getCurrentMap() == "") { 
		// DO SOMETHING IN THE FUTURE?~
		//}
		positionY = cameraHeight - MathUtils.random(22, 40) - gr.createMilitary().getHeight();
	}
}