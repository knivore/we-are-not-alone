/*
 * Copyright (C) 2013 Knivore Studios
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.andengine.classes;

import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.adt.pool.GenericPool;

import com.andengine.invaders.GameResources;

/**
 * @author Keh Chin Leong
 */
public class MissileAlertPool extends GenericPool<WeaponAlertSprite> {
	
	private GameResources gr;
	private VertexBufferObjectManager vbom;

	public MissileAlertPool(VertexBufferObjectManager vertexBufferObjM, GameResources gameResources) {
		vbom = vertexBufferObjM;
		gr = gameResources;
	}
	
	/**
	 * Called when a MissileAlertSprite is required but there isn't one in the pool
	 */
	@Override
	protected WeaponAlertSprite onAllocatePoolItem() {
		WeaponAlertSprite missileAlert = new WeaponAlertSprite(0, 0, gr.getmMissileAlertTextureRegion().deepCopy(), vbom);
		return missileAlert;
	}

	/**
	 * Called when a MissileAlertSprite is sent to the pool
	 */
	@Override
	protected void onHandleRecycleItem(final WeaponAlertSprite pMissileAlert) {
		pMissileAlert.setVisible(false);
	}

	/**
	 * Called just before a MissileAlertSprite is returned to the caller, this is where you
	 * write your initialize code i.e. set location, rotation, etc.
	 */
	@Override
	protected void onHandleObtainItem(final WeaponAlertSprite pMissileAlert) {
		pMissileAlert.reset();
	}
}
