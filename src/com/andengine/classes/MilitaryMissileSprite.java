/*
 * Copyright (C) 2013 Knivore Studios
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.andengine.classes;

import org.andengine.entity.IEntity;
import org.andengine.entity.modifier.IEntityModifier;
import org.andengine.entity.modifier.MoveModifier;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.modifier.IModifier;

import com.makersf.andengine.extension.collisions.entity.sprite.PixelPerfectAnimatedSprite;
import com.makersf.andengine.extension.collisions.opengl.texture.region.PixelPerfectTiledTextureRegion;

/**
 * @author Keh Chin Leong
 */
public class MilitaryMissileSprite extends PixelPerfectAnimatedSprite {

	// ===========================================================
	// Constants
	// ===========================================================
	private MoveModifier fireModifier;
	private String fireAngle;
	
	public MilitaryMissileSprite(float pX, float pY, PixelPerfectTiledTextureRegion pTiledTextureRegion, VertexBufferObjectManager vbom) {
		super(pX, pY, pTiledTextureRegion, vbom);
	}

	public void reset() {
		setPosition(0, 0);
		setVisible(true);
		clearEntityModifiers();
	}

	//-----------------------------OTHER METHODS-----------------------------//
	//-----------------------------------------------------------------------//

	public boolean offScreen(int cameraWidth, int cameraHeight) {
		if ((getX() > cameraWidth + getWidth() + 30) || (getX() < 0 - getWidth() - 30) || (getY() <= 0 - getHeight() - 30)) {
			return true;
		} else {
			return false;			
		}
	}
	
	public boolean getFacingDirection() {
		//FALSE = face >> (RIGHT) || TRUE = face << (LEFT)
		return isFlippedHorizontal();
	}

	public void setFacingDirection(boolean d) {
		//FALSE = face >> (RIGHT) || TRUE = face << (LEFT)
		setFlippedHorizontal(d);
	}

	public void fire1(float speed, float cameraWidth) {
		setFireAngle("FIRE1");
        float endX = 0;
    	float endY = 0 - (getHeight() + 20);
        float startX = 0;
    	
        if (getFacingDirection()) {
        	startX = getX();
        	endX = getX() - (cameraWidth * 1.5f);
        } else {
        	startX = getX() + (getWidth() * 2);
            endX = getX() + (cameraWidth * 1.5f);
        }
        
        fireModifier = new MoveModifier(speed, startX, endX, getY(), endY, new IEntityModifier.IEntityModifierListener() {
			@Override
			public void onModifierStarted(IModifier<IEntity> pModifier, IEntity pItem) { 
				//setTransform(getX(), getY(),0);
			}
	
			@Override
			public void onModifierFinished(IModifier<IEntity> pModifier, IEntity pItem) { }
	    });
        fireModifier.setAutoUnregisterWhenFinished(true);
        registerEntityModifier(fireModifier);
	}
	
	public void fire2(float speed, float cameraWidth, float cameraHeight) {
		setFireAngle("FIRE2");
        float endX = 0;
    	float endY = 0 - (cameraHeight * 1.5f);
        float startX = 0;
    	
        if (getFacingDirection()) {
        	startX = getX();
        	endX = getX() - (cameraWidth * 1.5f);
        } else {
        	startX = getX() + (getWidth() * 2f);
            endX = getX() + (cameraWidth * 1.5f);
        }
        
        fireModifier = new MoveModifier(speed, startX, endX, getY(), endY, new IEntityModifier.IEntityModifierListener() {
			@Override
			public void onModifierStarted(IModifier<IEntity> pModifier, IEntity pItem) { 
				//setTransform(getX(), getY(),0);
			}
	
			@Override
			public void onModifierFinished(IModifier<IEntity> pModifier, IEntity pItem) { }
	    });
        fireModifier.setAutoUnregisterWhenFinished(true);
        registerEntityModifier(fireModifier);
	}

	public String getFireAngle() {
		return fireAngle;
	}

	public void setFireAngle(String fireAngle) {
		this.fireAngle = fireAngle;
	}
}
