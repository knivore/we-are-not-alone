/*
 * Copyright (C) 2013 Knivore Studios
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.andengine.classes;

import org.andengine.engine.handler.physics.PhysicsHandler;
import org.andengine.entity.modifier.MoveXModifier;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

import com.makersf.andengine.extension.collisions.entity.sprite.PixelPerfectAnimatedSprite;
import com.makersf.andengine.extension.collisions.opengl.texture.region.PixelPerfectTiledTextureRegion;

/**
 * @author Keh Chin Leong
 */
public class LaserBeamSprite extends PixelPerfectAnimatedSprite {

	// ===========================================================
	// Constants
	// ===========================================================
	private MoveXModifier moveX;
	private PhysicsHandler mPhysicsHandler;
	private float laserSpeed, speed;
	
	public LaserBeamSprite(float pX, float pY, PixelPerfectTiledTextureRegion pTiledTextureRegion, VertexBufferObjectManager vbom) {
		super(pX, pY, pTiledTextureRegion, vbom);
		mPhysicsHandler = new PhysicsHandler(this);
		registerUpdateHandler(mPhysicsHandler);
	}

	public void reset() {
		setPosition(0, 0);
		setVisible(true);
	}

	//-----------------------------OTHER METHODS-----------------------------//
	//-----------------------------------------------------------------------//

	@Override
	protected void onManagedUpdate(float pSecondsElapsed) {
		if(isVisible()) {
			//laserSpeed += -1 * pSecondsElapsed;
			//moveRight(100 + laserSpeed);
			super.onManagedUpdate(pSecondsElapsed);
		}
	}
	/*
	public void moveLeft(float laserSpeedOverSecondsElapsed) {
		mPhysicsHandler.setVelocityX(-1 * (laserSpeedOverSecondsElapsed));
	}
	
	public void moveRight(float laserSpeedOverSecondsElapsed) {
		mPhysicsHandler.setVelocityX(laserSpeedOverSecondsElapsed);
	}
	*/
	
	public void moveRightToLeft(float speed, float distance) {
		moveX = new MoveXModifier(speed, getX(), getX() - distance);
		registerEntityModifier(moveX);
	}
	
	public void moveLeftToRight(float speed, float distance) {
		moveX = new MoveXModifier(speed, getX(), getX() + distance);
		registerEntityModifier(moveX);
	}

	public float getSpeed() {
		return speed;
	}
	
	public void setSpeed(float mSpeed) {
		speed = mSpeed;
	}
}
