/*
 * Copyright (C) 2013 Knivore Studios
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.andengine.classes;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.http.impl.cookie.DateParseException;
import org.apache.http.impl.cookie.DateUtils;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.SharedPreferences;
import android.text.format.DateFormat;

import com.andengine.invaders.GameResources;
import com.andengine.sharedpreferences.Gadgets_SharedPreference;
import com.andengine.sharedpreferences.Options_SharedPreference;
import com.andengine.sharedpreferences.Score_SharedPreference;
import com.andengine.sharedpreferences.Spacecraft_Meter_SharedPreference;
import com.andengine.sharedpreferences.Spacecraft_SharedPreference;

/**
 * Represents the player's progress in the game. 
 * The player's progress is the distance per country made, humans captured, 
 * gadgets count, evil & goodness meter, spacecraft status
 *
 * @author Keh Chin Leong
 */
public class SaveGame {
	private GameResources mGameResources;
	
    // serialization format version
    private static final String SERIAL_VERSION = "1.0";
    
    Gadgets_SharedPreference gadgets_SP;
	Options_SharedPreference options_SP;

	Spacecraft_Meter_SharedPreference spacecraft_meter_SP;
	Spacecraft_SharedPreference spacecraft_SP;
	
    boolean mPremiumNoAds;
    String payload;
    Date savedDate;

    /** Constructs an empty SaveGame object. No human's captured count & distance travelled per country. */
    public SaveGame(GameResources gameResources) {
		mGameResources = gameResources;
		
        gadgets_SP = new Gadgets_SharedPreference(mGameResources.getmBaseContext());
    	options_SP = new Options_SharedPreference(mGameResources.getmBaseContext());
    	spacecraft_meter_SP = new Spacecraft_Meter_SharedPreference(mGameResources.getmBaseContext());
    	spacecraft_SP = new Spacecraft_SharedPreference(mGameResources.getmBaseContext());    	
    }

    /** Constructs a SaveGame object from serialized data. */
    public SaveGame(GameResources gameResources, byte[] data) {
		mGameResources = gameResources;

        gadgets_SP = new Gadgets_SharedPreference(mGameResources.getmBaseContext());
    	options_SP = new Options_SharedPreference(mGameResources.getmBaseContext());
    	spacecraft_meter_SP = new Spacecraft_Meter_SharedPreference(mGameResources.getmBaseContext());
    	spacecraft_SP = new Spacecraft_SharedPreference(mGameResources.getmBaseContext());  
    	
        if (data == null) return; // default progress
        loadFromJson(new String(data));
    }

    /** Constructs a SaveGame object from a JSON string. */
    public SaveGame(GameResources gameResources, String json) {
		mGameResources = gameResources;
		
        gadgets_SP = new Gadgets_SharedPreference(mGameResources.getmBaseContext());
    	options_SP = new Options_SharedPreference(mGameResources.getmBaseContext());
    	spacecraft_meter_SP = new Spacecraft_Meter_SharedPreference(mGameResources.getmBaseContext());
    	spacecraft_SP = new Spacecraft_SharedPreference(mGameResources.getmBaseContext());  
    	
        if (json == null) return; // default progress
        loadFromJson(json);
    }

    /** Constructs a SaveGame object by reading from a SharedPreferences. */
    public SaveGame(GameResources gameResources, SharedPreferences sp, String key) {
		mGameResources = gameResources;
		
        gadgets_SP = new Gadgets_SharedPreference(mGameResources.getmBaseContext());
    	options_SP = new Options_SharedPreference(mGameResources.getmBaseContext());
    	spacecraft_meter_SP = new Spacecraft_Meter_SharedPreference(mGameResources.getmBaseContext());
    	spacecraft_SP = new Spacecraft_SharedPreference(mGameResources.getmBaseContext());  
    	
        loadFromJson(sp.getString(key, ""));
    }

    /** Replaces this SaveGame's content with the content loaded from the given JSON string. */
    public void loadFromJson(String json) {
        zero();
        if (json == null || json.trim().equals("")) return;

        try {
            JSONObject obj = new JSONObject(json);
            String format = obj.getString("VERSION");
            if (!format.equals(SERIAL_VERSION)) {
                throw new RuntimeException("Unexpected loot format " + format);
            }
            
            setSavedDate(obj.getString("UPDATED_DATE"));
            setPremiumNoAds(obj.getBoolean("mPremiumNoAds"));
            setPayload(obj.getString("PAYLOAD"));
            
            // Gadgets, Options, Score, Spacecraft Shared Preference
            Map<String, ?> gadgetMap = convertMapStringToMap(obj.getString("GADGETS_SP"));
            Gadgets_SharedPreference gadgets_SP = new Gadgets_SharedPreference(mGameResources.getmBaseContext());
            for (Entry<String, ?> entry : gadgetMap.entrySet()) {
                String key = entry.getKey();
                Object value = entry.getValue();

                if(key.toLowerCase().contains("Gadget".toLowerCase())) {
                	gadgets_SP.setGadgetCount(key, Integer.parseInt(value.toString()));
                } else if(key.toLowerCase().contains("EquippedBox1".toLowerCase())) {
                	gadgets_SP.setEquippedGadget1(Integer.parseInt(value.toString()));
                } else if(key.toLowerCase().contains("EquippedBox2".toLowerCase())) {
                	gadgets_SP.setEquippedGadget2(Integer.parseInt(value.toString()));
                } else if(key.toLowerCase().contains("totalGadgetBrought".toLowerCase())) {
                	gadgets_SP.setTotalGadgetBrought(Integer.parseInt(value.toString()));
                } else if(key.toLowerCase().contains("totalGadgetUsed".toLowerCase())) {
                	gadgets_SP.setTotalGadgetUsed(Integer.parseInt(value.toString()));
                } 
            }
            
            Map<String, ?> optionMap = convertMapStringToMap(obj.getString("OPTIONS_SP"));
            Options_SharedPreference options_SP = new Options_SharedPreference(mGameResources.getmBaseContext());
            for (Entry<String, ?> entry : optionMap.entrySet()) {
                String key = entry.getKey();
                Object value = entry.getValue();

                if(key.toLowerCase().contains("SFX".toLowerCase())) {
                	options_SP.saveSFX(Boolean.parseBoolean(value.toString()));
                } else if(key.toLowerCase().contains("Music".toLowerCase())) {
                	options_SP.saveMusic(Boolean.parseBoolean(value.toString()));
                } else if(key.toLowerCase().contains("ControlPads".toLowerCase())) {
                	options_SP.saveControlPads(Boolean.parseBoolean(value.toString()));
                }
            }
            
            Map<String, ?> spacecraftMeterMap = convertMapStringToMap(obj.getString("SPACECRAFT_METER_SP"));
            Spacecraft_Meter_SharedPreference spacecraft_meter_SP = new Spacecraft_Meter_SharedPreference(mGameResources.getmBaseContext());
            for (Entry<String, ?> entry : spacecraftMeterMap.entrySet()) {
                String key = entry.getKey();
                Object value = entry.getValue();

                if(key.toLowerCase().contains("EVIL_METER".toLowerCase())) {
                	spacecraft_meter_SP.setEvilMeter(Float.parseFloat(value.toString()));
                } else if(key.toLowerCase().contains("GOODNESS_METER".toLowerCase())) {
                	spacecraft_meter_SP.setGoodnessMeter(Float.parseFloat(value.toString()));
                }
            }

            Map<String, ?> spacecraftMap = convertMapStringToMap(obj.getString("SPACECRAFT_SP"));
            Spacecraft_SharedPreference spacecraft_SP = new Spacecraft_SharedPreference(mGameResources.getmBaseContext());
            for (Entry<String, ?> entry : spacecraftMap.entrySet()) {
                String key = entry.getKey();
                Object value = entry.getValue();
                                
                if(key.toLowerCase().contains("Active_Spacecraft".toLowerCase())) {
                	spacecraft_SP.saveActiveSpacecraft(Integer.parseInt(value.toString()));
                }

        		for (int i = 0; i < mGameResources.getUFOModelList().size(); i++) {
        			String spacecraftName;
        			if(i < 10) {
        				spacecraftName = "Spacecraft_0";
        			} else {
        				spacecraftName = "Spacecraft_";
        			}
                    if(key.toLowerCase().contains(spacecraftName + i + "_Status".toLowerCase())) {
                    	spacecraft_SP.saveSpacecraftStatus(Integer.parseInt(value.toString()));
                    }
        		}
            }
            
        	Map<String, ?> scoreMap = convertMapStringToMap(obj.getString("SCORE_SP"));
            for(String countryName : mGameResources.getCountryName()) {
                Score_SharedPreference score_SP = new Score_SharedPreference(mGameResources.getmBaseContext(), countryName);
                for (Entry<String, ?> entry : scoreMap.entrySet()) {
                    String key = entry.getKey();
                    Object value = entry.getValue();

                    if(key.toLowerCase().contains("DisintegratedHumans_"+countryName.toLowerCase())) {
                    	score_SP.setDisintegratedHumansPerCountry(Integer.parseInt(value.toString()));
                    } else if(key.toLowerCase().contains("Max_DisintegratedHumans_"+countryName.toLowerCase())) {
                    	score_SP.saveMaxDisintegratedHumansPerCountry(Integer.parseInt(value.toString()));
                    } else if(key.toLowerCase().contains("AbsorbedHumans_".toLowerCase())) {
                    	score_SP.setAbsorbedHumansPerCountry(Integer.parseInt(value.toString()));
                    } else if(key.toLowerCase().contains("Max_AbsorbedHumans_"+countryName.toLowerCase())) {
                    	score_SP.saveMaxAbsorbedHumansPerCountry(Integer.parseInt(value.toString()));
                    } else if(key.toLowerCase().contains("TotalGoldObtained".toLowerCase())) {
                    	score_SP.setTotalObtainedGold(Integer.parseInt(value.toString()));
                    } else if(key.toLowerCase().contains("DetuctableGoldObtained".toLowerCase())) {
                    	score_SP.setObtainedDetuctableGold(Integer.parseInt(value.toString()));
                    } else if(key.toLowerCase().contains("Max_GoldObtained_"+countryName.toLowerCase())) {
                    	score_SP.saveMaxGoldPerCountry(Integer.parseInt(value.toString()));
                    } else if(key.toLowerCase().contains("Distance".toLowerCase())) {
                    	score_SP.setCombinedDistance(Integer.parseInt(value.toString()));
                    } else if(key.toLowerCase().contains("Distance_"+countryName.toLowerCase())) {
                    	score_SP.saveMaxDistancePerCountry(Integer.parseInt(value.toString()));
                    } else if(key.toLowerCase().contains("GamesCompleted".toLowerCase())) {
                    	score_SP.setGamesCompleted(Integer.parseInt(value.toString()));
                    } else if(key.toLowerCase().contains("DeathByLaserStrike".toLowerCase())) {
                    	score_SP.setDeathByLaserStrike(Integer.parseInt(value.toString()));
                    } else if(key.toLowerCase().contains("DeathByMissile".toLowerCase())) {
                    	score_SP.setDeathByMissile(Integer.parseInt(value.toString()));
                    } else if(key.toLowerCase().contains("DeathByScatterMissile".toLowerCase())) {
                    	score_SP.setDeathByScatterMissile(Integer.parseInt(value.toString()));
                    } else if(key.toLowerCase().contains("HitByHumanRocketLauncher".toLowerCase())) {
                    	score_SP.setHitByHumanRocketLauncher(Integer.parseInt(value.toString()));
                    }
                }
            }
        }
        catch (JSONException ex) {
            ex.printStackTrace();
            throw new RuntimeException("Save data has a syntax error: " + json, ex);
        }
        catch (NumberFormatException ex) {
            ex.printStackTrace();
            throw new RuntimeException("Save data has an invalid number in it: " + json, ex);
        }
    }

    /** Serializes this SaveGame to an array of bytes. */
    public byte[] toBytes() {
        return toString().getBytes();
    }

    /** Serializes this SaveGame to a JSON string. */
    @Override
    public String toString() {
        try {
            JSONObject obj = new JSONObject();
            
            savedDate = new Date();
            obj.put("UPDATED_DATE", DateFormat.format("dd-MM-yyyy HH:mm:ss", savedDate));            
            obj.put("VERSION", SERIAL_VERSION);
            obj.put("PAYLOAD", payload == null ? "" : payload);
        	obj.put("mPremiumNoAds", mPremiumNoAds);
        	
        	obj.put("GADGETS_SP", gadgets_SP.getmGadgetDB().getAll());
        	obj.put("OPTIONS_SP", options_SP.getmOptionsDB().getAll());
        	obj.put("SPACECRAFT_METER_SP", spacecraft_meter_SP.getmSpacecraftMeterDB().getAll());
        	obj.put("SPACECRAFT_SP", spacecraft_SP.getmSpacecraftDB().getAll());
        	
            obj.put("SCORE_SP", new Score_SharedPreference(mGameResources.getmBaseContext(), "").getmScoreDB().getAll());
            
            return obj.toString();
        }
        catch (JSONException ex) {
            ex.printStackTrace();
            throw new RuntimeException("Error converting save data to JSON.", ex);
        }
    }

    /**
     * Computes the union of this SaveGame with the given SaveGame. The union will have any
     * levels present in either operand.
     *
     * @param other The other operand with which to compute the union.
     * @return The result of the union.
     */
    public SaveGame unionWith(SaveGame other) {
        SaveGame result = clone();
        if(result.getSavedDate().before(other.getSavedDate())) {
        	result = other;
        }
        return result;
    }

    /** Returns a clone of this SaveGame object. */
    public SaveGame clone() {
        SaveGame result = new SaveGame(mGameResources);
        return result;
    }

    /** Resets this SaveGame object to be empty. Empty means no stars on no levels. */
    public void zero() {
    }

    /** Returns whether or not this SaveGame is empty. Empty means no stars on no levels. */
    public boolean isZero() {
        return false;
    }

    /** Save this SaveGame object to a SharedPreferences. */
    public void save(SharedPreferences sp, String key) {
        SharedPreferences.Editor spe = sp.edit();
        spe.putString(key, toString());
        spe.commit();
    }

    public boolean getPremiumNoAds() {
        return mPremiumNoAds;
    }

    public void setPremiumNoAds(boolean premiumNoAds) {
    	mPremiumNoAds = premiumNoAds;
    }

	public Date getSavedDate() {
		return savedDate;
	}

	public void setSavedDate(String savedDate) {
		try {
			this.savedDate = DateUtils.parseDate(savedDate);
		} catch (DateParseException e) {
			this.savedDate = new Date();
		}
	}

	public String getPayload() {
		return payload;
	}

	public void setPayload(String payload) {
		this.payload = payload;
	}
	
	public Map<String, String> convertMapStringToMap(String str) {
	    String[] tokens = str.substring(1, str.length()-1).split(", |=");
	    Map<String, String> map = new HashMap<String, String>();
	    for (int i=0; i<tokens.length-1; )
	    	map.put(tokens[i++], tokens[i++]);
	    return map;
	}
}
