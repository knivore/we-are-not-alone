/*
 * Copyright (C) 2013 Knivore Studios
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.andengine.classes;

import org.andengine.engine.handler.physics.PhysicsHandler;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.math.MathUtils;

import com.makersf.andengine.extension.collisions.entity.sprite.PixelPerfectAnimatedSprite;
import com.makersf.andengine.extension.collisions.opengl.texture.region.PixelPerfectTiledTextureRegion;

/**
 * @author Keh Chin Leong
 */
public class CarSprite extends PixelPerfectAnimatedSprite {

	// ===========================================================
	// Constants
	// ===========================================================
	private static final int CAMERA_WIDTH = 1280;
	private static final int CAMERA_HEIGHT = 720;
	private PhysicsHandler mPhysicsHandler;
	private String carStatus;
	private float carSpeed = -174.3f;
	private boolean destroyedByLaserBeam;
	private Map mMap;
	
	public CarSprite(float pX, float pY, PixelPerfectTiledTextureRegion tiledTextureRegion, VertexBufferObjectManager vbom, Map map) {
		super(pX, pY, tiledTextureRegion, vbom);
		mPhysicsHandler = new PhysicsHandler(this);
		registerUpdateHandler(mPhysicsHandler);
		mMap = map;
		carStatus = "MOVE";
	}

	public void reset() {
		setPosition(CAMERA_WIDTH, CAMERA_HEIGHT - getHeight() - MathUtils.random(45f, 66f));
		setVisible(true);
		setAlpha(1);
		if (mMap.getCurrentMap().equalsIgnoreCase("singapore")) {
			setCurrentTileIndex(MathUtils.random(0, 3));
		} else {
			setCurrentTileIndex(MathUtils.random(0, 2));
		}
		carStatus = "MOVE";
	}

	@Override
	protected void onManagedUpdate(float pSecondsElapsed) {
		if(this.isVisible()) {
			if(carStatus.equals("MOVE")) {
				move(carSpeed + (-1 * (mMap.getParallaxLayer().calculateSpeedInPixel % mMap.getLayer1().getShapeWidthScaled())));
			} else if(carStatus.equals("STOP")) {
				stopMoving();
			}
			super.onManagedUpdate(pSecondsElapsed);
		}
	}
	
	//-----------------------------OTHER METHODS-----------------------------//
	//-----------------------------------------------------------------------//

	public float getcarSpeed() {
		return carSpeed;
	}

	public void setcarSpeed(float obstaclesSpeed) {
		this.carSpeed = obstaclesSpeed;
	}
	
	public boolean detectOffScreen() {
		if (getX() < 0 - getWidth() + 30) {
			return true;
		} else
			return false;
	}
	
	public void move(float speed) {
		mPhysicsHandler.setVelocityX(speed);
	}

	public void stopMoving() {
		mPhysicsHandler.setVelocityX(0);
	}
	
	public String getCarStatus() {
		return carStatus;
	}
	
	public void setCarStatus(String status) {
		carStatus = status;
	}
	
	public boolean getDestroyedByLaserBeam() {
		return destroyedByLaserBeam;
	}
	public void setDestroyedByLaserBeam(boolean status) {
		destroyedByLaserBeam = status;
	}
}
