/*
 * Copyright (C) 2013 Knivore Studios
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.andengine.classes;

import org.andengine.entity.modifier.AlphaModifier;
import org.andengine.entity.sprite.AnimatedSprite;
import org.andengine.opengl.texture.region.TiledTextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

/**
 * @author Keh Chin Leong
 */
public class GoldSprite extends AnimatedSprite {

	// ===========================================================
	// Constants
	// ===========================================================	
	public GoldSprite(float pX, float pY, TiledTextureRegion pTiledTextureRegion, VertexBufferObjectManager vbom) {
		super(pX, pY, pTiledTextureRegion, vbom);
	}

	public void reset() {
		setPosition(0, 0);
		setVisible(true);
		setAlpha(1.0f);
		setScale(0.5f);
	}

	//-----------------------------OTHER METHODS-----------------------------//
	//-----------------------------------------------------------------------//

	public void blink() {
		registerEntityModifier(new AlphaModifier(33, 0, 255));
	}

}
