/*
 * Copyright (C) 2013 Knivore Studios
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.andengine.classes;

import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.adt.pool.GenericPool;
import org.andengine.util.math.MathUtils;

import com.andengine.invaders.GameResources;

/**
 * @author Keh Chin Leong
 */
public class ObstaclesPool extends GenericPool<ObstaclesSprite> {
	
	private Map currentMap;
	private GameResources gr;
	private VertexBufferObjectManager vbom;
	private float cameraHeight;
	private float cameraWidth;
	public int humansCaptured;

	public ObstaclesPool(float CAMERA_HEIGHT, float CAMERA_WIDTH, VertexBufferObjectManager vertexBufferObjM, GameResources gameResources, Map map) {
		vbom = vertexBufferObjM;
		cameraHeight = CAMERA_HEIGHT;
		cameraWidth = CAMERA_WIDTH;
		gr = gameResources;
		currentMap = map;
	}
	
	/**
	 * Called when a Obstacle is required but there isn't one in the pool
	 */
	@Override
	protected ObstaclesSprite onAllocatePoolItem() {
		ObstaclesSprite sprite = null;
		if (currentMap.getCurrentMap().equalsIgnoreCase("singapore")) {
			switch(MathUtils.random(0, 1)) {
			case 0:
				//create lamp post at the right end of the screen
				final ObstaclesSprite LampPost = new ObstaclesSprite(cameraWidth, cameraHeight - gr.getmLampPostTextureRegion().getHeight() - 67, gr.getmLampPostTextureRegion(), vbom, currentMap);
				sprite = LampPost;
			case 1:
				//create traffic light at the right end of the screen
				final ObstaclesSprite TrafficLights = new ObstaclesSprite(cameraWidth, cameraHeight - gr.getmLampPostTextureRegion().getHeight()  - 67, gr.getmTrafficLightTextureRegion(), vbom, currentMap);
				sprite = TrafficLights;
			}
		} else if (currentMap.getCurrentMap().equalsIgnoreCase("sydney")) {
			//create lamp post at the right end of the screen
			final ObstaclesSprite LampPost = new ObstaclesSprite(cameraWidth, cameraHeight - gr.getmLampPostTextureRegion().getHeight() - 67, gr.getmLampPostTextureRegion(), vbom, currentMap);
			sprite = LampPost;
		}
		return sprite;
	}

	/**
	 * Called when a Obstacle is sent to the pool
	 */
	@Override
	protected void onHandleRecycleItem(final ObstaclesSprite pObstacles) {
		pObstacles.clearEntityModifiers();
		pObstacles.setVisible(false);
	}

	/**
	 * Called just before a Obstacle is returned to the caller, this is where you
	 * write your initialize code i.e. set location, rotation, etc.
	 */
	@Override
	protected void onHandleObtainItem(final ObstaclesSprite pObstacles) {
		pObstacles.reset();
	}
	
	public void setMap(Map map) {
		currentMap = map;
	}
}
