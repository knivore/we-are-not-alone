/*
 * Copyright (C) 2013 Knivore Studios
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.andengine.classes;

import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.adt.pool.GenericPool;

import com.andengine.invaders.GameResources;

/**
 * @author Keh Chin Leong
 */
public class MissileHeadPool extends GenericPool<MissileHeadSprite> {
	
	private GameResources gameResource;
	private VertexBufferObjectManager vbom;
	//private float cameraHeight;
	private float cameraWidth;
	private float yPosition;
	private float xPosition;

	public MissileHeadPool(float CAMERA_HEIGHT, float CAMERA_WIDTH, VertexBufferObjectManager vertexBufferObjM, GameResources gr) {
		vbom = vertexBufferObjM;
		//cameraHeight = CAMERA_HEIGHT;
		cameraWidth = CAMERA_WIDTH;
		gameResource = gr;
	}

	/**
	 * Called when a Missile Head is required but there isn't one in the pool
	 */
	@Override
	protected MissileHeadSprite onAllocatePoolItem() {
		//create missiles at the right end of the screen
		MissileHeadSprite missile = new MissileHeadSprite(cameraWidth, yPosition, gameResource.getmMissileHeadTextureRegion(), vbom);
		return missile;
	}

	/**
	 * Called when a Missile Head is sent to the pool
	 */
	@Override
	protected void onHandleRecycleItem(final MissileHeadSprite pMissileHead) {
		pMissileHead.setVisible(false);
	}

	/**
	 * Called just before a Missile Head is returned to the caller, this is where you
	 * write your initialize code i.e. set location, rotation, etc.
	 */
	@Override
	protected void onHandleObtainItem(final MissileHeadSprite pMissileHead) {
		pMissileHead.reset();
		pMissileHead.setY(yPosition);
		pMissileHead.setX(xPosition);
	}

	public void setXPosition(float xPos) {
		xPosition = xPos;
	}
	
	public void setYPosition(float yPos) {
		yPosition = yPos;
	}

}
