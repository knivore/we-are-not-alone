/*
 * Copyright (C) 2013 Knivore Studios
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.andengine.classes;

import java.util.ArrayList;

import org.andengine.engine.camera.Camera;
import org.andengine.entity.Entity;
import org.andengine.entity.scene.background.IBackground;
import org.andengine.entity.shape.IAreaShape;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.text.Text;
import org.andengine.entity.text.TextOptions;
import org.andengine.opengl.font.Font;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.util.GLState;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.modifier.IModifier;

import android.app.Activity;
import android.graphics.Paint;

/**
 * @author Keh Chin Leong
 */
public class ParallaxLayer extends Entity implements IBackground {
	// ===========================================================
	// Constants
	// ===========================================================

	// ===========================================================
	// Fields
	// ===========================================================

	private final ArrayList<ParallaxEntity> mParallaxEntities = new ArrayList<ParallaxEntity>();
	private int mParallaxEntityCount;

	protected float mParallaxValue;
	protected float mParallaxScrollValue;

	protected float mParallaxChangePerSecond;

	protected float mParallaxScrollFactor = 0.2f;

	private Camera mCamera;

	private float mCameraPreviousX;
	private float mCameraOffsetX;

	private float mLevelWidth = 0;

	private boolean mIsScrollable = false;	

	public float calculateSpeedInPixel;

	// ===========================================================
	// Constructors
	// ===========================================================
	public ParallaxLayer() {
	}

	public ParallaxLayer(final Camera camera, final boolean mIsScrollable){
		mCamera = camera;
		this.mIsScrollable = mIsScrollable;

		mCameraPreviousX = camera.getCenterX();
	}

	public ParallaxLayer(final Camera camera, final boolean mIsScrollable, final int mLevelWidth){
		mCamera = camera;
		this.mIsScrollable = mIsScrollable;
		this.mLevelWidth = mLevelWidth;

		mCameraPreviousX = camera.getCenterX();
	}

	// ===========================================================
	// Getter & Setter
	// ===========================================================
	public float getParallaxValue() {
		return mParallaxValue;
	}
	
	public void setParallaxValue(final float pParallaxValue) {
		mParallaxValue = pParallaxValue;
	}

	public float getParallaxChangePerSecond() {
		return mParallaxChangePerSecond;
	}
	
	public void setParallaxChangePerSecond(final float pParallaxChangePerSecond) {
		mParallaxChangePerSecond = pParallaxChangePerSecond;
	}

	public void setParallaxScrollFactor(final float pParallaxScrollFactor){
		mParallaxScrollFactor = pParallaxScrollFactor;
	}

	// ===========================================================
	// Methods for/from SuperClass/Interfaces
	// ===========================================================
	@Override
	public void onManagedDraw(GLState pGLState, Camera pCamera) {
		super.preDraw(pGLState, pCamera);

		final float parallaxValue = mParallaxValue;
		final float parallaxScrollValue = mParallaxScrollValue;
		final ArrayList<ParallaxEntity> parallaxEntities = mParallaxEntities;

		for(int i = 0; i < mParallaxEntityCount; i++) {
			if(parallaxEntities.get(i).mIsScrollable){
				parallaxEntities.get(i).onDraw(pGLState, pCamera, parallaxScrollValue, mLevelWidth);
			} else {
				parallaxEntities.get(i).onDraw(pGLState, pCamera, parallaxValue, mLevelWidth);
			}
		}
	}

	@Override
	protected void onManagedUpdate(float pSecondsElapsed) {
		if(mIsScrollable && mCameraPreviousX != mCamera.getCenterX()){
			mCameraOffsetX = mCameraPreviousX - mCamera.getCenterX();
			mCameraPreviousX = mCamera.getCenterX();

			mParallaxScrollValue += mCameraOffsetX * mParallaxScrollFactor;
			mCameraOffsetX = 0;
		}
		calculateSpeedInPixel = mParallaxChangePerSecond * pSecondsElapsed;
		//Log.d("WeAreNotAlone", "calculateSpeedInPixel : "+calculateSpeedInPixel);
		mParallaxValue += calculateSpeedInPixel;
		//Log.d("WeAreNotAlone", "mParallaxValue : "+mParallaxValue);
		super.onManagedUpdate(pSecondsElapsed);
	}

	// ===========================================================
	// Methods
	// ===========================================================
	
	public ParallaxEntity getParallaxEntity(final int parallaxPosition) {
		return mParallaxEntities.get(parallaxPosition);
	}
	
	public int getParallaxEntityCount() {
		return mParallaxEntityCount;
	}
	
	public void attachParallaxEntity(final ParallaxEntity parallaxEntity) {
		mParallaxEntities.add(parallaxEntity);
		mParallaxEntityCount++;
	}
	
	public boolean detachParallaxEntity(final ParallaxEntity pParallaxEntity) {
		mParallaxEntityCount--;
		final boolean success = mParallaxEntities.remove(pParallaxEntity);
		if(!success) {
			mParallaxEntityCount++;
		}
		return success;
	}
	
	public float calculateSpeedInPixel() {
		return calculateSpeedInPixel;
	}

	// ===========================================================
	// Inner and Anonymous Classes
	// ===========================================================

	public static class ParallaxEntity {
		// ===========================================================
		// Constants
		// ===========================================================

		// ===========================================================
		// Fields
		// ===========================================================

		final float mParallaxFactor;
		final IAreaShape mAreaShape;
		final boolean mIsScrollable;

		private final float shapeWidthScaled;

		// ===========================================================
		// Constructors
		// ===========================================================

		public ParallaxEntity(final float pParallaxFactor, final IAreaShape pAreaShape) {
			mParallaxFactor = pParallaxFactor;
			mAreaShape = pAreaShape;
			mIsScrollable = false;
			shapeWidthScaled = mAreaShape.getWidthScaled();
		}

		public ParallaxEntity(final float pParallaxFactor, final IAreaShape pAreaShape, final boolean mIsScrollable) {
			mParallaxFactor = pParallaxFactor;
			mAreaShape = pAreaShape;
			this.mIsScrollable = mIsScrollable;
			shapeWidthScaled = mAreaShape.getWidthScaled();
		}

		public ParallaxEntity(final float pParallaxFactor, final IAreaShape pAreaShape, final boolean mIsScrollable, final int mReduceFrequency) {
			mParallaxFactor = pParallaxFactor;
			mAreaShape = pAreaShape;
			this.mIsScrollable = mIsScrollable;
			shapeWidthScaled = mAreaShape.getWidthScaled() * mReduceFrequency;
		}

		// ===========================================================
		// Getter & Setter
		// ===========================================================
		public float getShapeWidthScaled() {
			return shapeWidthScaled;
		}

		// ===========================================================
		// Methods for/from SuperClass/Interfaces
		// ===========================================================

		// ===========================================================
		// Methods
		// ===========================================================

		public void onDraw(final GLState pGLState, final Camera pCamera, final float pParallaxValue, final float mLevelWidth) {
			pGLState.pushModelViewGLMatrix();
			{
				float widthRange;

				if(mLevelWidth != 0){
					widthRange = mLevelWidth;
				} else {
					widthRange = pCamera.getWidth();
				}

				float baseOffset = (pParallaxValue * mParallaxFactor) % shapeWidthScaled;
				//Log.d("WeAreNotAlone", "pParallaxValue : "+pParallaxValue);
				//Log.d("WeAreNotAlone", "AndEngine", "mParallaxFactor : "+mParallaxFactor);
				//Log.d("WeAreNotAlone", "shapeWidthScaled : "+shapeWidthScaled);
				//Log.d("WeAreNotAlone", "baseOffset : "+baseOffset);

				while(baseOffset > 0) {
					baseOffset -= shapeWidthScaled;
				}
				pGLState.translateModelViewGLMatrixf(baseOffset, 0, 0);

				float currentMaxX = baseOffset;

				do {
					mAreaShape.onDraw(pGLState, pCamera);
					pGLState.translateModelViewGLMatrixf(shapeWidthScaled - 1, 0, 0);
					currentMaxX += shapeWidthScaled;
				} while(currentMaxX < widthRange);
			}
			pGLState.popModelViewGLMatrix();
		}
	}
	

	public static class ParallaxEntitiesItemSprite extends Sprite {
		private float mScaledWidth;
		private float mXposition;

		public ParallaxEntitiesItemSprite(float pX, float pY, float pWidth, float pHeight, ITextureRegion pTextureRegion,
				VertexBufferObjectManager pSpriteVertexBufferObject, float scaledWidth, Activity activity) {
			super(pX, pY, pWidth, pWidth, pTextureRegion, pSpriteVertexBufferObject);
			mScaledWidth = scaledWidth;
			mXposition = (mScaledWidth - pTextureRegion.getWidth()) /2.0f;
			setPosition(mXposition, pY);

		}
		
		@Override
		public float getWidthScaled()
		{
			return mScaledWidth;
		}
		
		public float getXpoisition()
		{
			return mXposition;
		}
		
	}
	
	public static class ParallaxEntitiesItemText extends Text {

		private float mScaledWidth;
		private float mXposition;

		public ParallaxEntitiesItemText(int i, int j, Font mFont,
				String string, TextOptions textOptions,
				VertexBufferObjectManager vertexBufferObjectManager, float scaledWidth, Activity activity) {
			super(i, j, mFont, string, textOptions, vertexBufferObjectManager);
		
			mScaledWidth = scaledWidth;
			
			final float densityMultiplier = activity.getBaseContext().getResources().getDisplayMetrics().density;
			final float scaledPx = 20 * densityMultiplier;
			Paint paint = new Paint();
			paint.setTextSize(scaledPx);

			float size = paint.measureText(string);
			mXposition = (mScaledWidth - size) /2.0f;
			setPosition(mXposition, j);
		}

		@Override
		public float getWidthScaled()
		{
			return mScaledWidth;
		}
		
		public float getXpoisition()
		{
			return mXposition;
		}
		
	}


	// ===========================================================
	// Inner and Anonymous Classes
	// ===========================================================
	
	@Override
	public void registerBackgroundModifier(IModifier<IBackground> pBackgroundModifier) {}

	@Override
	public boolean unregisterBackgroundModifier(IModifier<IBackground> pBackgroundModifier) { return false; }

	@Override
	public void clearBackgroundModifiers() {}

	@Override
	public boolean isColorEnabled() { return false; }

	@Override
	public void setColorEnabled(boolean pColorEnabled) {}

}