/*
 * Copyright (C) 2013 Knivore Studios
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.andengine.classes;

import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.adt.pool.GenericPool;

import com.andengine.invaders.GameResources;

/**
 * @author Keh Chin Leong
 */
public class LampPool extends GenericPool<LampSprite> {
	
	private Map currentMap;
	private GameResources gr;
	private VertexBufferObjectManager vbom;
	private float cameraHeight, cameraWidth, lampOnFirstLoadXPos = 280;
	private boolean onFirstLoad;

	public LampPool(float CAMERA_HEIGHT, float CAMERA_WIDTH, VertexBufferObjectManager vertexBufferObjM, GameResources gameResources, Map map) {
		vbom = vertexBufferObjM;
		cameraHeight = CAMERA_HEIGHT;
		cameraWidth = CAMERA_WIDTH;
		gr = gameResources;
		currentMap = map;
	}
	
	/**
	 * Called when a lamp is required but there isn't one in the pool
	 */
	@Override
	protected LampSprite onAllocatePoolItem() {
		LampSprite sprite = null;
		if(onFirstLoad) {
			sprite = new LampSprite(lampOnFirstLoadXPos, cameraHeight - gr.getmLampPostTextureRegion().getHeight() - currentMap.getCurrentMapLayer1().getHeight() + 2, gr.getmLampPostTextureRegion(), vbom, currentMap);
			lampOnFirstLoadXPos += 500;
			sprite.setLampStatus("FIRSTLOADMOVE");
		} else
			sprite = new LampSprite(cameraWidth, cameraHeight - gr.getmLampPostTextureRegion().getHeight() - currentMap.getCurrentMapLayer1().getHeight() + 2, gr.getmLampPostTextureRegion(), vbom, currentMap);

		if(currentMap.getCurrentMap().equalsIgnoreCase("singapore"))
			sprite.setCurrentTileIndex(1);
		else
			sprite.setCurrentTileIndex(0);
		return sprite;
	}

	/**
	 * Called when a lamp is sent to the pool
	 */
	@Override
	protected void onHandleRecycleItem(final LampSprite lamp) {
		lamp.clearEntityModifiers();
		lamp.setVisible(false);
		lamp.setLampStatus("STOP");
		lamp.setX(cameraWidth);
	}

	/**
	 * Called just before a lamp is returned to the caller, this is where you
	 * write your initialize code i.e. set location, rotation, etc.
	 */
	@Override
	protected void onHandleObtainItem(final LampSprite lamp) {
		lamp.reset();
		lamp.setY(cameraHeight - gr.getmLampPostTextureRegion().getHeight() - currentMap.getCurrentMapLayer1().getHeight() + 2);
	}
	
	public void setMap(Map map) {
		currentMap = map;
	}
	
	public void setOnFirstLoad(boolean value) {
		onFirstLoad = value;
	}	
}
